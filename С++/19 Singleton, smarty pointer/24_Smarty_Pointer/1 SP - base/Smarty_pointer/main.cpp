#include <iostream>
using namespace std;
class Temp{
	int TEMP;	
	public:
		Temp(){
			TEMP=25;
		}
		void TempFunction(){
			cout<<"TEMP = "<<TEMP<<"\n\n";
		} 
		void TempSet(int T){
			TEMP=T;
		}
};
// �����, ����������� ����� ���������
class SmartPointer{
	// ����������������� ���������
	Temp*ptr;
	//������� ����� 
	int count_copy;

public:
	int get_count_copy(){
		return count_copy;
	}
	SmartPointer (){
		count_copy = 0;
		ptr = NULL;
	}
	//�����������
	SmartPointer (Temp*p){
		//���������� 0 ��� �������� ����� ���
		count_copy = 1;
		ptr=p;
	}
	// ����������� �����������
	SmartPointer (const SmartPointer&obj){
		cout << "SmartPointer Copy Constructor"<<endl;
		//��������� ����� - ����������� �������
		count_copy = obj.count_copy + 1;
		ptr=obj.ptr;		
	}
	//���������� ��������� �����
	SmartPointer operator=(const SmartPointer&obj){
		//��������� ����� - ����������� �������
			ptr=obj.ptr;
			count_copy++;
		//���������� ��� ������ ��� �������� a=b=c
		return *this;
	}
	// ����������� �������
	~SmartPointer(){
		
		//���� ������ ���� � ����� ���
		if(ptr!=NULL&&count_copy==1){
			cout<<"\n~Delete Object\n";
			//���������� ������
			delete[]ptr;
		}
		//� ��������� ������(������������ �����)
		else cout<<"\n~Delete Copy\n";
		count_copy--;
	} 
	//������ ������ ���������� ���������
	Temp* operator->(){
		return ptr;
	}

};

//int SmartPointer::count_copy = 0;

void main(){
	//������� ������
	Temp*main_ptr  = new Temp;
	Temp*main_ptr1 = new Temp;

	//�������������� ���� �������� ����� ���������
	SmartPointer PTR(main_ptr);
	//��������� ������ ������ ���������
	PTR->TempSet(100);
	PTR->TempFunction();
	// ������� ����� (������ ������������ �����������)
	SmartPointer PTR2=PTR;
	SmartPointer PTR3=PTR2;
	SmartPointer PTR4=PTR3;
	SmartPointer PTR5=PTR4;

	cout << "Count:"<<PTR5.get_count_copy() << endl;
	SmartPointer PTR6;
	PTR6 = PTR5;

	cout << "Count:"<< PTR6.get_count_copy() << endl;

	SmartPointer BadPTR(main_ptr1);
	cout << " BadPTR Count:"<< BadPTR.get_count_copy() << endl;


}


