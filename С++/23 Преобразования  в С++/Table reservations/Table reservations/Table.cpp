#include "Table.h"

Table::Table()	//default constructor
{
	ID = 0;
	sitting_places = 0;
	status = free_table;
}

Table::Table(int aID, int aSitting_places)	//overloaded constructor
{
	ID = aID;
	sitting_places = aSitting_places;
	status = free_table;
}

void Table::set_ID(int aID)	//method to set or change ID
{
	ID = aID;
}

void Table::set_sitting_places(int aSitting_places) //method to set or change number of sitting places
{
	sitting_places = aSitting_places;
}

//method to set or change status of table (reserved, free_table, occupied)
void Table::set_status(table_status aStatus, int aReserve_duration, string aName, string aPhone) 
{
	//removing curren reserve if it exists:
	if (status == reserved)	
		remove_reserve();		
	status = aStatus;
	//creating new reserve if needed:
	if (status == reserved)
		reserve_data.set_reserve(aName, aPhone, aReserve_duration);
}

//method to get table ID
int Table::get_ID() const
{
	return ID;
}

//method to get number of sitting places
int Table::get_sitting_places() const
{
	return sitting_places;
}

//method to get status of table (reserved, free_table, occupied)
table_status Table::get_status() const
{
	return status;
}

//method to get reserve duration in minutes
int Table::get_reserve_duration() const
{
	return reserve_data.get_reserve_duration();
}

//method to get name of reservator
string Table::get_reservator_name() const
{
	return reserve_data.get_reservator_name();
}

//method to get phone number of reservator
string Table::get_reservator_phone() const
{
	return reserve_data.get_reservator_phone();
}

//method to remove current reserve
void Table::remove_reserve()
{
	reserve_data.remove_reserve();
}

//boolean method to check if table has been still reserved 
//returns true if table is reserved, false if not
bool Table::check_reserve()
{
	return reserve_data.check_reserve();
}

//saves data of Table instance to file; takes the reffer of ofstream instance (file has to be already opened)  
void Table::print_to_file(ofstream & F) const
{
	if (!F)
		throw "Reserve: file was not opened, can't write";

	int buf = ID;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//ID writing

	buf = sitting_places;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//sitting_places writing

	buf = sizeof(status);
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));
	table_status tmp = status;
	F.write(reinterpret_cast<char*>(&tmp), buf);	//table_status writing
	try
	{
		reserve_data.print_to_file(F); //reserve_data writing
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}
}

//reads data of Table instance from file; takes the reffer of ofstream instance (file has to be already opened)  
void Table::read_from_file(ifstream & F)
{
	if (!F)
		throw "Reserve: file was not opened, can't read";

	F.read(reinterpret_cast<char*>(&ID), sizeof(int));	//ID reading
	F.read(reinterpret_cast<char*>(&sitting_places), sizeof(int));	//sitting_places reading
	int buf;
	F.read(reinterpret_cast<char*>(&buf), sizeof(int));
	F.read(reinterpret_cast<char*>(&status), buf);	//table_status reading
	try
	{
		reserve_data.read_from_file(F); //reserve_data reading}
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}
}

