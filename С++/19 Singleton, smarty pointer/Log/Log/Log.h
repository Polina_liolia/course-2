#pragma once
#include<iostream>
#include <memory>
#include"UniVar.h"
using std::shared_ptr;


class Log
{
private:
	static shared_ptr<Log> l;
	int reg_size;
	UniVar *reg;

private:
	Log();
	Log(const Log &aLog);
	

public:
	~Log();
	static shared_ptr<Log> getInstance();
	int get_reg_size()const;
	UniVar *get_reg()const;
	template<typename T>
	void add_reg(T arg);
	void del_reg(int index);
	UniVar operator[] (int i);
};


template<typename T>
inline void Log::add_reg(T arg)
{
	UniVar New(arg);
	reg_size++;
	UniVar *tmp = new UniVar[reg_size];
	for (int i = 0; i < reg_size - 1; i++)
		tmp[i] = reg[i];
	tmp[reg_size - 1] = New;
	delete[] reg;
	reg = tmp;
}
