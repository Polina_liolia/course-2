#pragma once
#include "manager_report.h"


class admin_report
	: public manager_report
{
private:
	vector<string> status_changed;
	vector<string> manager_name;
public:
	admin_report() {};
	~admin_report() {};

	void read_data(char * data_file);
	void report(char * data_file, char * draft_file, char * report_file);
	
};
	