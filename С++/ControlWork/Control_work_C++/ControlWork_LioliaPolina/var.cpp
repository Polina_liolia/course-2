#include "var.h"
#include <stdio.h>

var::var()						//default constructor initialises all variables with 0 or nullptr
{
	i_number = 0;
	d_number = 0;
	S = nullptr;
}

var::var(int aInt)				//constructor to initialise intiger variable
{
	i_number = aInt;
	d_number = 0;
	S = nullptr;
}

var::var(double aDouble)		//constructor to initialise double variable
{
	i_number = 0;
	d_number = aDouble;
	S = nullptr;
}

var::var(String aS)				//constructor to initialise Srting variable
{
	if (aS.str && aS.sz)
		S = aS;
	i_number = 0;
	d_number = 0;
}

var::var(const var &to_copy)	//copy constructor
{
	i_number = to_copy.i_number;
	d_number = to_copy.d_number;
	S = to_copy.S;
}

var::~var() {}		//destructor is empty because no dinamic variabeles were initialised in this class

/*methods to set meaning for every class property; only one
property in one moment can be initialised with non-zero meaning*/
void var::set_i_number(int a)
{
	i_number = a;
	d_number = 0;
	S = nullptr;
}

void var::set_d_number(double a)
{
	d_number = a;
	i_number = 0;
	S = nullptr;
}

void var::set_S(String a)
{
	S = a;
	i_number = 0;
	d_number = 0;
}

//methods to get meaning of every class property
int var::get_i_number()const
{
	return i_number;
}

double var::get_d_number()const
{
	return d_number;
}

String var::get_S()const
{
	return S;
}

void var::Show()
{
	if (i_number != 0)
		cout << i_number << "\n";
	else if (d_number != 0)
		cout << d_number << "\n";
	else
		S.print();
}

var var::operator= (int a)
{
	i_number = a;
	d_number = 0;
	S = nullptr;
	return *this;
}

var var::operator= (double a)
{
	d_number = a;
	i_number = 0;
	S = nullptr;
	return *this;
}

var var::operator= (String a)
{
	S = a;
	i_number = 0;
	d_number = 0;
	return *this;
}

var var::operator= (const var &A)
{
	i_number = A.i_number;
	d_number = A.d_number;
	S = A.S;
	return *this;
}


var var::operator+ (const var &A)
{
	var temp;
	if (i_number != 0)
	{
		if (A.i_number != 0)
			temp.i_number = i_number + A.i_number;
		else if (A.d_number != 0)
			temp.i_number = i_number + (int)A.d_number;
		else
			temp.i_number = i_number + (int)A.S; //operator int was overloaded in class String
		//other properties have to be initialised with 0 or nullptr
		temp.d_number = 0;
		temp.S = nullptr;
	}
	else if (d_number != 0)
	{
		if (A.i_number != 0)
			temp.d_number = d_number + (double)A.i_number;
		else if (A.d_number != 0)
			temp.d_number = d_number + A.d_number;
		else
			temp.d_number = d_number + (double)A.S; //operator double was overloaded in class String
		//other properties have to be initialised with 0 or nullptr
		temp.i_number = 0;
		temp.S = nullptr;
	}
	else
	{
		if (A.i_number != 0)
		{
			char number[10];
			_itoa_s(A.i_number, number, 10);		//converting int to char array
			temp.S = S + number;
		}
		else if (A.d_number != 0)
		{
			char number[20];
			sprintf_s(number, "%f", A.d_number);	//converting int to char array
			temp.S = S + number;
		}
			
		else
			temp.S = S + A.S;
		//other properties have to be initialised with 0
		temp.d_number = 0;
		temp.i_number = 0;
	}
	return temp;
}

//var var::operator- (const var &A)
//{
//	var temp;
//	if (i_number != 0)
//	{
//		if (A.i_number != 0)
//			temp.i_number = i_number - A.i_number;
//		else if (A.d_number != 0)
//			temp.i_number = i_number - (int)A.d_number;
//		else
//			temp.i_number = i_number - (int)A.S; //operator int was overloaded in class String
//												 //other properties have to be initialised with 0 or nullptr
//		temp.d_number = 0;
//		temp.S = nullptr;
//	}
//	else if (d_number != 0)
//	{
//		if (A.i_number != 0)
//			temp.d_number = d_number - (double)A.i_number;
//		else if (A.d_number != 0)
//			temp.d_number = d_number - A.d_number;
//		else
//			temp.d_number = d_number - (double)A.S; //operator double was overloaded in class String
//													//other properties have to be initialised with 0 or nullptr
//		temp.i_number = 0;
//		temp.S = nullptr;
//	}
//	else
//	{
//		if (A.i_number != 0)
//		{
//			char number[10];
//			_itoa_s(A.i_number, number, 10);		//converting int to char array
//			temp.S = S - number;
//		}
//		else if (A.d_number != 0)
//		{
//			char number[20];
//			sprintf_s(number, "%f", A.d_number);	//converting int to char array
//			temp.S = S - number;
//		}
//
//		else
//			temp.S = S - A.S;
//		//other properties have to be initialised with 0
//		temp.d_number = 0;
//		temp.i_number = 0;
//	}
//	return temp;
//}

var var::operator* (const var &A)
{
	var temp;
	if (i_number != 0)
	{
		if (A.i_number != 0)
			temp.i_number = i_number * A.i_number;
		else if (A.d_number != 0)
			temp.i_number = i_number * (int)A.d_number;
		else
			temp.i_number = i_number * (int)A.S; //operator int was overloaded in class String
												 //other properties have to be initialised with 0 or nullptr
		temp.d_number = 0;
		temp.S = nullptr;
	}
	else if (d_number != 0)
	{
		if (A.i_number != 0)
			temp.d_number = d_number * (double)A.i_number;
		else if (A.d_number != 0)
			temp.d_number = d_number * A.d_number;
		else
			temp.d_number = d_number * (double)A.S; //operator double was overloaded in class String
													//other properties have to be initialised with 0 or nullptr
		temp.i_number = 0;
		temp.S = nullptr;
	}
	else
	{
		if (A.i_number != 0)
		{
			char number[10];
			_itoa_s(A.i_number, number, 10);		//converting int to char array
			temp.S = S * number;
		}
		else if (A.d_number != 0)
		{
			char number[20];
			sprintf_s(number, "%f", A.d_number);	//converting int to char array
			temp.S = S * number;
		}

		else
			temp.S = S * A.S;
		//other properties have to be initialised with 0
		temp.d_number = 0;
		temp.i_number = 0;
	}
	return temp;
}

var var::operator/ (const var &A)
{
	var temp;
	if (i_number != 0)
	{
		if (A.i_number != 0)
			temp.i_number = i_number / A.i_number;
		else if (A.d_number != 0)
			temp.i_number = i_number / (int)A.d_number;
		else
			temp.i_number = i_number / (int)A.S; //operator int was overloaded in class String
												 //other properties have to be initialised with 0 or nullptr
		temp.d_number = 0;
		temp.S = nullptr;
	}
	else if (d_number != 0)
	{
		if (A.i_number != 0)
			temp.d_number = d_number / (double)A.i_number;
		else if (A.d_number != 0)
			temp.d_number = d_number / A.d_number;
		else
			temp.d_number = d_number / (double)A.S; //operator double was overloaded in class String
													//other properties have to be initialised with 0 or nullptr
		temp.i_number = 0;
		temp.S = nullptr;
	}
	else
	{
		if (A.i_number != 0)
		{
			char number[10];
			_itoa_s(A.i_number, number, 10);		//converting int to char array
			temp.S = S / number;
		}
		else if (A.d_number != 0)
		{
			char number[20];
			sprintf_s(number, "%f", A.d_number);	//converting int to char array
			temp.S = S / number;
		}

		else
			temp.S = S / A.S;
		//other properties have to be initialised with 0
		temp.d_number = 0;
		temp.i_number = 0;
	}
	return temp;
}

var var::operator+= (const var &A)
{
	if (i_number != 0)
	{
		if (A.i_number != 0)
			i_number += A.i_number;
		else if (A.d_number != 0)
			i_number += (int)A.d_number;
		else
			i_number += (int)A.S; //operator int was overloaded in class String
												 //other properties have to be initialised with 0 or nullptr
		d_number = 0;
		S = nullptr;
	}
	else if (d_number != 0)
	{
		if (A.i_number != 0)
			d_number += (double)A.i_number;
		else if (A.d_number != 0)
			d_number += A.d_number;
		else
			d_number += (double)A.S; //operator double was overloaded in class String
													//other properties have to be initialised with 0 or nullptr
		i_number = 0;
		S = nullptr;
	}
	else
	{
		if (A.i_number != 0)
		{
			char number[10];
			_itoa_s(A.i_number, number, 10);		//converting int to char array
			S += number;
		}
		else if (A.d_number != 0)
		{
			char number[20];
			sprintf_s(number, "%f", A.d_number);	//converting int to char array
			S += number;
		}

		else
			S += A.S;
		//other properties have to be initialised with 0
		d_number = 0;
		i_number = 0;
	}
	return *this;
}

//var var::operator-= (const var &A)
//{
//	if (i_number != 0)
//	{
//		if (A.i_number != 0)
//			i_number -= A.i_number;
//		else if (A.d_number != 0)
//			i_number -= (int)A.d_number;
//		else
//			i_number -= (int)A.S; //operator int was overloaded in class String
//												 //other properties have to be initialised with 0 or nullptr
//		d_number = 0;
//		S = nullptr;
//	}
//	else if (d_number != 0)
//	{
//		if (A.i_number != 0)
//			d_number -= (double)A.i_number;
//		else if (A.d_number != 0)
//			d_number -= A.d_number;
//		else
//			d_number -= (double)A.S; //operator double was overloaded in class String
//													//other properties have to be initialised with 0 or nullptr
//		i_number = 0;
//		S = nullptr;
//	}
//	else
//	{
//		if (A.i_number != 0)
//		{
//			char number[10];
//			_itoa_s(A.i_number, number, 10);		//converting int to char array
//			S -= number;
//		}
//		else if (A.d_number != 0)
//		{
//			char number[20];
//			sprintf_s(number, "%f", A.d_number);	//converting int to char array
//			S -= number;
//		}
//
//		else
//			S -= A.S;
//		//other properties have to be initialised with 0
//		d_number = 0;
//		i_number = 0;
//	}
//	return *this;
//}

var var::operator*= (const var &A)
{
	if (i_number != 0)
	{
		if (A.i_number != 0)
			i_number *= A.i_number;
		else if (A.d_number != 0)
			i_number *= (int)A.d_number;
		else
			i_number *= (int)A.S; //operator int was overloaded in class String
												 //other properties have to be initialised with 0 or nullptr
		d_number = 0;
		S = nullptr;
	}
	else if (d_number != 0)
	{
		if (A.i_number != 0)
			d_number *= (double)A.i_number;
		else if (A.d_number != 0)
			d_number *= A.d_number;
		else
			d_number *= (double)A.S; //operator double was overloaded in class String
													//other properties have to be initialised with 0 or nullptr
		i_number = 0;
		S = nullptr;
	}
	else
	{
		if (A.i_number != 0)
		{
			char number[10];
			_itoa_s(A.i_number, number, 10);		//converting int to char array
			S *= number;
		}
		else if (A.d_number != 0)
		{
			char number[20];
			sprintf_s(number, "%f", A.d_number);	//converting int to char array
			S *= number;
		}

		else
			S *= A.S;
		//other properties have to be initialised with 0
		d_number = 0;
		i_number = 0;
	}
	return *this;
}

var var::operator/= (const var &A)
{
	if (i_number != 0)
	{
		if (A.i_number != 0)
			i_number /= A.i_number;
		else if (A.d_number != 0)
			i_number /= (int)A.d_number;
		else
			i_number /= (int)A.S; //operator int was overloaded in class String
												 //other properties have to be initialised with 0 or nullptr
		d_number = 0;
		S = nullptr;
	}
	else if (d_number != 0)
	{
		if (A.i_number != 0)
			d_number /= (double)A.i_number;
		else if (A.d_number != 0)
			d_number /= A.d_number;
		else
			d_number /= (double)A.S; //operator double was overloaded in class String
													//other properties have to be initialised with 0 or nullptr
		i_number = 0;
		S = nullptr;
	}
	else
	{
		if (A.i_number != 0)
		{
			char number[10];
			_itoa_s(A.i_number, number, 10);		//converting int to char array
			S /= number;
		}
		else if (A.d_number != 0)
		{
			char number[20];
			sprintf_s(number, "%f", A.d_number);	//converting int to char array
			S /= number;
		}

		else
			S /= A.S;
		//other properties have to be initialised with 0
		d_number = 0;
		i_number = 0;
	}
	return *this;
}

bool var::operator< (const var &A)
{
	if (i_number != 0)
	{
		if (A.i_number != 0)
			return (i_number < A.i_number);
		else if (A.d_number != 0)
			return (i_number < (int)A.d_number);
		else
			return (i_number < (int)A.S); //operator int was overloaded in class String
	}
	else if (d_number != 0)
	{
		if (A.i_number != 0)
			return (d_number < (double)A.i_number);
		else if (A.d_number != 0)
			return (d_number < A.d_number);
		else
			return (d_number < (double)A.S); //operator double was overloaded in class String
	}
	else
	{
		if (A.i_number != 0)
		{
			char number[10];
			_itoa_s(A.i_number, number, 10);		//converting int to char array
			return (S < number);
		}
		else if (A.d_number != 0)
		{
			char number[20];
			sprintf_s(number, "%f", A.d_number);	//converting int to char array
			return (S < number);
		}
		else
			return (S < A.S);
	}
}

bool var::operator> (const var &A)
{
	if (i_number != 0)
	{
		if (A.i_number != 0)
			return (i_number > A.i_number);
		else if (A.d_number != 0)
			return (i_number > (int)A.d_number);
		else
			return (i_number > (int)A.S); //operator int was overloaded in class String
	}
	else if (d_number != 0)
	{
		if (A.i_number != 0)
			return (d_number > (double)A.i_number);
		else if (A.d_number != 0)
			return (d_number > A.d_number);
		else
			return (d_number > (double)A.S); //operator double was overloaded in class String
	}
	else
	{
		if (A.i_number != 0)
		{
			char number[10];
			_itoa_s(A.i_number, number, 10);		//converting int to char array
			return (S > number);
		}
		else if (A.d_number != 0)
		{
			char number[20];
			sprintf_s(number, "%f", A.d_number);	//converting int to char array
			return (S > number);
		}
		else
			return (S > A.S);
	}
}

bool var::operator<= (const var &A)
{
	if (i_number != 0)
	{
		if (A.i_number != 0)
			return (i_number <= A.i_number);
		else if (A.d_number != 0)
			return (i_number <= (int)A.d_number);
		else
			return (i_number <= (int)A.S); //operator int was overloaded in class String
	}
	else if (d_number != 0)
	{
		if (A.i_number != 0)
			return (d_number <= (double)A.i_number);
		else if (A.d_number != 0)
			return (d_number <= A.d_number);
		else
			return (d_number <= (double)A.S); //operator double was overloaded in class String
	}
	else
	{
		if (A.i_number != 0)
		{
			char number[10];
			_itoa_s(A.i_number, number, 10);		//converting int to char array
			return (S <= number);
		}
		else if (A.d_number != 0)
		{
			char number[20];
			sprintf_s(number, "%f", A.d_number);	//converting int to char array
			return (S <= number);
		}
		else
			return (S <= A.S);
	}
}

bool var::operator>= (const var &A)
{
	if (i_number != 0)
	{
		if (A.i_number != 0)
			return (i_number >= A.i_number);
		else if (A.d_number != 0)
			return (i_number >= (int)A.d_number);
		else
			return (i_number >= (int)A.S); //operator int was overloaded in class String
	}
	else if (d_number != 0)
	{
		if (A.i_number != 0)
			return (d_number >= (double)A.i_number);
		else if (A.d_number != 0)
			return (d_number >= A.d_number);
		else
			return (d_number >= (double)A.S); //operator double was overloaded in class String
	}
	else
	{
		if (A.i_number != 0)
		{
			char number[10];
			_itoa_s(A.i_number, number, 10);		//converting int to char array
			return (S >= number);
		}
		else if (A.d_number != 0)
		{
			char number[20];
			sprintf_s(number, "%f", A.d_number);	//converting int to char array
			return (S >= number);
		}
		else
			return (S >= A.S);
	}
}

bool var::operator== (const var &A)
{
	if (i_number != 0)
	{
		if (A.i_number != 0)
			return (i_number == A.i_number);
		else if (A.d_number != 0)
			return (i_number == (int)A.d_number);
		else
			return (i_number == (int)A.S); //operator int was overloaded in class String
	}
	else if (d_number != 0)
	{
		if (A.i_number != 0)
			return (d_number == (double)A.i_number);
		else if (A.d_number != 0)
			return (d_number == A.d_number);
		else
			return (d_number == (double)A.S); //operator double was overloaded in class String
	}
	else
	{
		if (A.i_number != 0)
		{
			char number[10];
			_itoa_s(A.i_number, number, 10);		//converting int to char array
			return (S == number);
		}
		else if (A.d_number != 0)
		{
			char number[20];
			sprintf_s(number, "%f", A.d_number);	//converting int to char array
			return (S == number);
		}
		else
			return (S == A.S);
	}
}

bool var::operator!= (const var &A)
{
	if (i_number != 0)
	{
		if (A.i_number != 0)
			return (i_number != A.i_number);
		else if (A.d_number != 0)
			return (i_number != (int)A.d_number);
		else
			return (i_number != (int)A.S); //operator int was overloaded in class String
	}
	else if (d_number != 0)
	{
		if (A.i_number != 0)
			return (d_number != (double)A.i_number);
		else if (A.d_number != 0)
			return (d_number != A.d_number);
		else
			return (d_number != (double)A.S); //operator double was overloaded in class String
	}
	else
	{
		if (A.i_number != 0)
		{
			char number[10];
			_itoa_s(A.i_number, number, 10);		//converting int to char array
			return (S != number);
		}
		else if (A.d_number != 0)
		{
			char number[20];
			sprintf_s(number, "%f", A.d_number);	//converting int to char array
			return (S != number);
		}
		else
			return (S != A.S);
	}
}



