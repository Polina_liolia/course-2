#include "Points_dinamic_array.h"
#include "Point.h"
using std::cout;

void main()
{
	point A("p", 1, 2);
	point B;
	B = A + 3333;
	B.print();

	point C;
	C = A + (float)500.3;
	C.print();

	point D;
	D = A + 9223372036854775.5;
	D.print();

	bool flag;
	if (B == A)
		flag = true;
	else
		flag = false;
	cout << flag << "\n";

	if (B != A)
		flag = true;
	else
		flag = false;
	cout << flag << "\n";

	Points_dinamic_array Arr1;
	Arr1 += A;
	Arr1 += C;
	Arr1.print();

	Points_dinamic_array Arr2;
	Arr2 += C;
	Arr2 += D;
	Arr2.print();

	Points_dinamic_array Arr3;
	Arr3 = Arr2;
	Arr3.print();

	Points_dinamic_array Arr4;
	Arr4 = Arr1 + Arr2;
	Arr4.print();

	Points_dinamic_array Arr5;
	Arr5 += Arr1;
	Arr5 += D;

	if (Arr3 == Arr2)
		flag = true;
	else
		flag = false;
	cout << flag << "\n";

	if (Arr3 == Arr5)
		flag = true;
	else
		flag = false;
	cout << flag << "\n";



}