#include "Element.h"
Element::Element()
{
	next = prev = nullptr;
}

Element::Element(ingredient aValue)
{
	value = aValue;
	next = prev =  0;
}

void Element::set_value(ingredient aValue)
{
	value = aValue;
}

ingredient Element::get_value()
{
	return value;
}

void Element::set_next(Element* aNext)
{
	next = aNext;
}

Element* Element::get_next()
{
	return next;
}

void Element::set_prev(Element* aPrev)
{
	prev = aPrev;
}
Element* Element::get_prev()
{
	return prev;
}
