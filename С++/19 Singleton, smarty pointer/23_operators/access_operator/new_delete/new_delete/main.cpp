#include <iostream>
using namespace std;


class A
{
	int x;
public:
	A(){
		x = 0;
	}
	void* operator new(size_t sz){
		cout << "operator new called!! "<<"("<<sz<<" bytes)"
			<< endl;
		return malloc(sz);
	}
	void* operator new[](size_t sz){
		cout << "operator new[] called!!"<<"("<<sz<<" bytes)"
			<< endl;
		return malloc(sz);
	}
	void operator delete(void* ptr){
		cout << "operator delete called!!"<< endl;
		free(ptr);
	}
	void operator delete[](void* ptr){	
		cout << "operator delete[] called!!"<< endl;
		free(ptr);
	}
};
void main()
{
	A *pa = new A; // sizeof(A)
	A *pa2 = new A[20];// sizeof(A) * 20

	delete[] pa2;
	delete pa;

}