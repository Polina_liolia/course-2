#include <windows.h>
#include <TChar.h>// char wchar_t - TCHAR
HINSTANCE hInstance;
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, 
	                       WPARAM wParam, LPARAM lParam) ;

int WINAPI WinMain(
				   HINSTANCE hInstance,    // ���������� �������� ���������� ����������
				   HINSTANCE hPrevInstance,// ���������� ����������� ���������� ����������
				   LPSTR lpCmdLine,        // ��������� �� ��������� ������
				   int nCmdShow= SW_SHOWMINIMIZED           // ���������� ��������� ����
				   )
{
	
	MSG uMsg; 
	HWND hMainWnd; 
	TCHAR szClassName[] = TEXT("MyClass"); 

	WNDCLASSEX wcex; // ���������� ���������� ���� ��������� �������� ������

	wcex.cbSize = sizeof(WNDCLASSEX); // ������ ��������� � ������
	wcex.style			= CS_HREDRAW | CS_VREDRAW;	// ����� ����
	wcex.lpfnWndProc	= (WNDPROC)WndProc; //����� ������� ���������
	wcex.cbClsExtra		= 0;						
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;		// ��������� ����������
	wcex.hIcon			= LoadIcon(hInstance,MAKEINTRESOURCE(IDI_APPLICATION));		// ����������� ������
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW); // ����������� �������
	wcex.hbrBackground	= GetSysColorBrush(COLOR_BTNFACE); // ��������� ����
	wcex.lpszMenuName	= NULL;		// ����������� ����
	wcex.lpszClassName	= szClassName;	// ��� ������
	wcex.hIconSm		= NULL; //����������� ��������� ������



	// ������������ ����� ���� 
	if (!RegisterClassEx(&wcex)) { 
		MessageBox(NULL, TEXT("Cannot register class"), TEXT("Error"), MB_OK);
		return 0; 
	}

	// ������� �������� ���� ���������� 
	hMainWnd = CreateWindow( 
		szClassName, 
		TEXT("A Hello Application"),
		WS_OVERLAPPEDWINDOW, 
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, 
		(HWND)NULL, 
		(HMENU)NULL, 
		(HINSTANCE)hInstance, NULL 
		); 

	// ���������� ���� ���� 
	ShowWindow(hMainWnd, nCmdShow); 
	// UpdateWindow(hMainWnd); // ��. ���������� � ������� "����������� ����.. 

	// ��������� ���� ��������� ��������� �� �������� ���������� 
	while (GetMessage(&uMsg, NULL, 0, 0)) { 
		TranslateMessage(&uMsg); 
		DispatchMessage(&uMsg); 
	}
	return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, 
	                     WPARAM wParam, LPARAM lParam) 
{ 
	
	
	HINSTANCE hInst;
	BOOL flag = false;
	hInst = hInstance;
	switch (uMsg) 
	{ 
	case WM_CREATE:
		{
			CreateWindow (TEXT("button"), TEXT("EDIT"),
				WS_CHILD | WS_VISIBLE |BS_PUSHBUTTON, 
				10, 10, 100, 30, hWnd, (HMENU) 1, 
				hInst, NULL);
		}
		break;
	case WM_PAINT: 
		{ 
			PAINTSTRUCT ps;
			RECT rect;
			HDC hde=BeginPaint (hWnd,&ps);
			GetClientRect (hWnd, &rect);
			DrawText (hde, TEXT("�������� �������"), -1, & rect, DT_SINGLELINE|DT_CENTER|DT_VCENTER);
			
			EndPaint (hWnd,&ps); 

		} 
		break; 
	case WM_COMMAND:
		{	
			int c;
			HWND hCommWnd;
			int ID;

			//������ � ���������� �������� �������������� ���������
			ID = LOWORD( wParam ) ;//�������������
			hCommWnd = HWND ( lParam );//��������� ����\������, �� �-��� ������ ���������
			c = HIWORD (wParam);//�����������
			// c == BN_CLICKED
			if(BN_CLICKED == c){
				switch ( ID )
				{
				case 1:
					{					
						MessageBox ( NULL, TEXT("OK"), TEXT(":)"), MB_OK );
						
					}
					break;
				}
			}

		}
		break;

	case WM_DESTROY: 
		PostQuitMessage(0); 
		break; 

	default: 
		return DefWindowProc(hWnd, uMsg, wParam, lParam); 
	} 

	return FALSE; 
}

