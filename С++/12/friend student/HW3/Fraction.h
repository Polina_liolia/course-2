#pragma once
#include <iostream>
#include <cstring>
using std::cout;
using std::cin;

class fraction
{
private:
	int numerator;
	int denominator;

public:
	fraction()
	{
		numerator = 0;
		denominator = 0;
	}

	fraction(int aNumerator, int aDenominator)
	{
		numerator = aNumerator;
		denominator = aDenominator;
	}

	public:

	void set_numerator(int aNumerator)
	{
		numerator = aNumerator;
	}
	
	void set_denominator(int aDenominator)
	{
		denominator = aDenominator;
	}

	int get_numerator()
	{
		return numerator;
	}

	int get_denominator()
	{
		return denominator;
	}

	void print()
	{
		cout << numerator << "/" << denominator << "\n";
	}

	int reduce();
};
