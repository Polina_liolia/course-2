var formF = document.forms.f;

function checkFormFields() {
    var name = formF.elements._name;
    var position = formF.elements._position;
    if (name.value == "" || position.value == "")
        {
            alert("Error: all fields have to be filled in!");
            event.preventDefault();
        }
}
formF.addEventListener("submit", checkFormFields);


var formF2 = document.forms.f2;

function showForm2() {
    var arr = [];
    for (var i = 0; i < formF2.elements.length; i++)
        if (formF2.elements[i].checked)
            arr.push(formF2.elements[i].value);
    alert(arr);
}

formF2.addEventListener("submit", showForm2);