#include"Log.h"

Log::Log()	//default constructor
{
	reg_size = 0;
	reg = nullptr;
}

Log::Log(const Log &aLog)	//copy constructor
{
	reg_size = aLog.reg_size;
	reg = new UniVar[reg_size];
	for (int i = 0; i < reg_size; i++)
		reg[i] = aLog.reg[i];
}

Log::~Log()	//destructor
{
	delete[] reg;
}

shared_ptr<Log> Log::getInstance()	//to get pointer on unique instance of class Log
{
	return l;
}

int Log::get_reg_size() const	//to get current size of log (number of registers)
{
	return reg_size;
}

UniVar * Log::get_reg() const	//to get pointer on UniVar array (all registers of Log)
{
	return reg;
}

void Log::del_reg(int index)	//to delete pointed register (takes index)
{
	try
	{
		if (index >= reg_size)
			throw "Trying to delete unexisting registration";
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
		return;
	}
	UniVar *tmp = new UniVar[reg_size - 1];
	for (int i = 0, j = 0; i < reg_size; i++)
		if (i != index)
		{
			tmp[j] = reg[i];
			j++;
		}
	reg_size--;
	delete[] reg;
	reg = tmp;
}

//overloaded operator[] (takes index of register, returns instance of UniVar)
UniVar Log::operator[](int i)
{
	try
	{
		if (i >= reg_size)
			throw "Trying to get unexisting registration";
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
		return UniVar();
	}
	return reg[i];
}
