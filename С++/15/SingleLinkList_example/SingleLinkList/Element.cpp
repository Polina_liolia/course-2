#include "Element.h"
Element::Element(){
	value = 0;
	next = 0;
}
Element::Element(int aValue){
	value = aValue;
	next = 0;
}
void Element::set_value(int aValue){
	value = aValue;
}
int Element::get_value(){
	return value;
}
void Element::set_next(Element* aNext){
	next = aNext;
}
Element* Element::get_next(){
	return next;
}