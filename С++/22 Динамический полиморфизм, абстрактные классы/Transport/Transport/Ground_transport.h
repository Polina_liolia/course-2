#pragma once
#include"Transport.h"

class Ground_transport : public Transport
{
protected:
	double engine_volume;

public:
	Ground_transport();
	Ground_transport(string aModel, double aWeight, double aMax_speed, speed aSpeed_units, double aEngine_volume);
	~Ground_transport() {}

public:
	void set_engine_volume(double aEngine_volume);
	double get_engine_volume()const;

	virtual speed get_speed_units()const;
	virtual void print();
};