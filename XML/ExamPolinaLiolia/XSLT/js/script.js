function loadXMLDoc(filename)
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", filename, false);
    try {xhttp.responseType = "msxml-document"} catch(err) {} // Helping IE11
    xhttp.send();
    return xhttp.responseXML;
}

function displayResult()
{
    var xml = loadXMLDoc("houses.xml");
    var xsl = loadXMLDoc("style.xsl");
    if (document.implementation && document.implementation.createDocument)
    {
        var xsltProcessor = new XSLTProcessor();
        xsltProcessor.importStylesheet(xsl);
        var resultDocument = xsltProcessor.transformToFragment(xml, document);
        document.getElementById("main").appendChild(resultDocument);
    }
}
displayResult();