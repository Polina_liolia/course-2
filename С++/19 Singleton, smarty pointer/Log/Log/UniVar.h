#pragma once
#include "MyString.h"
enum type_var {Int, Fl, St};

class UniVar
{
private:
	int Integer;
	float Float;
	String Str;
	type_var value;

public:
	UniVar();
	UniVar(int a);
	UniVar(float a);
	UniVar(String a);
	int get_Int()const;
	float get_Float()const;
	String get_str()const;
	void set(int i);
	void set(float f);
	void set(String s);
	UniVar operator=(int a);
	UniVar operator=(float a);
	UniVar operator=(String a);
	operator int();
	operator float();
	operator String();

	friend ostream& operator << (ostream& os, const UniVar &aVar);
};