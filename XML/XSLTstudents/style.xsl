<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <h2>Группа <xsl:value-of select="/group/@name"/></h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>ФИО</th>
                        <th>Математика</th>
                        <th>Биология</th>
                        <th>География</th>
                        <th>Физика</th>
                    </tr>
                    <!--applying template by pointed select:-->
                    <!--<xsl:apply-templates-->
                            <!--select = "/group"-->
                            <!--mode = "m1">-->
                    <!--</xsl:apply-templates>-->

                    <!--the same: calling template by its name: -->
                    <xsl:call-template name="sort_by_subject">
                        <xsl:with-param name="subject_name">subjects_list/math</xsl:with-param>
                    </xsl:call-template>
                </table>
            </body>
        </html>
    </xsl:template>


    <xsl:template match="/group" mode="m1" name="sort_by_subject">
        <xsl:param name="subject_name"/>
        <xsl:for-each select="/group/student">
            <xsl:sort select="$subject_name"
                      data-type="number"/>
            <tr>
                <td><xsl:value-of select="name"/></td>
                <td><xsl:value-of select="subjects_list/math"/></td>
                <td><xsl:value-of select="subjects_list/biology"/></td>
                <td><xsl:value-of select="subjects_list/geography"/></td>
                <td><xsl:value-of select="subjects_list/phisics"/></td>
            </tr>
        </xsl:for-each>
    </xsl:template>

<xsl:template match="/group" mode="m3" name="sort_by_name">
    <xsl:for-each select="/group/student">
        <xsl:sort select="name"/>
        <tr>
            <td><xsl:value-of select="name"/></td>
            <td><xsl:value-of select="subjects_list/math"/></td>
            <td><xsl:value-of select="subjects_list/biology"/></td>
            <td><xsl:value-of select="subjects_list/geography"/></td>
            <td><xsl:value-of select="subjects_list/phisics"/></td>
        </tr>
    </xsl:for-each>
</xsl:template>

<xsl:template match="/group" mode="m2" name="sort_by_marks">
    <xsl:param name="subjects_number"/>
    <xsl:for-each select="/group/student">
        <xsl:sort select='sum(subjects_list/*) div $subjects_number'
                  order = "descending"
                  data-type="number"/>
        <tr>
            <td><xsl:value-of select="name"/></td>
            <td><xsl:value-of select="subjects_list/math"/></td>
            <td><xsl:value-of select="subjects_list/biology"/></td>
            <td><xsl:value-of select="subjects_list/geography"/></td>
            <td><xsl:value-of select="subjects_list/phisics"/></td>
        </tr>
    </xsl:for-each>
</xsl:template>

</xsl:stylesheet>