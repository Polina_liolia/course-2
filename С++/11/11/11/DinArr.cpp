#include"DinArr.h"
#include"Remove.h"

DinArr::DinArr()
{
	arr = nullptr;
	sz = 0;
}

DinArr::DinArr(int *aArr, int aSz)
{
	sz = aSz;
	arr = new int[sz];
	for (int i = 0; i < sz; i++)
		arr[i] = aArr[i];
}

DinArr::DinArr(const DinArr &a)
{
	sz = a.sz;
	arr = new int[sz];
	for (int i = 0; i < sz; i++)
		arr[i] = a.arr[i];
}

DinArr::~DinArr()
{
	delete[] arr;
}

DinArr DinArr::operator=(const DinArr &a)
{
	if (sz > 0)
		delete[] arr;
	sz = a.sz;
	arr = new int[sz];
	for (int i = 0; i < sz; i++)
		arr[i] = a.arr[i];
	return *this;
}

void DinArr::add_element(int a)
{
	int *temp = new int[sz + 1];
	for (int i = 0; i < sz; i++)
		temp[i] = arr[i];
	temp[sz] = a;
	if (sz > 0)
		delete[] arr;
	sz++;
	arr = temp;
}

void DinArr::remove_element(int index)
{
	if (sz == 0)
		return;
	int *temp = new int[sz - 1];
	for (int i = 0, j = 0; i < sz; i++)
		if (i != index)
		{
			temp[j] = arr[i];
			j++;
		}
	delete[] arr;
	sz--;
	arr = temp;
}

void DinArr::print()
{
	for (int i = 0; i < sz; i++)
		std::cout << arr[i] << " ";
	std::cout << "\n";
}

void DinArr::remove_if(Remove functor)
{
	for (int k = 0; k < sz; k++)
		if (functor(arr[k]))
		{
			int *temp = new int[sz - 1];
			for (int i = 0, j = 0; i < sz; i++)
				if (i != k)
				{
					temp[j] = arr[i];
					j++;
				}
			delete[] arr;
			sz--;
			arr = temp;
			k--;
		}
}
