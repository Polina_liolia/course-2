window.onload=function(){

    var submit = document.querySelector("form>input:nth-of-type(1)");
    var form = document.forms.cookies;
    function addCoocie(){
        var name = form.elements.cookie_name.value;
        var val = form.elements.cookie_val.value;
        if (!name || !val)
        {
            event.preventDefault();
            return;
        }
        setCookie(name, val, 3600);
        form.elements.cookie_name.value="";
        form.elements.cookie_val.value = "";
    }
    submit.addEventListener("click", addCoocie);

    function showAllCookies() {
        var allCookies = document.cookie;
        var cookiesArr = allCookies.split("; ");
        for (var i=0; i< cookiesArr.length; i++)
            console.log(cookiesArr[i]);
    }
    var show_cookies = document.querySelector("form>input:nth-of-type(2)");
    show_cookies.addEventListener("click", showAllCookies);

    function clearAllCookies(){
        var regCookiename = /=(\w)*?; /ig;
        var cookieNames = document.cookie.split(regCookiename);
        for (var i = 0; i < cookieNames.length; i++)
            deleteCookie(cookieNames[i]);
    }
    var clear_cookies = document.querySelector("form>input:nth-of-type(3)");
    clear_cookies.addEventListener("click", clearAllCookies);
};
