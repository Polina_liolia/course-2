#pragma once
#include <iostream>
using std::ostream;


class queue
{
private:
	int size;
	int *items;

public:
	queue();  
	queue(int aItem);
	queue(const queue &aQueue);
	~queue();
	int get_size();
	void insert(int number); 
	const int pop();	
	int operator[](int index);	
	bool operator== (const queue &aQueue);	
	bool operator!= (const queue &aQueue);	
};