#include "Tree.h"
#include "Element.h"
#include <iostream>
using namespace std;
Tree::Tree(){
	root = NULL;
}
// ���������� �������� � ������
void Tree::add(int val){
	if( !root )
		root = new Element(val);
	else
	{
		Element *tmp = root;
		while(tmp)
		{
			if (val < tmp->get_value())
				if (tmp->get_left() == NULL)
				{
					tmp->set_left(new Element(val));
					break;
				}
				else tmp = tmp->get_left();
			else {
				if (val > tmp->get_value())
					if (tmp->get_right() == NULL)
					{
						tmp->set_right(new Element(val));
						break;
					}
					else tmp = tmp->get_right();
				else tmp = NULL;
			}
		}
	}
}


int Tree::min(){
	Element *tmp = root;
	while (tmp && tmp->get_left()){
		tmp = tmp->get_left();
	}
	return tmp?tmp->get_value():0;
}

void Tree::print()
{
	print_helper(root);
}

void Tree::print_helper(Element *el)	
{
	//simmetric
	if (el->get_left())
		print_helper(el->get_left()); 
	cout << el->get_value() << " ";
	if (el->get_right())
		print_helper(el->get_right());

	/*
	//root - min - max //������
	cout << el->get_value() << " ";
	if (el->get_left())
		print_helper(el->get_left()); 
	if (el->get_right())
		print_helper(el->get_right());
	*/

	/*
	//root - max - min	//��������
	cout << el->get_value() << " ";
	if (el->get_right())
		print_helper(el->get_right());
	if (el->get_left())
		print_helper(el->get_left()); 
	*/
}

