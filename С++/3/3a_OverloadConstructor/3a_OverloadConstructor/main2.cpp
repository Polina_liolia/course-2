#include <iostream>
using namespace std;
class A
{
	int x;
	char *name;
public:
	A(int aX = 0, char* aName = NULL) {
		x = aX;
		if (aName && strlen(aName)) {
			name = new char[strlen(aName) + 1];
			strcpy_s(name, strlen(aName) + 1, aName);
		}
		else name = NULL;
	}
	~A() {
		delete[] name;
	}
};
void main()
{
	A x;
	A y(45, "Hello");



}