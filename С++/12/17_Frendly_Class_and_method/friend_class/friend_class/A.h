#pragma once

class A{
	
	friend class B;
	int x;
	int y;
	friend void f1(A);

public:
	A(){
		x = y = 0;
	}


};

A z;

void f1(A a)
{
	a.x = a.y = 34;
	z.x = z.y = 56;
	A d;
	d.x = d.y = 45;
}