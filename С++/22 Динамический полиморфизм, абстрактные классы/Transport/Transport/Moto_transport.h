#pragma once
#include"Ground_transport.h"

enum moto_configuration {basic, tricicle, trike};

class Moto_transport : public Ground_transport
{
private:
	moto_configuration config;

public:
	Moto_transport();
	Moto_transport(string aModel, double aWeight, double aMax_speed, speed aSpeed_units, double aEngine_volume, moto_configuration aConfig);
	~Moto_transport() {};

public:
	void set_config(moto_configuration aConfig);
	moto_configuration get_config()const;

	virtual speed get_speed_units()const;
	virtual void print();
};