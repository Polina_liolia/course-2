#include <windows.h>
#include <tchar.h>
#include "resource.h"

HWND hList1, hList2, hEdit1, hEdit2;

BOOL CALLBACK DlgProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow)
{
	return DialogBox(hInstance, MAKEINTRESOURCE(IDD_DIALOG1), NULL, DlgProc); 
}

BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message)
	{
		case WM_CLOSE:
			EndDialog(hWnd, 0);
			return TRUE;

		case WM_INITDIALOG:
			// ������� ����������� ��������� ����������
			hList1 = GetDlgItem(hWnd, IDC_LIST1);
			hList2 = GetDlgItem(hWnd, IDC_LIST2);
			hEdit1 = GetDlgItem(hWnd, IDC_EDIT1);
			hEdit2 = GetDlgItem(hWnd, IDC_EDIT2);
			
			// ������� 8 ��������� � ������ � ��������� �������
			SendMessage(hList1, LB_ADDSTRING, 0, LPARAM(TEXT("����� ������")));
			SendMessage(hList1, LB_ADDSTRING, 0, LPARAM(TEXT("��������� ������")));
			SendMessage(hList1, LB_ADDSTRING, 0, LPARAM(TEXT("������� ��������")));
			SendMessage(hList1, LB_ADDSTRING, 0, LPARAM(TEXT("��������� �������")));
			SendMessage(hList1, LB_ADDSTRING, 0, LPARAM(TEXT("������� ������")));
			SendMessage(hList1, LB_ADDSTRING, 0, LPARAM(TEXT("���� �������")));
			SendMessage(hList1, LB_ADDSTRING, 0, LPARAM(TEXT("����� ������")));
			SendMessage(hList1, LB_ADDSTRING, 0, LPARAM(TEXT("���� �������")));
			
			// ��������� ������ ������� ��� ������ � ������������� �������
			SendMessage(hList2, LB_SETCOLUMNWIDTH, 170, 0);

			// ������� 8 ��������� � ������ � ������������� �������
			SendMessage(hList2, LB_ADDSTRING, 0, LPARAM(TEXT("���� ������")));
			SendMessage(hList2, LB_ADDSTRING, 0, LPARAM(TEXT("��������� ������� ������")));
			SendMessage(hList2, LB_ADDSTRING, 0, LPARAM(TEXT("������ ��������")));
			SendMessage(hList2, LB_ADDSTRING, 0, LPARAM(TEXT("�������� �������")));
			SendMessage(hList2, LB_ADDSTRING, 0, LPARAM(TEXT("����� ������")));
			SendMessage(hList2, LB_ADDSTRING, 0, LPARAM(TEXT("������� �������")));
			SendMessage(hList2, LB_ADDSTRING, 0, LPARAM(TEXT("������� ������")));
			SendMessage(hList2, LB_ADDSTRING, 0, LPARAM(TEXT("�������� �������")));
			return TRUE;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case IDC_ADD: 
					{
						// ��������� ����� ������ (�������� �����), ��������� � ��������� ����
						int length = SendMessage(hEdit1, WM_GETTEXTLENGTH, 0, 0);
						// ������� ������ ������������ �������
						TCHAR *pBuffer = new TCHAR[length + 1];
						// � ���������� ������ ��������� �����, �������� � �������� ����
						GetWindowText(hEdit1, pBuffer, length + 1);
						if(lstrlen(pBuffer))
						{
							// �������, ������� �� ��� � ������ �������� �������� ����� 
							int index = SendMessage(hList1, LB_FINDSTRINGEXACT, -1, LPARAM(pBuffer));
							if(index == LB_ERR)
								// ������� �������� ����� � ������
								SendMessage(hList1, LB_ADDSTRING, 0, LPARAM(pBuffer));
							else
								MessageBox(hWnd, TEXT("����� ���� ��� ����������!"), TEXT("���������� �����"), MB_OK | MB_ICONSTOP);
						}
						delete[] pBuffer;
					}
					break;
				case IDC_DEL:
					{
						// ������� ������ ���������� �������� ������
						int index = SendMessage(hList1, LB_GETCURSEL, 0, 0);
						if(index != LB_ERR) // ������ �� ������� ������?
						{
							// ��������� ����� �������� ���������� �����
							int length = SendMessage(hList1, LB_GETTEXTLEN, index, 0);
							// ������� ������ ������������ �������
							TCHAR *pBuffer = new TCHAR[length + 1];
							// ��������� �������� ����� � ���������� ������
							SendMessage(hList1, LB_GETTEXT, index, LPARAM(pBuffer));
							MessageBox(hWnd, pBuffer, TEXT("�������� �����"), MB_OK | MB_ICONINFORMATION);
							// ������ ������ �� ������
							SendMessage(hList1, LB_DELETESTRING, index, 0);
							delete[] pBuffer;
						}
						else
							MessageBox(hWnd, TEXT("���� �� ������!"), TEXT("�������� �����"), MB_OK | MB_ICONSTOP);
					}
					break;
				case IDC_FIND:
					{
						// ��������� ����� �������� ��������, ��������� � ��������� ����
						int length = SendMessage(hEdit2, WM_GETTEXTLENGTH, 0, 0);
						// ������� ������ ������������ �������
						TCHAR *pBuffer = new TCHAR[length + 1];
						// ��������� �������� ����� � ���������� ������
						GetWindowText(hEdit2, pBuffer, length + 1);
						if(lstrlen(pBuffer))
						{
							// ���������, ������� �� � ������ ������� �������� �����
							int index = SendMessage(hList1, LB_SELECTSTRING, -1, LPARAM(pBuffer));
							if(index == LB_ERR)
								MessageBox(hWnd, TEXT("���� �� ������!"), TEXT("����� �����"), MB_OK | MB_ICONSTOP);
						}
						delete[] pBuffer;
					}
					break;
				case IDC_DELALL:
					// ������� ���������� ������
					SendMessage(hList1, LB_RESETCONTENT, 0, 0);
					break;
				case IDC_GETSELITEMS:
					{
						// ��������� ���������� ��������� ��������� � ������ � ������������� �������
						int nCount = SendMessage(hList2, LB_GETSELCOUNT, 0, 0);
						// ������� ������ ������������ ������� ��� �������� �������� ��������� ��������� ������
						int *p = new int[nCount];
						// �������� ������������ ������ ��������� ���������� ��������� ������ 
						SendMessage(hList2, LB_GETSELITEMS, nCount, LPARAM(p));
						for(int i = 0; i < nCount; i++)
						{
							// ��������� ������ ������ �������� ������
							int length = SendMessage(hList2, LB_GETTEXTLEN, p[i], 0);
							// ������� ������ ������������ �������
							TCHAR *pBuffer = new TCHAR[length + 1];
							// � ���������� ������ ��������� ����� ���������� �������� ������
							SendMessage(hList2, LB_GETTEXT, p[i], LPARAM(pBuffer));
							MessageBox(hWnd, pBuffer, TEXT("������ � ������������� �������"), MB_OK | MB_ICONINFORMATION);
							delete[] pBuffer;
						}
					}
			}
			// � ������ � ��������� ������� ������� ����� ��� �������
			if(LOWORD(wParam) == IDC_LIST1 && HIWORD(wParam) == LBN_SELCHANGE)
			{
				// ������� ������ ���������� �������� ������
				int index = SendMessage(hList1, LB_GETCURSEL, 0, 0);
				if(index != LB_ERR) // ������ �� ������� ������?
				{
					// ��������� ����� �������� �����
					int length = SendMessage(hList1, LB_GETTEXTLEN, index, 0);
					// ������� ������ ������������ �������
					TCHAR *pBuffer = new TCHAR[length + 1];
					// � ���������� ������ ��������� ����� ���������� �������� ������
					SendMessage(hList1, LB_GETTEXT, index, LPARAM(pBuffer));
					// ��������� ����� � ��������� �������� ����
					SetWindowText(hWnd, pBuffer);
					delete[] pBuffer;
				}
			}
			return TRUE;
	}
	return FALSE;
}