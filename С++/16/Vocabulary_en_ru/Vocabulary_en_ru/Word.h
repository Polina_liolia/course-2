#pragma once
#include"MyString.h"

class Word
{
private:
	String en;		//word in english
	String rus;		//word in russian
	int counter;	//counter of word's calls

public:
	Word();
	Word(String aEn, String aRus, int aCounter);
	Word(const Word &aWord);
	~Word();

public:
	void set_word(String aEn, String aRus, int aCounter);
	void set_rus(String aRus);
	void change_rus(String aRus_new);
	int get_counter()const;
	void add_count();
	String get_en()const;
	String get_translation();
	Word operator=(const Word &tmp);

	bool operator==(const Word & tmp);

	bool operator!=(const Word & tmp);

	bool operator>(const Word & tmp);

	bool operator>=(const Word & tmp);

	bool operator<(const Word & tmp);

	bool operator<=(const Word & tmp);

	friend ostream & operator << (ostream& os, const Word &aWord);
};

