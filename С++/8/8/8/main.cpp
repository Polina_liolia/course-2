#include "Counter.h"

void main()
{
	Counter a(10, 20);
	a++;
	a.print();
	++a;
	a.print();
	--a;
	a.print();
	a--;
	a.print();
	a--;
	a.print();
	a--;
	a.print();

}