#include"Company.h"
#include"Search.h"
using std::ostream;

//global overload of outstream for Company
ostream& operator<<(ostream& os, Company& C)
{
	os << "Company name: " << C.name << "\n";
	os << "Description:\n" << C.description << "\n";
	os << "Accaunt type: ";
	switch (C.type)
	{
	case 0: cout << "Free\n"; break;
	case 1: cout << "silver\n"; break;
	case 2: cout << "gold\n"; break;
	case 3: cout << "platinum\n"; break;
	}
	os << "Key words: ";
	for (auto i : C.key_words)
		os << i << " ";
	os << "\n";
	return os;
}

//global overload of outstream for Search
ostream& operator<<(ostream& os, Search& S)
{
	int count = 0;
	for (auto i : S.all_companies)
		os << ++count << ". " << i << "\n";
	return os;
}

void main()
{
	Search test;
	test.add_company("C1", "Cars manufacturing, car sales", platinum);
	test.add_key_word(0, "car");

	test.add_company("C2", "Cars manufacturing, car sales, wheels", gold);
	test.add_key_word(1, "car");
	test.add_key_word(1, "sale");

	test.add_company("C3", "Cars manufacturing, car sales, wheels, oil, parts", silver);
	test.add_key_word(2, "car");
	test.add_key_word(2, "sale");
	test.add_key_word(2, "oil");

	test.add_company("C4", "Cars manufacturing, car sales, wheels, oil, gas, parts", Free);
	test.add_key_word(3, "car");
	test.add_key_word(3, "sale");
	test.add_key_word(3, "oil");
	cout << "Trying to add more than 3 key words using Free accaunt:\n";
	test.add_key_word(3, "part");
	
	test.add_company("C5", "", platinum);
	test.add_key_word(4, "bread");
	test.add_key_word(4, "butter");
	test.add_key_word(4, "oil");

	//the same key words as C2 has, but more words in description:
	test.add_company("C6", "Cars manufacturing, car sales, wheels, oil, gas, parts", gold);
	test.add_key_word(5, "car");
	test.add_key_word(5, "sale");

	//Company without current key words:
	test.add_company("C7", "Cars manufacturing, car sales, wheels, oil, gas, parts", gold);
	test.add_key_word(6, "clothes");
	test.add_key_word(6, "shoes");

	test.add_company("C8", "Gas, oil for cars", Free);
	test.add_key_word(7, "car");
	test.add_key_word(7, "oil");

	test.add_company("C9", "Furniture sale", silver);
	test.add_key_word(8, "sales");
	test.add_key_word(8, "oil");
	
	test.add_company("C10", "Olive oil sale", gold);
	test.add_key_word(9, "oil");

	test.add_company("C11", "Sale of gas, oil for cars", Free);
	test.add_key_word(10, "car");
	test.add_key_word(10, "oil");

	test.add_word_to_find("car");
	test.add_word_to_find("oil");
	test.add_word_to_find("sale");

	cout << "\nSearch rezults:\n";
	test.find_company_by_words();


}