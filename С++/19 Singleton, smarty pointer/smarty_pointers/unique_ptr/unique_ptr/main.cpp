#include <iostream>
#include <memory>

using namespace std;

class A{

public:
	A(){
		cout << "Constructor A" << endl;
	}

	~A(){
		cout << "Destructor A" << endl;
	}
};


void main(){

	const int arrSz = 5;
	unique_ptr<A> arr[arrSz];

	arr[0] = unique_ptr<A>(new A);
	arr[1] = unique_ptr<A>(new A);
	arr[2] = unique_ptr<A>(new A);
	arr[3] = unique_ptr<A>(new A);
	arr[4] = unique_ptr<A>(new A);
	
	for (int i = 0; i < arrSz; i++)
		cout << "arr[" << i  << "]= " << arr[i].get() << endl;
	
	unique_ptr<A> tmp;
	//tmp = arr[3]; // error;

	cout << "ewhwehwhe" << endl;


}