#pragma once
#include <iostream>
#include "Point.h"

/*
�������� + ������ �������� ��� ���������� ��������������� ������� ���������� ���� ������������ ��������. a = b + c.
2) �������� += ��� ���������� �������� ������� � ��� ������� ����������� ���� ��������.
3) �������� == ����������� �������� ��� �������.

*/
class Points_dinamic_array
{
private:
	point *arr;
	int sz;

public:
	Points_dinamic_array();
	Points_dinamic_array(const Points_dinamic_array &A);
	~Points_dinamic_array();

public:
	Points_dinamic_array operator= (const Points_dinamic_array &A);
	Points_dinamic_array operator+ (const Points_dinamic_array &A);
	Points_dinamic_array operator+= (const point &A);
	Points_dinamic_array operator+= (const Points_dinamic_array &A);
	bool operator== (const Points_dinamic_array &A);
	void print();
};