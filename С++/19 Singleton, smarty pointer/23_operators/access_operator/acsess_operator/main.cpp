#include <iostream>
using namespace std;


class A{
	int x;
public:
	A(){
		x = 0;
	}
	void a_method(){
		cout << "a_method called!!" << endl;
	}
	~A(){
		cout << "A::destructor" << endl;
	}
};

class B
{
	A* a;
public:
	B(){
		a = NULL;
	}
	B(A* arg){
		a = arg;
	}
	void set_a(A* arg){
		a = arg;
	}
	~B(){
		delete a;
	}
	
	A* operator->(){
		return a;
	}
};
void main()
{
	//I
	A* a = new A;
	B b(a);
	//II
	B b1(new A);
	
	b->a_method();
//  b-> === a
//  a->a_method();
	

	
}