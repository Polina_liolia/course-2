#include <iostream>
using namespace std;

class Point{
public:
	int x;
	int y;
	Point(int numb1){
		x = y = numb1;
	}
	Point(int numb1, int numb2){
		x = numb1;
		y = numb2;
	}
	void set_x(int ax){
		x = ax;
	}
	void set_y(int ay){
		y = ay;
	}
	
	
	
	void print_props(){
		cout<<"X: "<< x <<endl;
		cout<<"Y: "<< y <<endl;
	}
	~Point(){
		cout<<"Destructor "<< x <<" "<< y <<endl;
	}
} ;


void main(){
	Point p1(10);

	Point p2(10,20);
	p2.print_props();

	Point *p_point = NULL;
	p_point = &p2;
	p_point->print_props();

	Point *p_point2 = new Point(3,5);
	p_point2->print_props();


	
	
}
