var created, date, city, country, cityname, cityname2, citytime, step;
var datetime = [];
var pressure = [];
var temperature = [];
var humidity = [];
var cloudcover = [];
var windspeed = [];
var windgust = [];
var winddir = [];
var precipitation = [];

var xhttp = new XMLHttpRequest();
xhttp.open("GET", "weather.xml", true);
xhttp.send();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200)
    {
        parseXML(this);
        input_table();
    }
};

function parseXML(xml) {
    var xmlDoc = xml.responseXML;
    var weather = xmlDoc.getElementsByTagName("weather");   //array
    created = weather[0].getAttribute("created");
    date = new Date(weather[0].getAttribute("date"));
    city = xmlDoc.getElementsByTagName("city")[0].getAttribute("id");
    country = xmlDoc.getElementsByTagName("country")[0].childNodes[0].nodeValue;
    cityname = xmlDoc.getElementsByTagName("cityname")[0].childNodes[0].nodeValue;
    cityname2 = xmlDoc.getElementsByTagName("cityname2")[0].childNodes[0].nodeValue;
    citytime = xmlDoc.getElementsByTagName("citytime")[0].childNodes[0].nodeValue;
    step = xmlDoc.getElementsByTagName("step");
    
    for (var i = 0; i < step.length; i++)
    {
        datetime[i] = xmlDoc.getElementsByTagName("datetime")[i].childNodes[0].nodeValue;
        pressure[i] = xmlDoc.getElementsByTagName("pressure")[i].childNodes[0].nodeValue;
        temperature[i] = xmlDoc.getElementsByTagName("temperature")[i].childNodes[0].nodeValue;
        humidity[i] = xmlDoc.getElementsByTagName("humidity")[i].childNodes[0].nodeValue;
        cloudcover[i] = xmlDoc.getElementsByTagName("cloudcover")[i].childNodes[0].nodeValue;
        windspeed[i] = xmlDoc.getElementsByTagName("windspeed")[i].childNodes[0].nodeValue;
        windgust[i] = xmlDoc.getElementsByTagName("windgust")[i].childNodes[0].nodeValue;
        winddir[i] = xmlDoc.getElementsByTagName("winddir")[i].childNodes[0].nodeValue;
        precipitation[i] = xmlDoc.getElementsByTagName("precipitation")[i].childNodes[0].nodeValue;
    }
}

function input_table() {
    var $table = $("<table/>")
        .css("borderCollapse", "collapse")
        .prependTo(document.body);
    var $tr = $("<tr/>").appendTo($table);
    $('<td/>').text(created).appendTo($tr);
    $('<td/>').text(date).appendTo($tr);
    $('<td/>').text(city).appendTo($tr);
    $('<td/>').text(country).appendTo($tr);
    $('<td/>').text(cityname).appendTo($tr);
    $('<td/>').text(cityname2).appendTo($tr);
    $('<td/>').text(citytime).appendTo($tr);

    $tr = $("<tr/>").css("border", "1px solid black")
        .css("textAlign", "center")
        .appendTo($table);
    $('<td/>').text("datetime").css("border", "1px solid black").appendTo($tr);
    $('<td/>').text("pressure").css("border", "1px solid black").appendTo($tr);
    $('<td/>').text("temperature").css("border", "1px solid black").appendTo($tr);
    $('<td/>').text("humidity").css("border", "1px solid black").appendTo($tr);
    $('<td/>').text("cloudcover").css("border", "1px solid black").appendTo($tr);
    $('<td/>').text("windspeed").css("border", "1px solid black").appendTo($tr);
    $('<td/>').text("windgust").css("border", "1px solid black").appendTo($tr);
    $('<td/>').text("winddir").css("border", "1px solid black").appendTo($tr);
    $('<td/>').text("precipitation").css("border", "1px solid black").appendTo($tr);

    for (var i = 0; i < step.length; i++) {
        $tr = $("<tr/>").css("border", "1px solid black")
            .css("textAlign", "center")
            .appendTo($table);
        $('<td/>').text(datetime[i]).css("border", "1px solid black").appendTo($tr);
        $('<td/>').text(pressure[i]).css("border", "1px solid black").appendTo($tr);
        $('<td/>').text(temperature[i]).css("border", "1px solid black").appendTo($tr);
        $('<td/>').text(humidity[i]).css("border", "1px solid black").appendTo($tr);
        $('<td/>').text(cloudcover[i]).css("border", "1px solid black").appendTo($tr);
        $('<td/>').text(windspeed[i]).css("border", "1px solid black").appendTo($tr);
        $('<td/>').text(windgust[i]).css("border", "1px solid black").appendTo($tr);
        $('<td/>').text(winddir[i]).css("border", "1px solid black").appendTo($tr);
        $('<td/>').text(precipitation[i]).css("border", "1px solid black").appendTo($tr);
    }
}
