#pragma once
#include<iostream>
using std::ostream;
using std::istream;
#include<fstream>
using std::ios;
using std::ofstream;
using std::ifstream;

class date
{
private:
	short day;
	short month;
	short year;
	
	// friend ostream & operator << (ostream& os, const string &astring); //����� ������ string - ����� � ����� ������ string
	friend ostream& operator << (ostream& os, date &aDate);
	friend istream& operator >> (istream& is, date &aDate);

public:
	
	date()	//default constructor
	{
		day = 0;
		month = 0;
		year = 0;
	}

	date(short aDay, short aMonth, short aYear)	//overloaded constructor
	{
		day = aDay;
		month = aMonth;
		year = aYear;
	}

	date(const date &BD)	//copy constructor
	{
		day = BD.day;
		month = BD.month;
		year = BD.year;
	}

	~date() {}		//destructor

	// ���������� ��������� ������������
	// ��������� ������ �� ��������� ������ date
	// ���������� ������� ��������� ������ date
	date& operator= (const date &aDate)	
	{
		day = aDate.day;
		month = aDate.month;
		year = aDate.year;
		return *this;
	}


	bool operator==(const date &d)
	{
		return day == d.day && month == d.month && year == d.year;
	}

	bool operator!=(const date &d)
	{
		return day != d.day && month != d.month && year != d.year;
	}

	void print_to_file(ofstream & F)
	{
		F.write(reinterpret_cast<char*>(&day), sizeof(short));
		F.write(reinterpret_cast<char*>(&month), sizeof(short));
		F.write(reinterpret_cast<char*>(&year), sizeof(short));
	}

	void read_from_file(ifstream & F)
	{
		F.read(reinterpret_cast<char*>(&day), sizeof(short));
		F.read(reinterpret_cast<char*>(&month), sizeof(short));
		F.read(reinterpret_cast<char*>(&year), sizeof(short));
	}
};

