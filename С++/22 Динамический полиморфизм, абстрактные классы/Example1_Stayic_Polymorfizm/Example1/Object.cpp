#include "Object.h"
#include <iostream>
using namespace std;

double Object::square()
{
	cout << "Object::Square()"<< " - " << this << endl;

	return 0.0;
}
void Object::setX(int aX)
{
	cout << __FUNCTION__ << endl;
	x = aX;
}
void Object::setY(int aY)
{
	cout << __FUNCTION__ << endl;
	y = aY;
}
int Object::getX()
{
	return x;
}
int Object::getY()
{
	return y;
}
/**/