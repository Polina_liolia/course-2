#include "Overwather_transport.h"

Overwather_transport::Overwather_transport() : Wather_transport()	//default constructor, inicializes prop as "basic" (default)
{
	prop = inside_energy;
}

//overloaded constructor
Overwather_transport::Overwather_transport(string aModel, double aWeight, double aMax_speed, double aDisplacement, prop_type aProp, speed aSpeed_units) :
	Wather_transport(aModel, aWeight, aMax_speed, aDisplacement, aSpeed_units)
{
	prop = aProp;
}

//method to set or change property prop
void Overwather_transport::set_prop(prop_type aProp)
{
	prop = aProp;
}

//method to get meaning of property prop
prop_type Overwather_transport::get_prop() const
{
	return prop;
}

//virtual method to get meaning of property speed_units
speed Overwather_transport::get_speed_units() const
{
	cout << "Speed units for overwather transport - ";
	switch (speed_units)
	{
	case 0: cout << "km_h\n"; break;
	case 1: cout << "Mach\n"; break;
	case 2: cout << "miles_h\n"; break;
	case 3: cout << "knots\n"; break;
	}
	return speed_units;
}

//virtual method to print all information about Overwather_transport instance
void Overwather_transport::print()
{
	Wather_transport::print();	//calling of basic class method
	cout << "Prop type: ";
	switch (prop)
	{
	case 0: cout << "outside energy\n"; break;
	case 1: cout << "inside energy\n"; break;
	}
}