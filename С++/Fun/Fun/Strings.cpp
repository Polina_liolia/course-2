#include "Header.h"

// ������� ������ ������
int stringlen(char *string, int sz)
{
	int count = 0;
	for (int i = 0; i < sz; i++)
	{
		if (string[i] != '\0')
			count++;
		else break;
	}
	return count;
}


// ���������� n �������� ������ ������������ ��������
bool stringinput(char *string, int symbol, int amount)
{
	for (int i = 0; i < amount; i++)
	{
		if (string[i] == '\0')
		{
			cout << "Amount of symbols is bigger than size of string; " << i << " elements were inputed\n";
			return 0;
		}
		string[i] = symbol;
	}
	return 1;
}

//Checking, does the symbol indicates the end of a word or string, or not
bool word_end(char *str, int j)
{
	bool flag = false;
	if (str[j] == ' ' || str[j] == ',' || str[j] == '.' || str[j] == '!' || str[j] == '?' || str[j] == ':' || str[j] == ';' || str[j] == '"' || str[j] == '(' || str[j] == ')' || str[j] == '\0')
		flag = true;
	return flag;
}

//Returning the lenght of a string
int stringlen(char *string, int sz)
{
	int count = 0;
	for (int i = 0; string[i] != '\0' && i < sz; i++)
		count++;
	return count;
}


//����� ��������� ����� � ������
int word_search(char *text, int sz_text, char *word, int sz_word)
{
	char divs[12] = " .,!?:;()-\"";//a string with all variants of dividers in a string
	char *token = NULL;//a pointer on a word, that was found in a text
	char *next_token = NULL;//a pointer on a text, that was not analysed yet  
	token = strtok_s(text, divs, &next_token);//finding the first word in a string
	int count = 0;//counter of words, that have been already found
	while (token != NULL)//while there are tokens in a text
	{
		int compare = _stricmp(token, word);//strings comparison
		if (compare == 0)//if strings are the same
		{
			count++;
			cout << "A position of word in a text is " << count << "\n";
			return count;
		}
		count++;
		token = strtok_s(NULL, divs, &next_token);
	}
	cout << "A word was not found in a text\n";
	return -1;
}

//����� ��������� ����� � ������ ��� ����� �������� (returns the number of word found or -1 if word was not found)
int word_search_no_reg(char *text, int sz_text, char *word, int sz_word)
{
	char divs[12] = " .,!?:;()-\"";//a string with all variants of dividers in a string
	char *token = NULL;//a pointer on a word, that was found in a text
	char *next_token = NULL;//a pointer on a text, that was not analysed yet  
	token = strtok_s(text, divs, &next_token);//finding the first word in a string
	int count = 0;//counter of words, that have been already found
	while (token != NULL)//while there are tokens in a text
	{
		int compare = _stricmp(token, word);//strings comparison
		if (compare == 0)//if strings are the same
		{
			count++;
			return count;
		}
		count++;
		token = strtok_s(NULL, divs, &next_token);
	}
	return -1;
}

//����� ��������� ����� � ������ � ������ �������� (returns the number of word found or -1 if word was not found)
int word_search_reg(char *text, int sz_text, char *word, int sz_word)
{
	char divs[12] = " .,!?:;()-\"";//a string with all variants of dividers in a string
	char *token = NULL;//a pointer on a word, that was found in a text
	char *next_token = NULL;//a pointer on a text, that was not analysed yet  
	token = strtok_s(text, divs, &next_token);//finding the first word in a string
	int count = 0;//counter of words, that have been already found
	while (token != NULL)//while there are tokens in a text
	{
		int compare = strcmp(token, word);//strings comparison
		if (compare == 0)//if strings are the same
		{
			count++;
			return count;
		}
		count++;
		token = strtok_s(NULL, divs, &next_token);
	}
	return -1;
}


/* ���������� ������ ������ �������.
� �������� ���������� ���������� ������ �������� � ������.
������ mystrcpy(str, str2) ��� mystrcpy(str, "hello")*/
char *string_to_string(char *str1, const int sz1, const char *str2, const int sz2)
{
	if (sz1<sz2)
	{
		cout << "Error! Second array is bigger than first\n";
		return 0;
	}

	for (int i = 0; i<sz2 - 1; i++)
		str1[i] = str2[i];
	return str1;
}

// ����������� ���� �����
char *sum_strings(char *str1, const int sz1, char *str2, const int sz2)
{
	int lenght1 = stringlen(str1, sz1);
	int lenght2 = stringlen(str2, sz2);
	if (sz1 <= lenght1 + lenght2)
	{
		cout << "Error! Not enought size of a first string to place a second string\n";
		return 0;
	}
	for (int i = lenght1, j = 0; str2[j] != '\0'; i++, j++)
		str1[i] = str2[j];
	return str1;
}

//��������, ������� ��������� ������� ������, ������� �������� � ������� ����� ������.
void check_str(char* string, const int sz, int occupied, int free)
{
	occupied = stringlen(string, sz);
	free = sz - occupied;
	cout << "Total size " << sz << "\n";
	cout << "Occupied " << occupied << "\n";
	cout << "Free " << free << "\n";
}

//�������� �� ����� � m �� n �������� ������ � �������� ������ ������� � ������ ������.
char *shortcut_str(char* str1, const int sz1, char* str2, const int sz2, int m, int n)
{
	if (m<0 || n>sz1)
	{
		cout << "Error!\n";
		return 0;
	}
	for (int i = m; i <= n; i++)
		cout << str1[i] << " ";
	cout << "\n";
	for (int i = 0, j = m; j <= n; i++, j++)
		str2[i] = str1[j];
	return str2;
}

//������� � m �� n ��������, ������������ ������ � �������� �� �� �����.
char *part_str_del(char *str, int sz, int m, int n)
{
	if (m<0 || n>sz)
	{
		cout << "Error!\n";
		return 0;
	}
	for (int i = 0; i <= (n - m); i++)
	{
		str[m + i] = str[n + 1 + i];
		//	str[n + 1 + i] = str[n + 1 + i + (n - m+1)];
	}
	for (int i = n + 1; str[i] != '\0'; i++)
		str[i] = str[i + (n - m + 1)];
	str[sz - (n - m)] = '\0';
	cout << "\n" << str << "\n";
	return str;
}


//�������, ������� � ������ ����� ����������� ������ ����� ���������.
char* capital_letter(char *str)
{
	for (int i = 0; str[i] != '\0'; i++)
	{
		if (i == 0)//if it is the beginning of a string
			str[i] -= 32;
		else if (str[i] == '.')//if the end of a sentence was found
			for (int j = i + 1; str[j] != '\0'; j++)//searching for the next, non-space, symbol
				if (str[j] != ' ')
				{
					str[j] -= 32;
					break;
				}
	}
	return str;
}

//Finding the string lenght
int stringlen(char *string)
{
	int count = 0;
	for (int i = 0; string[i] != '\0'; i++)
		count++;
	return count;
}


//Indicating, is the symbol a digit, or not
bool digit_check(char *str, int i)
{
	bool flag = false;
	if (str[i] == '0' || str[i] == '1' || str[i] == '2' || str[i] == '3' || str[i] == '4' || str[i] == '5' || str[i] == '6' || str[i] == '7' || str[i] == '8' || str[i] == '9')
		flag = true;
	return flag;
}

//Checking, does the symbol indicates the end of a word or string, or not
bool word_end(char *str, int j)
{
	bool flag = false;
	if (str[j] == ' ' || str[j] == ', ' || str[j] == '. ' || str[j] == '!' || str[j] == '?' || str[j] == ': ' || str[j] == ';' || str[j] == '"' || str[j] == '(' || str[j] == ')' || str[j] == '\0')
		flag = true;
	return flag;
}

//Finding the longest word in a string and returning a pointer on string with this word
char* longest_word_str(char *str, int &longest_word_sz)
{
	int	max_symbols = 0,//maximal amount of symbols in one word
		max_word_index = 0,//an index of the longest word's first letter
		j = 0;
	//searching for the longest word in a string
	for (int i = 0; str[i] != '\0'; i++)
	{
		int symbols_count = 0;
		if (str[i] == ' ' && str[i + 1] != ' ')//if a space between two words inside a string was found
			j = i + 1;
		else if (i == 0 && str[i] != ' ')//if it is the beginning of a string
			j = i;
		else//if it is a symbol inside a word
			continue;
		//counting symbols of each word (except punctuation marks)
		for (j; str[j] != ' ' && str[j] != ',' && str[j] != '.' && str[j] != '!' && str[j] != '?' && str[j] != ':' && str[j] != ';' && str[j] != '"' && str[j] != '(' && str[j] != ')' && str[j] != '\0'; j++)
			symbols_count++;
		if (symbols_count > max_symbols)
		{
			max_symbols = symbols_count;
			max_word_index = j - max_symbols;
		}
	}
	char *p_max_word = new char[max_symbols];
	for (int i = max_word_index, k = 0; i < max_word_index + max_symbols; i++, k++)
		*(p_max_word + k) = str[i];
	*(p_max_word + max_symbols) = '\0';
	longest_word_sz = max_symbols;
	return p_max_word;
}

//Finding the longest word in a string and returning an index of its beginning
int longest_word_index(char *str)
{
	int	max_symbols = 0,//maximal amount of symbols in one word
		max_word_index = 0,//an index of the longest word's first letter
		j = 0;
	//searching for the longest word in a string
	for (int i = 0; str[i] != '\0'; i++)
	{
		int symbols_count = 0;
		if (str[i] == ' ' && str[i + 1] != ' ')//if a space between two words inside a string was found
			j = i + 1;
		else if (i == 0 && str[i] != ' ')//if it is the beginning of a string
			j = i;
		else//if it is a symbol inside a word
			continue;
		//counting symbols of each word (except punctuation marks)
		for (j; str[j] != ' ' && str[j] != ', ' && str[j] != '. ' && str[j] != '! ' && str[j] != '?' && str[j] != ': ' && str[j] != ';' && str[j] != '"' && str[j] != '(' && str[j] != ')' && str[j] != '\0'; j++)
			symbols_count++;
		if (symbols_count > max_symbols)
		{
			max_symbols = symbols_count;
			max_word_index = j - max_symbols;
		}
	}
	return max_word_index;
}

//Finding the shortest word in a string and returning a pointer on string with this word
char* shortest_word_str(char *str, int &shortest_word_sz)
{
	int	min_symbols = 0,//minimal amount of symbols in one word
		min_word_index = 0,//an index of the shortest word's first letter
		j = 0;
	//searching for the shortest word in a string
	for (int i = 0; str[i] != '\0'; i++)
	{
		int symbols_count = 0;
		if (str[i] == ' ' && str[i + 1] != ' ')//if a space between two words inside a string was found
			j = i + 1;
		else if (i == 0 && str[i] != ' ')//if it is the beginning of a string
			j = i;
		else//if it is a symbol inside a word
			continue;
		//counting symbols of each word (except punctuation marks)
		for (j; str[j] != ' ' && str[j] != ', ' && str[j] != '. ' && str[j] != '!' && str[j] != '?' && str[j] != ': ' && str[j] != ';' && str[j] != '"' && str[j] != '(' && str[j] != ')' && str[j] != '\0'; j++)
			symbols_count++;
		if (symbols_count < min_symbols || i == 0)
		{
			min_symbols = symbols_count;
			min_word_index = j - min_symbols;
		}
	}
	char *p_max_word = new char[min_symbols];
	for (int i = min_word_index, k = 0; i < min_word_index + min_symbols; i++, k++)
		*(p_max_word + k) = str[i];
	*(p_max_word + min_symbols) = '\0';
	shortest_word_sz = min_symbols;
	return p_max_word;
}

//����� ���� ������� ����, ������������ � ����� ���������� ��� ���������� �����, � ����� ���������� ������ ���� �����.
void lng_shrt_word_vowels(char *str, char* (*p)(char*, int &))
{
	int sz = 0;//the lenght of a string with a word
	int &r_sz = sz;
	p(str, r_sz);//to find the lenght of a string with a word
	char *p_word = new char[sz];//a dinamic array to save a word
	p_word = p(str, r_sz);

	char *p_vowels = new char[sz];//a dinamic array to save all vowel letters of the word
	bool *flag = new bool[sz];//a boolean dinamic array to mark the same letters
	*flag = false;
	int *same = new int[sz];//a dinamic array to save an amount of each vowel letter in a word
	int count_vowels = 0,//a number of vowel letters in a word 
		count_same = 1;//an amount of each vowel letter in a word
	const int sz_vowel = 33;
	char vowel_letters[sz_vowel] = "AEIOUYaeiouy�Ũ����������������";//a string with all vowel letters
																	  //checking every letter of a word, is it vowel or not
	for (int i = 0; i < r_sz; i++, count_same = 1)
		for (int k = 0; k < sz_vowel; k++)
			if (*(p_word + i) == vowel_letters[k] && *(flag + i) != true)//if the letter is vowel
			{
				*(p_vowels + count_vowels) = *(p_word + i);
				//searching for the same letters in this word, counting and marking them:
				for (int l = i + 1; l < sz; l++)
					if (*(p_word + l) == *(p_word + i) && *(flag + l) != true)
					{
						count_same++;
						*(flag + l) = true;
					}
				*(same + count_vowels) = count_same;
				count_vowels++;
				break;
			}
	//Outputting the rezults to console:
	if (p == longest_word_str)
		cout << "\nThe longest word is: \"";
	else
		cout << "\nThe shortest word is: \"";
	for (int i = 0; i < sz; i++)
		cout << *(p_word + i);
	cout << "\"\nThe vowel leters and their amount in this word:\n";
	for (int i = 0; i < count_vowels; i++)
		cout << *(p_vowels + i) << " - " << *(same + i) << "\n";
	delete[] p_vowels, flag, same, p_word;
}


//������ ���� ���� �� "*"
void change_digits(char *str)
{
	int i = longest_word_index(str);
	for (i; str[i] != ' ' && str[i] != '. ' && str[i] != ', ' && str[i] != '!' && str[i] != '?' && str[i] != ':' && str[i] != ';' && str[i] != '"' && str[i] != '(' && str[i] != ')' && str[i] != '\0'; i++)
		if (digit_check(str, i))//if it is a digit
			str[i] = '*';
}

//������ ���� ������ ������� (a) �� ������ (b)
void change_char(char *str, char a, char b)
{
	for (int i=0; str[i] != ' ' && str[i] != '. ' && str[i] != ', ' && str[i] != '!' && str[i] != '?' && str[i] != ':' && str[i] != ';' && str[i] != '"' && str[i] != '(' && str[i] != ')' && str[i] != '\0'; i++)
		if (str[i] == a)	
			str[i] = b;
}


//To change a word on ***
void hide_word(char *arr, int sz, int word_number)
{
	int count = 0;
	for (int i = 0; i < sz; i++)
	{
		if (word_end(arr, i) == true)
		{
			count++;
			if (count == word_number - 1)
			{
				while (word_end(arr, i) == true)
					i++;
				while (word_end(arr, i) != true)
				{
					arr[i] = '*';
					i++;
				}
				break;
			}
		}
	}
}

// ����� � ������� ��� ������� �����(��� ����������), ������� ����������� � ������ � ���������� ����.
int all_vowels(char *str)
{
	int sz = stringlen(str);// Finding the string lenght (size of a dinamic array to save all vowel letters of the string)
							//counting the number of words in a string
	int j = 0,//cycle counter of words in a string
		words_counter = 0;
	while (j <= sz)
	{
		if (word_end(str, j))
			words_counter++;
		j++;
	}

	char *p_vowels = new char[sz];//a dinamic array to save all vowel letters of the string
	bool *flag = new bool[sz];//a boolean dinamic array to mark the same letters
	*flag = false;
	int *same = new int[sz];//a dinamic array to save an amount of each vowel letter in a string
	int count_vowels = 0,//a number of vowel letters in a string 
		count_same = 1;//an amount of each vowel letter in a string
	const int sz_vowel = 33;
	char vowel_letters[sz_vowel] = "AEIOUYaeiouy�Ũ����������������";//a string with all vowel letters
																	  //checking every letter of a string, is it vowel or not
	for (int i = 0; i < sz; i++, count_same = 1)
		for (int k = 0; k < sz_vowel; k++)
			if (*(str + i) == vowel_letters[k] && *(flag + i) != true)//if the letter is vowel
			{
				*(p_vowels + count_vowels) = *(str + i);
				//searching for the same letters in this string, counting and marking them:
				for (int l = i + 1; l < sz; l++)
					if (*(str + l) == *(str + i) && *(flag + l) != true)
					{
						count_same++;
						*(flag + l) = true;
					}
				*(same + count_vowels) = count_same;
				count_vowels++;
				break;
			}
	//Outputting the rezults to console:
	cout << "There are " << words_counter << " words in a string.\n";
	cout << "The vowel leters and their amount in this string:\n";
	for (int i = 0; i < count_vowels; i++)
		cout << *(p_vowels + i) << " - " << *(same + i) << "\n";
	delete[] p_vowels, flag, same;
	return words_counter;
}

//������� ��������� �� �����, ���������� ���������� ���������� ����
char *max_digits_word(char* str, int &sz)
{
	int	max_digits = 0,//maximal amount of digits in one word
		max_digits_word_index = 0,//an index of a word with maximal amount of digits 
		word_sz = 0;//size of a word with maximal amount of digits
	for (int i = 0; str[i - 1] != '\0'; i++)//searching for a word with maximal amount of digits
	{
		int digits_count = 0,
			symbols_count = 0;
		//searching for any digit:
		if (digit_check(str, i))//if a digit was found
		{
			int j = 0;
			if (i != 0)//if it is not the first word in a string
				j = i - 1;
			//searching for the beginning of a word with a digit:
			while (word_end(str, j) == false)//while it is not the en of a word or string
			{
				if (j != 0)//if it is not the first word in a string
					j--;
				else break;//if it is the first word in a string
			}
			if (j != 0)//if it is not the first word in a string
				j += 1;
			//counting symbols and digits in a current word (from its beginning):
			for (j; str[j] != ' ' && str[j] != '. ' && str[j] != ', ' && str[j] != '!' && str[j] != '?' && str[j] != ':' && str[j] != ';' && str[j] != '"' && str[j] != '(' && str[j] != ')' && str[j] != '\0'; j++)
			{
				symbols_count++;
				if (digit_check(str, j))//if a digit was found
					digits_count++;
			}
			if (digits_count > max_digits)//if number of digits in a word is maximal
			{
				max_digits = digits_count;
				max_digits_word_index = j - symbols_count;
				word_sz = symbols_count;
				i = max_digits_word_index + symbols_count;
			}
		}
	}
	//creating a dinamic array with a word with maximal amount of digits
	sz = word_sz + 1;//saving a size of a new array
	char *p_max_digits_word = new char[sz];
	//copying a word with maximal amount of digits to this string:
	for (int i = max_digits_word_index, j = 0; j < sz; i++, j++)
		*(p_max_digits_word + j) = str[i];
	*(p_max_digits_word + sz - 1) = '\0';//marking the end of a string

	return p_max_digits_word;
}

//����� � ������� ��� �����, ���������� ���������� ���������� ������� ����.
void max_vowels_words(char* str)
{
	const int sz_vowel = 33;
	char vowel_letters[sz_vowel] = "AEIOUYaeiouy�Ũ����������������";//a string with all vowel letters
	int	max_vowels = 0;//maximal amount of vowels in one word
	for (int i = 0; str[i - 1] != '\0'; i++)//finding maximal amount of vowels in one word
	{
		int vowels_count = 0;
		for (int j = i; str[j] != ' ' && str[j] != '. ' && str[j] != ', ' && str[j] != '!' && str[j] != '?' && str[j] != ':' && str[j] != ';' && str[j] != '"' && str[j] != '(' && str[j] != ')' && str[j] != '\0'; j++)
		{
			for (int k = 0; k < sz_vowel; k++)//searching and counting vowel letters
				if (*(str + j) == vowel_letters[k])//if the letter is vowel
					vowels_count++;
			i = j;//turning to the index of a next word's beginning
		}

		if (vowels_count > max_vowels)//if number of vowels in a word is maximal
			max_vowels = vowels_count;
	}
	//finding and outputting to console all words with maximal amount of vowels:
	cout << "Word(s) with with maximal amount of vowel letters:\n";
	for (int i = 0; str[i - 1] != '\0'; i++)
	{
		int vowels_count = 0,
			j = i;
		for (j; str[j] != ' ' && str[j] != '. ' && str[j] != ', ' && str[j] != '!' && str[j] != '?' && str[j] != ':' && str[j] != ';' && str[j] != '"' && str[j] != '(' && str[j] != ')' && str[j] != '\0'; j++)
			for (int k = 0; k < sz_vowel; k++)//searching and counting vowel letters
				if (*(str + j) == vowel_letters[k])//if the letter is vowel
					vowels_count++;
		if (vowels_count == max_vowels)//if number of vowels in a word is maximal
		{
			for (int l = i; str[l] != ' ' && str[l] != '. ' && str[l] != ', ' && str[l] != '!' && str[l] != '?' && str[l] != ':' && str[l] != ';' && str[l] != '"' && str[l] != '(' && str[l] != ')' && str[l] != '\0'; l++)
				cout << str[l];
			cout << "\n";
		}
		i = j;//turning to the index of a next word's beginning
	}

}

//��������, �������� �� ������ �����������
bool palindrome(char* str)
{
	bool flag = true;//boolean variable to indicate if a string is not a palindrome
	int sz = stringlen(str);//finding the lenght of a string
	for (int i = 0, j = sz - 1; j > i; i++, j--)//comparing symbols from beginning and from the end of a string (except spaces, comas ect.)
	{
		//to miss spaces, comas ect. in the beginning half of a string:
		if (word_end(str, i))
			do
			{
				i++;
			} while (word_end(str, i));
			//to miss spaces, comas ect. in the second half of a string:
			if (word_end(str, j))
				do
				{
					j--;
				} while (word_end(str, j));
				if (str[i] != str[j] && str[i] + 32 != str[j] && str[i] != str[j] + 32)//if a string is not a palindrome
				{
					flag = false;
					cout << "\nA string \"" << str << "\" is not a palindrome.\n";
					return 0;
				}
	}
	cout << "\nA string \"" << str << "\" is a palindrome.\n";
	return 1;
}

//�������� ������������ ����������� ������ � �������������� ���������.
bool brackets_check(char *str)
{
	int flag = 0;
	for (int i = 0; str[i] != '\0'; i++)
	{
		if (str[i] == '(')
			flag++;
		if (str[i] == ')')
			flag--;
		if (flag < 0)
			return 0;
	}
	if (flag == 0)
		return 1;
	return 0;
}


//������� ������� ����� � ���������� ������� ���������.
int roman_to_dec(char *roman, int sz)
{
	int digits = stringlen(roman, sz);//number of digits in roman number
	int dec_number = 0;//a number, converted to decimal number system
	int* roman_convert = new int[digits];//a dinamic array to save meaning of every roman digits
	for (int i = 0; i<digits; i++)
		switch (roman[i])//converting every roman digit to decimal and saving results in array
		{
		case 'I': case 'i': roman_convert[i] = 1; break;
		case 'V': case 'v': roman_convert[i] = 5; break;
		case 'X': case 'x': roman_convert[i] = 10; break;
		case 'L': case 'l': roman_convert[i] = 50; break;
		case 'C': case 'c': roman_convert[i] = 100; break;
		case 'D': case 'd': roman_convert[i] = 500; break;
		case 'M': case 'm': roman_convert[i] = 1000; break;
		default: cout << "Error: unknown symbol\n"; return 0;
		}
	for (int i = digits - 1; i >= 0; i--)//calculaiting a decimal number
	{
		if (i == digits - 1)//the last digit of a roman number is always positive 
			dec_number += roman_convert[i];
		else if (roman_convert[i]<roman_convert[i + 1])//if the next digit is more than current, current digit is negative 
			dec_number -= roman_convert[i];
		else ////if the next digit is less than current (except the last digit of a number), current digit is positive
			dec_number += roman_convert[i];
	}
	delete[] roman_convert;
	return dec_number;
}

/*����������, ����� �� ��������� ������� �� ���� ������ ������, � ������� ������ ����������� ����� ����������
� ��� �� �����, �� ������� ������������ ����������*/
//additional function
bool chain(char* str, int sz, char first_letter)
{
	bool found = false;//to indicate, if the word to build a chain was found
	bool chain_rezult = false;//a result, returned by a function "chain" (true-if a chain was built)
	int word_index = 0,//index of the first letter of the word found
		symbols = 0;//counter of symbols of the word found
	for (int i = 0; i<sz && str[i - 1] != '\0'; i++)//searching for the word to build a chain
		if (word_end(str, i + 1) && word_end(str, i) == false && str[i] == first_letter)//checking all last letters
		{
			found = true;//a match is found
			for (int j = i; word_end(str, j) == false; j--)//searching for the beginning of a word
			{
				symbols++;
				if (j == 0 || word_end(str, j - 1))//if the beginning of a word was found
				{
					first_letter = str[j];//saving a new firs letter to build a chain
					word_index = j;
					break;
				}
			}
			//creating a new string, without the word found
			int sz_new = sz - symbols - 1;
			char *string = new char[sz_new];
			string[sz_new - 1] = '\0';//marking the end of a string
									  //creating a new string to save the word found
			char *current_word = new char[symbols + 1];
			current_word[symbols] = '\0';//marking the end of a string
										 //copying data to the new strings
			for (int i = 0, j = 0, k = 0; str[j] != '\0' && j < sz; j++)
			{
				if (j >= word_index && j < word_index + symbols && word_end(str, j) == false)//if a symbol of the current word was found
				{
					current_word[k] = str[j];//copying the current word to a new string
					k++;
					continue;
				}
				if (i == 0 && word_end(str, j))
					continue;//to avoid copying a space before first word
				if (string[i + 1] == '\0' && word_end(str, j))
					string[i] = '\0';//to avoid copying a space after the last word
				if (word_end(string, i - 1) && word_end(str, j))
					continue;//to avoid copying double spaces
				string[i] = str[j];//copying all other word left to a new string
				i++;
			}
			if (sz_new == 0)//if there are no words left and a chain has been succesfully built
			{
				cout << " ";
				for (int i = 0; word_end(current_word, i) == false; i++)
					cout << current_word[i];
				cout << " ";
				delete[] current_word;
				return 1;
			}
			chain_rezult = chain(string, sz_new, first_letter);
			if (chain_rezult)//if the chain was built, outputting current word to console
			{
				cout << " ";
				for (int i = 0; word_end(current_word, i) == false; i++)
					cout << current_word[i];
				cout << " ";
				delete[] current_word;
				return 1;
			}
			else // if the chain was not built, continue searching in a current string
			{
				found = false;
				symbols = 0;
				continue;
			}
		}
	if (found == false)//if the word with needed parameters was not found
		return 0;

}

//function to call from main
bool words_chain(char* words, int sz)
{
	bool chain_rezult = false;//a result, returned by a function "chain" (true-if a chain was built)
							  //counting words of a basic string
	int count = 0;
	sz = stringlen(words, sz) + 1;
	for (int i = 0; i < sz && words[i - 1] != '\0'; i++)
		if (word_end(words, i))
			count++;

	int word_number = 1;//a number of the word in a string, that will be taken as the last in a chain
	while (word_number <= count)
	{
		//counting symbols of a current last word in a chain, getting its first letter
		int symbols = 0;
		char first_letter = 0;
		for (int i = 0, j = 1; i < sz && words[i - 1] != '\0'; i++)
		{
			if (word_end(words, i))
				j++;//counting words
			if (word_number == 1)
			{
				first_letter = words[0];//saving a first letter of a current last word in a chain
				i = 0;
				while (word_end(words, i) == false)//counting symbols of a current last word
				{
					symbols++;
					i++;
				}
				break;
			}
			else
				if (j == word_number)//if the beginning of a current last word was found 
				{
					i++;//moving to the first letter of a current last word 
					first_letter = words[i];//saving a first letter of a current last word in a chain
					while (word_end(words, i) == false)//counting symbols of a current last word
					{
						symbols++;
						i++;
					}
					break;
				}
		}
		//creating a new string, without current last word
		int sz_new = sz - symbols - 1;
		char *string = new char[sz_new];
		string[sz_new - 1] = '\0';//marking the end of a string
								  //creating a new string to save current word
		char *current_word = new char[symbols + 1];
		current_word[symbols] = '\0';//marking the end of a string
									 //copying data to the new strings
		for (int i = 0, j = 0, k = 0, words_count = 0; i < sz_new && j < sz; j++)
		{
			if (word_end(words, j))
				words_count++;//counting words
			if (words_count == word_number - 1)
			{
				current_word[k] = words[j];//copying a current last word
				k++;
				continue;
			}
			if (i == 0 && word_end(words, j))
				continue;//to avoid copying a space 
			string[i] = words[j];//copying all other words 
			i++;
		}
		//trying to build a chain, using current last word
		chain_rezult = chain(string, sz_new, first_letter);
		if (chain_rezult)//if the chain was built
		{
			for (int i = 0; i <= symbols; i++)//outputting the last word to console
				cout << current_word[i];
			delete[] current_word, string;
			return 1;
		}
		//if the chain was not built
		delete[] current_word, string;
		word_number++;//turning to the next word
		if (word_number > count)//if all words have been already used and chain was not built
		{
			cout << "It is impossible to build a chain\n";
			return 0;
		}
	}

}