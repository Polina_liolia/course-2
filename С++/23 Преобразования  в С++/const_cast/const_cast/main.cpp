#include <iostream>

using std::cout;
using std::endl;

class ConstCastTest {
public:
	ConstCastTest();
	void setNumber( int ) ;
	int getNumber() const;
	void printNumber () const;
private :
	int number;
} ;

ConstCastTest::ConstCastTest(){
	number = 8;
}
void ConstCastTest:: setNumber ( int num ) { 
	number = num; 
}

int ConstCastTest::getNumber() const { 
	return number; 
}

void ConstCastTest::printNumber() const
{
	cout << "\nThe number after modification: ";

	//number--;
	const_cast< ConstCastTest * >( this )->number-- ;
	                          // const ConstCastTest *
	                          // ConstCastTest *
							  // (ConstCastTest *)->number--
	cout << number << endl;
	//number--;

}


int main ()
{
	const ConstCastTest x;
	//x.setNumber ( 8 ) ; // ������ private-������

	cout << "Start value: " << x.getNumber();
	x.printNumber();

	//=======================
	ConstCastTest t = x;
	return 0;
}