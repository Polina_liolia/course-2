#pragma once
#include <iostream>
using std::cout;

class Counter
{
private:
	const int min;
	const int max;
	int count;
public:
	Counter(int aMin = 0, int aMax = 0, int aCount = 0);
	~Counter();
	Counter operator++();
	Counter operator++(int);
	Counter operator--();
	Counter operator--(int);
	void print();
};