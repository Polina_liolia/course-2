#include"Personal_data.h"
#include<algorithm>
#include <functional> 
using std::unique;
using std::cout;

void main()
{
	vector<Personal_data> all_persons;
	Personal_data a("Surkov", "Vasil");
	Personal_data b("Alekseev", "Kirill");
	Personal_data c("Surkov", "Andrey");
	Personal_data d("Alekseev", "Petr");
	Personal_data e("Alekseev", "Petr");
	Personal_data f("Alekseev", "Kirill");
	Personal_data g("Alekseev", "Petr");
	Personal_data h("Alekseev", "Anton");
	Personal_data i("Alekseev", "Kirill");

	all_persons.push_back(a);
	all_persons.push_back(b);
	all_persons.push_back(c);
	all_persons.push_back(d);
	all_persons.push_back(e);
	all_persons.push_back(f);
	all_persons.push_back(g);
	all_persons.push_back(h);
	all_persons.push_back(i);

	vector <Personal_data>::iterator iter_unique_persons;

	iter_unique_persons = unique(all_persons.begin(), all_persons.end());
	cout << "Unique:\n";
	for (auto i : all_persons)
		std::cout << i.get_surname() << " " << i.get_name() << "\n";

	sort(all_persons.begin(), all_persons.end());
	cout << "Sort:\n";
	for (auto i : all_persons)
		std::cout << i.get_surname() << " " << i.get_name() << "\n";
}
