#include "Word.h"

/*
String en;		//word in english
String rus;		//word in russian
int counter;
*/

Word::Word()	//default constructor
{
	counter = 0;
}

/*overloaded constructor
takes: 
String aEn - word in english
String aRus - word in russian
int aCounter - counter of word's calls
*/
Word::Word(String aEn, String aRus, int aCounter)	
{
	en = aEn;
	rus = aRus;
	counter = aCounter;
}

Word::Word(const Word & aWord)	//copy constructor
{
	en = aWord.en;
	rus = aWord.rus;
	counter = aWord.counter;
}

Word::~Word()	//destructor
{
}

void Word::set_word(String aEn, String aRus, int aCounter)	//to set english word, russion translation and calls' counter
{
	en = aEn;
	rus = aRus;
	counter = aCounter;
}

void Word::set_rus(String aRus)	//to add a russian translation (takes translation in String)
{
	rus = aRus;
}


int Word::get_counter() const	//returns meaning of word's calls counter
{
	return counter;
}


void Word::add_count()	//to add one more calling of a word
{
	counter++;
}

String Word::get_en() const	//to get word in english (instance of class String)
{
	return en;
}

String Word::get_translation()	//to get word in russion (instance of class String)
{
	return rus;
}

// operators, overloaded for class Word:

Word Word::operator=(const Word & tmp)	
{
	en = tmp.en;
	rus = tmp.rus;
	counter = tmp.counter;
	return *this;
}

bool Word::operator==(const Word & tmp)
{
	return counter == tmp.counter;
}

bool Word::operator!=(const Word & tmp)
{
	return counter != tmp.counter;
}

bool Word::operator>(const Word & tmp)
{
	return counter > tmp.counter;
}

bool Word::operator>=(const Word & tmp)
{
	return counter >= tmp.counter;
}

bool Word::operator<(const Word & tmp)
{
	return counter < tmp.counter;
}

bool Word::operator<=(const Word & tmp)
{
	return counter <= tmp.counter;
}
