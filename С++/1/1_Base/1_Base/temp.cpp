class A{
public:
	int prop1;
	int prop2;
public:

	A();

	~A(){
		cout << "Destructor" << endl;
	}

	void set_props();

	void show(){
		cout << "prop1:" << prop1 << endl;
		cout << "prop2:" << prop2 << endl;
	}

};

A::A(){
		prop1 = 0;
		prop2 = 0;
}

void A::set_props(){
		prop1 = 10;
		prop2 = 20;
}
