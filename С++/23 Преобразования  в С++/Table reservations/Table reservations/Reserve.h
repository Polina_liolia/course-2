#pragma once
#include<iostream>
#include"Time.h"
#include <string>
using std::string;

class Reserve
{
private:
	Time reserve_start;
	int reserve_duration_min;
	string reservator_name;
	string reservator_phone;

public:
	Reserve();
	Reserve(string aName, string aPhone, int aDuration = 20);
	~Reserve() {};
	
public:
	void set_reserve(string aName, string aPhone, int aDuration = 20);
	void set_reserve_duration(int aReserve_duration_min);
	void set_reservator_name(string aName);
	void set_reservator_phone(string aPhone);

	int get_reserve_duration()const;
	string get_reservator_name()const;
	string get_reservator_phone()const;

	void remove_reserve();
	bool check_reserve();
	void print_to_file(ofstream &F)const;
	void read_from_file(ifstream &F);

	friend ostream& operator<<(ostream& os, Reserve& r);
};