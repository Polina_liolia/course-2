#include <iostream>
using std::cout;
using std::endl;

#include "functor.h"

void main()
{
	functor f;

	f(5,6);

	f.show();
}