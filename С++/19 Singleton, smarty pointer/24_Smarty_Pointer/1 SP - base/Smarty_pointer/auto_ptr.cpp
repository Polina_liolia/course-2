#include <memory>
#include <iostream>
using namespace std;
class A {
public:
	void some_method() {
		cout << "call some_method()..." << endl;
	}
	~A() {
		cout << "Destructor A!!!" << endl;
	}
};

A* test_function1() {
	return new A;
	
}


auto_ptr<A> test_function() {
	return auto_ptr<A>(new A);
}


void main(){
	// auto_ptr - int
	int * px = new int;
	*px = 9;
	auto_ptr<int> ptr(px);
	auto_ptr<int> ptr2 = ptr;
	cout << *(ptr2.get()) << endl;

	// auto_ptr - A
	auto_ptr<A> ptrA(new A);   /*A * x = new A; (x)*/
	auto_ptr<A> ptr2A = ptrA;
	//ptrA->some_method();
	ptr2A->some_method();


	auto_ptr<A> ptr3A = test_function();
	ptr3A->some_method();


}