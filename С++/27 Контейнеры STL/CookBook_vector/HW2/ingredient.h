#pragma once

class ingredient
{
public:
	char *item;
	float dose;

public:
	ingredient(char *aItem = nullptr, float aDose = 0);
	ingredient(const ingredient &temp);
	~ingredient();

	ingredient operator =(const ingredient &aI);
};