#pragma once
#include <iostream>

class A
{
private:
	char *name;
	int digit;
public:
	A();
	A(char* aName = nullptr, int aDigit = 0);
	~A();
};