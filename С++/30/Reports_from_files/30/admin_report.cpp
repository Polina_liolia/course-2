#include "admin_report.h"

void admin_report::read_data(char * data_file)
{
	ifstream data(data_file);
	// checking if file was successfully opened, othervice generating exception:
	if (!data)
		throw "data_file: Can't open file for reading";
	char ch;
	string parameter;
	string value;
	bool flag = false;
	//reading parameter identificator
	data.get(ch);
	while (true)
	{
		if (ch != '[')
		{
			if (!data.get(ch))
				break;
		}
		if (ch == '[')
			getline(data, parameter, ']');
		if (!data.get(ch))
			break;
		if (ch == '(')
			getline(data, value, ')');
		if (parameter == "name")
			customer_name = value;
		else if (parameter == "inqID")
			inquiry_number = value;
		else if (parameter == "item")
			item = value;
		else if (parameter == "amount")
			amount = value;
		else if (parameter == "options")
			additional_options = value;
		else if (parameter == "comments")
			comments = value;
		else if (parameter == "delivery")
			delivery_date = value;
		else if (parameter == "status")
			inquiry_status = value;
		else if (parameter == "status_changed")
			status_changed.push_back(value);
		else if (parameter == "manager_name")
			manager_name.push_back(value);
		parameter.erase();
		value.erase();
	}
	data.close();
}

void admin_report::report(char * data_file, char * draft_file, char * report_file)
{
	read_data(data_file);
	ifstream draft(draft_file);
	// checking if file was successfully opened, othervice generating exception:
	if (!draft)
		throw "draft_file: Can't open file for reading";

	ofstream report(report_file);
	// checking if file was successfully opened, othervice generating exception:
	if (!report)
		throw "report_file: Can't open file for writing";

	char ch = ' '; //to save readen chars
	string text;	//to save draft text
	string parameter;//to save readen parameter name
	int status_counter = 0,
		mgr_counter = 0;
	bool flag = false; //to indicate if parameter is mandatory

					   //reading draft file
	while (true)
	{
		flag = false;
		//reading draft text
		getline(draft, text, '[');
		if (text.size() != 0)	//if text was readen
		{
			ch = text[text.size() - 1];	//saving symbol of mandatory
			if (ch != '*' && ch != '%') //end of file
			{
				report << text << " ";
				break;
			}
			text.pop_back();	//deleting symbol * or %
			report << text << " ";	//writing draft text in report file
		}
		else
		{
			if (!draft.get(ch))
				break;
			else
				draft.seekg(-1, ios::cur);
		}

		if (ch == '*')
			flag = true;
		//reading parameter
		getline(draft, parameter, ']');
		//writing parameter value in report file:
		if (parameter == "name")
		{
			if (flag && customer_name.size() == 0)	//if mandatory parameter value was not readen from data file
				throw "Error: mandatory parameter is absent. Report was not generated";
			else
				report << customer_name << " ";	//writing to file
		}
		if (parameter == "inqID")
		{
			if (flag && inquiry_number.size() == 0)	//if mandatory parameter value was not readen from data file
				throw "Error: mandatory parameter is absent. Report was not generated";
			else
				report << inquiry_number << " ";	//writing to file
		}
		if (parameter == "item")
		{
			if (flag && item.size() == 0)	//if mandatory parameter value was not readen from data file
				throw "Error: mandatory parameter is absent. Report was not generated";
			else
				report << item << " ";	//writing to file
		}
		if (parameter == "amount")
		{
			if (flag && amount.size() == 0)	//if mandatory parameter value was not readen from data file
				throw "Error: mandatory parameter is absent. Report was not generated";
			else
				report << amount << " ";	//writing to file
		}
		if (parameter == "options")
		{
			if (flag && additional_options.size() == 0)	//if mandatory parameter value was not readen from data file
				throw "Error: mandatory parameter is absent. Report was not generated";
			else
				report << additional_options << " ";	//writing to file
		}
		if (parameter == "comments")
		{
			if (flag && comments.size() == 0)	//if mandatory parameter value was not readen from data file	
				throw "Error: mandatory parameter is absent. Report was not generated";
			else
				report << comments << " ";	//writing to file
		}
		if (parameter == "delivery")
		{
			if (flag && delivery_date.size() == 0) //if mandatory parameter value was not readen from data file
				throw "Error: mandatory parameter is absent. Report was not generated";
			else
				report << delivery_date << " ";	//writing to file
		}
		if (parameter == "status")
		{
			if (flag && inquiry_status.size() == 0) //if mandatory parameter value was not readen from data file
				throw "Error: mandatory parameter is absent. Report was not generated";
			else
				report << inquiry_status << " ";	//writing to file
		}
		if (parameter == "status_changed")
		{
			if (flag && status_changed.size() <= status_counter) //if mandatory parameter value was not readen from data file
				throw "Error: mandatory parameter is absent. Report was not generated";
			else
				report << status_changed[status_counter++] << " ";	//writing to file
		}
		if (parameter == "manager_name")
		{
			if (flag && manager_name.size() <= mgr_counter) //if mandatory parameter value was not readen from data file
				throw "Error: mandatory parameter is absent. Report was not generated";
			else
				report << manager_name[mgr_counter++] << " ";	//writing to file
		}

	}
	draft.close();
	report.close();
}
