#include "Vocabulary.h"
#include "Element.h"
#include <iostream>
using namespace std;
Vocabulary::Vocabulary(){
	root = NULL;
	print_count = 0;
}
void Vocabulary::add(String En, String Rus, int Counter)
{
	if (!root)
		root = new Element(En, Rus, Counter);
	else
	{
		Element *tmp = root;
		while (tmp)
		{
			if (Counter < tmp->get_value().get_counter())	//comparing wods' calls counters
				if (tmp->get_left() == nullptr)
				{
					tmp->set_left(new Element(En, Rus, Counter));
					break;
				}
				else tmp = tmp->get_left();
			else {
				if (Counter > tmp->get_value().get_counter())
					if (tmp->get_right() == nullptr)
					{
						tmp->set_right(new Element(En, Rus, Counter));
						break;
					}
					else tmp = tmp->get_right();
				else tmp = NULL;
			}
		}
	}
}

void Vocabulary::print()
{
	print_helper_straight(root);
}

void Vocabulary::print_top3_popular()
{
	print_count = 0;
	print_helper_popular(root);
}

void Vocabulary::print_top3_unpopular()
{
	print_count = 0;
	print_helper_unpopular(root);
}

void Vocabulary::print_helper_popular(Element * el)
{
	if (el->get_right() && print_count < 3)
	{
		print_count++;
		print_helper_popular(el->get_right());
	}
		cout << el->get_value() << " "; 
	if (el->get_left() && print_count < 3)
	{
		print_count++;
		print_helper_popular(el->get_left());
	}
	
}

void Vocabulary::print_helper_unpopular(Element * el)
{
	if (el->get_left() && print_count < 3)
	{
		print_count++;
		print_helper_unpopular(el->get_left());
	}
	cout << el->get_value() << " ";
	if (el->get_right() && print_count < 3)
	{
		print_count++;
		print_helper_unpopular(el->get_right());
	}	
}

void Vocabulary::print_helper_simmetric(Element * el)	//min - root - max  (to print 3 unpopular words)
{
	if (el->get_left() )
		print_helper_simmetric(el->get_left());
	cout << el->get_value() << " ";
	if (el->get_right())
		print_helper_simmetric(el->get_right());
}

void Vocabulary::print_helper_straight(Element * el)	//root - min - max	(to print all vocabulary)
{
	cout << el->get_value() << " ";
	if (el->get_left())
		print_helper_straight(el->get_left());
	if (el->get_right())
		print_helper_straight(el->get_right());
}

void Vocabulary::print_helper_reversed(Element * el)	//root - max - min	(to print 3 popular words)
{
	cout << el->get_value() << " ";
	if (el->get_right())
	{
		print_helper_reversed(el->get_right());
	}
	if (el->get_left())
	{
		print_helper_reversed(el->get_left());
	}
}


