#pragma once
#include <iostream>
#include <cstring>
using std::cin;
using std::cout;

class point3D		//�������� ������ ��� �������� ���� ��������� �����
{
private:			//�������� �������� ������ (����)
	float x;
	float y;
	float z;
	char *name;

public:
	point3D();
	point3D(float aX = 0, float aY = 0, float aZ = 0, char *aName = nullptr);
	point3D(const point3D &a);
	~point3D();

public:				//�������� ������ ������ (�������)
	inline void set_coordinateX(float Ix);
	inline void set_coordinateY(float Iy);
	inline void set_coordinateZ(float Iz);
	inline void set_name(char *Iname);
	inline void print_name();
	inline void print_coordinate();
};