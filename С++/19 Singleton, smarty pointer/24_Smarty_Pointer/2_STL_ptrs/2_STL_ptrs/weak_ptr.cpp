#include <iostream>
#include <memory>

std::weak_ptr<int> gw;

void some_function()
{
	if (std::shared_ptr<int> spt = gw.lock()) { // ���������� ����������� � shared_ptr ����� ��������������
		std::cout << *spt << "\n";
	}
	else {
		std::cout << "gw is expired\n";
	}
}

int main()
{
	if(true){
		int *x = new int;
		*x = 42;
		std::shared_ptr<int> sp(x);
		gw = sp;

		some_function();
	}

	some_function();
}