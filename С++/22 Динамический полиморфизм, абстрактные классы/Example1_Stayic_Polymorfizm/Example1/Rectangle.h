#pragma once
#include "object.h"

class Rectangle :	public Object
{
	public:
		Rectangle(void);
		double square();
		~Rectangle(void);
};
