#pragma once
#include"Air_transport.h"

class Aerostation : public Air_transport
{
private:
	double baloon_volume;

public:
	Aerostation();
	Aerostation(string aModel, double aWeight, double aMax_speed, speed aSpeed_units, double aMax_height, double aBaloon_volume);
	~Aerostation() {};

public:
	void set_baloon_volume(double aBaloon_volume);
	double get_baloon_volume()const;

	virtual speed get_speed_units()const;
	virtual void print();
};