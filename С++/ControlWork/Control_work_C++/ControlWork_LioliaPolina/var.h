#pragma once
#include <iostream>
#include "String.h"
using std::cout; 

class var
{
private:
	int i_number;
	double d_number;
	String S;

public:
	var();
	var(int aInt);
	var(double aDouble);
	var(String aS);
	var(const var &to_copy);
	~var();

public:
	/*
	����������� �������������� ���������: +, -, *, /, +=, -=, *=, /=
	� ��������� ���������: <, >, <=, >=, ==, !=
*/
	inline void set_i_number(int a);
	inline void set_d_number(double a);
	inline void set_S(String a);
	inline int get_i_number()const;
	inline double get_d_number()const;
	inline String get_S()const;
	void Show();
	var operator= (int a);
	var operator= (double a);
	var operator= (String a);
	var operator= (const var &A);
	var operator+ (const var &A);
	/*var operator- (const var &A);*/
	var operator* (const var &A);
	var operator/ (const var &A);
	var operator+= (const var &A);
	/*var operator-= (const var &A);*/
	var operator*= (const var &A);
	var operator/= (const var &A);
	bool operator< (const var &A);
	bool operator> (const var &A);
	bool operator<= (const var &A);
	bool operator>= (const var &A);
	bool operator== (const var &A);
	bool operator!= (const var &A);
};
