#pragma once
#include <iostream>

class Stack 
{
	int _capacity;
	int _size;
	int *items;

public:
	Stack();
	Stack(int capacity);
	Stack(const Stack &aStack);
	~Stack();
	void set_capacity(int aCapacity);
	void push(int number);
	int pop();
	int capacity()const;
	int size();
	bool isFull();
	bool isEmpty();
	int operator[](int index);
};