#include "Search.h"
#include <algorithm>
#include <functional> 


void Search::add_company(string aName, string aDescription, accaunt aType)	//overloaded constructor
{
	Company New(aName, aDescription, aType);
	all_companies.push_back(New);
}

void Search::remove_company(int index)	//to remove pointed company from vector (takes integer - index)
{
	if (index < 0 || index >= all_companies.size())
		throw "Wrong company index\n";
	all_companies.erase(all_companies.begin() + index);
}

void Search::remove_company(string aName)	//to remove pointed company from vector (takes string - Company's name)
{
	int counter = 0;
	for (auto i : all_companies)
	{
		if (i.get_name() == aName)
		{
			all_companies.erase(all_companies.begin() + counter);
			break;
		}
		counter++;
	}
}

//takes name of company, returns it's index in vector 'all companies'; if search failed, returns -1
int Search::get_company_index(string aName)
{
	int counter = 0;
	for (auto i : all_companies)
	{
		if (i.get_name() == aName)
			return counter;
		counter++;
	}
	return -1;
}

//takes name of company, returns it's index in vector 'search rezult'; if search failed, returns -1
int Search::get_company_index_in_search_rezult(string aName)
{
	int counter = 0;
	for (auto i : search_rezult)
	{
		if (i.get_name() == aName)
			return counter;
		counter++;
	}
	return -1;
}

void Search::set_name(int index, string aName)
{
	if (index < 0 || index >= all_companies.size())
		throw "Wrong company index\n";
	all_companies[index].set_name(aName);
}

void Search::set_description(int index, string aDescription)
{
	if (index < 0 || index >= all_companies.size())
		throw "Wrong company index\n";
	all_companies[index].set_description(aDescription);
}

void Search::set_type(int index, accaunt aType)
{
	if (index < 0 || index >= all_companies.size())
		throw "Wrong company index\n";
	all_companies[index].set_type(aType);
}

void Search::add_key_word(int index, string aKey_word)	//method to add key word for pointed Company
{
	if (index < 0 || index >= all_companies.size())
		throw "Wrong company index\n";
	all_companies[index].add_key_word(aKey_word);
}

void Search::change_key_word(int index, string aWord_old, string aWord_new)
{
	if (index < 0 || index >= all_companies.size())
		throw "Wrong company index\n";
	all_companies[index].change_key_word(aWord_old, aWord_new);
}

void Search::add_word_to_find(string word)	//method to add key word, that has to be found among Companies
{
	words_to_find.insert(word);
}

void Search::clear_words_to_find() //clears all words to find before new search
{
	words_to_find.clear();
}

string Search::get_name(int index) const
{
	if (index < 0 || index >= all_companies.size())
		throw "Wrong company index\n";
	return all_companies[index].get_name();
}

string Search::get_description(int index) const
{
	if (index < 0 || index >= all_companies.size())
		throw "Wrong company index\n";
	return all_companies[index].get_description();
}

accaunt Search::get_type(int index) const
{
	if (index < 0 || index >= all_companies.size())
		throw "Wrong company index\n";
	return all_companies[index].get_type();
}


bool more_key_words(Company a, Company b) //returns true if company 'a' has more key words than 'b' has, false if not
{
	if (a.get_key_words_found() == b.get_key_words_found())
		return a.get_key_words_in_description() > b.get_key_words_in_description();
	return a.get_key_words_found() > b.get_key_words_found();
}


void Search::clear_search_rezult()	//clears all search rezults before the new search
{
	search_rezult.clear();
}

void Search::find_company_by_words()
{	
	clear_search_rezult();	//clears all search rezults before the new search

	//finding all Companies with at least one key word
	int counter = 0;
	for (auto i : all_companies)
	{
		bool flag = false;
		int count_key_words = 0,
			count_key_words_description = 0;
		for (auto j : words_to_find)
		{
			if (i.find_key_word(j))
			{
				flag = true;
				count_key_words++;
			}
			if (i.find_key_word_in_description(j))
			{
				count_key_words_description++;
			}
		}
		if (flag)
		{
			//setting Company's properties 'key_words_found' and 'key_words_in_description' with counter's rezults:
			all_companies[counter].set_key_words_found(count_key_words);
			all_companies[counter].set_key_words_in_description(count_key_words_description);
			//placing Company with key words to search rezults vector:
			search_rezult.push_back(all_companies[counter]);
		}
		counter++;
	}
	//sorting vector by accaunt types:
	sort(search_rezult.begin(), search_rezult.end(), [](Company a, Company b)
	{
			return a.get_type() > b.get_type();
	});
	
	//finding positions of the first elements of every accaunt type groups:
	auto _platinum = std::find_if(search_rezult.begin(), search_rezult.end(), [](Company a)
	{
		return a.get_type() == platinum;
	});
	string platinum_name = (*_platinum).get_name();
	int platinum_index = get_company_index_in_search_rezult(platinum_name);
	
	auto _gold = std::find_if(search_rezult.begin(), search_rezult.end(), [](Company a)
	{
		return a.get_type() == gold;
	});
	string gold_name = (*_gold).get_name();
	int gold_index = get_company_index_in_search_rezult(gold_name);
	
	auto _silver = std::find_if(search_rezult.begin(), search_rezult.end(), [](Company a)
	{
		return a.get_type() == silver;
	});
	string silver_name = (*_silver).get_name();
	int silver_index = get_company_index_in_search_rezult(silver_name);
	
	auto _Free = std::find_if(search_rezult.begin(), search_rezult.end(), [](Company a)
	{
		return a.get_type() == Free;
	});
	string Free_name = (*_Free).get_name();
	int Free_index = get_company_index_in_search_rezult(Free_name);

	//finding end indexes of every accaunt group:
	//platinum:
	size_t platinum_end = 0;
	if (platinum_index == -1)
		platinum_end = -1;
	else if (gold_index != -1)
		platinum_end = gold_index - 1;
	else if (silver_index != -1)
		platinum_end = silver_index - 1;
	else if (Free_index != -1)
		platinum_end = Free_index - 1;
	else
		platinum_end = search_rezult.size() - 1;

	//gold:
	size_t gold_end = 0;
	if (gold_index == -1)
		gold_end = - 1;
	else if (silver_index != -1)
		gold_end = silver_index - 1;
	else if (Free_index != -1)
		gold_end = Free_index - 1;
	else
		gold_end = search_rezult.size() - 1;

	//silver:
	size_t silver_end = 0;
	if (silver_index == -1)
		silver_end = silver_index - 1;
	else if (Free_index != -1)
		silver_end = Free_index - 1;
	else
		silver_end = search_rezult.size() - 1;

	//Free:
	size_t Free_end = 0;
	if (Free_index == -1)
		Free_end = - 1;
	else
		Free_end = search_rezult.size() - 1;

	//sorting every group of accaunts, using predicate more_key_words:
	if (platinum_index != -1)
		sort(search_rezult.begin(), search_rezult.begin() + platinum_end, more_key_words);
	if (gold_index != -1)
		sort(search_rezult.begin() + gold_index, search_rezult.begin() + gold_end, more_key_words);
	if (silver_index != -1)
		sort(search_rezult.begin() + silver_index, search_rezult.begin() + silver_end, more_key_words);
	if (Free_index != -1)
		sort(search_rezult.begin() + Free_index, search_rezult.end(), more_key_words);

	show_search_rezult();	//prints search rezult to console

	clear_words_to_find(); //clears all words to find after the search ends
}

void Search::show_search_rezult()
{
	for (auto i : search_rezult)
	{
		cout << i;
		cout << "Raiting: " << i.get_key_words_found() << " / " << i.get_key_words_in_description() << "\n\n";
	}
}
