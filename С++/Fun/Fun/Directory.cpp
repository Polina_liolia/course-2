#include "Header.h"

//����� ����� ������������� �������
void find_maxsize_file(char* directory, char* mask, char* name_max, long long int &sz_max)
{
	_chdir(directory);
	_finddata_t  file;
	intptr_t h;
	h = _findfirst(mask, &file);
	if (h != -1)							//if a file was found according to the mask
	{
		sz_max = file.size;
		strcpy_s(name_max, 260, file.name);
		while (_findnext(h, &file) == 0)	//while file is found according to the mask
		{
			if (file.size > sz_max)
			{
				sz_max = file.size;
				strcpy_s(name_max, 260, file.name);
			}
		}
		cout << "The file wiht maximal size: " << name_max << " " << sz_max << endl;
		_findclose(h);						//closing a search stream
	}
	else
		cout << "No files found.\n";

}


//	����� ����� � ������������
bool find_file(char* directory, char* mask)
{
	_chdir(directory);
	bool flag = false;				//to indicate, what value was returned in the process of recursion
									//	char* dir_name = new char[260];		
	_finddata_t  file;
	intptr_t h;
	h = _findfirst(mask, &file);
	char buffer[260];				//to save current location
	_getcwd(buffer, 200);			//getting a current location
	if (h != -1)					//if a file was found according to the mask
	{
		_findclose(h);				//closing a search stream
		cout << "File was found in " << buffer << "\n";
		return 1;
	}
	else
	{
		h = _findfirst("*", &file);						//searcing for any files in a current directory
		if (h != -1)									//if a file was found according to the mask
		{
			if ((file.attrib & _A_SUBDIR) == _A_SUBDIR  && strcmp(file.name, ".") != 0 && strcmp(file.name, "..") != 0) //if it is a directory
			{
				_getcwd(buffer, 200);					//saving a current location
				flag = find_file(file.name, mask);		//searching for a file inside a directory, that was found
				if (flag)								//checking if the file was found
				{
					_findclose(h);						//closing a search stream
					return 1;
				}
				else									//if the file was not found
					_chdir(buffer);						//to be back to the previous diectory
			}
			while (_findnext(h, &file) == 0)			//while file is found according to the mask
			{
				if ((file.attrib & _A_SUBDIR) == _A_SUBDIR && strcmp(file.name, ".") != 0 && strcmp(file.name, "..") != 0)	//if it is a directory
				{
					_getcwd(buffer, 200);				//saving a current location
					flag = find_file(file.name, mask);	//searching for a file inside a directory, that was found
					if (flag)							//checking if the file was found
					{
						_findclose(h);					//closing a search stream
						return 1;
					}
					else								//if the file was not found
						_chdir(buffer);					//to be back to the previous diectory
				}
			}

		}
		else								//if no files were found in a diectory
		{
			_findclose(h);					//closing a search stream
			return 0;
		}
	}
	return 0;								//if the file was not found
}


//������� �������� ������� �����
long long int dir_sz(char* directory)
{
	_chdir(directory);
	long long int sz = 0;
	_finddata_t  file;
	intptr_t h;
	h = _findfirst("*", &file);					//searcing for any files in a current directory
	if (h != -1)								//if a file was found according to the mask
	{
		sz += file.size;						//summing a size of this file 
		while (_findnext(h, &file) == 0)		//while file is found according to the mask
		{
			if ((file.attrib & _A_SUBDIR) == _A_SUBDIR && strcmp(file.name, ".") != 0 && strcmp(file.name, "..") != 0)	//if it is a directory
				sz += dir_sz(file.name);		//summing a size of all its files 
			else
				sz += file.size;				//summing a size of this file 
		}
	}
	else										//if no files were found according to the mask
		return sz;
	return sz;
}
