#pragma once
#include <string.h>
class Point
{
	int x;
	int y;
	char* name;

public:
	Point()
	{
		x = 0;
		y = 0;
		name = 0;
	}
	Point (int aX, int aY)
	{
		x = aX;
		y = aY;
		name = NULL;
	}
	~Point()
	{
		delete[] name;
	}
	Point (int aX, int aY, char* aName)
	{
		x = aX;
		y = aY;
		if(aName)
		{
			name = new char[strlen(aName) + 1];
			strcpy(name, aName);
		}	else name = NULL;
	}
	void set_name(char * aName)
	{
		if(name) delete[] name;
		name = new char[strlen(aName) + 1];
		strcpy(name, aName);
	}
	void set_x(int aX){
		x = aX;
	}
	void set_y(int aY){
		y = aY;
	}
	Point (const Point &p)
	{
		x = p.x;
		y = p.y;
		name = NULL;
		if(p.name){
			name = new char[strlen(p.name) + 1];
			strcpy(name, p.name);
		}
	}
	int get_x()const {
		return x;
	}
	int get_y()const{
		return y;
	}
	char* get_name()const{
		return name;
	}
};