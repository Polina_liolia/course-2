#include "Student.h"



Student::Student(){}

Student::Student(string aName, string aSurname, date aBirth_day, string aAdress, string aSchool, string aUnivercity, string aSpeciality, string aPhone)
	: Schoolboy::Schoolboy(aName, aSurname, aBirth_day, aAdress, aSchool, aPhone),
	univercity(aUnivercity), speciality(aSpeciality)
{}


Student::~Student()
{}

void Student::set_univercity(string aUnivercity)
{
	univercity = aUnivercity;
}

void Student::set_speciality(string aSpeciality)
{
	speciality = aSpeciality;
}

string Student::get_univercity() const
{
	return univercity;
}

string Student::get_speciality() const
{
	return speciality;
}

Student Student::operator=(const Student & S)
{
	Schoolboy::operator=(S);
	univercity = S.univercity;
	speciality = S.speciality;
	return *this;
}

void Student::print_to_file(char * file_name)
{
	Schoolboy::print_to_file(file_name);

	ofstream F(file_name, ios::binary | ios::app);
	// checking if file was successfully opened, othervice generating exception:
	if (!F)
		throw "Student: Can't open file for writing";
	int buf = univercity.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));
	F.write(reinterpret_cast<char*>(&univercity[0]), buf);

	buf = speciality.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));
	F.write(reinterpret_cast<char*>(&speciality[0]), buf);
	
	F.close();
}

void Student::read_from_file(char * file_name)
{
	Schoolboy::read_from_file(file_name);
	ifstream F(file_name, ios::binary | ios::app);
	 //checking if file was successfully opened, othervice generating exception:
	if (!F)
		throw "Student: Can't open file for reading";
	int buf;
	F.read(reinterpret_cast<char*>(&buf), sizeof(int));
	univercity.resize(buf);
	F.read(reinterpret_cast<char*>(&univercity[0]), buf);

	F.read(reinterpret_cast<char*>(&buf), sizeof(int));
	speciality.resize(buf);
	F.read(reinterpret_cast<char*>(&speciality[0]), buf);

	F.close();
}
