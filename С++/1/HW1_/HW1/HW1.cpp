#include <iostream>
using namespace std;

/*����������� ����� Person ���������� ��������� ����:
-	�������;
-	���:
-	��������;
-	���-�� ������ ���;
-	����� ����������;
-	Email;
-	�������.

	��� ��������� ���� ������ ���� �������������. ����������� ������ set � get ��� ������� ����.

	����������� ����� ����������� ��������� �� �������� ���� � ������������ ������ ������� ����������.
*/

class Persone
{
public:
	char *surname = nullptr;
	char *name = nullptr;
	char *patronymic = nullptr;
	int age;				
	char *address = nullptr;
	char *email = nullptr;
	char *phone = nullptr;

public:
	//methods to set the meaning of every property:
	void set_surname(char* temp_surname)
	{
		delete[] surname;
		int sz = strlen(temp_surname) + 1;
		surname = new char[sz];
		strcpy_s(surname, sz, temp_surname);
	}

	void  set_name(char* temp_name)
	{
		delete[] name;
		int sz = strlen(temp_name) + 1;
		name = new char[sz];
		strcpy_s(name, sz, temp_name);
	}

	void  set_patronymic(char* temp_patronymic)
	{
		delete[] patronymic;
		int sz = strlen(temp_patronymic) + 1;
		patronymic = new char[sz];
		strcpy_s(patronymic, sz, temp_patronymic);
	}

	void  set_age(int temp_age)
	{
		age = temp_age;
	}

	void  set_address(char* temp_address)
	{
		delete[] address;
		int sz = strlen(temp_address) + 1;
		address = new char[sz];
		strcpy_s(address, sz, temp_address);
	}

	void  set_email(char* temp_email)
	{
		delete[] email;
		int sz = strlen(temp_email) + 1;
		email = new char[sz];
		strcpy_s(email, sz, temp_email);
	}

	void  set_phone(char* temp_phone)
	{
		delete[] phone;
		int sz = strlen(temp_phone) + 1;
		phone = new char[sz];
		strcpy_s(phone, sz, temp_phone);
	}

	//methods to get the meaning of every property:
	char *get_surname()
	{
		return surname;
	}

	char *get_name()
	{
		return name;
	}

	char *get_patronymic()
	{
		return patronymic;
	}

	int get_age()
	{
		return age;
	}

	char *get_address()
	{
		return address;
	}

	char *get_email()
	{
		return email;
	}

	char *get_phone()
	{
		return phone;
	}

	//a method to write all data to file
	int write_to_file(FILE *f, char *file_name)
	{
		fopen_s(&f, file_name, "wb");					//opens a binar file to write 
		if (f == NULL)									//if file was not opened
			return -1;
		int sz = 0;
		
		//writing an information to file:
		sz = strlen(surname);
		fwrite(&sz, 4, 1, f);							//size of buffer to write a string
		fwrite(surname, strlen(surname), 1, f);
		
		sz = strlen(name);
		fwrite(&sz, 4, 1, f);
		fwrite(name, strlen(name), 1, f);				//writing a name
		
		sz = strlen(patronymic);
		fwrite(&sz, 4, 1, f);
		fwrite(patronymic, strlen(patronymic), 1, f);	//writing a patronymic
		
		fwrite(&age, sizeof(int), 1, f);				//writing an age
		
		sz = strlen(address);
		fwrite(&sz, 4, 1, f);
		fwrite(address, strlen (address), 1, f);		//writing an address
		
		sz = strlen(email);
		fwrite(&sz, 4, 1, f);
		fwrite(email, strlen(email), 1, f);				//writing a email
		
		sz = strlen(phone);
		fwrite(&sz, 4, 1, f);
		fwrite(phone, strlen (phone), 1, f);			//writing a phone number
		
		fclose(f);										//closing file
		return 0;										//if file was successfully opened
	}
};

void main()
{
	Persone first;
	//setting meaning for every property of a first persone: 
	first.set_surname("Ivanov");
	first.set_name("Ivan");
	first.set_name("Vasilii");
	first.set_patronymic("Ivanovich");
	first.set_age(25);
	first.set_address("61000, Kharkiv, Lopanskaya Str., 30 - 63");
	first.set_email("ivan@gmail.com");
	first.set_phone("+3(067)785-96-32");
	
	//methods to get the meaning of every property of a first persone: 
	cout << first.get_surname() << endl;
	cout << first.get_name() << endl;
	cout << first.get_patronymic() << endl;
	cout << first.get_age() << endl;
	cout << first.get_address() << endl;
	cout << first.get_email() << endl;
	cout << first.get_phone() << endl;
	
	FILE *s=NULL;							//creating a pointer on a file 
	first.write_to_file(s, "test");		//to write a person's data in a file
}