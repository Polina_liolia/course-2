var fly = document.querySelector("div.fly");

var R = 100; //radius of moving trajectory
var elementRadius = fly.offsetWidth/2;

var pointerX = 0,
    pointerY = 0;

function moveCircle() {
    pointerX = event.x;
    pointerY = event.y;
}

document.addEventListener('mousemove', moveCircle);

var angle = 0;

setInterval(function () {
    var x = R * Math.cos((angle*Math.PI)/180);
    var y = R * Math.sin((angle*Math.PI)/180);
    angle++;
    fly.style.left = x + pointerX - elementRadius + 'px';
    fly.style.top =  y + pointerY - elementRadius + 'px';
}, 20);