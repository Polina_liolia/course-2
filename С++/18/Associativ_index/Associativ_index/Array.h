#pragma once
#include "Element.h"

template <typename KeyType, typename ValueType>

class Array{
public:

	typedef Element<KeyType,ValueType> ElemType;
	ElemType **_storage;//��������� �� ��������� �������
	 unsigned int _size;

public:

	Array(): _storage(0), _size(0){
	}

	void insert(KeyType aKey, ValueType aValue = ValueType()){
		//�������� �� �������������
		_storage = (ElemType**)realloc(_storage, ++_size*sizeof(ValueType));//*ValueType-������ ������

		_storage[_size - 1] = new ElemType(aKey, aValue);
	}

	ValueType& operator [](KeyType index){
		//search
		ElemType *ptr = 0;
		for(unsigned int i = 0; i < _size; i++){
			if(_storage[i]->getKey()==index){
				ptr = _storage[i];
				break;
			}
		}
		if(!ptr){
			insert(index);
			ptr = _storage[_size - 1];
		}
		
		return ptr->getValue();
	}

	int getSize(){
		return _size;
	}


};