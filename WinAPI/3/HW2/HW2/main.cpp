// ���� WINDOWS.H �������� �����������, �������, � ��������� ������� ������������ ��� ��������� ���������� ��� Windows. 
#define _UNICODE
#include <windows.h>
#include <tchar.h>
#include <Strsafe.h>
#include "resource.h"
#include <vector>
using std::vector;


// �������� ������� ���������
LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
TCHAR szClassWindow[] = TEXT("��������� ����������");	/* ��� ������ ���� */
vector<HWND> allButtonsH;

INT WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow)
{
	HWND hWnd;
	MSG Msg;
	WNDCLASSEX wcl;


	// 1. ����������� ������ ����
	wcl.cbSize = sizeof(wcl);	// ������ ��������� WNDCLASSE
								//������������ �� ����, ���� ������� ������ �� ����������� ��� �� ���������:

	wcl.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;	// CS (Class Style) - ����� ������ ����
														// CS_DBLCLKS - ��������� ������� ���� ����
	wcl.lpfnWndProc = WindowProc;	// ����� ������� ���������
	wcl.cbClsExtra = 0;		// ������������ Windows 
	wcl.cbWndExtra = 0; 	// ������������ Windows 
	wcl.hInstance = hInst;	// ���������� ������� ����������
	wcl.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON1));	// �������� ����������� ������
	wcl.hCursor = LoadCursor(NULL, IDC_ARROW);	// �������� ������������ �������	
	wcl.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	// ���������� ���� ����� ������
	wcl.lpszMenuName = NULL;	// ���������� �� �������� ����
	wcl.lpszClassName = szClassWindow;	// ��� ������ ����
	wcl.hIconSm = NULL;	// ���������� ��������� ������ ��� ����� � ������� ����

						// 2. ����������� ������ ����
	if (!RegisterClassEx(&wcl))
		return 0; // ��� ��������� ����������� - �����

				  // 3. �������� ����
				  // ��������� ���� �  ���������� hWnd ������������� ���������� ����
	hWnd = CreateWindowEx(
		0,		// ����������� ����� ����
		szClassWindow,	//��� ������ ����
		TEXT("������ Windows ����������"), // ��������� ����
		WS_OVERLAPPEDWINDOW,				// ����� ����
											/* ���������, �����, ����������� ������ �������, ��������� ����, ������ ������������ � ���������� ����  */
		CW_USEDEFAULT,	// �-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// y-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// ������ ����
		CW_USEDEFAULT,	// ������ ����
		NULL,			// ���������� ������������� ����
		NULL,			// ���������� ���� ����
		hInst,		// ������������� ����������, ���������� ����
		NULL);		// ��������� �� ������� ������ ����������

	
	
	HWND b1 = CreateWindow(L"button", L"Button1", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10, 10, 100, 30, hWnd, (HMENU)1, hInst, NULL);
	HWND b2 = CreateWindow(L"button", L"Button2", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10, 45, 100, 30, hWnd, (HMENU)2, hInst, NULL);
	HWND b3 = CreateWindow(L"button", L"Button3", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10, 80, 100, 30, hWnd, (HMENU)3, hInst, NULL);
	HWND b4 = CreateWindow(L"button", L"Button4", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10, 115, 100, 30, hWnd, (HMENU)1, hInst, NULL);
	HWND b5 = CreateWindow(L"button", L"Button5", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10, 150, 100, 30, hWnd, (HMENU)2, hInst, NULL);
	HWND b6 = CreateWindow(L"button", L"Button6", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10, 185, 100, 30, hWnd, (HMENU)3, hInst, NULL);
	
	allButtonsH.push_back(b1);
	allButtonsH.push_back(b2);
	allButtonsH.push_back(b3);
	allButtonsH.push_back(b4);
	allButtonsH.push_back(b5);
	allButtonsH.push_back(b6);


					// 4. ����������� ����
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd); // ����������� ����
	ShowWindow(b1, nCmdShow);
	ShowWindow(b2, nCmdShow);
	ShowWindow(b3, nCmdShow);
	ShowWindow(b4, nCmdShow);
	ShowWindow(b5, nCmdShow);
	ShowWindow(b6, nCmdShow);


						// 5. ������ ����� ��������� ���������
	while (GetMessage(&Msg, NULL, 0, 0)) // ��������� ���������� ��������� �� ������� ���������
	{
		TranslateMessage(&Msg);	// ���������� ���������
		DispatchMessage(&Msg);	// ��������������� ���������
	}
	return Msg.wParam;
}

/*
1) �������� ����������, � ������� ������ ������� ���������� ������� �����, ������ � ������� ������ ����.
����������� ���������� ���������� �������� � ��������� ����.
*/

int l_but = 0,
m_but = 0,
r_but = 0,
left = 0,
top = 0;

void show_status(HWND hWnd)
{
	RECT r;
	GetWindowRect(hWnd, &r);
	left = r.left;
	top = r.top;

	int const arraysize = 50;
	TCHAR msg[arraysize];
	size_t sz = arraysize * sizeof(TCHAR);
	LPCTSTR pszFormat = TEXT("L: %d M: %d R: %d Left: %d Top: %d");
	HRESULT hr = StringCbPrintf(msg, sz, pszFormat, l_but, m_but, r_but, left, top);
	SetWindowText(hWnd, msg);
}

int i = 0;
void show_hide_buttons(HWND hWnd)
{
	if (i == allButtonsH.size())
	{
		ShowWindow(allButtonsH[allButtonsH.size() - 1], SW_SHOW);
		i = 0;
	}
	ShowWindow(allButtonsH[i], SW_HIDE);
	if (i)
		ShowWindow(allButtonsH[i - 1], SW_SHOW);
	i++;
}


void show_buttons(HWND hWnd)
{
	for (int i = 0; i < allButtonsH.size(); i++)
		ShowWindow(allButtonsH[i], SW_SHOW);
}

//������������ ���� ��������� �� ����������
void keyboard_event(HWND hWnd, WPARAM wParam)
{
	RECT r;
	GetWindowRect(hWnd, &r);
	int width = r.right - r.left;
	int height = r.bottom - r.top;

	int cl_width = GetSystemMetrics(SM_CXFULLSCREEN);
	int cl_height = GetSystemMetrics(SM_CYFULLSCREEN);

	int move = 100;
	switch (wParam)
	{
	case VK_UP:
		top -= move; break;
	case VK_DOWN:
		top += move; break;
	case VK_LEFT:
		left -= move; break;
	case VK_RIGHT:
		left += move;
	case VK_RETURN:
		{
			top = 0;
			left = 0;
			width = 300;
			height = 300;
			SetTimer(hWnd, 1, 1000, NULL);
			SetTimer(hWnd, 2, 1, NULL);
		}
		break;
	case VK_ESCAPE:
	{
		KillTimer(hWnd, 1);
		KillTimer(hWnd, 2);
		show_buttons(hWnd); 
	}
		break;
	case 2:
	{
		if (left < cl_width - 300 && top == 0)
			left += 1;
		else if (top < cl_height - 300 && left !=0)
			top += 1;
		else if (left > 0 && top !=0)
			left -= 1;
		else if (top > 0 && left == 0)
			top -= 1;

	}
	default:
		break;
	}
	MoveWindow(hWnd, left, top, width, height, true);
}

//change name of Calculator window
int counter = 0;
void change_calc_header()
{
	HWND h_calc = FindWindow(TEXT("Windows.UI.Core.CoreWindow"), NULL);
	counter++;
	int const arraysize = 50;
	TCHAR msg[arraysize];
	size_t sz = arraysize * sizeof(TCHAR);
	LPCTSTR pszFormat = TEXT("Calculator name changed in %d time(s)!");
	HRESULT hr = StringCbPrintf(msg, sz, pszFormat, counter);
	SetWindowText(h_calc, msg);
}



LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	switch (uMessage)
	{
	case WM_DESTROY: // ��������� � ���������� ���������
		PostQuitMessage(0); // ������� ��������� WM_QUIT
		break;
	case WM_LBUTTONDOWN: //������ ����� ������ ����;
		l_but++;
		show_status(hWnd);
		change_calc_header();
		break;
	case WM_MBUTTONDOWN: // ������ ������� ������ ����;
		m_but++;
		show_status(hWnd);
		break;
	case WM_RBUTTONDOWN: // ������ ������ ������ ����;
		r_but++;
		show_status(hWnd);
		break;
	case WM_KEYDOWN: //������ �������
		keyboard_event(hWnd, wParam);
		break;
	case WM_TIMER:
		if (wParam == 1)
			show_hide_buttons(hWnd);
		if (wParam == 2)
			keyboard_event(hWnd, wParam);
	default:
		// ��� ���������, ������� �� �������������� � ������ ������� ������� ������������ ������� Windows �� ��������� �� ���������
		return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}
	return 0;
}