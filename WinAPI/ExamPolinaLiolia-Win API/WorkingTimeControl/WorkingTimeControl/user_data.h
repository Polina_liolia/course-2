#pragma once
#include <tchar.h>
class user_data
{
	TCHAR *login;
	TCHAR *password;
	TCHAR *email;
	TCHAR *phone;

public:
	user_data();
	user_data(TCHAR *aLogin, TCHAR *aPassword, TCHAR *aEmail, TCHAR *aPhone);
	~user_data();

	TCHAR *get_login();
	TCHAR *get_password();
	TCHAR *get_email();
	TCHAR *get_phone();

	bool operator==(const user_data & aUser_data);
};

