var zipper = document.querySelector('.zipper');

//возвращает координаты элемента относительно всей страницы (с учетом прокрутки):
function getCoords(elem) {
    var box = elem.getBoundingClientRect();
    //прибавляем к координатам в клиентской области текущую прокрутку:
    return {
        top: box.top + pageYOffset,
        left: box.left + pageXOffset,
        right: box.right + pageXOffset
    };
}

function onMouseDown() {

    //getting coordinates of zipper:
    var coordsZipper = getCoords(zipper);
    //getting its shift:
    var shiftX = event.pageX - coordsZipper.left;

    //getting coordinates of zipper:
    var limiter = document.querySelector(".limiter");
    var coordsLimiter = getCoords(limiter);

    //чтобы иметь возможность свободно перемещать элемент по всей странице (вне родительский элементов):
    zipper.style.position = 'absolute';
    document.body.appendChild(zipper);
    moveAt();

    zipper.style.zIndex = 1000; // над другими элементами

    function moveAt() {
        if(event.pageX > coordsLimiter.left && event.pageX < coordsLimiter.right)
        {
            zipper.style.left = event.pageX - shiftX + 'px';
            zipper.style.top = coordsZipper.top + 'px';
        }

    }
    document.addEventListener("mousemove", moveAt);

    //отпуская элемент, отменяем отслеживание движения мыши и отпускания её кнопки:
    function dropElement() {
        document.removeEventListener("mousemove", moveAt);
        document.removeEventListener("mouseup", dropElement);
    }
    document.addEventListener("mouseup", dropElement);

}

//чтобы избежать автозапуска встроенного drag and drop:
function preventStdDrag() {
    return false;
}

zipper.addEventListener("dragstart", preventStdDrag);

zipper.addEventListener("mousedown", onMouseDown);
