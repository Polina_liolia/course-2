// Namespaces.cpp : Defines the entry point for the console application.
//

#include "test_lib.h"
#include <iostream>
#include "lib.h"
#include "lib2.h"
using std::cout;
using std::endl;

int my_global = 87;

/*
namespace{
	int my_global;
}

*/

namespace MyNamespace{
	int first = 23;
	double second;
}

namespace MyNamespace2{
	char first[20] = "abc";
}

//using namespace MyNamespace2;

int xx = 8;

int main(int argc, char* argv[])
{
	
	double my_global = 6.7;

	cout << my_global << endl;
	cout << ::my_global << endl;


	
	
	my_fun::f1();

	int xx = 56;

	cout << xx << endl;
	cout << ::xx << endl;

	
	std::cout << "ertbwertng" << std::endl;

	/*
	
	//1
	MyNamespace::first = 23;
	cout << MyNamespace::first << endl;

	//2 
	using std::cout;
	using MyNamespace::first;
	cout << first << endl;
	cout << second << endl; //error
	
	
	//3
	using namespace MyNamespace;
	cout << first << endl;
	cout << second << endl; 

	*/

	/*
	
	cout << MyNamespace::first << endl;
	cout << MyNamespace2::first << endl;

	*/
	using namespace MyNamespace;
	using namespace MyNamespace2;
//	cout << first << endl;


	using namespace my_fun;
	f1();
	f2();


//	cout <<first << MyNamespace::first<< endl;
	//cout << MyNamespace::second << endl;

	return 0;
}

