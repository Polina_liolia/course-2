#include <iostream>
#include "Point.h"

using std::cout;
using std::endl;

void main() {
	Point<int> p(3, 5);

	cout << "X: " << p.getX() << endl;
	cout << "Y: " << p.getY() << endl;

	Point<double> p1(3.14, 5.67);

	cout << "X: " << p1.getX() << endl;
	cout << "Y: " << p1.getY() << endl;

}