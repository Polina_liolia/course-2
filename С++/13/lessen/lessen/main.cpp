#include "Stack.h"
#include<iostream>
using std::ostream;
using std::cout;
using std::endl;


ostream& operator << (ostream& cout, const Stack &inst) {
	for (int i = 0; i < inst.sz; i++) {
		cout << inst.mas[i] << " ";
}
	cout << endl;
	cout << "max_size = " << inst.max_size << endl;
	cout << "cur_size = " << inst.sz +1<< endl;
	return cout;
}
void main()
{
	Stack a;
	a.pop();
	a.push(12);
	a.push(45);
	a.push(8);
	a.push(12);
	a.push(45);
	a.push(8);
	a.push(12);
	a.push(45);
	a.push(8);
	a.push(12);
	a.push(45);
	a.push(8);
	cout <<a.pop() <<endl;
	cout << a.capacity() << endl;
	cout << a.size() << endl;
	cout << a.isFull() << endl;
	cout << a.isEmpty() << endl;
	cout << a;	
}