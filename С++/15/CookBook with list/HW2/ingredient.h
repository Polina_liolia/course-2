#pragma once
#include <iostream>
using std::ostream;

class ingredient
{
public:
	char *item;
	float dose;

public:
	ingredient(char *aItem = nullptr, float aDose = 0);
	ingredient(const ingredient &temp);
	ingredient operator=(ingredient temp);
	~ingredient();
	char *get_item()const;
	float get_dose()const;
	friend ostream& operator << (ostream& os, const ingredient &aIng);
};

