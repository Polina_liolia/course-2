#pragma once
#include <iostream>

class point		
{
private:				
	char *name;
	int x;
	int y;

public:
	point(char *aName = nullptr, int aX = 0, int aY = 0);
	point(char &aName, int aX=0, int aY=0);
	point (const point &temp);
	~point();

public:				
	inline void set_x(int aX);
	inline void set_y(int aY);
	void set_name(char *aName);	
	inline int get_x()const;
	inline int get_y()const;
	inline char *get_name()const;
	void print()const;
	point point::operator=(const point &aPoint);
	point operator+ (int a);
	point operator+ (float a);
	point operator+ (double a);
	bool operator==(const point &aPoint);
	bool operator!=(const point &aPoint);
};