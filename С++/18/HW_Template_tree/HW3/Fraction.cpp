#include "Fraction.h"

int fraction::reduce()			//to reduce a fraction
{
	int div;					//maximal divider to try
	if (numerator > denominator)
		div = denominator;
	else
		div = numerator;
	for (int i = div; i >= 1; i--)
	{
		if (numerator%i == 0 && denominator%i == 0)
		{
			numerator /= i;
			denominator /= i;
			return 0;
		}
	}
	return -1;
}