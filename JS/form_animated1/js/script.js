$(function () {
    $(".add_event form").submit(function () {
        var dStart = $("tr.date_start input").val();
        var dEnd = $("tr.date_end input").val();
        var name = $("tr.event_name input").val();
        var description = $("tr.description textarea").val();

        if (dStart == "")
        {
            if ($("tr.date_start td:nth-of-type(3)>img").length == 0)
            {
                $("<img>").attr("src", "img/required.png").addClass("required")
                    .addClass("bounceInRight")
                    .addClass("animated")
                    .appendTo($("tr.date_start td:nth-of-type(3)"));
            }
            event.preventDefault();
        }
        else
            $("tr.date_start td:nth-of-type(3)>img").remove();

        if (dEnd == "")
        {
            if($("tr.date_end td:nth-of-type(3)>img").length == 0)
            {
                $("<img>").attr("src", "img/required.png").addClass("required")
                    .addClass("bounceInRight")
                    .addClass("animated")
                    .appendTo($("tr.date_end td:nth-of-type(3)"));
            }
            event.preventDefault();
        }
        else
            $("tr.date_end td:nth-of-type(3)>img").remove();

        if (name == "")
        {
            if($("tr.event_name td:nth-of-type(3)>img").length == 0)
            {
                $("<img>").attr("src", "img/required.png").addClass("required")
                    .addClass("bounceInRight")
                    .addClass("animated")
                    .appendTo($("tr.event_name td:nth-of-type(3)"));
            }
            event.preventDefault();
        }
        else
            $("tr.event_name td:nth-of-type(3)>img").remove();

        if (description == "")
        {
            if ($("tr.description td:nth-of-type(3)>img").length == 0)
            {
                $("<img>").attr("src", "img/required.png").addClass("required")
                    .addClass("bounceInRight")
                    .addClass("animated")
                    .appendTo($("tr.description td:nth-of-type(3)"));
            }
            event.preventDefault();
        }
        else
            $("tr.description td:nth-of-type(3)>img").remove();
    })
});