// ���� WINDOWS.H �������� �����������, �������, � ��������� ������� ������������ ��� ��������� ���������� ��� Windows. 
#include <windows.h>
#include <tchar.h>
#include "resource.h"
#include <ctime>
#include <Windowsx.h>
#include <Commctrl.h>


// �������� ������� ���������
LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wp, LPARAM lp);
#define OPENDIALOGID 300 //constant identifier of button created

TCHAR szClassWindow[] = TEXT("��������� ����������");	/* ��� ������ ���� */


INT WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow)
{
	HWND hWnd;
	MSG Msg;
	WNDCLASSEX wcl;


	// 1. ����������� ������ ����
	wcl.cbSize = sizeof(wcl);	// ������ ��������� WNDCLASSE
								//������������ �� ����, ���� ������� ������ �� ����������� ��� �� ���������:
	wcl.style = CS_HREDRAW | CS_VREDRAW;	// CS (Class Style) - ����� ������ ����
	wcl.lpfnWndProc = WindowProc;	// ����� ������� ���������
	wcl.cbClsExtra = 0;		// ������������ Windows 
	wcl.cbWndExtra = 0; 	// ������������ Windows 
	wcl.hInstance = hInst;	// ���������� ������� ����������
	wcl.hIcon = LoadIcon(NULL, IDI_APPLICATION);	// �������� ����������� ������
	wcl.hCursor = LoadCursor(NULL, IDC_ARROW);	// �������� ������������ �������	
	wcl.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	// ���������� ���� ����� ������
	wcl.lpszMenuName = NULL;	// ���������� �� �������� ����
	wcl.lpszClassName = szClassWindow;	// ��� ������ ����
	wcl.hIconSm = NULL;	// ���������� ��������� ������ ��� ����� � ������� ����

						// 2. ����������� ������ ����
	if (!RegisterClassEx(&wcl))
		return 0; // ��� ��������� ����������� - �����

				  // 3. �������� ����
				  // ��������� ���� �  ���������� hWnd ������������� ���������� ����
	hWnd = CreateWindowEx(
		0,		// ����������� ����� ����
		szClassWindow,	//��� ������ ����
		TEXT("������ Windows ����������"), // ��������� ����
		WS_OVERLAPPEDWINDOW,				// ����� ����
											/* ���������, �����, ����������� ������ �������, ��������� ����, ������ ������������ � ���������� ����  */
		CW_USEDEFAULT,	// �-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// y-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// ������ ����
		CW_USEDEFAULT,	// ������ ����
		NULL,			// ���������� ������������� ����
		NULL,			// ���������� ���� ����
		hInst,		// ������������� ����������, ���������� ����
		NULL);		// ��������� �� ������� ������ ����������

	HWND b1 = CreateWindow(L"button", L"Open Dialog", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10, 10, 100, 30, hWnd, (HMENU)OPENDIALOGID, hInst, NULL);

	// 4. ����������� ����
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd); // ����������� ����



						// 5. ������ ����� ��������� ���������
	while (GetMessage(&Msg, NULL, 0, 0)) // ��������� ���������� ��������� �� ������� ���������
	{
		TranslateMessage(&Msg);	// ���������� ���������
		DispatchMessage(&Msg);	// ��������������� ���������
	}
	return (int)Msg.wParam;
}


RECT main_r;
int main_left, main_top, main_right, main_bottom;
LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	switch (uMessage)
	{
	case WM_DESTROY: // ��������� � ���������� ���������
		PostQuitMessage(0); // ������� ��������� WM_QUIT
		break;
	case WM_COMMAND:
	{
		if (HIWORD(wParam) == BN_CLICKED) //control-defined notification code
			switch (LOWORD(wParam)) //control identifier
			{
			case OPENDIALOGID:
			{
				HWND dialog = CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_DIALOG1), hWnd, DlgProc); //unmodal dialog 
				ShowWindow(dialog, SW_SHOW);//show unmodal dialog
				GetWindowRect(hWnd, &main_r);
				main_left = main_r.left,
					main_top = main_r.top,
					main_right = main_r.right,
					main_bottom = main_r.bottom;
			}
			break;
			}

	}
	break;
	default:
		// ��� ���������, ������� �� �������������� � ������ ������� ������� ������������ ������� Windows �� ��������� �� ���������
		return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}
	return 0;
}

//coordinates of the window:
RECT window_r;
int w_left, w_top, w_right, w_bottom;

RECT static_r;
int s_left, s_top, s_right, s_bottom;
int dragX, dragY, shiftX, shiftY;
int s2_left, s2_top, s2_right, s2_bottom, s2_width, s2_height;
bool dragflag = false;

INT_PTR CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)//��������� ������� � ���������� ����
{
	switch (message)
	{
	case WM_INITDIALOG:
	{
		GetWindowRect(hWnd, &window_r);
		w_left = window_r.left;
		w_top = window_r.top;
		w_right = window_r.right;
		w_bottom = window_r.bottom;
	}
	break;
	case WM_CLOSE:
		EndDialog(hWnd, 0); // ��������� ������
		return TRUE;
	case WM_COMMAND:
		if (HIWORD(wParam) == BN_CLICKED) //������� ������ �������
			switch (LOWORD(wParam)) //control identifier
			{
			case IDOK:
				EndDialog(hWnd, 0); //��������� ������
				return TRUE;
			case IDCANCEL:
				EndDialog(hWnd, 0); //��������� ������
				return FALSE;
			}
		break;
	case WM_MOUSEMOVE:
	{
		srand(time(NULL));
		int mouseX = GET_X_LPARAM(lParam);
		int mouseY = GET_Y_LPARAM(lParam);
		HWND hStatic = GetDlgItem(hWnd, IDC_STATIC1);
		GetWindowRect(hStatic, &static_r);
		s_left = static_r.left;
		s_top = static_r.top;
		s_right = static_r.right;
		s_bottom = static_r.bottom;
		int s_width = s_right - s_left,
			s_height = s_bottom - s_top;
		POINT coords;
		coords.x = s_left;
		coords.y = s_top;
		ScreenToClient(hWnd, &coords);
		if (mouseX >= coords.x && mouseX < coords.x + s_width && mouseY >= coords.y && mouseY < coords.y + s_height) //cursor is on the left 
		{
			int randX = rand() % (w_right - w_bottom - s_width);
			int randY = rand() % (w_bottom - w_top - s_height);
			MoveWindow(hStatic, randX, randY, s_width, s_height, true);
		}
		HWND hStatic2 = GetDlgItem(hWnd, IDC_STATIC2);
		int x = mouseX - shiftX,
			y = mouseY - shiftY;
		if (dragflag)
			MoveWindow(hStatic2, x, y, s2_width, s2_height, true);
	}
	break;
	case WM_LBUTTONDOWN:
	{
		int mouseX = GET_X_LPARAM(lParam);
		int mouseY = GET_Y_LPARAM(lParam);
		RECT static2_r;
		HWND hStatic2 = GetDlgItem(hWnd, IDC_STATIC2);
		GetWindowRect(hStatic2, &static2_r);
		s2_left = static2_r.left,
			s2_top = static2_r.top,
			s2_right = static2_r.right,
			s2_bottom = static2_r.bottom,
			s2_width = s2_right - s2_left,
			s2_height = s2_bottom - s2_top;
		POINT coords;
		coords.x = s2_left;
		coords.y = s2_top;
		ScreenToClient(hWnd, &coords);

		if (mouseX >= coords.x && mouseX <= coords.x + s2_width && mouseY >= coords.y && mouseY <= coords.y + s2_height)
		{
			shiftX = mouseX - coords.x,
			shiftY = mouseY - coords.y;
			dragflag = true;
		}
	}
		break;
	case WM_LBUTTONUP:
		dragflag = false;
		break;
	}
	return FALSE;
}