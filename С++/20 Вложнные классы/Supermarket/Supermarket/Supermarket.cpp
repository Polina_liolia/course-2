#include"Supermarket.h"

Supermarket::Supermarket()	//default constructor
{
	size_depts = 0;
	size_stuff = 0;
	all_stuff = nullptr;
}

Supermarket::Supermarket(const Supermarket & aS)	//copy constructor
{
	size_depts = aS.size_depts;
	depts = aS.depts;
	size_stuff = aS.size_stuff;
	if (size_stuff == 0)
		all_stuff = nullptr;
	else
	{
		all_stuff = new employee[size_stuff];
		for (int i = 0; i < size_stuff; i++)
			all_stuff[i] = aS.all_stuff[i];
	}
}

Supermarket::~Supermarket()
{
	delete[] all_stuff;
}

Supermarket Supermarket::operator=(const Supermarket & aS)
{
	if (size_stuff)
		delete[]all_stuff;
	size_depts = aS.size_depts;
	depts = aS.depts;
	size_stuff = aS.size_stuff;
	if (size_stuff == 0)
		all_stuff = nullptr;
	else
	{
		all_stuff = new employee[size_stuff];
		for (int i = 0; i < size_stuff; i++)
			all_stuff[i] = aS.all_stuff[i];
	}
	return *this;
}

void Supermarket::add_department(String aName, short aDay, short aMonth, short aYear)
{
	Department New(aName, aDay, aMonth, aYear);
	depts.add_in_order(New);
	size_depts++;
}

void Supermarket::remove_department(int index)
{
	try
	{
		if (index < 0 || index >= size_depts)
			throw "Wrong dept index";
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
		return;
	}
	depts.delByIndex(index);
	size_depts--;
	
}

void Supermarket::edit_dept_name(int index, String name_new, short aDay, short aMonth, short aYear)
{
	try
	{
		if (index < 0 || index >= size_depts)
			throw "Wrong dept index";
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
		return;
	}
	depts.get_value_ByIndex(index).set_dept_name(name_new, aDay, aMonth, aYear);
}

DoubleLinkList &Supermarket::get_depts()
{
	return depts;
}

int Supermarket::get_size_stuff() const
{
	return size_stuff;
}

employee * Supermarket::get_all_stuff()	//getting an array of all emloyees from every dept
{
	if (size_stuff)
		delete[] all_stuff;
	size_stuff = 0;
	if (!size_depts)
		return all_stuff = nullptr;
	//counting total number of employees in all depts:
	for (int i = 0; i < size_depts; i++)
		size_stuff += depts.get_value_ByIndex(i).get_size_dept_stuff();
	//creating an array to place all stuff:
	all_stuff = new employee[size_stuff];
	for (int i = 0, k = 0; i < size_depts; i++)
		for (int j = 0; j < depts.get_value_ByIndex(i).get_size_dept_stuff(); j++, k++)
			all_stuff[k] = depts.get_value_ByIndex(i).get_employee(j);
	//sorting of all_stuff array (insert sort):
	int j = 0;
	for (int i = 1; i < size_stuff; i++)
	{
		employee x = *(all_stuff + i);
		for (j = i - 1; j >= 0 && *(all_stuff + j) > x; j--)
			*(all_stuff + j + 1) = *(all_stuff + j);
		*(all_stuff + j + 1) = x;
	}
	return all_stuff;
}

