#include "Aerostation.h"

Aerostation::Aerostation() : Air_transport()	//default constructor sets aviation baloon_volume = 0 (default)
{
	baloon_volume = 0;
}

//overloaded constructor
Aerostation::Aerostation(string aModel, double aWeight, double aMax_speed, speed aSpeed_units, double aMax_height, double aBaloon_volume) :
	Air_transport(aModel, aWeight, aMax_speed, aSpeed_units, aMax_height)
{
	baloon_volume = aBaloon_volume;
}

//method to set or change property baloon_volume
void Aerostation::set_baloon_volume(double aBaloon_volume)
{
	baloon_volume = aBaloon_volume;
}

//method to get meaning of property baloon_volume
double Aerostation::get_baloon_volume() const
{
	return baloon_volume;
}

//virtual method to get meaning of property speed_units
speed Aerostation::get_speed_units() const
{
	cout << "Speed units for aerostation - ";
	switch (speed_units)
	{
	case 0: cout << "km_h\n"; break;
	case 1: cout << "Mach\n"; break;
	case 2: cout << "miles_h\n"; break;
	case 3: cout << "knots\n"; break;
	}
	return speed_units;
}

//virtual method to print all information about Aerostation instance
void Aerostation::print()
{
	Air_transport::print();	//calling of basic class method
	cout << "Baloon volume: " << baloon_volume << " cubic m\n"; 
}
