#pragma once
#include "object.h"

class Rectangle :	public Object
{
public:
	Rectangle(void);
	virtual double square();
	
public:
	~Rectangle(void);
};
