#include "employee.h"

employee::employee()	//default constructor
{
	birth.day = 0;
	birth.month = 0;
	birth.year = 0;
	type = consultant;
}
//overloaded constructor:
employee::employee(String aSurname, String aName, position aType, String aPhone, short aDay, short aMonth, short aYear)
{
	name = aName;
	surname = aSurname;
	phone = aPhone;
	birth.day = aDay;
	birth.month = aMonth;
	birth.year = aYear;
	type = aType;
}
employee::employee(const employee &a)	//copy constructor
{
	name = a.name;
	surname = a.surname;
	phone = a.phone;
	birth.day = a.birth.day;
	birth.month = a.birth.month;
	birth.year = a.birth.year;
	type = a.type;
}
void employee::set_name(String aName)						//sets or changes employee's name
{
	name = aName;
}
void employee::set_surname(String aSurname)					//sets or changes employee's surname
{
	surname = aSurname;
}
void employee::set_phone(String aPhone)						//sets or changes employee's phone
{
	phone = aPhone;
}

void employee::set_position(position aType)					//sets or changes employee's position
{
	type = aType;
}
void employee::set_birth_date(short aDay, short aMonth, short aYear)	//returns employee's bith date
{
	birth.day = aDay;
	birth.month = aMonth;
	birth.year = aYear;
}

position employee::get_position()const						//returns employee's position
{
	return type;
}

String employee::get_name() const
{
	return name;
}

String employee::get_surname() const
{
	return surname;
}

String employee::get_phone() const
{
	return phone;
}

date employee::get_birth_date() const
{
	return birth;
}

bool employee::operator<(const employee & a)				//to compare employees by surname
{
	return surname < a.surname;
}

bool employee::operator<=(const employee & a)
{
	return surname <= a.surname;
}

bool employee::operator>(const employee & a)				//to compare employees by surname
{
	return surname > a.surname;
}

bool employee::operator>=(const employee & a)
{
	return surname >= a.surname;
}

bool employee::operator==(const employee & a)
{
	return name == a.name && surname == a.surname && type == a.type;
}
