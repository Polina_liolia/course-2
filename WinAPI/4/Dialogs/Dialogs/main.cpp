// ���� WINDOWS.H �������� �����������, �������, � ��������� ������� ������������ ��� ��������� ���������� ��� Windows. 
#include <windows.h>
#include <tchar.h>
#include "resource.h"


// �������� ������� ���������
LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
/*BOOL*/ DLGPROC CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wp, LPARAM lp);
#define OPENDIALOGID 300 //constant identifier of button created

TCHAR szClassWindow[] = TEXT("��������� ����������");	/* ��� ������ ���� */

INT WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow)
{
	HWND hWnd;
	MSG Msg;
	WNDCLASSEX wcl;

	
	// 1. ����������� ������ ����
	wcl.cbSize = sizeof(wcl);	// ������ ��������� WNDCLASSE
	//������������ �� ����, ���� ������� ������ �� ����������� ��� �� ���������:
	wcl.style = CS_HREDRAW | CS_VREDRAW;	// CS (Class Style) - ����� ������ ����
	wcl.lpfnWndProc = WindowProc;	// ����� ������� ���������
	wcl.cbClsExtra = 0;		// ������������ Windows 
	wcl.cbWndExtra  = 0; 	// ������������ Windows 
	wcl.hInstance = hInst;	// ���������� ������� ����������
	wcl.hIcon = LoadIcon(NULL, IDI_APPLICATION);	// �������� ����������� ������
	wcl.hCursor = LoadCursor(NULL, IDC_ARROW);	// �������� ������������ �������	
	wcl.hbrBackground = (HBRUSH) GetStockObject(WHITE_BRUSH);	// ���������� ���� ����� ������
	wcl.lpszMenuName = NULL;	// ���������� �� �������� ����
	wcl.lpszClassName = szClassWindow;	// ��� ������ ����
	wcl.hIconSm = NULL;	// ���������� ��������� ������ ��� ����� � ������� ����

	// 2. ����������� ������ ����
	if (!RegisterClassEx(&wcl))
		return 0; // ��� ��������� ����������� - �����

	// 3. �������� ����
	// ��������� ���� �  ���������� hWnd ������������� ���������� ����
	hWnd = CreateWindowEx(
		0,		// ����������� ����� ����
		szClassWindow,	//��� ������ ����
		TEXT("������ Windows ����������"), // ��������� ����
		WS_OVERLAPPEDWINDOW,				// ����� ����
		/* ���������, �����, ����������� ������ �������, ��������� ����, ������ ������������ � ���������� ����  */
		CW_USEDEFAULT,	// �-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// y-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// ������ ����
		CW_USEDEFAULT,	// ������ ����
		NULL,			// ���������� ������������� ����
		NULL,			// ���������� ���� ����
		hInst,		// ������������� ����������, ���������� ����
		NULL);		// ��������� �� ������� ������ ����������
	
	HWND b1 = CreateWindow(L"button", L"Open Dialog", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10, 10, 100, 30, hWnd, (HMENU)OPENDIALOGID, hInst, NULL);

	// 4. ����������� ����
	ShowWindow(hWnd, nCmdShow);
	ShowWindow(b1, nCmdShow);
	UpdateWindow(hWnd); // ����������� ����

	
	
	// 5. ������ ����� ��������� ���������
	while(GetMessage(&Msg, NULL, 0, 0)) // ��������� ���������� ��������� �� ������� ���������
	{
		TranslateMessage(&Msg);	// ���������� ���������
		DispatchMessage(&Msg);	// ��������������� ���������
	}
	return Msg.wParam;
}	



LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	switch(uMessage)
	{
		case WM_DESTROY: // ��������� � ���������� ���������
			PostQuitMessage(0); // ������� ��������� WM_QUIT
			break;
		case WM_COMMAND:
		{
			if (HIWORD(wParam) == BN_CLICKED) //control-defined notification code
			switch (LOWORD(wParam)) //control identifier
			{
			case OPENDIALOGID:
				/*MessageBox(NULL, TEXT("Text"), TEXT("Alert"), MB_OK);*/
				//DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(TestDialog), hWnd, DlgProc);//modal dialog - binding dialog box calling on the button click
				HWND dialog = CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(TestDialog), hWnd, DlgProc); //unmodal dialog 
				ShowWindow(dialog, SW_SHOW);//show unmodal dialog
				HWND buttonOK = GetDlgItem(dialog, IDOK);
				HWND buttonCancel = GetDlgItem(dialog, IDCANCEL);
				HWND button1 = GetDlgItem(dialog, IDC_BUTTON1);
				HWND textField = GetDlgItem(dialog, IDC_EDIT1);
				
			}
		 }
		break;
		default:
			// ��� ���������, ������� �� �������������� � ������ ������� ������� ������������ ������� Windows �� ��������� �� ���������
			return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}
	return 0;
}

/*BOOL*/DLGPROC CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)//��������� ������� � ���������� ����
{
	switch (message)
	{
	case WM_CLOSE:
		EndDialog(hWnd, 0); // ��������� ������
		return;// TRUE;
	case WM_COMMAND:
		if (HIWORD(wParam) == BN_CLICKED) //������� ������ �������
			switch (LOWORD(wParam)) //control identifier
			{
			case IDOK:
				EndDialog(hWnd, 0); //��������� ������
				return;// TRUE;
			case IDCANCEL:
				EndDialog(hWnd, 0); //��������� ������
				return;// FALSE;
			case IDC_BUTTON1:
			{
				HWND textField = GetDlgItem(hWnd, IDC_EDIT1);
				HWND staticField = GetDlgItem(hWnd, IDC_STATIC1);
				TCHAR text[260];
				GetWindowText(textField, text, 260);
				SetWindowText(staticField, text);
			}
			break;
			}
		if (HIWORD(wParam) == EN_CHANGE)//text field changing
		{
			switch (LOWORD(wParam)) //control identifier
			{
			case IDC_EDIT1:
			{
				HWND textField = GetDlgItem(hWnd, IDC_EDIT1);
				HWND staticField = GetDlgItem(hWnd, IDC_STATIC1);
				TCHAR text[260];
				GetWindowText(textField, text, 260);
				SetWindowText(staticField, text);
				if (lstrlen(text) > 0)
					EnableWindow(GetDlgItem(hWnd, IDC_BUTTON1), TRUE);
				else
					EnableWindow(GetDlgItem(hWnd, IDC_BUTTON1), FALSE);
			}
			break;
			}
		}
	}
	//return FALSE;
}