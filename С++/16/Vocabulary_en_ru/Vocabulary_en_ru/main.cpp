#include <iostream>
#include "Vocabulary.h"
#include "MyString.h"
#include "Word.h"
using std::ostream;
using std::cout;

//globaly overloaded output of String
ostream& operator << (ostream& os, const String &aString)
{
	if (aString.get_String() != nullptr)
		os << aString.str << " ";
	return os;
}

//globaly overloaded output of Word
ostream & operator << (ostream& os, const Word &aWord)
{
	os << aWord.en << "(popularity: " << aWord.counter << ") - " << aWord.rus;
	os << "\n";
	return os;
}

void main()
{
	setlocale(0, "RUS");
	Vocabulary A;
	A.add("dog", "������", 5);
	A.add("cat", "�����", 3);
	A.add("mouse", "����", 1);
	A.add("pig", "������", 10);
	A.add("cow", "������", 2);
	A.add("horse", "������", 1);
	A.add("rat", "�����", 7);
	A.add("tiger", "����", 15);
	A.print();
	std::cout << "___________________________\n";
	/*A.print_top3_popular();
	std::cout << "___________________________\n";
	A.print_top3_unpopular();
	std::cout << "___________________________\n";*/

	cout << "Testing search and output of word \"rat\":\n";
	A.show_word("rat");

	cout << "\nTesting translation changing:\n";
	A.set_translation("horse", "����");
	A.show_word("horse");

	cout << "\nTesting printing of TOP-3 popular words:\n";
	A.print_top3_popular();

	cout << "\nTesting printing of TOP-3 unpopular words:\n";
	A.print_top3_unpopular();
}