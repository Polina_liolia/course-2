#include "Personal_data.h"

Personal_data::Personal_data(string aSurname, string aName)
{
	surname = aSurname;
	name = aName;
}

void Personal_data::set_name(string aName)
{
	name = aName;
}

void Personal_data::set_surname(string aSurname)
{
	surname = aSurname;
}

string Personal_data::get_name() const
{
	return name;
}

string Personal_data::get_surname() const
{
	return surname;
}

bool Personal_data::operator>(const Personal_data & D)
{
	if (surname == D.surname)
		return name > D.name;
	else
		return surname > D.surname;

}

bool Personal_data::operator<(const Personal_data & D)
{
	if (surname == D.surname)
		return name < D.name;
	else
		return surname < D.surname;
}

