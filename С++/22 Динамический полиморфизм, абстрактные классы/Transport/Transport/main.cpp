#include"Auto_transport.h"
#include"Moto_transport.h"
#include"Overwather_transport.h"
#include"Underwather_transport.h"
#include"Aviation.h"
#include"Aerostation.h"



void main()
{
	Transport *T [3]; //creating a dinamic array of basic class instances
	//initializing it's elements with instances of child-classes:
	T[0] = new Wather_transport((string)"SL-23", 15000.0, 30.0, 1500);		
	T[1] = new Ground_transport((string)"GL-5", 3000.0, 240.0, km_h, 3000.0);
	T[2] = new Air_transport((string)"AN-2", 5000.0, 0.9, Mach, 1500.0);

	//testing virtual methods (methods of child classes are working):

	T[0]->get_speed_units();
	T[0]->print();
	cout << "\n";

	T[1]->get_speed_units();
	T[1]->print();
	cout << "\n";

	T[2]->get_speed_units();
	T[2]->print();
	cout << "\n";

	Ground_transport *GT[2];//creating a dinamic array of basic class instances
	//initializing it's elements with instances of child-classes:
	GT[0] = new Auto_transport((string)"Mercedes-Benz Axor", 6450.0, 150.0, km_h, 16000.0, truck);
	GT[1] = new Moto_transport((string)"ZF150ZK", 700.0, 70.0, km_h, 1000.0, tricicle);

	//testing virtual methods (methods of child classes are working):
	GT[0]->get_speed_units();
	GT[0]->print();
	cout << "\n";

	GT[1]->get_speed_units();
	GT[1]->print();
	cout << "\n";

	Wather_transport *WT[2];//creating a dinamic array of basic class instances
	//initializing it's elements with instances of child-classes:
	WT[0] = new Overwather_transport((string)"Royal Clipper", 200000.0, 13.5, 5061.0, outside_energy, knots);
	WT[1] = new Underwather_transport((string)"Yellow submarine", 110000.0, 45.3, 978.3, non_athom, knots);
	
	//testing virtual methods (methods of child classes are working):
	WT[0]->get_speed_units();
	WT[0]->print();
	cout << "\n";

	WT[1]->get_speed_units();
	WT[1]->print();
	cout << "\n";

	Air_transport *AT[2];//creating a dinamic array of basic class instances
	//initializing it's elements with instances of child-classes:
	AT[0] = new Aviation((string)"AN-2", 3500, 190, km_h, 4500.0, civil);
	AT[1] = new Aerostation((string)"Air baloon AX-7", 250, 10, km_h, 4000, 2180);

	//testing virtual methods (methods of child classes are working):
	AT[0]->get_speed_units();
	AT[0]->print();
	cout << "\n";

	AT[1]->get_speed_units();
	AT[1]->print();
	cout << "\n";
}