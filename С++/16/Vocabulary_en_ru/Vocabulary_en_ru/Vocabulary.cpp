#include "Vocabulary.h"
#include "Element.h"
#include <iostream>
using namespace std;
Vocabulary::Vocabulary()	//default constructor
{
	root = nullptr;
}
Vocabulary::~Vocabulary()	//destructor
{
	delete root;
}

void Vocabulary::add(String En, String Rus, int Counter)	//to add element (word) to binar tree (vocabulary)
{
	if (!root)
		root = new Element(En, Rus, Counter);
	else
	{
		Element *tmp = root;
		while (tmp)
		{
			if (En < tmp->get_en())	//comparing words
				if (tmp->get_left() == nullptr)
				{
					tmp->set_left(new Element(En, Rus, Counter));
					break;
				}
				else tmp = tmp->get_left();
			else {
				if (En > tmp->get_en())
					if (tmp->get_right() == nullptr)
					{
						tmp->set_right(new Element(En, Rus, Counter));
						break;
					}
					else tmp = tmp->get_right();
				else tmp = NULL;
			}
		}
	}
}

void Vocabulary::print()	//to print all elements 
{
	print_helper_straight(root);
}

void Vocabulary::print_top3_popular()	//to print 3 most popular (depending on calls amount) elements
{
	Word max("max", "max", 0);	//default word for initialization
	//array with 3 most popular words:
	Word *top = new Word[3];
	top[0] = top[1] = top[2] = max;
	print_helper_popular(root, top);
	cout << top[0] << top[1] << top[2];	//outputting result to console
}


Word *Vocabulary::print_helper_popular(Element * el, Word *top)
{
	if (el->get_value() > top[0])	//setting the most popular element
	{
		top[2] = top[1];
		top[1] = top[0];
		top[0] = el->get_value();
	}
	else if (el->get_value() > top[1] && el->get_value().get_en() != top[0].get_en())	//setting element on the 2-nd popularity position
	{
		top[2] = top[1];
		top[1] = el->get_value();
	}
	//setting element on the 3-rd popularity position:
	else if (el->get_value() > top[2] && el->get_value().get_en() != top[0].get_en() && el->get_value().get_en() != top[1].get_en())
		top[2] = el->get_value();
	if (el->get_right())
		top = print_helper_popular(el->get_right(), top);
	if (el->get_left())
		top = print_helper_popular(el->get_left(), top);
	return top;	//returns array with 3 most popular words
}


void Vocabulary::print_top3_unpopular()	//to print 3 most unpopular (depending on calls amount) elements
{
	Word max("min", "min", 0);	//default word for initialization
	Word *top = new Word[3];	//array with 3 most unpopular words:
	top[0] = top[1] = top[2] = max;
	print_helper_unpopular(root, top);
	cout << top[0] << top[1] << top[2];	//outputting result to console
}

Word *Vocabulary::print_helper_unpopular(Element * el, Word *top)
{
	if (el->get_value() < top[0] || top[0].get_en() == "min")	//setting the most unpopular element
	{
		top[2] = top[1];
		top[1] = top[0];
		top[0] = el->get_value();
	}
	//setting element on the 2-nd unpopularity position:
	else if ((el->get_value() < top[1] && el->get_value().get_en() != top[0].get_en()) || top[1].get_en() == "min")	
	{
		top[2] = top[1];
		top[1] = el->get_value();
	}
	//setting element on the 3-rd unpopularity position:
	else if ((el->get_value() < top[2] && el->get_value().get_en() != top[0].get_en() && el->get_value().get_en() != top[1].get_en()) || top[0].get_en() == "min")
		top[2] = el->get_value();
	if (el->get_right())
		top = print_helper_unpopular(el->get_right(), top);
	if (el->get_left())
		top = print_helper_unpopular(el->get_left(), top);
	return top;	//returns array with 3 most unpopular words
}

void Vocabulary::show_word(String word)	//to find and print pointed word
{
	show_word_helper(root, word);
}

void Vocabulary::show_word_helper(Element * el, String word)
{
	if (el->get_en() == word)
	{
		cout << el->get_value();
		return;
	}
	else
	{
		if (el->get_right())
			show_word_helper(el->get_right(), word);
		if (el->get_left())
			show_word_helper(el->get_left(), word);
	}
}


void Vocabulary::set_translation(String en, String ru)	//to find pointed word and set / change translation for it
{
	set_transl_helper(root, en, ru);
}

void Vocabulary::set_transl_helper(Element * el, String en, String ru)
{
	if (el->get_en() == en)
	{
		el->set_translation(ru);
		return;
	}
	else
	{
		if (el->get_right())
			set_transl_helper(el->get_right(), en, ru);
		if (el->get_left())
			set_transl_helper(el->get_left(), en, ru);
	}
}


void Vocabulary::print_helper_simmetric(Element * el)	//min - root - max  (to print 3 unpopular words)
{
	if (el->get_left() )
		print_helper_simmetric(el->get_left());
	cout << el->get_value() << " ";
	if (el->get_right())
		print_helper_simmetric(el->get_right());
}

void Vocabulary::print_helper_straight(Element * el)	//root - min - max	(to print all vocabulary)
{
	cout << el->get_value() << " ";
	if (el->get_left())
		print_helper_straight(el->get_left());
	if (el->get_right())
		print_helper_straight(el->get_right());
}

void Vocabulary::print_helper_reversed(Element * el)	//root - max - min	(to print 3 popular words)
{
	cout << el->get_value() << " ";
	if (el->get_right())
	{
		print_helper_reversed(el->get_right());
	}
	if (el->get_left())
	{
		print_helper_reversed(el->get_left());
	}
}


