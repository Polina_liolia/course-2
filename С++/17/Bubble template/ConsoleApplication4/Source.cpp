#include <iostream>
using namespace std;

template <typename T, int size>
void bubleSort(T (&arr)[size]);

void main() {

	const int sz = 5;
	int arr[sz] = { 7, 4 , 6, 3, 1 };

	double arr2[sz] = { 7.5, 4.2 , 6.5, 3.7, 11.67 };

	for (int i = 0; i < sz; i++) {
		cout << " " << arr[i];
	}

	cout << endl << "========================" << endl;

	bubleSort(arr);
	
	cout << "=================================" << endl;
	for (int i = 0; i < sz; i++)
		cout << " " << arr[i];
	cout << endl;

	bubleSort(arr2);

	cout << "=================================" << endl;
	for (int i = 0; i < sz; i++)
		cout << " " << arr2[i];
	cout << endl;
}

template <typename T, int size>
void bubleSort(T (&arr)[size])
{
	for (int b = 0; b < size - 1; b++) {
		for (int i = 0; i < size - 1; i++) {
			if (arr[i] > arr[i + 1]) {
				T j = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = j;
			}
		}
	}
}
