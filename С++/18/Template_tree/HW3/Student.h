#pragma once
#include <iostream>
#include <cstring>
using std::cout;
using std::cin;
using std::ostream;
#include "Birth_date.h"
#include "MyString.h"


class Student
{
private:
	String surname;
	String name;
	String patronymic;
	birth_date birth;
	String phone;
	String univercity;
	String country;
	String city;
	String group;
	
public:
	Student();

	//overloaded constructor:
	Student(String aSurname, String aName, String aPatronymic, short aDay, short aMonth, short aYear, String aPhone, String aUnivercity, String aCountry, String aCity, String aGroup);
	Student(const Student &a);
	~Student();

public:
	//methods to set the meaning of every property:

	void set_surname(String aSurname);
	void set_name(String aName);
	void set_patronymic(String aPatronymic);
	void set_birthdate(short aDay, short aMonth, short aYear)
	{
		birth.day = aDay;
		birth.month = aMonth;
		birth.year = aYear;
	}
	void set_phone(String aPhone);
	void set_univercity(String aUnivercity);
	void set_country(String aCountry);
	void set_city(String aCity);
	void set_group(String aGroup);
	int print_to_file(FILE *f, char* file_name);

	//methods to get the meaning of every property:
	String get_surname()
	{
		return surname;
	}

	String get_name()
	{
		return name;
	}

	String get_patronymic()
	{
		return patronymic;
	}

	int get_birth_day()
	{
		return birth.day;
	}

	int get_birth_month()
	{
		return birth.month;
	}

	int get_birth_year()
	{
		return birth.year;
	}

	String get_phone()
	{
		return phone;
	}

	String get_univercity()
	{
		return univercity;
	}

	String get_country()
	{
		return country;
	}

	String get_city()
	{
		return city;
	}

	String get_group()
	{
		return group;
	}

	void print_data();		//to print information about student

	Student operator=(Student a);
	bool operator==(Student a);
	bool operator!=(Student a);
	bool operator>(Student a);
	bool operator<(Student a);
	bool operator>=(Student a);
	bool operator<=(Student a);

	friend ostream& operator << (ostream& os, const Student &aSt);
};