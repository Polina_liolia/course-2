#pragma once
#include"Table.h"
#include<fstream>

class Reservations_system
{
private:
	int tables_IDs_counter;
	int tables_size;
	Table *all_tables;

public:
	Reservations_system();
	Reservations_system(const Reservations_system &aSystem);
	~Reservations_system();

public:
	int get_tables_IDs_counter()const;
	int get_tables_size()const;
	Table *get_all_tables();
	void add_table(int aSitting_places);
	int get_table_index_by_ID(int aID)const;
	void remove_table(int aID);

	void edit_table(int aID, int aSitting_places);
	void set_table_status(int aID, table_status aStatus, int aReserve_duration = 0, string aName = string(), string aPhone = string());
	int get_sitting_places(int aID)const;
	table_status get_status(int aID)const;
	int get_reserve_duration(int aID)const;
	string get_reservator_name(int aID)const;
	string get_reservator_phone(int aID)const;
	void remove_reserve(int aID);
	bool check_reserve(int aID);
	void print_to_file(char* file_name);
	void read_from_file(char* file_name);

	friend ostream& operator<<(ostream& os, Reservations_system& rs);
};