#pragma once

class Element
{
	int value;
	Element* next;
	Element* prev;

public:
	Element();
	Element(int aValue);
	void set_value(int aValue);
	int get_value();
	void set_next(Element* aNext);
	void set_prev(Element* aPrev);
	Element* get_next();
	Element* get_prev();



};