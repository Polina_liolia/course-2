#pragma once
#include"Air_transport.h"
enum aviation_type { military, civil };

class Aviation : public Air_transport
{
private:
	aviation_type type;

public:
	Aviation();
	Aviation(string aModel, double aWeight, double aMax_speed, speed aSpeed_units, double aMax_height, aviation_type aType);
	~Aviation() {};

public:
	void set_type(aviation_type aType);
	aviation_type get_type()const;

	virtual speed get_speed_units()const;
	virtual void print();
};