#pragma once
#include <iostream>
using std::cout;
using std::endl;

class functor
{
	int a;
	int b;
public:
	functor();
	bool operator()(int, int);
	void show();
};