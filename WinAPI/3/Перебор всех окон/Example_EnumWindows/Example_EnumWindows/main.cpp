
#include <windows.h>
#include "resource.h"


HINSTANCE hInstance;

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) ;
BOOL CALLBACK DialogProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) ;
BOOL CALLBACK EnumChildProc(HWND hwndChild, LPARAM lParam);
BOOL CALLBACK EnumWindowsProc(HWND hwnd,LPARAM lParam);


int WINAPI WinMain(
HINSTANCE hInstance,    // ���������� �������� ���������� ����
HINSTANCE hPrevInstance,// ���������� ����������� ���������� ����
LPSTR lpCmdLine,        // ��������� �� ��������� ������
int nCmdShow= SW_SHOWMINIMIZED           // ���������� ��������� ����
)
{
	MSG uMsg; 
	HWND hMainWnd; 
	TCHAR szClassName[] = L"MyClass"; 
	
	WNDCLASSEX wcex; // ���������� ���������� ���� ��������� �������� ������

	wcex.cbSize = sizeof(WNDCLASSEX); // ������ ��������� � ������
	wcex.style		= CS_HREDRAW | CS_VREDRAW;	// ����� ����
	wcex.lpfnWndProc	= (WNDPROC)WndProc; //����� ������� ���������
	wcex.cbClsExtra		= 0;						
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;		// ��������� ����������
	wcex.hIcon			= LoadIcon(hInstance,MAKEINTRESOURCE(IDI_ICON1));		// ����������� ������
	wcex.hCursor		= LoadCursor(hInstance, MAKEINTRESOURCE(IDC_CURSOR1)); // ����������� �������
	//wcex.hCursor		= LoadCursor(NULL, IDC_ARROW); // ����������� �������
	wcex.hbrBackground	= GetSysColorBrush(COLOR_BTNFACE); // ��������� ����
	wcex.lpszMenuName	= NULL;		// ����������� ����
	wcex.lpszClassName	= szClassName;	// ��� ������
	wcex.hIconSm		= NULL; //����������� ��������� ������

	DWORD dw = GetLastError();

	// ������������ ����� ���� 
	if (!RegisterClassEx(&wcex)) { 
		MessageBox(NULL, L"Cannot register class", L"Error", MB_OK); 
		return 0; 
	}
	
	// ������� �������� ���� ���������� 
	hMainWnd = CreateWindow( 
	szClassName, L"A Hellol Application", WS_OVERLAPPEDWINDOW, 
	CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, 
	(HWND)NULL, (HMENU)NULL, 
	(HINSTANCE)hInstance, NULL 
	); 

	// ���������� ���� ���� 
	ShowWindow(hMainWnd, nCmdShow); 
	// UpdateWindow(hMainWnd); // ��. ���������� � ������� "����������� ����.. 

	// ��������� ���� ��������� ��������� �� �������� ���������� 
	while (GetMessage(&uMsg, NULL, 0, 0)) { 
	TranslateMessage(&uMsg); 
	DispatchMessage(&uMsg); 
	}

	return 0;

}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) 
{ 
    PAINTSTRUCT ps; 
    HDC hdc; 
	HINSTANCE hInst;
	RECT rcClient; 


	hInst = hInstance;


 
    switch (uMsg) 
    { 
	case WM_CREATE:
		{
			CreateWindow (L"button", L"CHILD", WS_CHILD | WS_VISIBLE |BS_PUSHBUTTON, 10, 10, 100, 30, hWnd, (HMENU) 1, hInst, NULL);
			CreateWindow (L"button", L"TOP-LEVEL", WS_CHILD | WS_VISIBLE |BS_PUSHBUTTON, 120, 10, 100, 30, hWnd, (HMENU) 2, hInst, NULL);
			CreateWindow (L"button", L"EDIT3", WS_CHILD | WS_VISIBLE |BS_PUSHBUTTON, 230, 10, 100, 30, hWnd, (HMENU) 3, hInst, NULL);

		}
	

		break;
        case WM_COMMAND:
		{	
			int c;
			HWND hCommWnd;
			int ID;

			//������ � ���������� �������� �������������� ���������
			ID = LOWORD( wParam ) ;//�������������
			hCommWnd = HWND ( lParam );//��������� ����\������, �� �-��� ������ ���������
			c = HIWORD (wParam);//�����������

			switch ( ID )
			{
			case 1:
				{
					GetClientRect(hWnd, &rcClient); 

					EnumChildWindows(hWnd, EnumChildProc, (LPARAM) &rcClient); 
					return 0;
				}
				break;
			case 2:
				{
					EnumWindows(EnumWindowsProc,0);
					return 0;
				}break;
			}
			
		}
			break;
         case WM_DESTROY: 
          
            PostQuitMessage(0); 
		
            break; 
 
        default: 
            return DefWindowProc(hWnd, uMsg, wParam, lParam); 
    } 
 
    return FALSE; 
}

int i = -1;
BOOL CALLBACK EnumChildProc(HWND hwndChild, LPARAM lParam) 
{ 

	LPRECT rcParent; 
     
	i++; 
    rcParent = (LPRECT) lParam; 
    MoveWindow(hwndChild, 
               (rcParent->right / 3) * i, 
               0, 
               rcParent->right / 3, 
               rcParent->bottom, 
               TRUE); 
 
    ShowWindow(hwndChild, SW_SHOW); 
 
    return TRUE;
}


BOOL CALLBACK EnumWindowsProc(HWND hwnd,LPARAM lParam)
{
	ShowWindow(hwnd, SW_SHOWMAXIMIZED);
	//SetWindowText(hwnd,L"qwedcqwecqwec");
	//SendMessage(hwnd, WM_DESTROY,0,0);
	return true;
}
