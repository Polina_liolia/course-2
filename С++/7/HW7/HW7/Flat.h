#pragma once

class flat
{
private:
	char *address;
	float square;
	float price;

public:
	flat(char *aAddress = nullptr, float aSquare = 0, float aPrice = 0);
	flat(const flat &aFlat);
	~flat();

public:
	void set_address(char *aAddress);
	inline void set_square(float aSquare);
	inline void set_price(float aPrice);
	inline char *get_address()const;
	inline float get_square()const;
	inline float get_price()const;
	inline void print()const;
	bool operator==(const flat &aFlat)const;
	flat operator=(const flat &aFlat);
	bool operator>(const flat &aFlat)const;



};