#include "String.h"
using std::cout;

void main()
{
	String s;
	
	String s1 = "abc";
	cout << "s1 ";
	s1.print();

	String s2 = s1;
	cout << "s2 ";
	s2.print();

	s = "abc";
	cout << "s ";
	s.print();

	s += "def";
	cout << "s ";
	s.print();

	s = s1 + s2;
	cout << "s ";
	s.print();

	s = s1 + "abc";
	cout << "s ";
	s.print();

	s += "a";
	cout << "s ";
	s.print();

	bool flag = false;
	
	if (s == "abc")
		flag = true;
	else 
		flag = false;
	cout << flag <<"\n";

	if (s != "abc")
		flag = true;
	else
		flag = false;
	cout << flag << "\n";

	if(s1 > s2)
		flag = true;
	else
		flag = false;
	cout << flag << "\n";

	if (s1 > "ab")
		flag = true;
	else
		flag = false;
	cout << flag << "\n";

	if (s1 < s2)
		flag = true;
	else
		flag = false;
	cout << flag << "\n";

	if (s1 < "ab")
		flag = true;
	else
		flag = false;
	cout << flag << "\n";

}