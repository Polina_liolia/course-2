function loadXMLDoc(filename)
{
    if (window.ActiveXObject)
    {
        var xhttp = new ActiveXObject("Msxml2.XMLHTTP");
    }
    else
    {
        var xhttp = new XMLHttpRequest();
    }
    xhttp.open("GET", filename, false);
    try {xhttp.responseType = "msxml-document"} catch(err) {} // Helping IE11
    xhttp.send();
    return xhttp.responseXML;
}

function displayResult()
{
    var xml = loadXMLDoc("students.xml");
    var xsl = loadXMLDoc("style.xsl");
// code for IE
//            if (window.ActiveXObject || xhttp.responseType == "msxml-document")
//            {
//              var ex = xml.transformNode(xsl);
//                document.getElementById("example").innerHTML = ex;
//            }
// code for Chrome, Firefox, Opera, etc.
    if (document.implementation && document.implementation.createDocument)
    {
        var xsltProcessor = new XSLTProcessor();
        xsltProcessor.importStylesheet(xsl);
        var resultDocument = xsltProcessor.transformToFragment(xml, document);
        document.getElementById("example").appendChild(resultDocument);
    }
}
displayResult();