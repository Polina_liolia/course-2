#pragma once
#include <iostream>

class Point2D
{
private:
	char*name;
	int x;
	int y;

public:
	Point2D();
	Point2D(const Point2D &a);
	~Point2D();

public:
	void set_name(char* Iname);
	void set_coordinateX(int Ix);
	void set_coordinateY(int Iy);
	bool operator== (const Point2D &aPoint);
	bool operator!= (const Point2D &aPoint);
};
