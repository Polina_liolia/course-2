#include "Reserve.h"

Reserve::Reserve()	//default constructor
{
	reserve_duration_min = 0;
}

Reserve::Reserve(string aName, string aPhone, int aDuration)	//overloaded constructor
{
	reservator_name = aName;
	reservator_phone = aPhone;
	reserve_duration_min = aDuration;
	reserve_start.set_current_time();
}

void Reserve::set_reserve(string aName, string aPhone, int aDuration)	//method to set reservatoin data
{
	reservator_name = aName;
	reservator_phone = aPhone;
	reserve_duration_min = aDuration;
	reserve_start.set_current_time();
}

void Reserve::set_reserve_duration(int aReserve_duration_min)	//method to set reserve duration in minutes
{
	reserve_duration_min = aReserve_duration_min;
}

void Reserve::set_reservator_name(string aName)	//method to set name of reservator
{
	reservator_name = aName;
}

void Reserve::set_reservator_phone(string aPhone) //method to set phone number of reservator
{
	reservator_phone = aPhone;
}

int Reserve::get_reserve_duration() const		//method to get reserve duration in minutes
{
	return reserve_duration_min;
}

string Reserve::get_reservator_name() const		//method to get name of reservator
{
	return reservator_name;
}

string Reserve::get_reservator_phone() const	 //method to get phone number of reservator
{
	return reservator_phone;
}

void Reserve::remove_reserve()	 //method to remove reserve by setting all properties with null-meanings
{
	reserve_start.set_hours(0);
	reserve_start.set_minutes(0);
	reserve_duration_min = 0;
	reservator_name = string();
	reservator_phone = string();
}

//to check, is current reserve still actual 
//returns true if actual, false - if not; 
//removes unactual reserve
bool Reserve::check_reserve()	
{
	//checking if reservation has been set:
	if (reserve_duration_min == 0)
		return false;	//reserve has not been set
	//if reserve has been set, checking if it is still actual:
	//getting current time:
	struct tm newtime;
	__time64_t long_time;
	errno_t err;
	// Get time as 64-bit integer: 
	_time64(&long_time);
	// Convert to local time:
	err = _localtime64_s(&newtime, &long_time);
	if (err)
	{
		throw "Invalid argument to _localtime64_s\n";
	}
	//setting current time to class properties:
	int current_time_minutes = (newtime.tm_hour) * 60 + (newtime.tm_min);
	if (reserve_start.get_time_in_minutes() >= ((60 * 24) - reserve_duration_min))	//if reservation time is around 00:00
	{
		if (current_time_minutes > reserve_start.get_time_in_minutes())	//if it is before 00:00 at clock
			return true;	//reserve time is ended
		else if (current_time_minutes > (reserve_duration_min - (60 * 24 - reserve_start.get_time_in_minutes())))	//if it is after 00:00 at clock
			return false;	//reserve time is ended
		else
			return true;	//reserve is actual
	}
	if (current_time_minutes > (reserve_start.get_time_in_minutes() + reserve_duration_min))
	{
		remove_reserve();
		return false; //reserve time is ended
	}	
	return true;	//reserve is actual
}

//saves data of Reserve instance to file; takes the reffer of ofstream instance (file has to be already opened)  
void Reserve::print_to_file(ofstream & F) const
{
	if (!F)
		throw "Reserve: file was not opened, can't write";
	
	try
	{
		reserve_start.print_to_file(F);							//reserve_start writing
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}
	size_t var = reserve_duration_min;
	F.write(reinterpret_cast<char*>(&var), sizeof(int));	//reserve_duration_min writing
	
	
	string buf = reservator_name;
	var = buf.size();
	F.write(reinterpret_cast<char*>(&var), sizeof(int));	 
	F.write(reinterpret_cast<char*>(&buf[0]), var);					//reservator_name writing
	
	buf = reservator_phone;
	var = buf.size();
	F.write(reinterpret_cast<char*>(&var), sizeof(int));	
	F.write(reinterpret_cast<char*>(&buf[0]), var);					//reservator_phone writing
}	

//reads data of Reserve instance from file; takes the reffer of ofstream instance (file has to be already opened)  
void Reserve::read_from_file(ifstream & F)
{
	if (!F)
		throw "Reserve: file was not opened, can't read";

	try 
	{
		reserve_start.read_from_file(F);										//reserve_start reading
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}
	F.read(reinterpret_cast<char*>(&reserve_duration_min), sizeof(int));	//reserve_duration_min reading

	int buf;
	F.read(reinterpret_cast<char*>(&buf), sizeof(int));
	reservator_name.resize(buf);
	F.read(reinterpret_cast<char*>(&reservator_name[0]), buf);					//reservator_name str reading

	F.read(reinterpret_cast<char*>(&buf), sizeof(int));
	reservator_phone.resize(buf);
	F.read(reinterpret_cast<char*>(&reservator_phone[0]), buf);				//reservator_phone reading
}
