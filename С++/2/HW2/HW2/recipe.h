#pragma once
#include "ingredient.h"

class recipe
{
private:
	char *name;
	char *type;
	char *process;
	int sz_components;
	ingredient **components;

public:
	recipe();
	recipe(char *name, char *type, char *process);
	~recipe();

public:
	inline char *get_name();
	inline char *get_type();
	inline char *get_process();
	void set_name(char *aName);
	void set_type(char *aType);
	void set_process(char *aProcess);
	void set_ingridient(char *aItem, float aDose);
	void del_ingridient(int ingredient_number);
	int save_recipe_to_file(FILE *f, char *file_name);
	void print_recipe_name();
	void print_recipe();
	void print_ingredients();
};
