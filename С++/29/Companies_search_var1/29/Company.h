#pragma once
#include <iostream>
#include <set>
#include <string>
using std::string;
using std::set;
using std::cout;
using std::ostream;

enum accaunt { Free, silver, gold, platinum };

class Company
{
private:
	string name;
	string description;
	accaunt type;
	set<string> key_words;
	int key_words_found;
	int key_words_in_description;

public:
	Company();
	Company(string aName, string aDescription, accaunt aType = Free);
	~Company() {}

	void set_name(string aName);
	void set_description(string aDescription);
	void set_type(accaunt aType);
	void add_key_word(string aKey_word);
	void change_key_word(string aWord_old, string aWord_new);
	void set_key_words_found(int amount);
	void set_key_words_in_description(int amount);

	string get_name()const;
	string get_description()const;
	accaunt get_type()const;
	int get_key_words_found()const;
	int get_key_words_in_description()const;
	
	bool find_key_word(string aKey_word);
	bool find_key_word_in_description(string aKey_word);

	friend ostream& operator<<(ostream& os, Company& C);
};