#pragma once
#include "Element.h"
class SingleLinkList
{
	Element* head;
	Element* tail;
public:
	SingleLinkList();
	void add(int aValue);
	void show();
	bool delByIndex(int index);
	~SingleLinkList();
	bool insertByIndex(int index, int value);

};