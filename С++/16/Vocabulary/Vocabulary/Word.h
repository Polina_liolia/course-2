#pragma once
#include"MyString.h"

class Word
{
private:
	String en;		//word in english
	int size_rus;
	String *rus;	//word in russian
	int counter;	//counter of word's calls

public:
	Word();
	Word(String aEn, String aRus, int aCounter);
	Word(const Word &aWord);
	~Word();

public:
	void set_word(String aEn, String aRus, int aCounter);
	void add_rus(String aRus);
	void del_rus(String aRus);
	void del_rus(int number);
	void change_rus(String aRus_old, String aRus_new);
	void change_rus(int number, String aRus_new);
	int get_counter()const;
	void add_count();

	friend ostream & operator << (ostream& os, const Word &aWord);
};

