#include "Graduate.h"

Graduate::Graduate(){}

Graduate::Graduate(string aName, string aSurname, date aBirth_day, string aAdress, string aSchool, string aUnivercity, string aSpeciality, string aDepartment, string aPhone)
	:Student::Student(aName, aSurname, aBirth_day, aAdress, aSchool, aUnivercity, aSpeciality, aPhone), department(aDepartment)
{}


Graduate::~Graduate(){}

void Graduate::set_department(string aDepartment)
{
	department = aDepartment;
}

string Graduate::get_department() const
{
	return department;
}

void Graduate::add_scientific_interest(string aInterest)
{
	scientific_interests.insert(aInterest);
}

void Graduate::remove_scientific_interest(string aInterest)
{
	scientific_interests.erase(aInterest);
}

Graduate Graduate::operator=(const Graduate & G)
{
	Student::operator=(G);
	department = G.department;
	scientific_interests = G.scientific_interests;
	return *this;
}

void Graduate::print_to_file(char * file_name)
{
	Student::print_to_file(file_name);
	ofstream F(file_name, ios::binary | ios::app);
	// checking if file was successfully opened, othervice generating exception:
	if (!F)
		throw "Graduate: Can't open file for writing";
	int buf = department.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));
	F.write(reinterpret_cast<char*>(&department[0]), buf);

	buf = scientific_interests.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));

	for (auto i : scientific_interests)
	{
		buf = i.size();
		F.write(reinterpret_cast<char*>(&buf), sizeof(int));
		F.write(reinterpret_cast<char*>(&i[0]), buf);
	}
	F.close();
}

void Graduate::read_from_file(char * file_name)
{
	Student::read_from_file(file_name);
	ifstream F(file_name, ios::binary | ios::app);
	// checking if file was successfully opened, othervice generating exception:
	if (!F)
		throw "Graduate: Can't open file for reading";
	int buf;
	F.read(reinterpret_cast<char*>(&buf), sizeof(int));
	department.resize(buf);
	F.read(reinterpret_cast<char*>(&department[0]), buf);

	int interests_sz;
	F.read(reinterpret_cast<char*>(&interests_sz), sizeof(int));
	for (int i = 0; i < interests_sz; i++)
	{
		string tmp;
		F.read(reinterpret_cast<char*>(&buf), sizeof(int));
		tmp.resize(buf);
		F.read(reinterpret_cast<char*>(&tmp[0]), buf);
		scientific_interests.insert(tmp);
	}
	F.close();
}
