#pragma once
#include <iostream>
#include "Functions.h"

class Array
{
private:
	int *arr;
	int sz;

public:
	Array();
	Array(int *aArr, int aSz);
	Array(const Array &aArr);
	~Array();

public:
	inline int get_sz()const;
	inline int get_element(int index)const;
	void print();
	Array operator= (const Array &A);
	Array operator= (int a);
	Array operator+ (const Array &A);
	Array operator+ (int a);
	Array operator+= (const Array &A);
	Array operator+= (int a);
	friend void find_numbers(Array A, int start, int end, search_type aSearch_type = line, sort_type aSort_type = bubble);

};

