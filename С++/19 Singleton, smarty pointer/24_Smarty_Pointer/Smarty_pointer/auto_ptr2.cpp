#include <memory>
#include <iostream>
using namespace std;

class A{
public:
	~A(){
		cout << "Destructor A!!!!!" << endl;
	}
	void method1(){
	}
};

auto_ptr<A> creator(){

	auto_ptr<A> ptr(new A);
	return ptr;
}

void main(){
	auto_ptr<A> ptr = creator();
	ptr->method1();
	cout << "=================="<<endl;
	
}