#include "Point3D.h"
#include "Point.h"
Point3D::Point3D()
{
	x = y = z = 0;
}
Point3D::Point3D(int t)
{
	x = y = z = t;
}
Point3D::Point3D(int ax, int ay, int az)
{
	x = ax;
	y = ay;
	z = az;
}

Point3D::Point3D(Point p){
	x = p.x;
	y = p.y;
	z = 0;

	cout << "overload constructor (conversion)\n";
}

void Point3D::show()
{
	cout << "x = " << x << 
		    "   y = " << y << 
			"   z = " << z  << endl;
}

int Point3D::getX()const{
	return x;
}

int Point3D::getY()const{
	return y;
}

int Point3D::getZ()const{
	return z;
}
/*
Point3D operator=(const Point& p){
cout << "Point3D operator=(const Point& p)\n";
return Point3D(p.x, p.y, 0);
}
*/