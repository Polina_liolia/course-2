#include "String.h"

String::String()									//1
{
	sz = 0;
	str = nullptr;
}

String::String(char *aStr)							//2.1
{
	if (aStr && strlen(aStr))
	{
		sz = strlen(aStr) + 1;
		str = new char[sz];
		strcpy_s(str, sz, aStr);
	}
	else
	{
		sz = 0;
		str = nullptr;
	}
}

String::String(const String &aString)				//2.2
{
	if (aString.str && aString.sz)
	{
		sz = aString.sz;
		str = new char[sz];
		strcpy_s(str, sz, aString.str);
	}
	else
	{
		sz = 0;
		str = nullptr;
	}
}

String::~String()
{
	delete[] str;
}

void String::set_string(char *aStr)
{
	if (str)
		delete[] str;
	if (aStr && strlen(aStr))
	{
		sz = strlen(aStr) + 1;
		str = new char[sz];
		strcpy_s(str, sz, aStr);
	}
	else
	{
		sz = 0;
		str = nullptr;
	}
}

inline char *String::get_string()const 
{
	return str;
}

inline int String::get_stringlen()const
{
	return sz;
}

String String::operator=(const String &aString)		//3
{
	if (str)
		delete[] str;
	if (aString.str && aString.sz)
	{
		sz = aString.sz;
		str = new char[sz];
		strcpy_s(str, sz, aString.str);
	}
	else
	{
		sz = 0;
		str = nullptr;
	}
	return *this;
}

String String::operator+=(const String &aString)	//4
{
	if (aString.str && aString.sz)
	{
		if (str && sz)
		{
			char *temp = new char[sz + aString.sz - 1];
			strcpy_s(temp, sz, str);
			sz += aString.sz - 1;
			strcat_s(temp, sz, aString.str);
			delete[] str;
			str = new char[sz];
			str = temp;
		}
		else
		{
			sz = aString.sz;
			str = new char[sz];
			strcpy_s(str, sz, aString.str);
		}
	}
	return *this;
}

String String::operator+=(char a)					//6
{
	if (a)
	{
		sz++;
		char *temp = new char[sz];
		if (str)
		{
			strcpy_s(temp, sz - 1, str);
			temp[sz - 2] = a;
			temp[sz - 1] = '/0';
			delete[] str;
			str = new char[sz];
			str = temp;
		}
		else
		{
			str = new char[1];
			str[0] = a;
		}
	}
	return *this;
}

String String::operator+(const String &aString)		//5.1
{
	if (aString.str && aString.sz)
	{
		if (str && sz)
		{
			char *temp = new char[sz + aString.sz - 1];
			strcpy_s(temp, sz, str);
			int sz_tmp = sz + aString.sz - 1;
			strcat_s(temp, sz_tmp, aString.str);
			String tmp(temp);
			return tmp;
		}
		else
			return aString;
	}
	else
		return *this;
}

String String::operator+(char *aStr)				//5.2
{
	if (aStr)
	{
		if (str && sz)
		{
			int sz_tmp = sz + strlen(aStr);
			char *temp = new char[sz_tmp];
			strcpy_s(temp, sz, str);
			strcat_s(temp, sz_tmp, aStr);
			String tmp(temp);
			return tmp;
		}
		else
		{
			String tmp(aStr);
			return tmp;
		}
	}
	else
		return *this;
}

bool String::operator==(const String &aString)		//7.1
{
	if (!str && !aString.str)
		return false;
	if (strcmp(str, aString.str) == 0)
		return true;
	return false;
}

bool String::operator==(char *aStr)					//8.1
{
	if (!str && !aStr)
		return false;
	if (strcmp(str, aStr) == 0)
		return true;
	return false;
}

bool String::operator!=(const String &aString)		//7.2
{
	if (!str && !aString.str)
		return false;
	if (strcmp(str, aString.str) != 0)
		return true;
	return false;
}

bool String::operator!=(char *aStr)					//8.2
{
	if (!str && !aStr)
		return false;
	if (strcmp(str, aStr) != 0)
		return true;
	return false;
}

bool String::operator>(const String &aString)		//9.1
{
	if (!str && !aString.str)
		return false;
	if (strcmp(str, aString.str) > 0)
		return true;
	return false;
}

bool String::operator>(char *aStr)					//9.2
{
	if (!str && !aStr)
		return false;
	if (strcmp(str, aStr) > 0)
		return true;
	return false;
}

bool String::operator<(const String &aString)		//9.3	
{
	if (!str && !aString.str)
		return false;
	if (strcmp(str, aString.str) < 0)
		return true;
	return false;
}

bool String::operator<(char *aStr)					//9.4
{
	if (!str && !aStr)
		return false;
	if (strcmp(str, aStr) < 0)
		return true;
	return false;
}

void String::print()
{
	std::cout << str << "\n";
}

char& String::operator[](int aIndex)
{
	return str[aIndex];
}