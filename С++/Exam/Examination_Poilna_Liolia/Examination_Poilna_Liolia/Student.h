#pragma once
#include "Schoolboy.h"
class Student :
	public Schoolboy
{
protected:
	string univercity;
	string speciality;

public:
	Student();
	Student(string aName, string aSurname, date aBirth_day, string aAdress, string aSchool, string aUnivercity, string aSpeciality, string aPhone = "");
	~Student();

	void set_univercity(string aUnivercity);
	void set_speciality(string aSpeciality);

	string get_univercity()const;
	string get_speciality()const;

	virtual Student operator=(const Student &S);

	friend ostream& operator << (ostream& os, Student &S);
	friend istream& operator >> (istream& is, Student &S);

	void print_to_file(char* file_name);
	void read_from_file(char* file_name);
};

