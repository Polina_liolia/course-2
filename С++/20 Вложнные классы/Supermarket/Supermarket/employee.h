#pragma once
#include  "MyString.h"
#include "date.h"

enum position { manager, sales, consultant, cashier, cleaner, security }; //enum type created for workers

class employee
{
private:
	String name;
	String surname;
	String phone;
	date	birth;
	position type;

public:
	
	employee();
	employee(String aSurname, String aName, position aType, String aPhone, short aDay=0, short aMonth=0, short aYear=0);//overloaded constructor
	employee(const employee &a);
	~employee() {};
	
	void set_name(String aName);
	void set_surname(String aSurname);
	void set_phone(String aPhone);
	void set_birth_date(short aDay, short aMonth, short aYear);
	void set_position(position atype);

	String get_name()const;
	String get_surname()const;
	String get_phone()const;
	date get_birth_date()const;
	position get_position()const;

	bool operator<(const employee &a);
	bool operator<=(const employee &a);
	bool operator>(const employee &a);
	bool operator>=(const employee &a);
	bool operator==(const employee &a);
	
	friend ostream& operator<<(ostream& os, const employee& dt);
};

