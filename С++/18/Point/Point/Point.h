#pragma once
template<typename T>
class Point
{
public:
	Point() {
		x = y = 0;
	}

	Point(T aX, T aY){
		x = aX;
		y = aY;
	}

	void setX(T aX){
		x = aX;
	}
	void setY(T aY){
		y = aY;
	}

	T getX(){
		return x;
	}

	T getY(){
	return y;
}

private:
	T x;
	T y;
};

