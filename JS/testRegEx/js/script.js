var allHTML;
window.onload = function() {

    //to show parced data after click on button:
    function show_parced_data() {
        var info_overlay = document.querySelector(".overlay");
        info_overlay.style.display="block";
    }
    var button_show_parced_data = document.querySelector(".button_parce");
   button_show_parced_data.addEventListener("click", show_parced_data);

   //to hide parced data after click on close icon:
    function hide_parced_data() {
        var info_overlay = document.querySelector(".overlay");
        info_overlay.style.display="none";
    }
    var close_icon = document.querySelector(".parced_data>img");
    close_icon.addEventListener("click", hide_parced_data);

    //getting all HTML code of the page:
    allHTML = document.documentElement.innerHTML;
    //searching for all other (external) scripts, except the current one:
    //var outsorceScript = /<script(( ((type)|(src)|())=(?:"|')[^j]\6(.|\n)*?)|(>(.|\n)*?))<\/script>/i;

    //getting an element to store and output parced data:
    var parcedData = document.querySelector(".parced_data");

    var regLink = /["']canonical["'] +href *= *["'](\w+:\/\/[a-z0-9./\-_:]+)["']/i;
    var link = allHTML.match(regLink);
    var title = parcedData.appendChild(document.createElement("span"));
    title.innerText = "Adverticement link: ";
    if(title != null){
        var newEl = parcedData.appendChild(document.createElement("p"));
        newEl.innerText = link[1];
    }

    var regAuthorData = /"offer-user__details"(?:.*?|\n*?)*?<(?: *|\n*)*a(?: *|\n*)*href(?: *|\n*)*=(?: *|\n*)*["'](.+)*['"](?:.*?|\n*?)*?>(?: *|\n*)*(.+)*</i;
    var authorData = allHTML.match(regAuthorData);
    title = parcedData.appendChild(document.createElement("span"));
    title.innerText = "Author's olx page: ";
    if (authorData != null) {
        newEl = parcedData.appendChild(document.createElement("p"));
        newEl.innerText = authorData[1];
    }
        title = parcedData.appendChild(document.createElement("span"));
        title.innerText = "Author's name: ";
    if (authorData != null) {
        newEl = parcedData.appendChild(document.createElement("p"));
        newEl.innerText = authorData[2];
    }

    var regAuthorLogo = /["']offer-sidebar__box offer-sidebar__box--business["'](?:.*?|\n*?)*?<(?: *|\n*)*img(?: *|\n*)*src(?: *|\n*)*=(?: *|\n*)*['"](?: *|\n*)*(.*)*['"]/i;
    var authorLogo = allHTML.match(regAuthorLogo);
    title = parcedData.appendChild(document.createElement("span"));
    title.innerText = "Author's logo: ";
    if (authorLogo != null) {
        newEl = parcedData.appendChild(document.createElement("p"));
        newEl.innerText = authorLogo[1];
    }

    var regAdTitle = /["'](?: *|\n*)offer-titlebox(?: *|\n*)"(?: *|\n*)>(?: *|\n*)*<(?: *|\n*)h1(?: *|\n*)>(?: *|\n*) *(.*|\n*)<\/h1>/i;
    var adTitle = allHTML.match(regAdTitle);
    title = parcedData.appendChild(document.createElement("span"));
    title.innerText = "Title: ";
    if (adTitle != null) {
        newEl = parcedData.appendChild(document.createElement("p"));
        newEl.innerText = adTitle[1];
    }

    var regPrice = /"fright optionsbar"(?:.*?|\n*)*?"pricelabel tcenter"(?:.*?|\n*)*?<(?: *|\n)*strong(?:.*?|\n*)*?>(.*)<(?: *|\n)*\/(?: *|\n)*strong/i;
    var price = allHTML.match(regPrice);
    title = parcedData.appendChild(document.createElement("span"));
    title.innerText = "Price: ";
    if (price != null) {
        newEl = parcedData.appendChild(document.createElement("p"));
        newEl.innerText = price[1];
    }

    var regRegion = /["']offer-titlebox__details["'](?:.*?|\n*?)*?show-map-link(?:.*?|\n*?)*?strong>(.*)*?<\/strong/i;
    var region = allHTML.match(regRegion);
    title = parcedData.appendChild(document.createElement("span"));
    title.innerText = "Region: ";
    if (region != null) {
        newEl = parcedData.appendChild(document.createElement("p"));
        newEl.innerText = region[1];
    }

    var regAdTime = /["']offer-titlebox__details["'](?:.*?|\n*?)*?show-map-link(?:.*?|\n*?)*?<em>(?:.*?|\n*?)*?(\d{1,2} *: *\d{1,2})/i;
    var adTime = allHTML.match(regAdTime);
    title = parcedData.appendChild(document.createElement("span"));
    title.innerText = "Advertisement time: ";
    if (adTime != null) {
        newEl = parcedData.appendChild(document.createElement("p"));
        newEl.innerText = adTime[1];
    }

    var regDate = /<em>(?:.*?|\n*?)*?(?:\d{1,2} *: *\d{1,2})(?:.*?|\b)*?(\d{1,2}[a-zа-яёі0-9 ]*?\d{2,4})/i;
    var date = allHTML.match(regDate);
    title = parcedData.appendChild(document.createElement("span"));
    title.innerText = "Advertisement date: ";
    if (date != null) {
        newEl = parcedData.appendChild(document.createElement("p"));
        newEl.innerText = date[1];
    }

    var regAdNumber = /<em>(?:.*?|\n*?)*?<small>(?:.*?|\n*?)*?(\d+)/i;
    var adNumber = allHTML.match(regAdNumber);
    title = parcedData.appendChild(document.createElement("span"));
    title.innerText = "Advertisement number: ";
    if (adNumber != null) {
        newEl = parcedData.appendChild(document.createElement("p"));
        newEl.innerText = adNumber[1];
    }

    var regAuthor = /<th>(?: *|\n*)Объявление(?: *|\n*)от(?:.*?|\n*?)*?<strong>(?: *|\n*){0,}<a(?:.*?|\n*?)*?>(?: *|\n*){0,}((?:.*?|\n*?)*?)<\/a>/i;
    var author = allHTML.match(regAuthor);
    title = parcedData.appendChild(document.createElement("span"));
    title.innerText = "Advertisement author: ";
    if (author != null) {
        newEl = parcedData.appendChild(document.createElement("p"));
        newEl.innerText = author[1];
    }

    var regHouseType = /<(?: *|\n*)th(?: *|\n*)>(?: *|\n*)Тип(?: *|\n*)*дома(?:.*?|\n*?)*?<(?: *|\n*)strong(?: *|\n*)>(?: *|\n*){0,}<a(?:.*?|\n*?)*?>(?: *|\n*){0,}((?:.*?|\n*?)*?)<(?: *|\n*)\/a(?: *|\n*)>/i;
    var houseType = allHTML.match(regHouseType);
    title = parcedData.appendChild(document.createElement("span"));
    title.innerText = "House type: ";
    if (houseType != null) {
        newEl = parcedData.appendChild(document.createElement("p"));
        newEl.innerText = houseType[1];
    }

    var regHouseSquare = /<(?: *|\n*)th(?: *|\n*)>(?: *|\n*)Площадь(?: *|\n*)*дома(?:.*?|\n*?)*?<(?: *|\n*)strong(?: *|\n*)>(?: *|\n*){0,}(\d+)/i;
    var houseSquare = allHTML.match(regHouseSquare);
    title = parcedData.appendChild(document.createElement("span"));
    title.innerText = "House square: ";
    if (houseSquare != null) {
        newEl = parcedData.appendChild(document.createElement("p"));
        newEl.innerText = houseSquare[1];
    }

    var regDescription = /textContent(?:.*?|\n*?)*?<(?:.*?|\n*?)*?>(?: *|\n)*((?:\w*|[а-яёі ]*|\s*|[,+./\!?:;'"#%^&*()@\-=]*|\b)+)*(?:<(?: *|\n)*(?:.*?|\n*?)*?>(?: *|\n)*)*((?:\w*|[а-яёі ]*|\s*|[,+./\!?:;'"#%^&*()@\-=]*|\b)+)*</i;
    var description = allHTML.match(regDescription);
    title = parcedData.appendChild(document.createElement("span"));
    title.innerText = "Description: ";
    if (description != null) {
        for (var i = 1; i < description.length; i++)
        {
             newEl = parcedData.appendChild(document.createElement("p"));
             newEl.innerText = description[i];
        }
    }

    var regImages = /"photo-glow"(?:.*?|\n*?)*?<(?: *|\n*)*img(?: *|\n*)*src(?: *|\n*)*=(?: *|\n*)*['"]((?:\w|[,+./\!?:;'"#%^&*()@\-=])*)+"/ig;
    var images = [];
    var result;
    while (result !== null)
    {
        result = regImages.exec(allHTML);
        if (result == null)
            break;
        for (i = 1; i < result.length; i++)
            images.push(result[i]);
    }
    title = parcedData.appendChild(document.createElement("span"));
    title.innerText = "Photos list: ";
    for (i = 0; i < images.length; i++)
    {
        newEl = parcedData.appendChild(document.createElement("p"));
        newEl.innerText = (i+1) + ". " + images[i];
    }

};

