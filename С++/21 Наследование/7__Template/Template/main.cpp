#include <iostream>
using std::cout;
using std::endl;
//�����-��������
template <class T>
class Pair
{
protected:
	T a;
	T b;
public:
	Pair();
	Pair(T t1, T t2);

};

template<class T>
Pair<T>::Pair()
{
	a = T(0);
	b = T(0);
}

// ����������� ������ ��������
template <class T>
Pair <T>::Pair(T t1, T t2) : a(t1), b(t2)
{
	/*
		a = t1;
		b = t2;
	*/
}


template <class T>
class Trio : public Pair <T>
{
protected:
	T c;
public:
	Trio(T t1, T t2, T t3);
	void show() {
		cout << a << " " << b << " " << c << endl;
	}
};
template <class T>
Trio<T>::Trio(T t1, T t2, T t3)
	: Pair <T>(t1, t2), c(t3)
{
}


void main()
{
	Pair<int> p1;
	Pair<double> p2;
	Pair<float> p3(3.6f, 5.2f);


	Trio<int> t(1, 2, 3);
	t.show();

	Trio<double> t2(4.7, 2.3, 3.67);
	t2.show();
}