#pragma once
#include"Reserve.h"

enum table_status { reserved, free_table, occupied };

class Table
{
private:
	int ID;
	int sitting_places;
	table_status status;
	Reserve reserve_data;
	
public:
	Table();
	Table(int aID, int aSitting_places);
	~Table() {};

public:
	void set_ID(int aID);
	void set_sitting_places(int aSitting_places);
	void set_status(table_status aStatus, int aReserve_duration = 0, string aName = string(), string aPhone = string());
	int get_ID()const;
	int get_sitting_places()const;
	table_status get_status()const;
	int get_reserve_duration()const;
	string get_reservator_name()const;
	string get_reservator_phone()const;
	void remove_reserve();
	bool check_reserve();
	void print_to_file(ofstream &F)const;
	void read_from_file(ifstream &F);
	
	friend ostream& operator<<(ostream& os, Table& t);
};