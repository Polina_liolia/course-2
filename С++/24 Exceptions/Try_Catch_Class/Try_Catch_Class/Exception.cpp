#include "Exception.h"
#include <string.h>
Exception::Exception(){
	message = 0;
}
Exception::Exception(const Exception& aEx){
	message = new char[strlen(aEx.message) + 1];
	strcpy(message, aEx.message);
}
Exception::Exception(char* aMessage){
	message = new char[strlen(aMessage) + 1];
	strcpy(message, aMessage);
}
char* Exception::get_message(){
	return message;
}
Exception::~Exception(){
	delete[] message;
}

