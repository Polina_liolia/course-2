#pragma once
class Point{
	int x;
	int y;
public:	
	Point();
	Point(int numb1);
	Point(int aX, int aY);

	void set_x(int ax);
	void set_y(int ay);
	void print_props();

	~Point();
};
