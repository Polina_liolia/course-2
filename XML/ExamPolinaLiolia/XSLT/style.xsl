<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <h2>Immovable objects</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Object name</th>
                        <th>Type</th>
                        <th>Address</th>
                        <th>Price, $</th>
                        <th>Built date (year)</th>
                        <th>Floors number</th>
                        <th>Rooms number</th>
                        <th>Square, acres</th>
                    </tr>
                    <!--<xsl:call-template name="sort_by_date"/>-->
                    <!--<xsl:call-template name="sort_by_rooms_number"/>-->
                    <xsl:call-template name="sort_by_rooms_price"/>
                </table>
            </body>
        </html>
    </xsl:template>


    <xsl:template match="/immovable_objects" name="sort_by_date">
        <xsl:for-each select="/immovable_objects/object">
            <xsl:sort select="built_date"
                      order="ascending"
                      data-type="number"/>
            <tr>
                <td><xsl:value-of select="name"/></td>
                <td><xsl:value-of select="@type"/></td>
                <td><xsl:value-of select="address"/></td>
                <td><xsl:value-of select="price"/></td>
                <td><xsl:value-of select="built_date"/></td>
                <td><xsl:value-of select="floors_number"/></td>
                <td><xsl:value-of select="rooms_number"/></td>
                <td><xsl:value-of select="square"/></td>
            </tr>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="/immovable_objects" name="sort_by_rooms_number">
        <xsl:for-each select="/immovable_objects/object">
            <xsl:sort select="rooms_number"
                      order="ascending"
                      data-type="number"/>
            <tr>
                <td><xsl:value-of select="name"/></td>
                <td><xsl:value-of select="@type"/></td>
                <td><xsl:value-of select="address"/></td>
                <td><xsl:value-of select="price"/></td>
                <td><xsl:value-of select="built_date"/></td>
                <td><xsl:value-of select="floors_number"/></td>
                <td><xsl:value-of select="rooms_number"/></td>
                <td><xsl:value-of select="square"/></td>
            </tr>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="/immovable_objects" name="sort_by_rooms_price">
        <xsl:for-each select="/immovable_objects/object">
            <xsl:sort select="price"
                      order="ascending"
                      data-type="number"/>
            <tr>
                <td><xsl:value-of select="name"/></td>
                <td><xsl:value-of select="@type"/></td>
                <td><xsl:value-of select="address"/></td>
                <td><xsl:value-of select="price"/></td>
                <td><xsl:value-of select="built_date"/></td>
                <td><xsl:value-of select="floors_number"/></td>
                <td><xsl:value-of select="rooms_number"/></td>
                <td><xsl:value-of select="square"/></td>
            </tr>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>