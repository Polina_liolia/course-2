//Using HTML 5

var ul1 = document.querySelector(".ul1");
var ul2 = document.querySelector(".ul2");

function dragging ()
{
    if (event.target.tagName != "LI")
        return;
    event.target.style.color = "red";
}

function stopDragging ()
{
    if (event.target.tagName != "LI")
        return;
    event.target.style.color = "";
    event.target.classList.remove("ondrag");
}

function onDropField ()
{
    if (event.target.tagName == "LI")
        var target = event.target.parentNode;
    else
        target = event.target;
    if (!target.classList.contains("dropHere"))
        return;
    target.style.borderColor = "red";
}

function outDropField() {
    // if (event.target.tagName == "LI")
    //     var target = event.target.parentNode;
    // else
    //     target = event.target;
    // if (!target.classList.contains("dropHere"))
    //     return;
    event.target.style.borderColor = "";
}

function dragOver() {
    // By default, data/elements cannot be dropped in other elements.
    // To allow a drop, we must prevent the default handling of the element
    event.preventDefault();
}

function startDrag() {
    // The dataTransfer.setData() method sets the data type and the value of the dragged data
    event.dataTransfer.setData("Text", event.target.tagName);
    event.target.classList.add("ondrag");
}

function dropElement() {
    if (event.target.tagName == "LI")
        var target = event.target.parentNode;
    else
        target = event.target;
    if (!target.classList.contains("dropHere"))
        return;
    var dragElement = document.querySelector(".ondrag");
    target.appendChild(dragElement);
    dragElement.classList.remove(".ondrag");
    dragElement.style.color="";
    target.style.borderColor="";
}

ul1.addEventListener("drag", dragging);
ul1.addEventListener("dragend", stopDragging);
ul1.addEventListener("dragenter", onDropField);
ul1.addEventListener("dragleave", outDropField);
ul1.addEventListener("dragover", dragOver);
ul1.addEventListener("dragstart", startDrag);
ul1.addEventListener("drop", dropElement);

ul2.addEventListener("drag", dragging);
ul2.addEventListener("dragend", stopDragging);
ul2.addEventListener("dragenter", onDropField);
ul2.addEventListener("dragleave", outDropField);
ul2.addEventListener("dragover", dragOver);
ul2.addEventListener("dragstart", startDrag);
ul2.addEventListener("drop", dropElement);


//only JS using:
var dragDiv = document.getElementById('dragDiv');

//styles for div:
dragDiv.style.backgroundColor="red";
dragDiv.style.width="70px";
dragDiv.style.height="50px";
dragDiv.style.textAlign="center";
dragDiv.style.paddingTop="25px";


//возвращает координаты элемента относительно всей страницы (с учетом прокрутки):
function getCoords(elem) {
    var box = elem.getBoundingClientRect();
    //прибавляем к координатам в клиентской области текущую прокрутку:
    return {
        top: box.top + pageYOffset,
        left: box.left + pageXOffset
    };
}

    function onMouseDown() {

    var coords = getCoords(dragDiv);
    var shiftX = event.pageX - coords.left;
    var shiftY = event.pageY - coords.top;

    //чтобы иметь возможность свободно перемещать элемент по всей странице (вне родительский элементов):
    dragDiv.style.position = 'absolute';
    document.body.appendChild(dragDiv);
    moveAt();

    dragDiv.style.zIndex = 1000; // над другими элементами

    function moveAt() {
        dragDiv.style.left = event.pageX - shiftX + 'px';
        dragDiv.style.top = event.pageY - shiftY + 'px';
    }
    document.addEventListener("mousemove", moveAt);

    //отпуская элемент, отменяем отслеживание движения мыши и отпускания её кнопки:
    function dropElement() {
        document.removeEventListener("mousemove", moveAt);
        dragDiv.removeEventListener("mouseup", dropElement);
    }
    dragDiv.addEventListener("mouseup", dropElement);

}

//чтобы избежать автозапуска встроенного drag and drop:
function preventStdDrag() {
    return false;
}

dragDiv.addEventListener("dragstart", preventStdDrag);

dragDiv.addEventListener("mousedown", onMouseDown);