#include "user_data.h"
//using namespace std;


user_data::user_data()
{
	login = new TCHAR[100];
	password = new TCHAR[100];
	email = new TCHAR[100];
	phone = new TCHAR[100];
}

user_data::user_data(TCHAR *aLogin, TCHAR *aPassword, TCHAR *aEemail, TCHAR *aPhone)
{
	login = new TCHAR[sizeof(aLogin) + 1];
	password = new TCHAR[sizeof(aPassword) + 1];
	email = new TCHAR[sizeof(aEemail) + 1];
	phone = new TCHAR[sizeof(aPhone) + 1];
	_tcscpy_s(login, sizeof(aLogin), aLogin);
	_tcscpy_s(password, sizeof(aPassword), aPassword);
	_tcscpy_s(email, sizeof(aEemail), aEemail);
	_tcscpy_s(phone, sizeof(aPhone), aPhone);
}


user_data::~user_data()
{
	delete[] login, password, email, phone;
}


TCHAR * user_data::get_login()
{
	return login;
}

TCHAR * user_data::get_password()
{
	return password;
}

TCHAR * user_data::get_email()
{
	return email;
}

TCHAR * user_data::get_phone()
{
	return phone;
}

bool user_data::operator==(const user_data & aUser_data)
{
	if (!_tcscmp(login, aUser_data.login) && !_tcscmp(password, aUser_data.password))
		return true;
	return false;
}
