#include <iostream>
using namespace std;
class a
{
public:
	int x;
	a (){
		x=0;
	}
};
class b:virtual public a{
};
class c:virtual public a{
};
class d:public b, public c
{};

void main()
{
	a a1;
	a1.x = 0;

	b b1;
	b1.x = 0;

	c c1;
	c1.x = 0;
	
	d e;
	e.x=3;	//��� virtual - ���������������: ����� d ����������� ��� �������� � - �� b � �� � (� ���������� ������������ �� �)
}
