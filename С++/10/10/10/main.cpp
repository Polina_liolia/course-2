#include "Array.h"
#include "Converter.h"
using std::cout;

void main()
{
	int ar[] = { 1, 2, 3, 4, 5 };
	Array A(ar, 5);
	cout << A["abc"]<<"\n";
	cout << A["hefhwdhwje"] << "\n";

	Converter B;
	cout << B[1] << "\n";
	cout << B["Jan"] << "\n";
	cout << B["aug"] << "\n";
	cout << B["fdvrgr"] << "\n";
	cout << B[13] << "\n";
}