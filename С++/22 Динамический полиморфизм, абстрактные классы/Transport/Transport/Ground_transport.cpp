#include "Ground_transport.h"


Ground_transport::Ground_transport() : Transport()	//default constructor
{
	speed_units = km_h;
	engine_volume = 0;
}

//overloaded constructor
Ground_transport::Ground_transport(string aModel, double aWeight, double aMax_speed, speed aSpeed_units, double aEngine_volume) :
	Transport(aModel, aWeight, aMax_speed, aSpeed_units)
{
	engine_volume = aEngine_volume;
}

//method to set or change property engine_volume
void Ground_transport::set_engine_volume(double aEngine_volume)
{
	engine_volume = aEngine_volume;
}

//method to get meaning of property engine_volume
double Ground_transport::get_engine_volume() const
{
	return engine_volume;
}

//virtual method to get meaning of property speed_units
speed Ground_transport::get_speed_units() const
{
	cout << "Speed units of ground transport - "; 
	switch (speed_units)
	{
	case 0: cout << "km_h\n"; break;
	case 1: cout << "Mach\n"; break;
	case 2: cout << "miles_h\n"; break;
	case 3: cout << "knots\n"; break;
	}
	return speed_units;
}

//virtual method to print ell information about Ground_transport instance
void Ground_transport::print()
{
	Transport::print();//calling of basic class method
	cout << "Engine volume: " << engine_volume << " cubic sm\n";
}
