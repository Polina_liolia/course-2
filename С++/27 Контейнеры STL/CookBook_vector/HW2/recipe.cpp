#include <iostream>
#include <cstring>
#include "ingredient.h"
#include "recipe.h"

using std::cout;
using std::cin;

	recipe::recipe()
	{
		name = nullptr;
		type = nullptr;
		process = nullptr;
	}

	recipe::recipe(char *aName, char *aType, char *aProcess)
	{
		if (aName && strlen(aName)) 
		{
			name = new char[strlen(aName) + 1];
			strcpy_s(name, strlen(aName) + 1, aName);
		}
		else 
			name = nullptr;
		
		if (aType && strlen(aType))
		{
			type = new char[strlen(aType) + 1];
			strcpy_s(type, strlen(aType) + 1, aType);
		}
		else
			type = nullptr;

		if (aProcess && strlen(aProcess))
		{
			process = new char[strlen(aProcess) + 1];
			strcpy_s(process, strlen(aProcess) + 1, aProcess);
		}
		else
			process = nullptr;
	}

	recipe::recipe(const recipe &temp)
	{
		if (temp.name && strlen(temp.name))
		{
			name = new char[strlen(temp.name) + 1];
			strcpy_s(name, strlen(temp.name) + 1, temp.name);
		}
		else
			name = nullptr;

		if (temp.type && strlen(temp.type))
		{
			type = new char[strlen(temp.type) + 1];
			strcpy_s(type, strlen(temp.type) + 1, temp.type);
		}
		else
			type = nullptr;

		if (temp.process && strlen(temp.process))
		{
			process = new char[strlen(temp.process) + 1];
			strcpy_s(process, strlen(temp.process) + 1, temp.process);
		}
		else
			process = nullptr;
		components = temp.components;
	}

	recipe::~recipe()
	{
		delete[] name;
		delete[] type;
		delete[] process;
	}

	inline char *recipe::get_name()const					//inline function to get a name
	{
		return name;
	}

	inline char *recipe::get_type()const					//inline function to get a type of a dish
	{
		return type;
	}

	inline char *recipe::get_process()const					//inline function to get a process of cooking a dish
	{
		return process;
	}

	void recipe::set_name(char *aName)			//setting a name of a dish
	{
		if (name)
			delete[] name;
		size_t sz = strlen(aName) + 1;
		name = new char[sz];
		strcpy_s(name, sz, aName);
	}


	void recipe::set_type(char *aType)			//setting a type of a dish
	{
		if (type)
			delete[] type;
		size_t sz = strlen(aType) + 1;
		type = new char[sz];
		strcpy_s(type, sz, aType);
	}

	void recipe::set_process(char *aProcess)	//setting a process of cooking a dish
	{
		if (process)
			delete[] process;
		size_t sz = strlen(aProcess) + 1;
		process = new char[sz];
		strcpy_s(process, sz, aProcess);
	}

	void recipe::set_ingridient(char *aItem, float aDose)	//setting an components (items and dose) for a dish
	{
		ingredient New (aItem, aDose);
		components.push_back(New);
	}

	void recipe::del_ingridient(int ingredient_number)	//to delete pointed ingridient
	{
		if (ingredient_number <= 0 || ingredient_number > components.size())	//checking the correctness of ingredient_number input
		{
			cout << "No ingridient with such a number found.\n";
			return;
		}
		components.erase(components.begin() + ingredient_number - 1);
	}


	int recipe::save_recipe_to_file(std::ofstream &F)const
	{
		//to write a recipe name
		size_t sz = strlen(name) + 1;
		F.write(reinterpret_cast<char*>(&sz), sizeof(sz));
		F.write(name, sz);				

		//to write a recipe type
		sz = strlen(type) + 1;
		F.write(reinterpret_cast<char*>(&sz), sizeof(sz));
		F.write(type, sz);

		//to write a recipe process
		sz = strlen(process) + 1;
		F.write(reinterpret_cast<char*>(&sz), sizeof(sz));
		F.write(process, sz);

		//writing a total amount of components
		size_t sz_comp = components.size();
		F.write(reinterpret_cast<char*>(&sz_comp), sizeof(sz_comp));

		for (int i = 0; i < sz_comp; i++)
		{
			size_t sz = strlen(components[i].item) + 1;
			F.write(reinterpret_cast<char*>(&sz), sizeof(sz));		//writing a size of string
			F.write(components[i].item, sz);						//writing an item
			float dose = components[i].dose;
			F.write(reinterpret_cast<char*>(&dose), sizeof(float));	//writing a dose
		}
		return 0;
	}

	int recipe::read_recipe_from_file(std::ifstream &F)
	{
		size_t sz;
		F.read(reinterpret_cast<char*>(&sz), sizeof(size_t));
		name = new char[sz];
		F.read(name, sz);

		//to read a recipe type
		F.read(reinterpret_cast<char*>(&sz), sizeof(size_t));
		type = new char[sz];
		F.read(type, sz);

		//to write a recipe process
		F.read(reinterpret_cast<char*>(&sz), sizeof(size_t));
		process = new char[sz];
		F.read(process, sz);

		//writing a total amount of components
		size_t sz_comp;
		F.read(reinterpret_cast<char*>(&sz_comp), sizeof(size_t));
		
		for (int i = 0; i < sz_comp; i++)
		{
			F.read(reinterpret_cast<char*>(&sz), sizeof(size_t));		//writing a size of string
			char *item = new char[sz];
			F.read(item, sz);										//writing an item
			float dose;
			F.read(reinterpret_cast<char*>(&dose), sizeof(float));	//writing a dose
			ingredient New(item, dose);
			components.push_back(New);
		}
		return 0;
	}

	void recipe::print_recipe_name()const							//to print recipe name
	{
		cout << name << "\n";
	}

	void recipe::print_recipe()								//to print all recipe information
	{
		cout << "Recept name: " << name << "\n";
		cout << "Type: " << type << "\n";
		cout << "Cooking process: " << process << "\n";
		cout << "Ingridients:\n";
		vector <ingredient>::iterator Iter;
		int count = 1;
		for (Iter = components.begin(); Iter != components.end(); Iter++)
			cout << count++ << ". " << (*Iter).item << " - " << (*Iter).dose << "g\n";
		cout << "\n";
	}

	void recipe::print_ingredients()							//to print the list of components
	{
		vector <ingredient>::iterator Iter;
		int count = 1;
		for (Iter = components.begin(); Iter != components.end(); Iter++, count++)
			cout << count << ". " << (*Iter).item << " - " << (*Iter).dose << "g\n";
		cout << "\n";
	}

	recipe recipe::operator=(const recipe & temp)
	{
		if (temp.name && strlen(temp.name))
		{
			name = new char[strlen(temp.name) + 1];
			strcpy_s(name, strlen(temp.name) + 1, temp.name);
		}
		else
			name = nullptr;

		if (temp.type && strlen(temp.type))
		{
			type = new char[strlen(temp.type) + 1];
			strcpy_s(type, strlen(temp.type) + 1, temp.type);
		}
		else
			type = nullptr;

		if (temp.process && strlen(temp.process))
		{
			process = new char[strlen(temp.process) + 1];
			strcpy_s(process, strlen(temp.process) + 1, temp.process);
		}
		else
			process = nullptr;
		components = temp.components;
		return *this;
	}
