#pragma once
#include <iostream>
#include <cstring>
using std::cout;
using std::cin;
#include "Birth_date.h"

class Student
{
private:
	char *surname;
	char *name;
	char *patronymic;
	birth_date birth;
	char *phone;
	char *univercity;
	char *country;
	char *city;
	char *group;
	
public:
	Student();

	//overloaded constructor:
	Student(char *aSurname, char *aName, char *aPatronymic, short aDay, short aMonth, short aYear, char *aPhone, char *aUnivercity, char *aCountry, char *aCity, char *aGroup);

	~Student();

public:
	//methods to set the meaning of every property:

	void set_surname(char *aSurname);
	void set_name(char *aName);
	void set_patronymic(char *aPatronymic);
	void set_birthdate(short aDay, short aMonth, short aYear)
	{
		birth.day = aDay;
		birth.month = aMonth;
		birth.year - aYear;
	}
	void set_phone(char *aPhone);
	void set_univercity(char *aUnivercity);
	void set_country(char *aCountry);
	void set_city(char *aCity);
	void set_group(char *aGroup);

	
	//methods to get the meaning of every property:
	char *get_surname()
	{
		return surname;
	}

	char *get_name()
	{
		return name;
	}

	char *get_patronymic()
	{
		return patronymic;
	}

	int get_birth_day()
	{
		return birth.day;
	}

	int get_birth_month()
	{
		return birth.month;
	}

	int get_birth_year()
	{
		return birth.year;
	}

	char *get_phone()
	{
		return phone;
	}

	char *get_univercity()
	{
		return univercity;
	}

	char *get_country()
	{
		return country;
	}

	char *get_city()
	{
		return city;
	}

	char *get_group()
	{
		return group;
	}

	void print_data();		//to print information about student

};