var xhttp = new XMLHttpRequest();
xhttp.open("GET", "weather.xml", true);
xhttp.send();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        $(function () {
        $("#_show").on("click", function () {
                var xml = xhttp.responseXML;
                var txt = "", path;
                path = $("input[type='text']").val();
                var nodes = xml.evaluate(path, xml, null, XPathResult.ANY_TYPE, null);
                var result = nodes.iterateNext();
                while (result) {
                    if(result.childNodes[0])
                       txt += result.childNodes[0].nodeValue + "<br>";
                    else
                        txt += result.nodeValue + "<br>";
                    result = nodes.iterateNext();
                }
                $("#result").html(txt);
            });

        });
    }
};
