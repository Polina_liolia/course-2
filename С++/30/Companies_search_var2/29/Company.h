#pragma once
#include <iostream>
#include <set>
#include <string>
using std::string;
using std::set;
using std::cout;
using std::ostream;

enum accaunt { Free, silver, gold, platinum };

class Company
{
private:
	string name;
	string description;
	accaunt type;
	set<string> key_words;
	
	class counter
	{
	private:
		int key_words_found;
		int key_words_in_description;

	public:
		counter();
		counter(int aKey_words_found, int aKey_words_in_description);
		~counter() {};

		void set_key_words_found(int rezult);
		void set_key_words_in_description(int rezult);
		int get_key_words_found()const;
		int get_key_words_in_description()const;

		bool operator>(const counter &a);
		bool operator<(const counter &a);
	};

	counter rate;

public:
	Company();
	Company(string aName, string aDescription, accaunt aType = Free);
	~Company() {}

	void set_name(string aName);
	void set_description(string aDescription);
	void set_type(accaunt aType);
	void set_rate(int aKey_words_found, int aKey_words_in_description);
	void add_key_word(string aKey_word);
	void change_key_word(string aWord_old, string aWord_new);
	
	
	string get_name()const;
	string get_description()const;
	accaunt get_type()const;
	int get_key_words_found()const;
	int get_key_words_in_description()const;
	
	bool find_key_word(string aKey_word);
	bool find_key_word_in_description(string aKey_word);

	bool operator==(const Company & aCompany);

	bool operator==(const set<string>& words_list);

	bool operator==(const Company aCompany);
	bool operator==(const set<string> WL);
	bool operator>(const Company aCompany);
	bool operator<(const Company aCompany);

	friend ostream& operator<<(ostream& os, Company& C);
};