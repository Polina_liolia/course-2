#include "Header.h"

//Bubble sort
void bubble_sort(int *p_arr, int str, int col)
{
	for (int b = 0, k = str*col; b < str*col - 1; b++, k--)
		for (int i = 0; i < k - 1; i++)
			if (*(p_arr + i) > *(p_arr + i + 1))
			{
				int j = *(p_arr + i);
				*(p_arr + i) = *(p_arr + i + 1);
				*(p_arr + i + 1) = j;
			}
}

//Insert sort
void insert_sort(int *p_arr, int str, int col)
{
	int j = 0;
	for (int i = 1; i < col*str; i++)
	{
		int x = *(p_arr + i); //��������� �������
		for (j = i - 1; j >= 0 && *(p_arr + j) > x; j--)
			*(p_arr + j + 1) = *(p_arr + j);
		*(p_arr + j + 1) = x;//������� � ������������ ���.
	}
}

//Select sort
void select_sort(int *p_arr, int str, int col)
{
	for (int j = 0; j < col*str; j++)
	{
		int min = *(p_arr + j);
		int min_pos = j;
		for (int i = j; i < col*str; i++)
		{
			if (*(p_arr + i) < min)
			{
				min_pos = i;
				min = *(p_arr + i);
			}
		}
		*(p_arr + min_pos) = *(p_arr + j);
		*(p_arr + j) = min;
	}
}

//Insert sort (ascending)
void insert_sort_ascending(int *p_arr, int sz)
{
	int j = 0;
	for (int i = 1; i < sz; i++)
	{
		int x = *(p_arr + i); //��������� �������
		for (j = i - 1; j >= 0 && *(p_arr + j) > x; j--)
			*(p_arr + j + 1) = *(p_arr + j);
		*(p_arr + j + 1) = x;//������� � ������������ ���.
	}
}

//Insert sort (descending)
void insert_sort_descending(int *p_arr, int sz)
{
	int j = 0;
	for (int i = 1; i < sz; i++)
	{
		int x = *(p_arr + i); //��������� �������
		for (j = i - 1; j >= 0 && *(p_arr + j) < x; j--)
			*(p_arr + j + 1) = *(p_arr + j);
		*(p_arr + j + 1) = x;//������� � ������������ ���.
	}
}

//Insert sort (from odd to even)
void insert_sort_odd_even(int *p_arr, int sz)
{
	int j = 0;
	for (int i = 1; i < sz; i++)
	{
		int x = *(p_arr + i); //��������� �������
							  /*if the element is even, it has to be raplaced to the beginning of array
							  (on a correct position relative to the other even elements - local ascending sort)*/
		if (x % 2 == 0)
		{
			for (j = i - 1; j >= 0 && ((*(p_arr + j) > x && *(p_arr + j) % 2 == 0) || *(p_arr + j) % 2 != 0); j--)
				*(p_arr + j + 1) = *(p_arr + j);
			*(p_arr + j + 1) = x;//������� � ������������ ���.
								 /*if an element was replaced, correcting an index of current element to avoid passing the next element*/
			if (j + 1 != i)
				i--;
		}
		/*if the element is odd it has to be raplaced to the end of array
		(on a correct position relative to the other odd elements - local ascending sort)*/
		else
		{
			for (j = i + 1; j <sz && ((*(p_arr + j) < x && *(p_arr + j) % 2 != 0) || *(p_arr + j) % 2 == 0); j++)
				*(p_arr + j - 1) = *(p_arr + j);
			*(p_arr + j - 1) = x;//������� � ������������ ���.
								 /*if an element was replaced, correcting an index of current element to avoid passing the next element*/
			if (j - 1 != i)
				i--;
		}
	}
}

//Insert sort with additional options
void insert_sort(int *arr, int sz, int type)
{
	void(*p)(int*, int);//a prototype of a pointer on a function, that will be used
	switch (type)//initialising a pointer with a function, depending on a type of sort
	{
	case 1: p = insert_sort_ascending; break;
	case 2: p = insert_sort_descending; break;
	case 3: p = insert_sort_odd_even; break;
	default: p = insert_sort_ascending;//ascending sort is default to avoid a situation of non-initialised pointer
	}
	p(arr, sz);
}


//���������� ���������� �������
void sort_arr_col(int **ar, int str, int col)
{
	for (int a = 0; a < col * str; a++)
	{
		for (int i = 0; i < str; i++)
			for (int j = 0; j < col; j++)
			{
				if (j != 0)
					if (ar[i][j] < ar[i][j - 1])
					{
						int temp = ar[i][j];
						ar[i][j] = ar[i][j - 1];
						ar[i][j - 1] = temp;
					}
				if (j == 0 && i != 0)
					if (ar[i][j] < ar[i - 1][col - 1])
					{
						int temp = ar[i][j];
						ar[i][j] = ar[i - 1][col - 1];
						ar[i - 1][col - 1] = temp;
					}
			}
	}
}

//������� ���������� ���������� ��������� ���� �������� �� �����������
void sort2arr(int **arr1, int **arr2, int str, int col)
{
	for (int a = 0; a < (col * str) * 2; a++)
	{
		for (int i = 0; i < str * 2; i++)
			for (int j = 0; j < col; j++)
			{
				if (i < str)
				{
					if (j != 0)
						if (arr1[i][j] < arr1[i][j - 1])
						{
							int temp = arr1[i][j];
							arr1[i][j] = arr1[i][j - 1];
							arr1[i][j - 1] = temp;
						}
					if (j == 0 && i != 0)
						if (arr1[i][j] < arr1[i - 1][col - 1])
						{
							int temp = arr1[i][j];
							arr1[i][j] = arr1[i - 1][col - 1];
							arr1[i - 1][col - 1] = temp;
						}
				}
				if (i == str && j == 0)
					if (arr2[i / str][j] < arr1[i - 1][col - 1])
					{
						int temp = arr2[i / str][j];
						arr2[i / str][j] = arr1[i - 1][col - 1];
						arr1[i - 1][col - 1] = temp;
					}
				if (i == str && j != 0 || i>str)
				{
					if (j != 0)
						if (arr2[i / str][j] < arr2[i / str][j - 1])
						{
							int temp = arr2[i / str][j];
							arr2[i / str][j] = arr2[i / str][j - 1];
							arr2[i / str][j - 1] = temp;
						}
					if (j == 0)
						if (arr2[i / str][j] < arr2[i / str - 1][col - 1])
						{
							int temp = arr2[i / str][j];
							arr2[i / str][j] = arr2[i / str - 1][col - 1];
							arr2[i / str - 1][col - 1] = temp;
						}
				}

			}
	}
}

//������� ���������� ���������� �������

void sort_arr_7col(int **ar, int str, int col)
{
	for (int a = 0; a < col*str; a++)
	{
		for (int i = 0; i < str; i++)
			for (int j = 0; j < col; j++)
			{
				if (j != 0)
					if (ar[i][j] < ar[i][j - 1])
					{
						int temp = ar[i][j];
						ar[i][j] = ar[i][j - 1];
						ar[i][j - 1] = temp;
					}
				if (j == 0 && i != 0)
					if (ar[i][j] < ar[i - 1][col - 1])
					{
						int temp = ar[i][j];
						ar[i][j] = ar[i - 1][col - 1];
						ar[i - 1][col - 1] = temp;
					}
			}
	}
}

//Sotring strings of a two-dimensional dinamic array by abc
void sort_2din_arr_str(char** str, int sz1, int sz2)
{
	for (int i = 0; i < sz1; i++)
		for (int j = 0; j < sz1 - 1; j++)
			if (_stricmp(str[j], str[j + 1])>0)	//if the second string is bigger, than the first, changing them
			{
				char *temp = str[j];
				str[j] = str[j + 1];
				str[j + 1] = temp;
			}
}

