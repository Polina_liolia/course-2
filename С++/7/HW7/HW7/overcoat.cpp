#include <iostream>
#include "overcoat.h"
using std::cout;

overcoat::overcoat(char *aType, char *aColor, float aPrice)
{
	if (aType && strlen(aType))
	{
		type = new char[strlen(aType) + 1];
		strcpy_s(type, strlen(aType) + 1, aType);
	}
	else
		type = nullptr;

	if (aColor && strlen(aColor))
	{
		color = new char[strlen(aColor) + 1];
		strcpy_s(color, strlen(aColor) + 1, aColor);
	}
	else
		color = nullptr;

	price = aPrice;
}

overcoat::overcoat(const overcoat &aOvercoat) 
{
	if (aOvercoat.type && strlen(aOvercoat.type))
	{
		type = new char[strlen(aOvercoat.type) + 1];
		strcpy_s(type, strlen(aOvercoat.type) + 1, aOvercoat.type);
	}
	else
		type = nullptr;

	if (aOvercoat.color && strlen(aOvercoat.color))
	{
		color = new char[strlen(aOvercoat.color) + 1];
		strcpy_s(color, strlen(aOvercoat.color) + 1, aOvercoat.color);
	}
	else
		color = nullptr;

	price = aOvercoat.price;
}

overcoat::~overcoat()
{
	delete[] type;
	delete[] color;
}

void overcoat::set_type(char *aType)
{
	if (type)
		delete[] type;
	type = new char[strlen(aType) + 1];
	strcpy_s(type, strlen(aType) + 1, aType);
}

void overcoat::set_color(char *aColor)
{
	if (color)
		delete[] color;
	color = new char[strlen(aColor) + 1];
	strcpy_s(color, strlen(aColor) + 1, aColor);
}

inline void overcoat::set_price(float aPrice)
{
	price = aPrice;
}

inline char *overcoat::get_type()const
{
	return type;
}

inline char *overcoat::get_color()const
{
	return color;
}

inline float overcoat::get_price()const
{
	return price;
}

void overcoat::print()const
{
	cout << "Type: " << get_type() << "\n" << "Color: " << get_color() << "\n" << "Price: " << get_price() << "\n\n";
}

bool overcoat::operator==(const overcoat &aOvercoat)const		//checking, are overcoat types the same
{
	if (strcmp(type, aOvercoat.type) == 0)
		return true;
	return false;
}

overcoat overcoat::operator=(const overcoat &aOvercoat)			//assigns the value of another example to current one
{
	if (aOvercoat.type)
	{
		if (type)
			delete[] type;
		type = new char[strlen(aOvercoat.type) + 1];
		strcpy_s(type, strlen(aOvercoat.type) + 1, aOvercoat.type);
	}

	if (aOvercoat.color)
	{
		if (color)
			delete[] color;
		color = new char[strlen(aOvercoat.color) + 1];
		strcpy_s(color, strlen(aOvercoat.color) + 1, aOvercoat.color);
	}

	price = aOvercoat.price;

	return *this;
}

bool overcoat::operator>(const overcoat &aOvercoat)const		//comparing the same type coats prices
{
	if (strcmp(type, aOvercoat.type) == 0 && price > aOvercoat.price)
		return true;
	return false;
}