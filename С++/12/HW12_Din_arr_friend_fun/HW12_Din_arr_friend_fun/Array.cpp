#include "Array.h"

Array::Array()
{
	arr = nullptr;
	sz = 0;
}

Array::Array(int *aArr, int aSz)
{
	sz = aSz;
	arr = new int[sz];
	for (int i=0; i<sz; i++)
		arr[i] = aArr[i];
}

Array::Array(const Array &A)
{
	sz = A.sz;
	arr = new int[sz];
	for (int i = 0; i < sz; i++)
		arr[i] = A.arr[i];
}

Array::~Array()
{
	delete[] arr;
}

int Array::get_sz()const
{
	return sz;
}

int Array::get_element(int index)const
{
	return arr[index];
}

void Array::print()
{
	for (int i = 0; i < sz; i++)
		std::cout << arr[i] << " ";
	std::cout << "\n";
}

Array Array::operator= (const Array &A)
{
	sz = A.sz;
	if (sz > 0)
		delete[] arr;
	arr = new int[sz];
	for (int i = 0; i < sz; i++)
		arr[i] = A.arr[i];
	return *this;
}

Array Array::operator= (int a)
{
	sz = 1;
	if (sz > 0)
		delete[] arr;
	arr = new int[sz];
	arr[0] = a;
	return *this;
}

Array Array::operator+ (const Array &A)
{
	Array temp;
	temp.sz = sz + A.sz;
	temp.arr = new int [temp.sz];
	for (int i=0, j=0; i < temp.sz; i++)
	{
		if (i < sz)
			temp.arr[i] = arr[i];
		else
		{
			temp.arr[i] = A.arr[j];
			j++;
		}
	}
	return temp;
}

Array Array::operator+ (int a)
{
	Array temp;
	temp.sz = sz + 1;
	temp.arr = new int[temp.sz];
	for (int i = 0; i < sz; i++)
		temp.arr[i] = arr[i];
	temp.arr[sz] = a;
	return temp;
}

Array Array::operator+= (const Array &A)
{
	int *temp = new int[sz + A.sz];
	for (int i = 0, j = 0; i < sz + A.sz; i++)
	{
		if (i < sz)
			temp[i] = arr[i];
		else
		{
			temp[i] = A.arr[j];
			j++;
		}
	}
	if (sz > 0)
		delete[] arr;
	sz += A.sz;
	arr = temp;
	return *this;
}

Array Array::operator+= (int a)
{
	int *temp = new int[sz + 1];
	for (int i = 0; i < sz; i++)
		temp[i] = arr[i];
	temp[sz] = a;
	if (sz > 0)
		delete[] arr;
	sz++;
	arr = temp;
	return *this;
}

