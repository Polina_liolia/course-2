#include "Word.h"

/*
String en;		//word in english
int size_rus;
String *rus;	//word in russian
int counter;
*/

Word::Word()	//default constructor
{
	size_rus = 0;
	rus = nullptr;
	counter = 0;
}

/*overloaded constructor
takes: 
String aEn - word in english
String aRus - word in russian
int aCounter - counter of word's calls
*/
Word::Word(String aEn, String aRus, int aCounter)	
{
	en = aEn;
	size_rus = 1;
	rus = new String[size_rus];
	rus[0] = aRus;
	counter = aCounter;
}

Word::Word(const Word & aWord)	//copy constructor
{
	en = aWord.en;
	size_rus = aWord.size_rus;
	if (size_rus == 0)
		rus = nullptr;
	else
	{
		rus = new String[size_rus];
		for (int i = 0; i < size_rus; i++)
			rus[i] = aWord.rus[i];
	}
	counter = aWord.counter;
}

Word::~Word()	//destructor
{
	delete[] rus;
}

void Word::set_word(String aEn, String aRus, int aCounter)
{
	en = aEn;
	if (size_rus != 0)
		delete[]rus;
	size_rus = 1;
	rus = new String[size_rus];
	rus[0] = aRus;
	counter = aCounter;
}

void Word::add_rus(String aRus)	//to add a russian translation (takes translation in String)
{
	size_rus++;
	if (size_rus == 1)
	{
		rus = new String[size_rus];
		rus[0] = aRus;
	}
	else
	{
		String *tmp = new String[size_rus];
		for (int i = 0; i < size_rus - 1; i++)
			tmp[i] = rus[i];
		tmp[size_rus - 1] = aRus;
		delete[] rus;
		rus = tmp;
	}
	counter++;
}

void Word::del_rus(String aRus)		//to delete russion translation (takes translation in String)
{
	int rus_index = -1;
	try
	{
		//serching for word to delete
		for (int i = 0; i < size_rus; i++)
			if (rus[i] == aRus)
			{
				rus_index = i;
				break;
			}
		if (rus_index == -1)
			throw "Translation not found";	//generating exception if russion word not found
	}
	catch (char *msg)	//outputs error message and returns
	{
		std::cout << msg << "\n";
		return;
	}
	size_rus--;
	if (size_rus == 0)
		delete[] rus;
	else
	{
		String *tmp = new String[size_rus];
		for (int i = 0, j = 0; i < size_rus + 1; i++)
		{
			if (i != rus_index)
			{
				tmp[j] = rus[i];
				j++;
			}
		}
		delete[] rus;
		rus = tmp;
	}
	counter++;
}

void Word::del_rus(int number)	//to delete russion translation (takes number of translation (starts from 1))
{
	try 
	{ 
		if ((number - 1) < 0 || (number - 1) >= size_rus)
			throw "A wrong translation number";	//generating exception if russion word not found
	}
	catch (char *msg)	//outputs error message and returns
	{
		std::cout << msg << "\n";
		return;
	}
	int rus_index = number - 1;
	size_rus--;
	if (size_rus == 0)
		delete[] rus;
	else
	{
		String *tmp = new String[size_rus];
		for (int i = 0, j = 0; i < size_rus + 1; i++)
		{
			if (i != rus_index)
			{
				tmp[j] = rus[i];
				j++;
			}
		}
		delete[] rus;
		rus = tmp;
	}
	counter++;
}

void Word::change_rus(String aRus_old, String aRus_new)	//to change russian translation (takes old and new translations in String)
{
	int rus_index = -1;
	try
	{
		//serching for word to change
		for (int i = 0; i < size_rus; i++)
			if (rus[i] == aRus_old)
			{
				rus_index = i;
				break;
			}
		if (rus_index == -1)
			throw "Translation not found";	//generating exception if russion word not found
	}
	catch (char *msg)	//outputs error message and returns
	{
		std::cout << msg << "\n";
		return;
	}
	rus[rus_index] = aRus_new; //changing translation
	counter++;
}

void Word::change_rus(int number, String aRus_new)	//to change russian translation (takes number of old translations and a new one in String)
{
	try
	{
		if ((number - 1) < 0 || (number - 1) >= size_rus)
			throw "A wrong translation number";	//generating exception if russion word not found
	}
	catch (char *msg)	//outputs error message and returns
	{
		std::cout << msg << "\n";
		return;
	}
	rus[number-1] = aRus_new; //changing translation
	counter++;
}

int Word::get_counter() const	//returns meaning of word's calls counter
{
	return counter;
}

void Word::add_count()	//to add one more calling of a word
{
	counter++;
}


