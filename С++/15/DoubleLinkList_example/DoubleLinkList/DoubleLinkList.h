#pragma once
#include "Element.h"
class DoubleLinkList
{
	Element* head;
	Element* tail;
public:
	DoubleLinkList();
	void add(int aValue);
	void show();
	void show_reverse();
	bool delByIndex(unsigned int index);
	//bool delPackage(unsigned int index, unsigned int quantity);
	~DoubleLinkList();
	bool insertByIndex(int index, int value);
	bool del_package(int index, int number);	//int number - total number of elements to delete

};