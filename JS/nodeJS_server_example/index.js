var router = require("./js/router");
var server = require("./js/server");
var requestHandlers = require("./js/requestHandlers");

var handle = {};
handle["/addresses"] = requestHandlers.start;
// handle["/start"] = requestHandlers.start;
// handle["/pressure"] = requestHandlers.pressure;
// handle["/service"] = requestHandlers.service;
// handle["/damage"] = requestHandlers.damage;
// handle["/style"] = requestHandlers.style;
// handle["/frontendscript"] = requestHandlers.frontendscript;

server.start(router.route, handle);
