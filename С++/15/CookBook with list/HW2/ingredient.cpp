#include <iostream>
#include <cstring>
using std::cout;
using std::cin;
#include "ingredient.h"

	ingredient::ingredient(char *aItem, float aDose)
	{
		if (aItem && strlen(aItem))
		{
			item = new char[(strlen(aItem) + 1)];
			strcpy_s(item, (strlen(aItem) + 1), aItem);
		}
		else
			item = nullptr;
		dose = aDose;
	}

	ingredient::ingredient(const ingredient &temp)
	{
		if (temp.item && strlen(temp.item))
		{
			item = new char[(strlen(temp.item) + 1)];
			strcpy_s(item, (strlen(temp.item) + 1), temp.item);
		}
		else
			item = nullptr;
		dose = temp.dose;
	}

	ingredient ingredient::operator=(ingredient temp)
	{
		if (temp.item && strlen(temp.item))
		{
			item = new char[(strlen(temp.item) + 1)];
			strcpy_s(item, (strlen(temp.item) + 1), temp.item);
		}
		else
			item = nullptr;
		dose = temp.dose;
		return *this;
	}

	ingredient::~ingredient()
	{
		delete[] item;
	}

	char * ingredient::get_item() const
	{
		return item;
	}

	float ingredient::get_dose() const
	{
		return dose;
	}
