#include <iostream>
using namespace std;
class a
{
public:
	int x;
	int y;
	a (){
		x=0;
	}

	void f1(){
		cout << "A::f1()" << endl;
	}
};

class b:public virtual a
{};
class c:public virtual a
{};

class d:public b, public c
{
public:	
	void f1()
	{
		cout << "D::f1()" << endl;
		a::f1();
		cout << x << "(" << &x << ")"<< endl;
		cout << b::x << "(" << &(b::x) << ")" << endl;
		cout << c::x << "(" << &(c::x) << ")" << endl;
	}
};

void main()
{
	d e;
	e.x = 3;
	e.f1();
}
