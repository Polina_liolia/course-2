#include "example.h"
#include "Point3D.h"

void main()
{
	A test("one", 1);
	A test1 = test;

	point3D a(3, 2, 1, "point a");
	a.print_name();
	a.print_coordinate();
	point3D b = a;
	b.print_name();
	b.print_coordinate();
}