#pragma once
#include"Wather_transport.h"

enum prop_type { outside_energy, inside_energy };

class Overwather_transport : public Wather_transport
{
private:
	prop_type prop;

public:
	Overwather_transport();
	Overwather_transport(string aModel, double aWeight, double aMax_speed, double aDisplacement, prop_type aProp, speed aSpeed_units = knots );
	~Overwather_transport() {};

public:
	void set_prop(prop_type aProp);
	prop_type get_prop()const;

	virtual speed get_speed_units()const;
	virtual void print();

};