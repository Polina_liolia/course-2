$(document).ready(function(){
    $("#reg").validate({

        rules:{
            login:{
                required: true,
                minlength: 6,
                maxlength: 20
            },
            password:{
                required: true,
                minlength: 8,
                maxlength: 15
            },
            conf_password:{
                required: true,
                equalTo: "#password"

            },
            email:{
                required: true,
                email: true
            }
        }

//         messages:{
//             login:{
//                 required: "Это поле обязательно для заполнения",
//                 minlength: "Минмальная длина - 6 символов",
//                 maxlength: "Минмальная длина - 20 символов"
//             },
//             password:{
//                 required: "Это поле обязательно для заполнения",
//                 minlength:"Минмальная длина - 8 символов",
//                 maxlength: "Минмальная длина - 15 символов"
//
// },
//             conf_password:{
//                 required: "Это поле обязательно для заполнения",
//                 equalTo: "Введены два разных пароля"
//             },
//             email:{
//                 required: "Это поле обязательно для заполнения",
//                 email: "Не корректный адрес электронной почты"
//             }
//         }

    });

});

var form_reg = document.forms.reg;
var usersList = [];

function registerUser() {
    if (!login.value || !password.value || !email.value)
    {
        event.preventDefault();
        return;
    }
    var logins_str = localStorage.getItem("logins_list");
    var logins = logins_str  == undefined ? [] : logins_str.split(',');
    for (var i = 0; i <logins.length; i++)
        if (logins[i] == login.value)
        {
            all_users.innerText = "A user with such login already exists!";
            event.preventDefault();
            return;
        }
    logins.push(login.value);
    logins_str = logins.join(",");
    localStorage.setItem("logins_list", logins_str);

    var user_data = {};
    user_data.password = password.value;
    user_data.email = email.value;
    user_data = JSON.stringify(user_data);
    localStorage.setItem(login.value, user_data);
}
form_reg.addEventListener("submit", registerUser);

var showAllUsers = document.querySelector("#reg > input[type='button']:nth-child(3)");

function showAll() {
    var logins_str = localStorage.getItem("logins_list");
    var logins = logins_str  == undefined ? [] : logins_str.split(',');
    if (logins.length == 0)
        all_users.innerText = "No users found";
    else
    {
        all_users.innerText = "";
        for (var i = 0; i < logins.length; i++)
        {
            all_users.innerText += logins[i];
            var user_data = localStorage.getItem(logins[i]);
            user_data = JSON.parse(user_data);
            all_users.innerText += " (" + user_data.email + ") - " + user_data.password + "\n";
        }
    }
}
showAllUsers.addEventListener("click", showAll);


var authorization_form = document.forms.authorization;
function authorization_check() {
    if (!check_login.value || !check_password.value)
    {
        authorization_result.innerText = "Error: not all fields are filled in!";
        event.preventDefault();
        return;
    }
    var logins_str = localStorage.getItem("logins_list");
    var logins = logins_str  == undefined ? [] : logins_str.split(',');
    if (logins.length == 0) {
        authorization_result.innerText = "No users found";
        event.preventDefault();
    }
    else
    {
        all_users.innerText = "";
        for (var i = 0; i < logins.length; i++) {
            var user_data = localStorage.getItem(logins[i]);
            user_data = JSON.parse(user_data);
            if ((check_login.value == logins[i] || check_login.value == user_data.email) &&
                user_data.password == check_password.value) {
                authorization_result.innerText = "Welcome, " + logins[i] + "!";
                return;
            }
        }
        authorization_result.innerText = "Wrong login or password.";
        event.preventDefault();
    }
}
authorization_form.addEventListener("submit", authorization_check);


