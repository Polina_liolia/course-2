#include "Aviation.h"

Aviation::Aviation() : Air_transport()	//default constructor sets aviation type as civil (default)
{
	type = civil;
}

//overloaded constructor
Aviation::Aviation(string aModel, double aWeight, double aMax_speed, speed aSpeed_units, double aMax_height, aviation_type aType) :
	Air_transport(aModel, aWeight, aMax_speed, aSpeed_units, aMax_height)
{
	type = aType;
}

//method to set or change property type
void Aviation::set_type(aviation_type aType)
{
	type = aType;
}

//method to get meaning of property type
aviation_type Aviation::get_type() const
{
	return type;
}

//virtual method to get meaning of property speed_units
speed Aviation::get_speed_units() const
{
	cout << "Speed units for aviation - ";
	switch (speed_units)
	{
	case 0: cout << "km_h\n"; break;
	case 1: cout << "Mach\n"; break;
	case 2: cout << "miles_h\n"; break;
	case 3: cout << "knots\n"; break;
	}
	return speed_units;
}

//virtual method to print all information about Aviation instance
void Aviation::print()
{
	Air_transport::print();	//calling of basic class method
	cout << "Aviation type: ";
	switch (type)
	{
	case 0: cout << "military\n"; break;
	case 1: cout << "civil\n"; break;
	}
}
