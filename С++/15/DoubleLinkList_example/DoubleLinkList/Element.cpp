#include "Element.h"
Element::Element()
{
	value = 0;
	next = prev = nullptr;
}
Element::Element(int aValue)
{
	value = aValue;
	next = prev =  0;
}
void Element::set_value(int aValue)
{
	value = aValue;
}
int Element::get_value()
{
	return value;
}
void Element::set_next(Element* aNext)
{
	next = aNext;
}
Element* Element::get_next()
{
	return next;
}


void Element::set_prev(Element* aPrev)
{
	prev = aPrev;
}
Element* Element::get_prev()
{
	return prev;
}
