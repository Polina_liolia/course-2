
//takes the amount of rows and columns and creates table
function createTable(rows, cols) {
    document.write('<table cellspacing="5px", frame="box", width = "200px", margin = "auto", align = "center"; style="display: inline-block; box-shadow: 5px 5px 2px 0px rgba(0,0,0,0.55); border-radius: 5%; background-repeat: no-repeat; ' + bcgrnd);
    for (var i = 0; i < rows; i++)
    {
        document.write("<tr>");
        for (var j = 0; j < cols; j++)
            document.write("<td></td>");
        document.write("</tr>");
    }
    document.write("</table>");
}

//takes Data object, returns the number of days in current month
function days_in_month (date)
{
    var current_month = date.getMonth();
    while (date.getMonth() === current_month)
    {
        var current_day = date.getDate();
        date.setDate(++current_day);
    }
    return current_day - 1;
}

var now = new Date();
var year=now.getFullYear();
var month = now.getMonth();
var day = now.getDate();

var monthes = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

var font_clr  = "black";
function choose_font_color()
{
    font_clr = (month == 11 || month <= 1) ? 'blue">':
        (month <= 4) ? 'green">' :
            (month <= 7) ? 'green">' :
                'red">';
}

var bcgrnd = "white";

function choose_bcg ()
{
    bcgrnd = (month == 11 || month <= 1) ? 'background-size: 120%; background-image: url(img/winter.jpg)">':
        (month <= 4) ? 'background-size: 120%; background-image: url(img/spring.jpg)">':
            (month <= 7) ? 'background-size: 100%; background-image: url(img/summer.jpg)">':
                'background-size: 160%; background-image: url(img/autumn.png)">';
}

function make_calendar() {
    document.write('<div style="width:1000px; margin: auto">');
    document.write('<div style="width:400px; margin: auto">');
    document.write('<p style="text-align: center; margin-bottom: 0; padding-bottom: 0; font-style: italic; color: ' + font_clr + monthes[month] + ", " + year + "</p>");
    document.write('<input type="button" id="prev_month" style="display: inline-block; box-shadow: 2px 2px 1px 0px rgba(0,0,0,0.55); border-radius: 15%; margin: 2% 2% 2% 2%; width: 70px" value=' + monthes[month - 1] + '>');
    createTable(6, 7);
    document.write('<input type="button" id="next_month" style="display: inline-block; box-shadow: 2px 2px 1px 0px rgba(0,0,0,0.55); border-radius: 15%; margin: 2% 2% 2% 2%; width: 70px" value=' + monthes[month + 1] + '>');
    document.write('</div>');
    document.write('</div>');

    prev_month.onclick = function () {
        if (month - 1 < 0)
            now.setMonth(11);
        else
            now.setMonth(--month);

        document.close();
        choose_font_color();
        choose_bcg();
        make_calendar();
    }

    next_month.onclick = function () {
        if (month + 1 > 11)
            now.setMonth(0);
        else
            now.setMonth(++month);
        document.close();
        choose_font_color();
        choose_bcg();
        make_calendar();
    }

    var first_day_of_month_number = new Date(year, month, 1).getDay();
    first_day_of_month_number = (first_day_of_month_number === 0) ? 7 : first_day_of_month_number;

    var arg = now; //to avoid changing of object Data now in function
    var total_days_in_month = days_in_month(arg);

    var days_of_week = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
    var Calendar = document.getElementsByTagName('table');
    var Calendar_row = Calendar[0].getElementsByTagName('tr');
    var rows_amount = Calendar_row.length;
//inputting the first row with day names:
    for (i = 0; i < 7; i++) {
        if (i < 5)  //working days
            document.getElementsByTagName('td')[i].innerHTML = '<span style = "font-weight: bold">' + days_of_week[i] + '</span>';
        else //weekends
            document.getElementsByTagName('td')[i].innerHTML = '<span style = "font-weight: bold; color: ' + font_clr + days_of_week[i] + '</span>';
    }
    //inputting a calendar:
    var prev_month_days_amount = days_in_month(new Date(((month == 0) ? year - 1 : year), ((month == 0) ? 11 : month - 1), 28));

    var current_day = 1;
    var display_last_month_start = prev_month_days_amount - first_day_of_month_number + 2;  //first day of previous month to show in current calendar
    var flag = false;//to indicate that month is ended and days of next month started
    for (i = 1; i < rows_amount && current_day <= total_days_in_month; i++) {
        for (var j = 0; j < 7; j++) {
            if (current_day > total_days_in_month) {
                flag = true;
                current_day = 1;
            }

            if (flag)//days of the next month
                Calendar_row[i].getElementsByTagName('td')[j].innerHTML = '<span style = "color: grey">' + current_day++ + '</span>';
            else if (j < first_day_of_month_number - 1 && i == 1)//days of the previous month
                Calendar_row[i].getElementsByTagName('td')[j].innerHTML = '<span style = "color: grey">' + display_last_month_start++ + '</span>';
            else if (current_day == day)//current day
                Calendar_row[i].getElementsByTagName('td')[j].innerHTML = '<span style = "font-weight: bold; color: white; background-color: ' + font_clr + current_day++ + '</span>';
            else if (j < 5)//working days
                Calendar_row[i].getElementsByTagName('td')[j].innerHTML = '<span>' + current_day++ + '</span>';
            else//weekends
                Calendar_row[i].getElementsByTagName('td')[j].innerHTML = '<span style = "color: ' + font_clr + current_day++ + '</span>';

        }
    }
}

choose_font_color();
choose_bcg();
make_calendar();