#include <iostream>
using std::cout;
using std::endl;
#include "SomeClass.h"

SomeClass::SomeClass(){
	some_field = 0;
}


SomeClass::~SomeClass(){
	cout << "SomeClass destructor." << endl;
}

void SomeClass::someClassFunction() {
	some_field = 3;
	cout << "SomeClass::someClassFunction()" << endl;
}
