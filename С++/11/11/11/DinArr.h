#pragma once
#include <iostream>
#include "Remove.h"

class DinArr
{
private:
	int *arr;
	int sz;

public:
	DinArr();
	DinArr(int *aArr, int aSz);
	DinArr(const DinArr &a);
	~DinArr();

public:
	DinArr operator=(const DinArr &a);
	void add_element(int a);
	void remove_element(int a);
	void print();
	void remove_if(Remove functor);
};