#include"Schoolboy.h"
#include"Student.h"
#include"Graduate.h"
#include <map>
#include <iterator>
using std::multimap;
using std::pair;
using std::iterator;


//globaly overloaded output of class date
ostream& operator << (ostream& os, date &aDate)
{
	os << aDate.day << "." << aDate.month << "." << aDate.year << "\n";
	return os;
}

//globaly overloaded input of class date
istream& operator >> (istream& is, date &aDate)
{
	short tmp;
	is >> tmp; //day input
	aDate.day = tmp;
	is >> tmp; //month input
	aDate.month = tmp;
	is >> tmp; //year input
	aDate.year = tmp;
	return is;
}


//globaly overloaded output of class Schoolboy
ostream& operator << (ostream& os, Schoolboy &S)
{
	os << "Name: " << S.name << "\n";
	os << "Surname: " << S.surname << "\n";
	os << "Birth date: " << S.birth_day << "\n";
	os << "Address: " << S.address << "\n";
	os << "School: " << S.school << "\n";
	os << "Phone: " << S.phone << "\n";
	return os;
}

//globaly overloaded input of class Schoolboy
istream& operator >> (istream& is, Schoolboy &S)
{
	string tmp;
	date d;
	is >> tmp; //name input
	S.name = tmp;
	is >> tmp; //surname input
	S.surname = tmp;
	is >> d; //date input
	S.birth_day = d;
	is >> tmp; //address input
	S.address = tmp;
	is >> tmp; //school input
	S.school = tmp;
	is >> tmp; //phone input
	S.phone = tmp;
	return is;
}


//globaly overloaded output of class Student
ostream& operator << (ostream& os, Student &S)
{
	operator<<(std::cout, static_cast<Schoolboy>(S));
	os << "Univercity: " << S.univercity << "\n";
	os << "Speciality: " << S.speciality << "\n";
	return os;
}

//globaly overloaded input of class Student
istream& operator >> (istream& is, Student &S)
{
	string tmp;
	operator>>(std::cin, static_cast<Schoolboy>(S));
	is >> tmp; //univercity input
	S.univercity = tmp;
	is >> tmp; //speciality input
	S.speciality = tmp;
	return is;
}


//globaly overloaded output of class Graduate
ostream& operator << (ostream& os, Graduate &S)
{
	operator<<(std::cout, static_cast<Student>(S));
	os << "Department: " << S.department << "\n";
	return os;
}

//globaly overloaded input of class Graduate
istream& operator >> (istream& is, Graduate &S)
{
	string tmp;
	operator >> (std::cin, static_cast<Student>(S));
	is >> tmp; //department input
	S.department = tmp;
	return is;
}

//to compare multimap elements
bool compare(Schoolboy *A, Schoolboy *B)
{
	return A->get_surname() > B->get_surname();
}


void main()
{
	//test instances:
	date b_day(5, 5, 1995);
	Schoolboy A("Ivan", "Ivanov", b_day, "ul.Klochkovskaya 3A", "# 85", "0957458521");
	Student B("Petr", "Petrov", b_day, "ul.Klochkovskaya 7A", "# 185", "KhNEU", "management", "0957458522");
	Graduate C("Sergey", "Sokolov", b_day, "ul.Klochkovskaya 17A", "# 285", "Karazina", "history", "Historical", "0957458523" );

	std::cout << A << '\n';
	std::cout << B << '\n';
	std::cout << C << '\n';
	std::cout <<"_______________________________\n";

	//placing instances to multimap:
	multimap<string, Schoolboy*> people; //key value - Surname
	typedef pair<string, Schoolboy*> Schoolboy_pair;
	typedef pair<string, Student*> Student_pair;
	typedef pair<string, Graduate*> Graduate_pair;
	
	people.insert(Schoolboy_pair(static_cast<string>("Ivanov"),  new Schoolboy("Ivan", "Ivanov", b_day, "ul.Klochkovskaya 3A", "# 85", "0957458521")));
	people.insert(Student_pair(static_cast<string>("Petrov"), new Student ("Petr", "Petrov", b_day, "ul.Klochkovskaya 7A", "# 185", "KhNEU", "management", "0957458522")));
	people.insert(Graduate_pair(static_cast<string>("Sokolov"), new Graduate("Sergey", "Sokolov", b_day, "ul.Klochkovskaya 17A", "# 285", "Karazina", "history", "Historical", "0957458523")));

	multimap<string, Schoolboy*>::iterator it = people.begin();
	for (it = people.begin(); it != people.end(); it++)
		std::cout << *(*it).second << "\n";
	
	try
	{
		C.print_to_file("graduate.bin");
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}

	/*Graduate D;
	try
	{
		D.read_from_file("graduate.bin");
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}
	std::cout << "D:\n" << D;*/
}