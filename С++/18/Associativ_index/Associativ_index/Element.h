#pragma once

template <typename T1, typename T2>

class Element{
	T1 _key;
	T2 _value;

public:
	Element():_key(0),_value(0){
	}
	Element(T1 aKey, T2 aValue):_key(aKey),_value(aValue){
	}
	T1 getKey(){
	return _key;
	}
	T2& getValue(){
	return _value;
	}
	void setKey(T1 aKey){
		_key = aKey;
	}
	void setValue(T2 aValue){
		_value = aValue;
	}
};