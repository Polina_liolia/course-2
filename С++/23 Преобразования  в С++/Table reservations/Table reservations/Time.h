#pragma once
#include<ctime>
#include <iostream>
using std::ostream;
using std::istream;
#include<fstream>
using std::ios;
using std::ofstream;
using std::ifstream;


class Time
{
private:
	int hours;
	int minutes;

public:
	Time();
	Time(int aHours, int aMinutes);
	~Time() {};
	
public:
	void set_hours(int aHours);
	void set_minutes(int aMinutes);
	void set_time_in_minutes(int time_minutes);
	int get_hours()const;
	int get_minutes()const;
	int get_time_in_minutes()const;
	void set_current_time();
	void print_to_file(ofstream &F)const;
	void read_from_file(ifstream &F);
	
	Time operator=(const Time &aTime);
	Time operator += (int minutes);
	Time operator+(int minutes);
	Time operator-(int minutes);

	bool operator>(const Time & aTime);
	bool operator<(const Time & aTime);
	bool operator>=(const Time & aTime);
	bool operator<=(const Time & aTime);

	friend ostream& operator<<(ostream& os, Time& t);
};