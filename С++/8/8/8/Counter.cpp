#include "Counter.h"

Counter::Counter(int aMin, int aMax, int aCount) :min(aMin), max(aMax)
{
	count = aMin;
}

Counter::~Counter() {}

Counter Counter::operator++()
{
	if (count+1 >= max)
	{
		count = min;
		return Counter(min, max, count);
	}
	count++;
	return *this;
}

Counter Counter::operator++(int)
{
	if (count + 1 >= max)
	{
		int temp_count = count;
		count = min;
		return Counter(min, max, temp_count);
	}
	return Counter(min, max, count++);
}

Counter Counter::operator--()
{
	if (count - 1 <= min)
	{
		count = min;
		return Counter(min, max, count);
	}
	count--;
	return *this;

}

Counter Counter::operator--(int)
{
	if (count - 1 <= min)
	{
		int temp_count = count;
		count = min;
		return Counter(min, max, temp_count);
	}
	return Counter(min, max, count--);

}

void Counter::print()
{
	cout << count << "\n";
}