#pragma once
#include "Student.h"
#include <set>
using std::set;
class Graduate :
	public Student
{
private:
	string department;
	set<string> scientific_interests;

public:
	Graduate();
	Graduate(string aName, string aSurname, date aBirth_day, string aAdress, string aSchool, string aUnivercity, string aSpeciality, string aDepartment, string aPhone = "");
	~Graduate();

	void set_department(string aDepartment);
	string get_department()const;

	void add_scientific_interest(string aInterest);
	void remove_scientific_interest(string aInterest);

	Graduate operator=(const Graduate &G);

	void print_to_file(char * file_name);
	void read_from_file(char* file_name);

	friend ostream& operator << (ostream& os, Graduate &S);
	friend istream& operator >> (istream& is, Graduate &S);
};

