#pragma once
#include <iostream>
using std::cout;
using std::ostream;
#include "Person.h"

class Teacher : public Person
{
private:
	String speciality;

public:
	Teacher();
	//overloaded constructor:
	Teacher(String aSurname, String aName, String aPatronymic, short aDay, short aMonth, short aYear, String aPhone, String aUnivercity, String aCountry, String aCity, String aSpeciality);
	Teacher(const Teacher &a);
	~Teacher();

public:
	//methods to set the meaning of every property:
	void set_speciality(String aSpeciality);

	int print_to_file(FILE * f, char * file_name);

	//methods to get the meaning of every property:
	String get_speciality()const;

	void print_data();		//to print information about Teacher

	friend ostream& operator << (ostream& os, const Teacher &aSt);
};