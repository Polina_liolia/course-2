var currentNewsID = 0;
var maxNewsID = 4;
$(function () {
    $("#prev_btn").click(function () {
        currentNewsID--;
        if (currentNewsID < 0)
            currentNewsID = maxNewsID;
        $('#preloader').show();
        setTimeout(function () {
            $.get("ajax.php", {"newsid": currentNewsID},
                function (text) {           //if successfully loaded
                    $('#preloader').hide();
                    $("#message").html(text);
                }, "json")
                .fail(function (text, textStatus) {
                console.log(textStatus);
                $("#message").html(textStatus);
                $('#preloader').hide();
            })
        }, 1000);
    });
    $("#next_btn").click(function () {
        currentNewsID++;
        if (currentNewsID > maxNewsID)
            currentNewsID = 0;
        $('#preloader').show();
        setTimeout(function () {
            $.get("ajax.php", {"newsid": currentNewsID},
                function (text) {       //if successfully loaded
                    $('#preloader').hide();
                    $("#message").html(text);
                }, "json")
                .fail(function (text, textStatus) {
                console.log(textStatus);
                $("#message").html(textStatus);
                $('#preloader').hide();
            })
        }, 1000);

    });
    $.get("ajax.php", {"newsid": currentNewsID},
        function (text) {       //if successfully loaded
            $('#preloader').hide();
            $("#message").html(text);
        }, "json")
        .fail(function (text, textStatus) {
        console.log(textStatus);
        $("#message").html(textStatus);
        $('#preloader').hide();
    })
});

