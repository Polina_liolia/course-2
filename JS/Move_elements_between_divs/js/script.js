//adding events to add elements to divs by the button click:
var buttonAdd1 = document.querySelector(".main > input:first-of-type");
var buttonAdd2 = document.querySelector(".main > input:nth-of-type(2)");

var ul1 = document.querySelector(".main > ul:first-of-type");
var ul2 = document.querySelector(".main > ul:last-of-type");

function div1AddLi()
{
    var liNew =document.createElement('li');
    liNew.classList.add('draggable');
    ul1.appendChild(liNew);
    liNew.innerHTML = prompt("Input text", "New text");
}

function div2AddLi()
{
    var liNew =document.createElement('li');
    liNew.classList.add('draggable');
    ul2.appendChild(liNew);
    liNew.innerHTML = prompt("Input text", "New text");
}

buttonAdd1.addEventListener("click", div1AddLi);
buttonAdd2.addEventListener("click", div2AddLi);

//adding events to select elements of divs:
function SelectLi()
{
    var target = event.target;
    if (target.tagName != "LI")
        return;
    if (target.parentElement == ul1)  //if it is the element of the first div
    {
        if (target.dataset.status != "selected")
        {
            target.style.backgroundColor = "rgb(166, 0, 41)";
            target.dataset.status = "selected";
        }
        else
        {
            target.style.backgroundColor = "hotpink";
            target.dataset.status = undefined;
        }
    }
    else    //if it is the element of the second div
    {
        if (target.dataset.status != "selected")
        {
            target.style.backgroundColor = "rgb(0, 108, 145)";
            target.dataset.status = "selected";
        }
        else
        {
            target.style.backgroundColor = "deepskyblue";
            target.dataset.status = undefined;

        }
    }
}
ul1.addEventListener("click", SelectLi);
ul2.addEventListener("click", SelectLi);

var buttonMove1 = document.querySelector('input:nth-of-type(3)'); //move right
var buttonMove2 = document.querySelector('input:nth-of-type(4)'); //move left
var buttonMoveAll = document.querySelector('input:nth-of-type(5)'); //move all

//adding events to buttons to move elements between divs:
function buttonMovesElement()
{
    if (event.target == buttonMove1 || event.target == buttonMoveAll)    //if move right or move all
    {
        var allLi = document.querySelectorAll('ul:first-of-type>li'); //getting all li-elements of the left div
        for (var i = 0; i < allLi.length; i++)
        {
            if (allLi[i].dataset.status == "selected")
            {
                allLi[i].dataset.status = undefined;
                allLi[i].style.backgroundColor = "deepskyblue";
                ul2.appendChild(allLi[i]);
            }
        }
    }
    if (event.target == buttonMove2 || event.target == buttonMoveAll)       //if move left or move all
    {
        allLi = document.querySelectorAll('ul:last-of-type>li'); //getting all li-elements of the right div
        for (var i = 0; i < allLi.length; i++)
        {
            if (allLi[i].dataset.status == "selected")
            {
                allLi[i].dataset.status = undefined;
                allLi[i].style.backgroundColor = "hotpink";
                ul1.appendChild(allLi[i]);
            }
        }
    }
}

buttonMove1.addEventListener("click", buttonMovesElement);
buttonMove2.addEventListener("click", buttonMovesElement);
buttonMoveAll.addEventListener("click", buttonMovesElement);


//Drag’n’Drop of elements:

var DragManager;
DragManager = new function () {

    /**
     * составной объект для хранения информации о переносе:
     * {
   *   elem - элемент, на котором была зажата мышь
   *   avatar - аватар
   *   downX/downY - координаты, на которых был mousedown
   *   shiftX/shiftY - относительный сдвиг курсора от угла элемента
   * }
     */
    var dragObject = {};

    var self = this;

    function onMouseDown(e) {

        if (e.which != 1) return;

        var elem = e.target.closest('.draggable');
        if (!elem) return;

        dragObject.elem = elem;

        // запомним, что элемент нажат на текущих координатах pageX/pageY
        dragObject.downX = e.pageX;
        dragObject.downY = e.pageY;

        return false;
    }

    function onMouseMove(e) {
        if (!dragObject.elem) return; // элемент не зажат

        if (!dragObject.avatar) { // если перенос не начат...
            var moveX = e.pageX - dragObject.downX;
            var moveY = e.pageY - dragObject.downY;

            // если мышь передвинулась в нажатом состоянии недостаточно далеко
            if (Math.abs(moveX) < 3 && Math.abs(moveY) < 3) {
                return;
            }

            // начинаем перенос
            dragObject.avatar = createAvatar(e); // создать аватар
            if (!dragObject.avatar) { // отмена переноса, нельзя "захватить" за эту часть элемента
                dragObject = {};
                return;
            }

            // аватар создан успешно
            // создать вспомогательные свойства shiftX/shiftY
            var coords = getCoords(dragObject.avatar);
            dragObject.shiftX = dragObject.downX - coords.left;
            dragObject.shiftY = dragObject.downY - coords.top;

            startDrag(e); // отобразить начало переноса
        }

        // отобразить перенос объекта при каждом движении мыши
        dragObject.avatar.style.left = e.pageX - dragObject.shiftX + 'px';
        dragObject.avatar.style.top = e.pageY - dragObject.shiftY + 'px';

        return false;
    }


    function onMouseUp(e) {
        if (dragObject.avatar) { // если перенос идет
            finishDrag(e);
        }

        // перенос либо не начинался, либо завершился
        // в любом случае очистим "состояние переноса" dragObject
        dragObject = {};
    }

    function finishDrag(e) {
        var dropElem = findDroppable(e);
        var sibling = findSibling(e);

        if (!dropElem) {
            self.onDragCancel(dragObject);
        } else {
            self.onDragEnd(dragObject, dropElem, sibling);
        }
    }

    function createAvatar(e) {

        // запомнить старые свойства, чтобы вернуться к ним при отмене переноса
        var avatar = dragObject.elem;
        var old = {
            parent: avatar.parentNode,
            nextSibling: avatar.nextSibling,
            position: avatar.position || '',
            left: avatar.left || '',
            top: avatar.top || '',
            zIndex: avatar.zIndex || ''
        };

        // функция для отмены переноса
        avatar.rollback = function () {
            old.parent.insertBefore(avatar, old.nextSibling);
            avatar.style.position = old.position;
            avatar.style.left = old.left;
            avatar.style.top = old.top;
            avatar.style.zIndex = old.zIndex
        };

        return avatar;
    }

    function startDrag(e) {
        var avatar = dragObject.avatar;

        // инициировать начало переноса
        document.body.appendChild(avatar);
        avatar.style.zIndex = 9999;
        avatar.style.position = 'absolute';
    }

    function findDroppable(event) {
        // спрячем переносимый элемент
        dragObject.avatar.style.visibility = "hidden";
        // dragObject.avatar.hidden = true;

        // получить самый вложенный элемент под курсором мыши
        var elem = document.elementFromPoint(event.clientX, event.clientY);

        // показать переносимый элемент обратно
        dragObject.avatar.style.visibility = "visible";
        // dragObject.avatar.hidden = false;

        if (elem == null) {
            // такое возможно, если курсор мыши "вылетел" за границу окна
            return null;
        }

        return elem.closest('.droppable');
    }

    function findSibling(event) {
        // спрячем переносимый элемент
        dragObject.avatar.style.visibility = "hidden";
        // dragObject.avatar.hidden = true;

        // получить самый вложенный элемент под курсором мыши
        var elem = document.elementFromPoint(event.clientX, event.clientY);

        // показать переносимый элемент обратно
        dragObject.avatar.style.visibility = "visible";
        // dragObject.avatar.hidden = false;

        if (elem == null) {
            // такое возможно, если курсор мыши "вылетел" за границу окна
            return null;
        }

        return elem.closest('.draggable');
    }

    document.onmousemove = onMouseMove;
    document.onmouseup = onMouseUp;
    document.onmousedown = onMouseDown;

    this.onDragEnd = function (dragObject, dropElem, sibling) {
        dragObject.elem.style.position="static";
        if (dropElem == ul1)
            dragObject.elem.style.backgroundColor = "hotpink";
        if (dropElem == ul2)
            dragObject.elem.style.backgroundColor = "deepskyblue";

        dropElem.insertBefore(dragObject.elem, sibling);
    };
    this.onDragCancel = function (dragObject) {
        dragObject.avatar.rollback();
    };

};


function getCoords(elem) {
    var box = elem.getBoundingClientRect();

    return {
        top: box.top + pageYOffset,
        left: box.left + pageXOffset
    };

}
