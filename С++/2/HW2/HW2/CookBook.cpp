#include <iostream>
#include <cstring>
using std::cout;
using std::cin;
#include "recipe.h"
#include "CookBook.h"

CookBook::CookBook()
	{
		sz_ar = 0;
		ar = nullptr;
	}

CookBook::~CookBook()
	{
		delete[] ar;
	}
	

	void CookBook::add_recipe(char *aName, char *aType, char *aProcess)		//to add a recipe
	{
		recipe **temp = new recipe*[(sz_ar + 1)];
		for (int i = 0; i < sz_ar; i++)
			temp[i] = ar[i];
		temp[sz_ar] = new recipe (aName, aType, aProcess);
		delete[] ar;
		sz_ar++;
		ar = new recipe*[sz_ar];
		for (int i = 0; i < sz_ar; i++)
			ar[i] = temp[i];
		delete[] temp;
	}

	void CookBook::change_recipe_name(int recipe_number, char *aName)			//to edit a recipe name
	{
		ar[recipe_number - 1][0].set_name(aName);
	}

	void CookBook::change_recipe_type(int recipe_number, char *aType)			//to edit a recipe type
	{
		ar[recipe_number - 1][0].set_type(aType);
	}

	void CookBook::change_recipe_process(int recipe_number, char *aProcess)	//to edit a recipe process
	{
		ar[recipe_number - 1][0].set_process(aProcess);
	}


	void CookBook::del_recipe(int recipe_number)								//to delete a recipe
	{
		recipe **temp = new recipe*[sz_ar -1];
		for (int i = 0, j = 0; i < sz_ar; i++)
		{
			if (i == recipe_number - 1)
				continue;
			temp[j] = ar[i];
			j++;
		}
		delete[] ar;
		sz_ar--;
		ar = new recipe*[sz_ar];
		for (int i = 0; i < sz_ar; i++)
			ar[i] = temp[i];
		delete[] temp; 
	}

	void CookBook::add_ingridient(char *aItem, float aDose, int recipe_number)	//to add an ingridient to a pointed recipe
	{
		ar[recipe_number - 1][0].set_ingridient(aItem, aDose);
	}

	void CookBook::change_ingridient_item(int recipe_number, int ingridient_number, char* aItem, float aDose)	//to change pointed ingridient of a recipe
	{
		del_recipe_ingridient(recipe_number, ingridient_number);
		add_ingridient(aItem, aDose, recipe_number);
	}

	void CookBook::del_recipe_ingridient(int recipe_number, int ingridient_number)	//to delete pointed ingridient of a pointed recipe
	{
		ar[recipe_number - 1][0].del_ingridient(ingridient_number - 1);
	}

	int CookBook::save_to_file(FILE *f, char *file_name)		//to write all data to file
	{
		//writing an information to file:
		for (int i = 0; i < sz_ar; i++)
			if (!ar[i][0].save_recipe_to_file(f, file_name))
				return -1;									//if opening file failed
		return 0;
	}

	void CookBook::print_recipes_list()							//to show the list of recipes
	{
		if (sz_ar == 0)
		{
			cout << "No recipes to show.\n";
			return;
		}
		for (int i = 0; i < sz_ar; i++)
		{
			cout << (i+1) << ". ";
			ar[i][0].print_recipe_name();
		}
	}

	void CookBook::print_recipe_information(int recipe_number)
	{
		ar[recipe_number - 1][0].print_recipe();
	}

	void CookBook::print_recipe_ingridients(int recipe_number)	//to show the list of ingridients for pointed recipe
	{
		ar[recipe_number - 1][0].print_ingredients();
	}

