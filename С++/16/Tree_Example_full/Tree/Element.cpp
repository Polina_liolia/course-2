#include "Element.h"
#include <stdio.h>
Element::Element(){
	left = right = NULL;
}
Element::Element(int x){
	right = left = NULL;
	value = x;
}
void Element::set_value(int aX){
	value = aX;
}
void Element::set_left(Element* aLeft){
	left = aLeft;
}
void Element::set_right(Element* aRight){
	right = aRight;
}
int Element::get_value(){
	return value;
}
Element* Element::get_left()
{
	return left;
}
Element* Element::get_right()
{
	return right;
}