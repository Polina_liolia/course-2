#include<windows.h>
#include<commctrl.h>
#pragma comment (lib, "comctl32.lib")

HIMAGELIST im;
HICON icon;
HBITMAP bm;

VOID CALLBACK SmartyVasiliy(HWND hwnd,
						UINT uMsg,
						UINT_PTR idEvent,
						DWORD dwTime);



void ShowError(DWORD);
LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);
void RegisterClass(TCHAR*, HINSTANCE);
UINT TimerID = 8;
int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hProvInstance, PSTR szCmdLine, int iCmdShow)
{
	TCHAR szAppName[] = L"Hello Win";
	HWND hwnd;
	MSG msg;
RegisterClass(szAppName, hInstance);
	hwnd = CreateWindow( szAppName, L"my window", WS_OVERLAPPEDWINDOW | WS_HSCROLL, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL );
	InitCommonControls();
	ShowWindow( hwnd, iCmdShow );
	UpdateWindow( hwnd );
	// I
	TimerID = SetTimer(NULL, 0/*no sense */,1000,SmartyVasiliy);
	// II
	//SetTimer(hwnd, TimerID ,1000,NULL);

	while( GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage( &msg );
		DispatchMessage( &msg );
	}
return (int)msg.wParam;
}

void RegisterClass(TCHAR * name, HINSTANCE hInst)
{
	WNDCLASSEX wc;
	wc.cbSize = sizeof(wc);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInst;
	wc.hIcon = LoadIcon( NULL, IDI_APPLICATION );
	wc.hIconSm = LoadIcon( NULL, IDI_APPLICATION );
	wc.hCursor = LoadCursor( NULL, IDC_ARROW );
	wc.hbrBackground = ( HBRUSH ) GetStockObject( WHITE_BRUSH );
	wc.lpszClassName = name;
	wc.lpszMenuName = MAKEINTRESOURCE(NULL);
	RegisterClassEx( &wc );

}
LRESULT CALLBACK WndProc( HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam )
{
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rect;
	switch( iMsg )
	{
		case WM_CREATE:
			{
			return 0;
			}
		case WM_PAINT:
			{
			hdc = BeginPaint( hwnd, &ps );
			GetClientRect( hwnd, &rect );
			EndPaint( hwnd, &ps );
			return 0;
			}
		case WM_DESTROY:
			PostQuitMessage( 0 );
			return 0;
		case WM_LBUTTONDOWN:
			
			return 0;
		case WM_TIMER:{
				//KillTimer(hwnd,8);
				if(wParam == 8){
					MessageBox(NULL,L"WM_TIMER", L"WM_TIMER", MB_OK);
				}
				
				return 0;
		}
	}
	return DefWindowProc( hwnd, iMsg, wParam, lParam );
}

void ShowError(DWORD Err)
{
	LPVOID lpMsgBuf;
	if (!FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		FORMAT_MESSAGE_FROM_SYSTEM | 
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		Err,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		(LPTSTR) &lpMsgBuf,
		0,
		NULL ))
	{
	return;
	}
	MessageBox( NULL, (LPCTSTR)lpMsgBuf, L"Error", MB_OK | MB_ICONINFORMATION );
	LocalFree( lpMsgBuf );
}


VOID CALLBACK SmartyVasiliy(HWND hwnd,UINT uMsg,UINT_PTR idEvent,DWORD dwTime)
{
	//KillTimer(NULL,TimerID);
	MessageBox(NULL,L"TimerProc", L"TIMER", MB_OK);
}