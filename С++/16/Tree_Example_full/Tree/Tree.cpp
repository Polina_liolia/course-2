#include "Tree.h"
#include "Element.h"
#include <iostream>
using namespace std;
Tree::Tree()
{
	root = NULL;
}
void Tree::add(int val)// ���������� �������� � ������	
{
	if( !root )
		root = new Element(val);
	else
	{
		Element *tmp = root;
		while(tmp)
		{
			if (val < tmp->get_value())
				if (tmp->get_left() == NULL)
				{
					tmp->set_left(new Element(val));
					break;
				}
				else tmp = tmp->get_left();
			else
				if (val > tmp->get_value())
					if (tmp->get_right() == NULL)
					{
						tmp->set_right(new Element(val));
						break;
					}
					else tmp = tmp->get_right();

				else tmp = NULL; // break; // return;
		}
	}
}
// ���������� 3 ���� ���������� ��������� ������ : ������, �������� � ������������.
// ������:		 1) ������������� ��������, 
//               2) ������� � ������,  
//               3) ������� � �������.
// ��������:	 1) ������� � ������,		
//               2) ������� � �������,  
//               3) �������������.
// ������������: 1) ������� � ������,		
//               2) �������������,	  
//               3) ������� � �������.
void Tree::print(){
	print_helper(root);
}
void Tree::print_helper(Element* el){

	if(el->get_left()) print_helper(el->get_left());
	cout << el->get_value() << endl;
	if(el->get_right()) print_helper(el->get_right());
}


void Tree::del_element(int val)
{
	Element *tmp = root;
	Element * parent = NULL;
	if(root && root->get_value() == val)
	{ // root delete
		//tmp = root;
		if(root->get_left() == NULL && root->get_right() == NULL){
			delete root;
			root=NULL;
			return;
		}else{
			if(root->get_left() != NULL && root->get_right() != NULL){
				Element* min = root->get_right();
				root = root->get_right();
				while(min->get_left())
					min = min->get_left();
				min->set_left(tmp->get_left());

			}else{
				if(root->get_left())
					root = root->get_left();
				else 
					root = root->get_right(); 
			}
			delete tmp;	
		}
	}else
		while(tmp){
			if (val < tmp->get_value())
				if (tmp->get_left() != NULL){
					parent = tmp;
					tmp = tmp->get_left();
				}else{
					// no element found
					return;
				}
			else
				if (val > tmp->get_value())
					if (tmp->get_right() != NULL){
						parent = tmp;
						tmp = tmp->get_right();
					}else{
						// no element found
						return;
					}
				else if(val == tmp->get_value()){
					if(tmp->get_left() == NULL && tmp->get_right() == NULL){
						if(parent->get_left() == tmp)
							parent->set_left(NULL);
						else 
							parent->set_right(NULL); 
						delete tmp;
						tmp = NULL;
					}else if(tmp->get_left() != NULL && tmp->get_right() != NULL){
						if(parent->get_left() == tmp)
							parent->set_left(tmp->get_right());
						else 
							parent->set_right(tmp->get_right()); 
						Element* min = tmp->get_right();
						while(min->get_left())
							min = min->get_left();
						min->set_left(tmp->get_left());
						delete tmp;
						tmp=NULL;
					}else{
						if(parent->get_left() == tmp)
							parent->set_left(tmp->get_left()?tmp->get_left():tmp->get_right());
						else 
							parent->set_right(tmp->get_left()?tmp->get_left():tmp->get_right()); 
						delete tmp;
						tmp=NULL;
					}
				}
		}
}






