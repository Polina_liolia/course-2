#include <iostream>
using namespace std;
#include "Point.h"

Point::Point(){
	x = 0;
	y = 0;
	name = NULL;
}

Point::Point(int x, int y) {
	x = 0;
	y = 0;
	name = NULL;
}

Point::Point(int x, int y, char* aName){
	this->x = x;
	this->y = y;
	int strLen = 0;
	if(strLen = strlen(aName)){
		name = new char[strLen + 1];
		strcpy_s(name, strLen + 1, aName);
	}
}

Point::Point(const Point& aPoint){
	x = aPoint.x;
	y = aPoint.y;
	int strLen = 0;
	if(strLen = strlen(aPoint.name)){
		name = new char[strLen + 1];
		strcpy_s(name, strLen + 1, aPoint.name);
	}
}

Point Point::operator=(const Point& aPoint){
	x = aPoint.x;
	y = aPoint.y;
	int strLen = 0;
	if(strLen = strlen(aPoint.name)){
		delete[] name;
		name = new char[strLen + 1];
		strcpy_s(name, strLen + 1, aPoint.name);
	}
	return *this;
}


// ����������� ����� ���������� ���������

Point Point::operator ++(int){
	cout << "a++" <<endl;
	/*
	Point t(x,y);
	x++;
	y++;
	return t;
	
	*/
	return Point(x++,y++);
}

// ���������� ����� ���������� ��������� (�����)
Point Point::operator ++()
{
	cout << "++a (prefix, basic)" << endl;
	x++;
	y++;
	return *this;//Point(x,y);
}

//  a[i++]    a[++i]   ???????

void Point::show()
{
	cout << "x:" << x << "   y:" << y  << "    Name: "<< name << endl;
}

Point::~Point(){
	delete[] name;
}
