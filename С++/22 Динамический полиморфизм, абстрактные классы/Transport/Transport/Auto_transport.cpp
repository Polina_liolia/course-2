#include "Auto_transport.h"

Auto_transport::Auto_transport() : Ground_transport()	//default constructor, inicializes body as "sedan" (default)
{
	body = sedan;
}

//overloaded constructor
Auto_transport::Auto_transport(string aModel, double aWeight, double aMax_speed, speed aSpeed_units, double aEngine_volume, body_type aBody) :
	Ground_transport(aModel, aWeight, aMax_speed, aSpeed_units, aEngine_volume)
{
	body = aBody;
}

//method to set or change property body
void Auto_transport::set_body(body_type aBody)
{
	body = aBody;
}

//method to get meaning of property body
body_type Auto_transport::get_body() const
{
	return body;
}

//virtual method to get meaning of property speed_units
speed Auto_transport::get_speed_units() const
{
	cout << "Speed units for auto transport - ";
	switch (speed_units)
	{
	case 0: cout << "km_h\n"; break;
	case 1: cout << "Mach\n"; break;
	case 2: cout << "miles_h\n"; break;
	case 3: cout << "knots\n"; break;
	}
	return speed_units;
}

//virtual method to print all information about Auto_transport instance
void Auto_transport::print()
{
	Ground_transport::print();	//calling of basic class method
	cout << "Body type: "; 
	switch (body)
	{
	case 0: cout << "sedan\n"; break;
	case 1: cout << "hatchback\n"; break;
	case 2: cout << "van\n"; break;
	case 3: cout << "truck\n"; break;
	}
}
