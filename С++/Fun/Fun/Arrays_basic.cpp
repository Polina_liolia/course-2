#include "Header.h"

//Inputting an array with random numbers
void randinput(int *arr, int sz, int x, int y)
{
	srand(time(NULL));
	for (int i = 0; i<sz; i++)
		*(arr + i) = rand() % (y - x + 1) + x;
}


//���������� ���������� ������� ���������� �������
void randinput2(int **arr, int str, int col, int x, int y)
{
	srand(time(NULL));
	for (int i = 0; i<str; i++)
		for (int j = 0; j<col; j++)
			arr[i][j] = rand() % (y - x + 1) + x;
}


//Random inputting of array (numbers in second string are always lower than in the first one)

void randinput2_decreas(int *arr, int str, int col)
{
	for (int i = 0; i < str*col; i++)
	{
		if ((i / col) % 2 == 0)
			*(arr + i) = rand() % (15 - 5 + 1) + 5;
		else
			*(arr + i) = rand() % (-2 - (-13) + 1) + (-13);
	}
}


//Outputting to console
void arr_print(int *arr, int sz)
{
	for (int i = 0; i < sz; i++)
		cout << *(arr + i) << " ";
	cout << "\n";
}



//����� �� ����� ���������� �������
void arr_print2(int **arr, int str, int col)
{
	for (int i = 0; i < str; i++)
	{
		for (int j = 0; j < col; j++)
			cout << arr[i][j] << " ";
		cout << "\n";
	}
	cout << "\n";
}

//Creating of a two-dimensional dinamic array
void create_2din_arr(int** arr, int sz1, int sz2)
{
	for (int i = 0; i < sz1; i++)
		arr[i] = new int[sz2];
}

void create_2din_arr(char** str, int sz1, int sz2)
{
	for (int i = 0; i < sz1; i++)
		str[i] = new char[sz2];
}

//Inputting a two-dimensional dinamic array with random numbers
void input_2din_arr(int** arr, int sz1, int sz2, int x, int y)
{
	for (int i = 0; i < sz1; i++)
		for (int j = 0; j < sz2; j++)
			arr[i][j] = rand() % (y - x + 1) + x;
}

void input_2din_arr(char** str, int sz1, int sz2, int x, int y)
{
	for (int i = 0; i < sz1; i++)
		for (int j = 0; j < sz2; j++)
			str[i][j] = rand() % (y - x + 1) + x;
}

//Inputting a two-dimensional dinamic array using cin
void input_2din_arr_cin(int** arr, int sz1, int sz2)
{
	for (int i = 0; i < sz1; i++)
		for (int j = 0; j < sz2; j++)
			cin >> arr[i][j];
}

//Outputting a two-dimensional dinamic array to console
void output_2din_arr(int** arr, int sz1, int sz2)
{
	for (int i = 0; i < sz1; i++)
	{
		for (int j = 0; j < sz2; j++)
			cout << arr[i][j] << " ";
		cout << "\n";
	}
}

void output_2din_arr(char** str, int sz1, int sz2)
{
	for (int i = 0; i < sz1; i++)
	{
		for (int j = 0; j < sz2; j++)
			cout << str[i][j] << " ";
		cout << "\n";
	}
}

//Deleting a two-dimensional dinamic array
void delete_2din_arr(int** arr, int sz1, int sz2)
{
	for (int i = 0; i < sz1; i++)
		delete[] arr[i];
}

void delete_2din_arr(char** str, int sz1, int sz2)
{
	for (int i = 0; i < sz1; i++)
		delete[] str[i];
}

//Creating of a three-dimensional dinamic array
void create_3din_arr(int*** arr, int plane, int str, int col)
{
	for (int i = 0; i < plane; i++)
	{
		arr[i] = new int*[str];
		for (int j = 0; j < str; j++)
			arr[i][j] = new int[col];
	}
}

//Inputting a three-dimensional dinamic array with random numbers
void input_3din_arr(int*** arr, int plane, int str, int col, int x, int y)
{
	for (int i = 0; i < plane; i++)
		for (int j = 0; j < str; j++)
			for (int k = 0; k<col; k++)
				arr[i][j][k] = rand() % (y - x + 1) + x;
}

//Outputting a three-dimensional dinamic array to console
void output_3din_arr(int*** arr, int plane, int str, int col)
{
	for (int i = 0; i < plane; i++)
	{
		for (int j = 0; j < str; j++)
		{
			for (int k = 0; k < col; k++)
				cout << arr[i][j][k] << " ";
			cout << "\n";
		}
		cout << "\n";
	}
}

//Deleting a three-dimensional dinamic array
void delete_3din_arr(int*** arr, int plane, int str, int col)
{
	for (int i = 0; i < plane; i++)
		for (int j = 0; j < str; j++)
			delete[] arr[i][j];
	for (int i = 0; i < plane; i++)
		delete[] arr[i];

}
