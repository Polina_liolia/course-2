var field = document.querySelector('.main>div:nth-of-type(2)');
var ball = document.querySelector('.main>div:nth-of-type(2)>img');
var msg = document.querySelector('.Goal');
var score = document.querySelector('.main>div:first-of-type');

var msg_top = "Top gate goal!";
var msg_bottom = "Bottom gate goal!";

var score_top = 0;
var score_bottom = 0;

function moveBall()
{
    var _top = event.clientY - field.getBoundingClientRect().top + field.clientTop - 25;
    var _left = event.clientX - field.getBoundingClientRect().left + field.clientLeft - 25;
    ball.style.top = _top + 'px';
    ball.style.left = _left + 'px';

    function clear_msg() {
        msg.innerHTML = "";
    }

    function ballInCenter() {
        ball.style.top = 225 + 'px';
        ball.style.left = 225 + 'px';
    }

    if (_left > 150 && _left < 350 && _top < 25) {
        msg.innerHTML = msg_top;
        setTimeout(clear_msg, 1000);
        score_top++;
        setTimeout(ballInCenter, 1000);
    }

    if (_left > 125 && _left < 325 && _top > 425 )
    {
        msg.innerHTML = msg_bottom;
        setTimeout(clear_msg, 1000);
        score_bottom++;
        setTimeout(ballInCenter, 1000);
    }
    score.innerHTML = 'Top: ' + score_top + ' Bottom: ' + score_bottom;

}

field.addEventListener("click", moveBall);