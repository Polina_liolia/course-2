#include <iostream>
#include <cstring>
#include "ingredient.h"
#include "recipe.h"

using std::cout;
using std::cin;

	recipe::recipe()
	{
		name = nullptr;
		type = nullptr;
		process = nullptr;
		sz_components = 0;
		components = nullptr;
	}

	recipe::recipe(char *aName, char *aType, char *aProcess)
	{
		if (aName && strlen(aName)) 
		{
			name = new char[strlen(aName) + 1];
			strcpy_s(name, strlen(aName) + 1, aName);
		}
		else 
			name = nullptr;
		
		if (aType && strlen(aType))
		{
			type = new char[strlen(aType) + 1];
			strcpy_s(type, strlen(aType) + 1, aType);
		}
		else
			type = nullptr;

		if (aProcess && strlen(aProcess))
		{
			process = new char[strlen(aProcess) + 1];
			strcpy_s(process, strlen(aProcess) + 1, aProcess);
		}
		else
			process = nullptr;

		sz_components = 0;
		components = nullptr;
	}

	recipe::recipe(const recipe &temp)
	{
		if (temp.name && strlen(temp.name))
		{
			name = new char[strlen(temp.name) + 1];
			strcpy_s(name, strlen(temp.name) + 1, temp.name);
		}
		else
			name = nullptr;

		if (temp.type && strlen(temp.type))
		{
			type = new char[strlen(temp.type) + 1];
			strcpy_s(type, strlen(temp.type) + 1, temp.type);
		}
		else
			type = nullptr;

		if (temp.process && strlen(temp.process))
		{
			process = new char[strlen(temp.process) + 1];
			strcpy_s(process, strlen(temp.process) + 1, temp.process);
		}
		else
			process = nullptr;

		sz_components = temp.sz_components;
		components = new ingredient*[sz_components];
		for (int i = 0; i < temp.sz_components; i++)
			components[i] = new ingredient(temp.components[i][0].item, temp.components[i][0].dose);
	}

	recipe::~recipe()
	{
		delete[] name;
		delete[] type;
		delete[] process;
		delete[] components;
	}

	inline char *recipe::get_name()const					//inline function to get a name
	{
		return name;
	}

	inline char *recipe::get_type()const					//inline function to get a type of a dish
	{
		return type;
	}

	inline char *recipe::get_process()const					//inline function to get a process of cooking a dish
	{
		return process;
	}

	void recipe::set_name(char *aName)			//setting a name of a dish
	{
		if (name)
			delete[] name;
		int sz = strlen(aName) + 1;
		name = new char[sz];
		strcpy_s(name, sz, aName);
	}


	void recipe::set_type(char *aType)			//setting a type of a dish
	{
		if (type)
			delete[] type;
		int sz = strlen(aType) + 1;
		type = new char[sz];
		strcpy_s(type, sz, aType);
	}

	void recipe::set_process(char *aProcess)	//setting a process of cooking a dish
	{
		if (process)
			delete[] process;
		int sz = strlen(aProcess) + 1;
		process = new char[sz];
		strcpy_s(process, sz, aProcess);
	}

	void recipe::set_ingridient(char *aItem, float aDose)	//setting an components (items and dose) for a dish
	{
		ingredient **temp = new ingredient*[(sz_components + 1)];
		for (int i = 0; i < sz_components; i++)
			temp[i] = components[i];
		temp[sz_components] = new ingredient (aItem, aDose);
		if (sz_components != 0)
			delete[] components;
		sz_components++;
		components = temp;
	}

	void recipe::del_ingridient(int ingredient_number)	//to delete pointed ingridient
	{
		if (ingredient_number >= sz_components)			//checking the correctness of ingredient_number input
		{
			cout << "No ingridient with such a number found.\n";
			return;
		}
		ingredient **temp = new ingredient*[(sz_components - 1)];
		for (int i = 0, j = 0; i < sz_components; i++)		//moving all elements exept element to delete to a temporary array
		{
			if (i == ingredient_number)						//except deleted ingredient
				continue;
			temp[j] = components[i];
			j++;
		}
		delete[] components;
		sz_components--;
		components = temp;
	}


	int recipe::save_recipe_to_file(std::ofstream &F)const
	{
		//to write a recipe name
		int sz = strlen(name) + 1;
		F.write(reinterpret_cast<char*>(&sz), sizeof(sz));
		F.write(name, sz);				

		//to write a recipe type
		sz = strlen(type) + 1;
		F.write(reinterpret_cast<char*>(&sz), sizeof(sz));
		F.write(type, sz);

		//to write a recipe process
		sz = strlen(process) + 1;
		F.write(reinterpret_cast<char*>(&sz), sizeof(sz));
		F.write(process, sz);

		//writing a total amount of components
		int sz_comp = sz_components;
		F.write(reinterpret_cast<char*>(&sz_comp), sizeof(sz_comp));

		for (int i = 0; i < sz_comp; i++)
		{
			int sz = strlen(components[i][0].item) + 1;
			F.write(reinterpret_cast<char*>(&sz), sizeof(sz));		//writing a size of string
			F.write(components[i][0].item, sz);						//writing an item
			F.write(reinterpret_cast<char*>(&components[i][0].dose), sizeof(int));	//writing a dose
		}
		return 0;
	}

	int recipe::read_recipe_from_file(std::ifstream &F)
	{
		int sz;
		F.read(reinterpret_cast<char*>(&sz), sizeof(int));
		name = new char[sz];
		F.read(name, sz);

		//to read a recipe type
		F.read(reinterpret_cast<char*>(&sz), sizeof(int));
		type = new char[sz];
		F.read(type, sz);

		//to write a recipe process
		F.read(reinterpret_cast<char*>(&sz), sizeof(int));
		process = new char[sz];
		F.read(process, sz);

		//writing a total amount of components
		int sz_comp;
		F.read(reinterpret_cast<char*>(&sz_comp), sizeof(int));
		components = new ingredient*[sz];
		for (int i = 0; i < sz_comp; i++)
			components[i] = new ingredient;

		for (int i = 0; i < sz_comp; i++)
		{
			F.read(reinterpret_cast<char*>(&sz), sizeof(int));		//writing a size of string
			components[i][0].item = new char[sz];
			F.read(components[i][0].item, sz);						//writing an item
			F.read(reinterpret_cast<char*>(&components[i][0].dose), sizeof(int));	//writing a dose
		}
		return 0;
	}

	void recipe::print_recipe_name()const							//to print recipe name
	{
		cout << name << "\n";
	}

	void recipe::print_recipe()const								//to print all recipe information
	{
		cout << "Recept name: " << name << "\n";
		cout << "Type: " << type << "\n";
		cout << "Cooking process: " << process << "\n";
		cout << "Ingridients:\n";
		for (int i = 0; i < sz_components; i++)
			cout << (i+1) << ". " << components[i][0].item << " - " << components[i][0].dose << "g\n";
	}

	void recipe::print_ingredients()const							//to print the list of components
	{
		for (int i = 0; i < sz_components; i++)
			cout << (i + 1) << ". " << components[i][0].item << " - " << components[i][0].dose << "g\n";
	}

	recipe *recipe::operator- (const recipe &A)
	{
		recipe unic (name, type, process);
		bool *flag1 = new bool[sz_components];
		bool *flag2 = new bool[A.sz_components];

		for (int i = 0; i < sz_components; i++)
			flag1[i] = true;

		for (int i = 0; i < A.sz_components; i++)
			flag2[i] = true;

		for (int i = 0; i < sz_components; i++)
			for (int j = 0; j < A.sz_components; j++)
				if (strcmp(components[i][0].item, A.components[j][0].item) == 0)
				{
					flag1[i] = false;
					flag2[j] = false;
				}
		
		for (int i = 0; i < sz_components; i++)
			if (flag1[i])
				unic.set_ingridient(components[i][0].item, components[i][0].dose);

		for (int i = 0; i < A.sz_components; i++)
			if (flag2[i])
				unic.set_ingridient(A.components[i][0].item, A.components[i][0].dose);

		return &unic;
	}
