#include <iostream>
#include "Point3D.h"
#include "Student.h"
#include "Fraction.h"



void main()
{
	point3D A;				//������� ��������� ������
	A.set_name("coord1");	//������ ��� �����
	A.set_coordinateX(10);	//������ �������� �
	A.set_coordinateY(11);	//������ �������� �
	A.set_coordinateZ(12);	//������ �������� Z
	A.print_name();			//�������������
	A.print_coordinate();

	point3D B ("coord2", 20, 21, 22);
	FILE *f = nullptr;
	B.save_to_file(f, "Point2");	//��������� ������ � ����

	point3D C;
	C.read_file(f, "Point2");		//������ ������ �� �����
	C.print_name();					//�������������
	C.print_coordinate();

	Student First("Ivanov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "25-ZS-55");
	First.print_data();

	fraction a(558, 124);
	a.print();
	a.reduce();
	a.print();
}