#include <iostream>
#include <time.h>
using namespace std;
class functor{
	int counter;
public:
	functor(){
		counter = 0;
	}
	bool operator()(int aX, int aY){
		if(aX > aY){
			counter++;
			return true;
		}
		return false;
	}
	void show(){
		cout << "Counter:" << counter << endl;
	} 
};

void bubbleSort(int aArr[], int aArrSize, functor&);

void main(){
	srand((unsigned int) time(NULL));
	const int sz = 50;
	int arr[sz] = {0};

	for(int i = 0; i < sz; i++)
		arr[i] = rand()%20;
		
	for(int i = 0; i < sz; i++){
		cout << " "<< arr[i];
	}
	cout << endl;

	//1
	functor f;
	bubbleSort(arr,sz, f);
	f.show();

	//2
	bubbleSort(arr,sz, functor());

	
	cout << endl << "=================================" << endl;
	for ( int i = 0; i < sz; i++)
		cout << " " << arr[i];
	cout << endl;
}

void bubbleSort(int aArr[], int aArrSize, functor &pDirection){

	for (int b = 0; b < aArrSize - 1; b++){
		for ( int i = 0; i < aArrSize - 1; i++){
			if (pDirection(aArr[i], aArr[i + 1])){
				int j = aArr[i];
				aArr[i] = aArr[i + 1];
				aArr[i + 1] = j;
			}

		}
		
	}

}
