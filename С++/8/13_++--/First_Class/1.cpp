
# include <iostream>
using namespace std;
// ���������� ��� �������
// ��������� ������
#include <malloc.h>

class SomeClass{
	int some;

public:
	// ������������� ��������� new � delete, 
	// ����� ����� ������������ �������������� 
	// ���������, ������������ ������ ����������
	void * operator new(size_t size,char* str = "New");
	void   operator delete(void* ptr);

	// ������������� ��������� new [] � delete [] 
	void * operator new [] (size_t fullsize, char* str = "New []");
	void   operator delete [] (void* ptr);
};

void * SomeClass::operator new( size_t size,char* str)
{	
	cout <<"\n"<<str<<"\n";	
	/*
	�����, ��� ��������� ������ ������������ ����������� ������� 
	void *malloc( size_t size );
	� �� ��������� size - ���������� ����, ������� ����� 
	��������. ���� ������ ����������, �� �� malloc ������������ 
	����� ������ ����������� �����.
	*/
	void*ptr = malloc(size);

	if(!ptr){
		cout<<"\nError memory new!!!\n";
	}
	else{
		cout<<"\nMemory new - OK!!!\n";
	}
	return ptr;	
}
void * SomeClass::operator new[]( size_t fullsize,char* str)
{
	cout <<"\n"<<str<<"\n";	
	/*
	�����, ��� ��������� ������ ������������ ����������� ������� 
	void *malloc( size_t size );
	� �� ��������� size - ���������� ����, ������� ����� 
	��������. ���� ������ ����������, �� �� malloc ������������ 
	����� ������ ����������� �����.
	*/
	void*ptr = malloc(fullsize);

	if(!ptr){
		cout<<"\nError memory new[]!!!\n";
	}
	else{
		cout<<"\nMemory new[] - OK!!!\n";
	}
	return ptr;	
}

void SomeClass::operator delete( void* ptr)
{
	
	/*
	��� ������� ������ ������������ ����������� �������
	void free( void * memblock );
	������� free ������� ������, memblock - ����� ������
	���������� �������
	*/
	free(ptr);
	cout<<"\nDelete memory\n";
}

void SomeClass::operator delete[](void* ptr)
{
	free(ptr);
	cout<<"\nDelete [] memory\n";
}

void main()
{
	/*
	���������� 
	�������� new( size_t size,char* str="New")
	�� ����� size ����� ���������� ������ ������ SomeClass,
	� str ������� �������� �� ��������� �.�. "New"
	*/
	SomeClass *p = new SomeClass;

	/*
	���������� 
	�������� new[](size_t size,char* str="New[]")
	�� ����� size ����� ���������� ������ ������ SomeClass,
	���������� �� ���������� ������������� ���������
	� str ������� �������� �� ��������� �.�. "New[]"
	*/
	SomeClass *r = new SomeClass [3];	

	/*
	���������� �������� delete(void* ptr)
	�� ����� ptr ����� ���������� ����� ������,
	���������� ��� ������ �
	*/
	delete p;

	/*
	���������� �������� delete[](void* ptr)
	�� ����� ptr ����� ���������� ����� ������,
	���������� ��� ������ r
	*/
	delete[]r;	
}

