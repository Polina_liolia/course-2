#include <iostream>
using namespace std;
#include "SingleLinkList.h"
SingleLinkList::SingleLinkList(){
	tail = head = NULL;
}
void SingleLinkList::add(int aValue){
	Element* tmp_el = new Element(aValue);
	
	if(head) tail->set_next(tmp_el);
	else head = tmp_el;
	
	tail = tmp_el;
}

void SingleLinkList::show(){
	Element* tmp_el = head;
	while(tmp_el)
	{
		cout << tmp_el->get_value()<<'\n';
		tmp_el = tmp_el->get_next();
	}
}

SingleLinkList::~SingleLinkList(){
		while(head){
			Element* tmp_el = head;
			head = head->get_next();
			delete tmp_el;
		}
}

bool SingleLinkList::delByIndex(int index)
{
	if(index > 0){
		Element *prev = head;
	
		for(int i=0; i<index - 1 && prev->get_next(); i++)
			prev = prev->get_next();

		if(prev->get_next()){
			Element* tmp = prev->get_next();
			prev->set_next(prev->get_next()->get_next());
			if(tmp == tail)  tail = prev;
			delete tmp;
		}
	}else{
		Element* tmp = head;
		head = head->get_next();
		delete tmp;
	}
	return true;
}


bool SingleLinkList::insertByIndex(int index, int value){
	if(index > 0){                   //6
		Element *prev = head;

		for(int i=0; i<index - 1 && prev; i++)//3 
			prev = prev->get_next(); // prev = NULL

		if(prev){
			Element* insElem = new Element(value);
			insElem->set_next(prev->get_next());
			prev->set_next(insElem);
			if(prev == tail)  tail = insElem;
		}else return false;
	}else{
		Element* insElem = new Element(value);
		insElem->set_next(head);
		head = insElem;
		if (!tail) tail = head;
	}
	return true;
}
