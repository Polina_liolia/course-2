#include <iostream>
using namespace std;

void test_fun(int x){
	/*
	if (x < 0){
		cout << x << endl;
		throw 1;
	}
	else if(x > 0){
			 cout << x*x << endl;
			 throw -2;
		 }else throw 3.6;
	*/	 
	 cout << "++++++++++++++++" << endl;
	 throw "mama mila ramu";
	 cout << "----------------" << endl;
}
void main()
{
	///try catch throw

	//test_fun(-10);

	try{
		test_fun(10);
		cout << "test_fun(-10); OK" << endl;
		//===============================
		//test_fun(200);
		//cout << "test_fun(200); OK" << endl;
		//===============================
		test_fun(0);
		cout << "test_fun(0); OK" << endl;
		
	}
	catch(int error){
		cout << "Int Exeption:" << error << endl;
	}
	catch (double error){
		cout << "double Exeption:" << error << endl;

	}
	catch(...){
		cout << "Unknown Exeption."  << endl;

	}
	
}