var querystring = require("querystring"),
    fs = require("fs");

function addresses(response, postData) {
    console.log("Request handler 'start' was called.");
    fs.readFile("./index.html", "binary", function(error, file) {
        if (error) {
            response.writeHead(500, {"Content-Type": "text/plain"});
            response.write(error + "\n");
            response.end();
        } else {
            response.writeHead(200, {"Content-Type": "text/html"});
            response.write(file, "binary");
            response.end();
        }
    });
}

function start(response, postData) {
    console.log("Request handler 'start' was called.");
    fs.readFile("./index.html", "binary", function(error, file) {
        if (error) {
            response.writeHead(500, {"Content-Type": "text/plain"});
            response.write(error + "\n");
            response.end();
        } else {
            response.writeHead(200, {"Content-Type": "text/html"});
            response.write(file, "binary");
            response.end();
        }
    });
}

function pressure(response, postData) {
    console.log("Request handler 'pressure' was called.");
    fs.readFile("./pressure.html", "binary", function(error, file) {
        if (error) {
            response.writeHead(500, {"Content-Type": "text/plain"});
            response.write(error + "\n");
            response.end();
        } else {
            response.writeHead(200, {"Content-Type": "text/html"});
            response.write(file, "binary");
            response.end();
        }
    });
}

function service(response, postData) {
    console.log("Request handler 'service' was called.");
    fs.readFile("./service.html", "binary", function(error, file) {
        if (error) {
            response.writeHead(500, {"Content-Type": "text/plain"});
            response.write(error + "\n");
            response.end();
        } else {
            response.writeHead(200, {"Content-Type": "text/html"});
            response.write(file, "binary");
            response.end();
        }
    });
}

function damage(response, postData) {
    console.log("Request handler 'damage' was called.");
    fs.readFile("./damage.html", "binary", function(error, file) {
        if (error) {
            response.writeHead(500, {"Content-Type": "text/plain"});
            response.write(error + "\n");
            response.end();
        } else {
            response.writeHead(200, {"Content-Type": "text/html"});
            response.write(file, "binary");
            response.end();
        }
    });
}

function style(response, postData) {
    console.log("Request handler 'style' was called.");
    fs.readFile("css/style.css", "binary", function(error, file) {
        if (error) {
            console.log("style err");
            response.writeHead(500, {"Content-Type": "text/plain"});
            response.end();
        } else {
            fs.stat(pathname, function(err, stat) {
                response.setHeader('Content-Type', mime.lookup(pathname));
                response.setHeader('Last-Modified', stat.mtime);

                if (request.headers['if-none-match'] === etag) {
                    response.statusCode = 304;
                    response.end();
                }
                else {
                    var etag = stat.size + '-' + Date.parse(stat.mtime);
                    response.setHeader('Content-Length', stat.size);
                    response.setHeader('ETag', etag);

                    response.writeHead(200);
                    response.write(file, "binary");
                    response.end();
                }
            })
        }
    });
}

function frontendscript(response, postData) {
    console.log("Request handler 'frontendscript' was called.");

}

exports.start = start;
exports.pressure = pressure;
exports.service = service;
exports.damage = damage;