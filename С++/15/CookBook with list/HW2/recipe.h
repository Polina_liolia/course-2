#pragma once
#include "ingredient.h"
#include "DoubleLinkList.h"

class recipe
{
private:
	char *name;
	char *type;
	char *process;
	DoubleLinkList components;

public:
	recipe();
	recipe(char *name, char *type, char *process);
	recipe(const recipe &temp);
	~recipe();

public:
	inline char *get_name()const;
	inline char *get_type()const;
	inline char *get_process()const;
	void set_name(char *aName);
	void set_type(char *aType);
	void set_process(char *aProcess);
	void set_ingridient(char *aItem, float aDose);
	void del_ingridient(int ingredient_number);
	int save_recipe_to_file(FILE *f, char *file_name)const;
	void print_recipe_name()const;
	void print_recipe()const;
	void print_ingredients()const;
};
