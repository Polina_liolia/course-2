#pragma once
#include "ingredient.h"
#include<fstream>

class recipe
{
private:
	char *name;
	char *type;
	char *process;
	int sz_components;
	ingredient **components;

public:
	recipe();
	recipe(char *name, char *type, char *process);
	recipe(const recipe &temp);
	~recipe();

public:
	inline char *get_name()const;
	inline char *get_type()const;
	inline char *get_process()const;
	void set_name(char *aName);
	void set_type(char *aType);
	void set_process(char *aProcess);
	void set_ingridient(char *aItem, float aDose);
	void del_ingridient(int ingredient_number);
	int save_recipe_to_file(std::ofstream & F) const;
	int read_recipe_from_file(std::ifstream & F);
	void print_recipe_name()const;
	void print_recipe()const;
	void print_ingredients()const;
	recipe *operator- (const recipe &A);
};
