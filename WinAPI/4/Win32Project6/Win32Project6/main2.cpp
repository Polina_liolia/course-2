#include <windows.h>
#include <TChar.h>// char wchar_t - TCHAR
#include "resource.h"
HINSTANCE hInstance;
#define OPENDIALOGID 300
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, 
	                       WPARAM wParam, LPARAM lParam) ;

BOOL CALLBACK DlgProc(HWND hWnd, UINT mes, WPARAM wp, LPARAM lp);

int WINAPI WinMain(
				   HINSTANCE hInstance,    // ���������� �������� ���������� ����������
				   HINSTANCE hPrevInstance,// ���������� ����������� ���������� ����������
				   LPSTR lpCmdLine,        // ��������� �� ��������� ������
				   int nCmdShow= SW_SHOWMINIMIZED           // ���������� ��������� ����
				   )
{
	
	MSG uMsg; 
	HWND hMainWnd; 
	TCHAR szClassName[] = TEXT("MyClass"); 

	WNDCLASSEX wcex; // ���������� ���������� ���� ��������� �������� ������

	wcex.cbSize = sizeof(WNDCLASSEX); // ������ ��������� � ������
	wcex.style			= CS_HREDRAW | CS_VREDRAW;	// ����� ����
	wcex.lpfnWndProc	= (WNDPROC)WndProc; //����� ������� ���������
	wcex.cbClsExtra		= 0;						
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;		// ��������� ����������
	wcex.hIcon			= LoadIcon(hInstance,MAKEINTRESOURCE(IDI_APPLICATION));		// ����������� ������
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW); // ����������� �������
	wcex.hbrBackground	= CreateSolidBrush(RGB(56,56,255)); // ��������� ����
	wcex.lpszMenuName	= NULL;		// ����������� ����
	wcex.lpszClassName	= szClassName;	// ��� ������
	wcex.hIconSm		= NULL; //����������� ��������� ������



	// ������������ ����� ���� 
	if (!RegisterClassEx(&wcex)) { 
		MessageBox(NULL, TEXT("Cannot register class"), TEXT("Error"), MB_OK);
		return 0; 
	}

	// ������� �������� ���� ���������� 
	hMainWnd = CreateWindow( 
		szClassName, 
		TEXT("A Hello Application"),
		WS_OVERLAPPEDWINDOW, 
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, 
		(HWND)NULL, 
		(HMENU)NULL, 
		(HINSTANCE)hInstance, NULL 
		); 

	// ���������� ���� ���� 
	ShowWindow(hMainWnd, nCmdShow); 
	// UpdateWindow(hMainWnd); // ��. ���������� � ������� "����������� ����.. 

	// ��������� ���� ��������� ��������� �� �������� ���������� 
	while (GetMessage(&uMsg, NULL, 0, 0)) { 
		TranslateMessage(&uMsg); 
		DispatchMessage(&uMsg); 
	}
	return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, 
	                     WPARAM wParam, LPARAM lParam) 
{ 
	
	
	HINSTANCE hInst;
	BOOL flag = false;
	hInst = hInstance;
	switch (uMsg) 
	{ 
	case WM_CREATE:
		{
			CreateWindow (TEXT("button"), TEXT("EDIT"),
				WS_CHILD | WS_VISIBLE |BS_PUSHBUTTON, 
				10, 10, 100, 30, hWnd, (HMENU) 1, 
				hInst, NULL);

			CreateWindow(TEXT("button"), TEXT("COLOR"),
				WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				10, 40, 100, 30, hWnd, (HMENU)2,
				hInst, NULL);

			CreateWindow(TEXT("button"), TEXT("Open Dialog"),
				WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				200, 200, 200, 30, hWnd, (HMENU)OPENDIALOGID,
				hInst, NULL);
		}
		break;
	
	case WM_COMMAND:
		{	
			int c;
			HWND hCommWnd;
			int ID;

			//������ � ���������� �������� �������������� ���������
			ID = LOWORD( wParam ) ;//�������������
			hCommWnd = HWND ( lParam );//��������� ����\������, �� �-��� ������ ���������
			c = HIWORD (wParam);//�����������
			// c == BN_CLICKED
			if(BN_CLICKED == c){
				switch ( ID )
				{
				case 1:
				{
					MessageBox(NULL, TEXT("OK"), TEXT(":)"), MB_OK);
				}break; 
				case 2:
				{
					SetClassLong(hWnd,
						GCL_HBRBACKGROUND, 
						(LONG)CreateSolidBrush(RGB(rand()%255, rand() % 255, 
							rand() % 255)));
					InvalidateRect(hWnd, NULL, true);
				}break;

				case OPENDIALOGID: {
					DialogBox(GetModuleHandle(NULL),
						MAKEINTRESOURCE(IDD_DIALOG1),
						hWnd, DlgProc);
				}break;
				}//switch
			}// if BN_CLICKED

		}
		break;
	

	case WM_DESTROY: 
		PostQuitMessage(0); 
		break; 

	default: 
		return DefWindowProc(hWnd, uMsg, wParam, lParam); 
	} 

	return FALSE; 
}


BOOL CALLBACK DlgProc(HWND hWnd, UINT mes, WPARAM wParam, LPARAM lParam)
{
	switch (mes)
	{
	case WM_CLOSE:
		// ��������� ����������� ������
		EndDialog(hWnd,0); // ��������� ����
							 // ������������� ���� ��������� ��������
		return TRUE;
	
	case WM_INITDIALOG: {
		EnableWindow(GetDlgItem(hWnd, IDC_BUTTON2), false);
	}break;

	case WM_COMMAND: {
		DWORD ID = LOWORD(wParam);
		DWORD Code = HIWORD(wParam);



		switch (ID) {
		case IDOK: {
			if (BN_CLICKED == Code) {

			}
		}break;
		case IDCANCEL: {
			if (BN_CLICKED == Code) {

			}
		}break;
		case IDC_BUTTON2: {
			if (BN_CLICKED == Code) {
				// IDC_STATIC3
				// IDC_EDIT2
				TCHAR buff[200] = { 0 };
				GetDlgItemText(hWnd, IDC_EDIT2, buff, 200);
				SetDlgItemText(hWnd, IDC_STATIC3, buff);
			}
		}break;
		
		case IDC_EDIT2: {
			if (EN_CHANGE == Code) {
				TCHAR buff[200] = { 0 };
				GetDlgItemText(hWnd, IDC_EDIT2, buff, 200);
				if (lstrlen(buff) > 0) {
					EnableWindow(GetDlgItem(hWnd, IDC_BUTTON2), true);
				}
				else {
					EnableWindow(GetDlgItem(hWnd, IDC_BUTTON2), false);

				}
			}
		}break;
		
					   
		}
			return TRUE;
		}
		
	}
	return FALSE;
}


