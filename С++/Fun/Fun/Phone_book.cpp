#include "Header.h"

#define static 1

struct persone
{
#ifdef static
	char name[20];
	char surname[20];
	char patronymic[20];
#else
	char *name;
	char *surname;
	char *patronymic;
#endif
};

struct date
{
	short day;
	int month;
	int year;
};

struct phonebook
{
	persone full_name;
	date birthday;
	char phone[15];
};

//readig existing file, inputting information from it to structure phonebook
void read_file(phonebook** arr, int &str, int col, char* file_name)
{
	FILE *f;						//creating a file
	fopen_s(&f, file_name, "a+");	//opens file to read and write to the end of a file (to create a file if it does not exist)
	fseek(f, 0, SEEK_SET);			//moving to the beginning of a file
	if (fgetc(f) == EOF)			//if it is an empty file	
	{
		fclose(f);
		return;
	}
	fseek(f, 0, SEEK_SET);			//moving to the beginning of a file
	int i = str;
	do
	{
		if (fgetc(f) == EOF)		//if it is the end of a file
			break;
		fseek(f, -1, SEEK_CUR);		//moving on one character back after fgetc check (to avoid passing symbols)
		arr[i] = new phonebook[col];
		str++;
		fscanf_s(f, "%20s\t", &arr[i][0].full_name.name, 20);
		fscanf_s(f, "%20s\t", &arr[i][0].full_name.surname, 20);
		fscanf_s(f, "%20s\t", &arr[i][0].full_name.patronymic, 20);
		fscanf_s(f, "%i.%i.%i\t", &arr[i][0].birthday.day, &arr[i][0].birthday.month, &arr[i][0].birthday.year);
		fscanf_s(f, "%15s\n", &arr[i][0].phone, 15);
		i++;
	} while (true);
	fclose(f);						//closes file
}

void print_to_file(phonebook** arr, int str, char* file_name)
{
	FILE *f;						//creating a file
	fopen_s(&f, file_name, "w+");	//opens file to write
	if (f == NULL)					//if file was not opened
	{
		cout << "Opening file error!\n";
		return;
	}
	for (int i = 0; i < str; i++)
	{
		fprintf(f, "%20s\t", arr[i][0].full_name.name);
		fprintf(f, "%20s\t", arr[i][0].full_name.surname);
		fprintf(f, "%20s\t", arr[i][0].full_name.patronymic);
		fprintf(f, "%i.", arr[i][0].birthday.day);
		fprintf(f, "%i.", arr[i][0].birthday.month);
		fprintf(f, "%i\t", arr[i][0].birthday.year);
		fprintf(f, "%15s\n", arr[i][0].phone);
	}
	fclose(f);						//closes file

}


//Creating of a two-dimensional dinamic array
void create_2din_arr(phonebook** arr, int sz1, int sz2)
{
	for (int i = 0; i < sz1; i++)
		arr[i] = new phonebook[sz2];
}

//Deleting a two-dimensional dinamic array
void delete_2din_arr(phonebook** arr, int sz1, int sz2)
{

	for (int i = 0; i < sz1; i++)
		delete[] arr[i];
}

//Creating contacts for TEST
void create_contact_test(phonebook **arr, int &str, int col)
{
	str = 4;
	arr[0] = new phonebook[col];
	arr[1] = new phonebook[col];
	arr[2] = new phonebook[col];
	arr[3] = new phonebook[col];

	arr[0][0] = { { { "Polina" },{ "Liolia" },{ "Aleksandrovna" } },{ 14, 8, 1989 },{ "+380689620635" } };
	arr[1][0] = { { { "Anatolii" },{ "Liolia" },{ "Viktorovich" } },{ 26, 4, 1989 },{ "+380689611986" } };
	arr[2][0] = { { { "Oksana" },{ "Odrinskaya" },{ "Vladimirovna" } },{ 05, 7, 1968 },{ "+380677710459" } };
	arr[3][0] = { { { "Aleksandr" },{ "Odrinskiy" },{ "Anatolievich" } },{ 29, 9, 1966 },{ "+380677751227" } };
}

//Creating a contact
void create_contact(phonebook **arr, int &str, int col)
{
	str++;
	arr[str - 1] = new phonebook[col];
	cout << "Let's create a new contact.\nInput the name: ";
#ifdef static
	gets_s(arr[str - 1][0].full_name.name);
#else
	gets_s(arr[str - 1][0].full_name->name);
#endif
	cout << "Input a patronymic: ";
#ifdef static
	gets_s(arr[str - 1][0].full_name.patronymic);
#else
	gets_s(arr[str - 1][0].full_name->patronymic);
#endif
	cout << "Input a surname: ";
#ifdef static
	gets_s(arr[str - 1][0].full_name.surname);
#else
	gets_s(arr[str - 1][0].full_name->surname);
#endif
	cout << "Input the day of a birth: ";
	cin >> arr[str - 1][0].birthday.day;
	if (arr[str - 1][0].birthday.day < 1 || arr[str - 1][0].birthday.day > 31)
		do
		{
			cout << "Error! Day of birth has to be fom 1 to 31. Try again.\n";
			cin >> arr[str - 1][0].birthday.day;

		} while (arr[str - 1][0].birthday.day < 1 || arr[str - 1][0].birthday.day > 31);
		cin.get();
		cout << "Input the number of month of a birth:\n1 - January\n2 - Fabruary\n3 - March\n4 - April\n5 - May\n6 - June\n7 - July\n8 - August\n9 - September\n10 - October\n11 - November\n12 - December\n";
		cin >> arr[str - 1][0].birthday.month;
		if (arr[str - 1][0].birthday.month < 1 || arr[str - 1][0].birthday.month > 12)
			do
			{
				cout << "Error! Month of birth has to be fom 1 to 12. Try again.\n";
				cin >> arr[str - 1][0].birthday.month;
			} while (arr[str - 1][0].birthday.month < 1 || arr[str - 1][0].birthday.month > 12);
			cin.get();
			cout << "Input the year of a birth: ";
			cin >> arr[str - 1][0].birthday.year;
			if (arr[str - 1][0].birthday.year < 1900 || arr[str - 1][0].birthday.year > 2016)
				do
				{
					cout << "Error! Year of birth has to be fom 1900 to 2016. Try again.\n";
					cin >> arr[str - 1][0].birthday.year;
				} while (arr[str - 1][0].birthday.year < 1900 || arr[str - 1][0].birthday.year > 2016);
				cin.get();
				cout << "Input the phone number (format: +380*********)";
				gets_s(arr[str - 1][0].phone); //inputting a phone number
				if (strncmp(arr[str - 1][0].phone, "+380", 4) != 0)
				{
					cout << "Error! An inputting format is +380*********. Try again.\n";
					do {
						gets_s(arr[str - 1][0].phone); //inputting a phone number
						bool flag = true;//indicating the correctness of number inputting (does it consists of all digits)
						if (strlen(arr[str - 1][0].phone) != 13) // if a phone number does not consists of 13 symbols
						{
							cout << "Error! A phone number has to consist of 13 symbols. Try again.\n";
							continue; //continue inputting phone
						}
						else
							for (int i = 5; i < 13; i++)
								if (isdigit(*(arr[str - 1][0].phone + i)) == 0)//if a phone number does not consists of only digits
								{
									cout << "Error! A phone number has to consist of only digits. Try again.\n";
									flag = false;
									break;
								}
						if (flag == false)
							continue;	//continue inputting phone
					} while (strncmp(arr[str - 1][0].phone, "+380", 4) != 0);//checking an inputting format +380*********
				}
#ifdef static
				cout << "Congrats! A new contact \"" << arr[str - 1][0].full_name.name << " " << arr[str - 1][0].full_name.patronymic << " " << arr[str - 1][0].full_name.surname << "\" has been successfully created!\n";
#else
				cout << "Congrats! A new contact \"" << arr[str - 1][0].full_name->name << " " << arr[str - 1][0].full_name->patronymic << " " << arr[str - 1][0].full_name->surname << "\" has been successfully created!\n";
#endif
}

//Shows the list of all contacts (name, patronymic, surname) 
void show_contacts_list(phonebook **arr, int &str, int col)
{
	for (int i = 0; i < str; i++)
	{
		cout << (i + 1) << ". ";
		for (int j = 0; j < col; j++)
#ifdef static
			cout << arr[i][j].full_name.name << " " << arr[i][j].full_name.patronymic << " " << arr[i][j].full_name.surname << "\n";
#else
			cout << arr[i][j].full_name->name << " " << arr[i][j].full_name->patronymic << " " << arr[i][j].full_name->surname << "\n";
#endif
	}
}

//Shows all contacts from start to end (default - all existing contacts)
void show_contacts(phonebook **arr, int &str, int col, int start, int end)
{
	if (end == -1)//if default settings were not changed (means outputting all contacts)
		end = str - 1;//correcting parameter "end" to output all contacts
	for (int i = start; i <= end; i++)
	{
		cout << (i + 1) << ". ";
		for (int j = 0; j < col; j++)
		{
#ifdef static
			cout << "Full name: " << arr[i][j].full_name.name << " " << arr[i][j].full_name.patronymic << " " << arr[i][j].full_name.surname << "\n";
#else
			cout << "Full name: " << arr[i][j].full_name->name << " " << arr[i][j].full_name->patronymic << " " << arr[i][j].full_name->surname << "\n";
#endif
			cout << "Date of birth: ";
			switch (arr[i][j].birthday.month)
			{
			case 1: cout << "January, "; break;
			case 2: cout << "Fabruary, "; break;
			case 3: cout << "March, "; break;
			case 4: cout << "April, "; break;
			case 5: cout << "May, "; break;
			case 6: cout << "June, "; break;
			case 7: cout << "July, "; break;
			case 8: cout << "August, "; break;
			case 9: cout << "September, "; break;
			case 10: cout << "October, "; break;
			case 11: cout << "November, "; break;
			case 12: cout << "December, "; break;
			}
			cout << arr[i][j].birthday.day << ", " << arr[i][j].birthday.year << "\n";
			cout << "Phone number: ";
			for (int k = 0; k < 13; k++)
			{
				switch (k)
				{
				case 3: cout << "("; break;
				case 6: cout << ")"; break;
				case 9: case 11: cout << "-"; break;
				}
				cout << *(arr[i][j].phone + k);
			}
			cout << "\n";
		}
	}
}

//Sotring all contacts by name or surname (name_surname: true-by name, false-by surname)
void bubble(phonebook **arr, int &str, int col, bool name_surname)
{
	if (name_surname)
	{
		for (int b = 0, k = str; b < str - 1; b++, k--)
			for (int i = 0; i < k - 1; i++)
#ifdef static
				if (_stricmp(arr[i][0].full_name.name, arr[i + 1][0].full_name.name) > 0)	//if the second string is less, than the first, changing them
#else
				if (_stricmp(arr[i][0].full_name->name, arr[i + 1][0].full_name->name) > 0)
#endif
				{
					phonebook temp = arr[i][0];
					arr[i][0] = arr[i + 1][0];
					arr[i + 1][0] = temp;
				}
	}
	else
		for (int b = 0, k = str; b < str - 1; b++, k--)
			for (int i = 0; i < k - 1; i++)
#ifdef static
				if (_stricmp(arr[i][0].full_name.surname, arr[i + 1][0].full_name.surname) > 0)	//if the second string is less, than the first, changing them
#else
				if (_stricmp(arr[i][0].full_name->surname, arr[i + 1][0].full_name->surname) > 0)
#endif
				{
					phonebook temp = arr[i][0];
					arr[i][0] = arr[i + 1][0];
					arr[i + 1][0] = temp;
				}
}


void insert(phonebook **arr, int &str, int col, bool name_surname)
{
	if (name_surname)
	{
		int j = 0;
		for (int i = 1; i < col*str; i++)
		{
#ifdef static
			phonebook temp = arr[i][0];
			for (j = i - 1; j >= 0 && (_stricmp(arr[j][0].full_name.name, arr[i][0].full_name.name) > 0); j--)
				arr[j + 1][0] = arr[j][0];
			arr[j + 1][0] = temp;
#else
			phonebook temp = arr[i][0];
			for (j = i - 1; j >= 0 && (_stricmp(arr[j][0].full_name->name, arr[i][0].full_name->name) > 0); j--)
				arr[j + 1][0] = arr[j][0];
			arr[j + 1][0] = temp;
#endif
		}
	}

	else
	{
		int j = 0;
		for (int i = 1; i < col*str; i++)
		{
#ifdef static
			phonebook temp = arr[i][0];
			for (j = i - 1; j >= 0 && (_stricmp(arr[j][0].full_name.surname, arr[i][0].full_name.surname) > 0); j--)
				arr[j + 1][0] = arr[j][0];
			arr[j + 1][0] = temp;
#else
			char temp = arr[i][0];
			for (j = i - 1; j >= 0 && (_stricmp(arr[j][0].full_name->surname, arr[i][0].full_name->surname) > 0); j--)
				arr[j + 1][0] = arr[j][0];
			arr[j + 1][0] = temp;
#endif
		}
	}

}

void select(phonebook **arr, int &str, int col, bool name_surname)
{
	if (name_surname)
	{
		for (int j = 0; j < col*str; j++)
		{
#ifdef static
			phonebook min = arr[j][0];
#else
			phonebook min = arr[j][0];
#endif
			int min_pos = j;
			for (int i = j; i < col*str; i++)
#ifdef static
				if (_stricmp(min.full_name.name, arr[i][0].full_name.name) > 0)
#else
				if (_stricmp(min.full_name->name, arr[i][0].full_name->name) > 0)
#endif
				{
					min_pos = i;
					min = arr[i][0];
				}
			arr[min_pos][0] = arr[j][0];
			arr[j][0] = min;
		}
	}
	else
	{
		for (int j = 0; j < col*str; j++)
		{
#ifdef static
			phonebook min = arr[j][0];
#else
			phonebook min = arr[j][0];
#endif
			int min_pos = j;
			for (int i = j; i < col*str; i++)
#ifdef static
				if (_stricmp(min.full_name.surname, arr[i][0].full_name.surname) > 0)
#else
				if (_stricmp(min.full_name->surname, arr[i][0].full_name->surname) > 0)
#endif
				{
					min_pos = i;
					min = arr[i][0];
				}
			arr[min_pos][0] = arr[j][0];
			arr[j][0] = min;
		}
	}
}

//Searching or deleting a contact (by name) (search_or_delete: true - search, false - delete)
void contact_search_or_delete_name(phonebook **arr, int &str, int col, bool search_or_delete)
{
	int del_count = 0;
	char search_name[20];
	if (search_or_delete)
		cout << "Input the name to search ";
	else
		cout << "Input the name to delete ";
	gets_s(search_name);
	for (int i = 0; i < str; i++)
		for (int j = 0; j < col; j++)
#ifdef static
			if (_stricmp(arr[i][j].full_name.name, search_name) == 0)//comparing  names
#else
			if (_stricmp(arr[i][j].full_name->name, search_name) == 0)
#endif
				if (search_or_delete)
					show_contacts(arr, str, col, i, i);//outputting search rezult
				else		//if delete
				{
					for (int k = i; k < str - 1; k++)//replacing elements 
						arr[k][0] = arr[k + 1][0];
					delete[] arr[str - 1];//deleting the last element
					str--;//correcting number of strings
					del_count++;//counting deleted elements
					i--;//correcting a number of curent string to aviod the passing
				}
	if (search_or_delete == false)
		cout << "\n" << del_count << " contact(s) was(were) succesfully deleted.\n";
	del_count = 0;
}

//Searching or deleting a contact by the surname (search_or_delete: true - search, false - delete)
void contact_search_or_delete_surname(phonebook **arr, int &str, int col, bool search_or_delete)
{
	int del_count = 0;
	char search_surname[20];
	if (search_or_delete)
		cout << "Input the surname to search ";
	else
		cout << "Input the surname to delete ";
	gets_s(search_surname);
	for (int i = 0; i < str; i++)
		for (int j = 0; j < col; j++)
#ifdef static
			if (_stricmp(arr[i][j].full_name.surname, search_surname) == 0)//comparing  surnames
#else
			if (_stricmp(arr[i][j].full_name->surname, search_surname) == 0)
#endif
				if (search_or_delete)
					show_contacts(arr, str, col, i, i);//outputting search rezult
				else		//if delete
				{
					for (int k = i; k < str - 1; k++)//replacing elements 
						arr[k][0] = arr[k + 1][0];
					delete[] arr[str - 1];//deleting the last element
					str--;//correcting number of strings
					del_count++;//counting deleted elements
					i--;//correcting a number of curent string to aviod the passing
				}
	if (search_or_delete == false)
		cout << "\n" << del_count << " contact(s) was(were) succesfully deleted.\n";
	del_count = 0;

}

//Searching or deleting a contact (by patronymic) (search_or_delete: true - search, false - delete)
void contact_search_or_delete_patronymic(phonebook **arr, int &str, int col, bool search_or_delete)
{
	int del_count = 0;
	char search_patronymic[20];
	if (search_or_delete)
		cout << "Input the patronymic to search ";
	else
		cout << "Input the patronymic to delete ";
	gets_s(search_patronymic);
	for (int i = 0; i < str; i++)
		for (int j = 0; j < col; j++)
#ifdef static
			if (_stricmp(arr[i][j].full_name.patronymic, search_patronymic) == 0)//comparing  patronymics
#else
			if (_stricmp(arr[i][j].full_name->patronymic, search_patronymic) == 0)
#endif
				if (search_or_delete)
					show_contacts(arr, str, col, i, i);
				else		//if delete
				{
					for (int k = i; k < str - 1; k++)//replacing elements 
						arr[k][0] = arr[k + 1][0];
					delete[] arr[str - 1];//deleting the last element
					str--;//correcting number of strings
					del_count++;//counting deleted elements
					i--;//correcting a number of curent string to aviod the passing
				}
	if (search_or_delete == false)
		cout << "\n" << del_count << " contact(s) was(were) succesfully deleted.\n";
	del_count = 0;
}

//Searching or deleting a contact by the day of birth (search_or_delete: true - search, false - delete)
void contact_search_or_delete_birthday(phonebook **arr, int &str, int col, bool search_or_delete)
{
	int del_count = 0;
	int search_day = 0;
	if (search_or_delete)
		cout << "Input the day of birth to search ";
	else
		cout << "Input the day of birth to delete ";
	cin >> search_day;
	cin.get();
	for (int i = 0; i < str; i++)
		for (int j = 0; j < col; j++)
			if (arr[i][j].birthday.day == search_day)//comparing days of birth
				if (search_or_delete)
					show_contacts(arr, str, col, i, i);//outputting search rezult
				else		//if delete
				{
					for (int k = i; k < str - 1; k++)//replacing elements 
						arr[k][0] = arr[k + 1][0];
					delete[] arr[str - 1];//deleting the last element
					str--;//correcting number of strings
					del_count++;//counting deleted elements
					i--;//correcting a number of curent string to aviod the passing
				}
	if (search_or_delete == false)
		cout << "\n" << del_count << " contact(s) was(were) succesfully deleted.\n";
	del_count = 0;
}

//Searching or deleting a contact (by the month of a birth) (search_or_delete: true - search, false - delete)
void contact_search_or_delete_birthmonth(phonebook **arr, int &str, int col, bool search_or_delete)
{
	int del_count = 0;
	int search_month = 0;
	if (search_or_delete)
		cout << "Input the month of birth to search ";
	else
		cout << "Input the month of birth to delete ";
	do
	{
		cin >> search_month;
		cin.get();
	} while (search_month < 1 || search_month > 12);
	for (int i = 0; i < str; i++)
		for (int j = 0; j < col; j++)
			if (arr[i][j].birthday.month == search_month)//comparing monthes of birth
				if (search_or_delete)
					show_contacts(arr, str, col, i, i);//outputting search rezult
				else		//if delete
				{
					for (int k = i; k < str - 1; k++)//replacing elements 
						arr[k][0] = arr[k + 1][0];
					delete[] arr[str - 1];//deleting the last element
					str--;//correcting number of strings
					del_count++;//counting deleted elements
					i--;//correcting a number of curent string to aviod the passing
				}
	if (search_or_delete == false)
		cout << "\n" << del_count << " contact(s) was(were) succesfully deleted.\n";
	del_count = 0;
}

//Searching or deleting a contact (by the year of a birth) (search_or_delete: true - search, false - delete)
void contact_search_or_delete_birthyear(phonebook **arr, int &str, int col, bool search_or_delete)
{
	int del_count = 0;
	int search_year = 0;
	if (search_or_delete)
		cout << "Input the year of birth to search ";
	else
		cout << "Input the year of birth to delete ";
	cin >> search_year;
	cin.get();
	for (int i = 0; i < str; i++)
		for (int j = 0; j < col; j++)
			if (arr[i][j].birthday.year == search_year)//comparing years of birth
				if (search_or_delete)
					show_contacts(arr, str, col, i, i);//outputting search rezult
				else		//if delete
				{
					for (int k = i; k < str - 1; k++)//replacing elements 
						arr[k][0] = arr[k + 1][0];
					delete[] arr[str - 1];//deleting the last element
					str--;//correcting number of strings
					del_count++;//counting deleted elements
					i--;//correcting a number of curent string to aviod the passing
				}
	if (search_or_delete == false)
		cout << "\n" << del_count << " contact(s) was(were) succesfully deleted.\n";
	del_count = 0;
}

//Searching or deleting a contact (by the phone number or it's part) (search_or_delete: true - search, false - delete)
void contact_search_or_delete_phone(phonebook **arr, int &str, int col, bool search_or_delete)
{
	int del_count = 0;
	char search_phone[15];
	if (search_or_delete)
		cout << "Input phone number (or part of it) to search ";
	else
		cout << "Input phone number (or part of it) to delete ";
	gets_s(search_phone);
	for (int i = 0; i < str; i++)
		for (int j = 0; j < col; j++)
			if (strstr(arr[i][j].phone, search_phone) != 0)//comparing phones
				if (search_or_delete)
					show_contacts(arr, str, col, i, i);//outputting search rezult
				else		//if delete
				{
					for (int k = i; k < str - 1; k++)//replacing elements 
						arr[k][0] = arr[k + 1][0];
					delete[] arr[str - 1];//deleting the last element
					str--;//correcting number of strings
					del_count++;//counting deleted elements
					i--;//correcting a number of curent string to aviod the passing
				}
	if (search_or_delete == false)
		cout << "\n" << del_count << " contact(s) was(were) succesfully deleted.\n";
	del_count = 0;
}


//Deleting all existing contacts
void del_all_contacts(phonebook **arr, int &str)
{
	for (int i = 0; i < str; i++)
		delete[] arr[i];
	str = 0;
	cout << "All your contacts were succesfully deleted.\n";
}