#include "Company.h"

Company::Company()	//default constructor
{
	type = Free;
	key_words_found = 0;
	key_words_in_description = 0;
}

Company::Company(string aName, string aDescription, accaunt aType)	//overloaded constructor
{
	if (aName.size() > 100)
		cout << "Maximal length of name 100 characters! Data was not saved.\n";
	else
		name = aName;
	if (aDescription.size() > 500)
		cout << "Maximal length of description 500 characters! Data was not saved.\n";
	else
		description = aDescription;
	type = aType;
	key_words_found = 0;
	key_words_in_description = 0;
}


void Company::set_name(string aName)	//method to set or change name of Company
{
	if (aName.size() > 100)
		cout << "Maximal length of name 100 characters! Data was not saved.\n";
	else
		name = aName;
}

void Company::set_description(string aDescription)	//method to set or change Company's description
{
	if (aDescription.size() > 500)
		cout << "Maximal length of description 500 characters! Data was not saved.\n";
	else
		description = aDescription;
}

void Company::set_type(accaunt aType)	//method to set or change accaunt type of Company
{
	type = aType;
}

void Company::add_key_word(string aKey_word)	//adds key word (if limit is not over) for Company
{
	if (type == Free && key_words.size() == 3)
	{
		cout << "You have already used all key words (3 for free accaunt)! Data was not saved.\n";
		return;
	}
	else if (type == silver && key_words.size() == 5)
	{
		cout << "You have already used all key words (5 for silver accaunt)! Data was not saved.\n";
		return;
	}
	else if (type == gold && key_words.size() == 10)
	{
		cout << "You have already used all key words (10 for gold accaunt)! Data was not saved.\n";
		return;
	}
	else if (type == platinum && key_words.size() == 20)
	{
		cout << "You have already used all key words (20 for platinum accaunt)! Data was not saved.\n";
		return;
	}
	else
		key_words.insert(aKey_word);
}

void Company::change_key_word(string aWord_old, string aWord_new)	//changes pointed key word
{
	size_t before = key_words.size();
	key_words.erase(aWord_old);	//trying to erase old word 
	size_t after = key_words.size();
	if (before != after) //if key word was found and erased
		key_words.insert(aWord_new);
	else //if word was not found
		cout << "Key word " << aWord_old << " was not found. Data was not changed.\n";
}

void Company::set_key_words_found(int amount)	//method to set or change the amount of pointed key_words, found in current Company
{
	key_words_found = amount;
}

void Company::set_key_words_in_description(int amount)//method to set or change the amount of pointed key_words, found in current Company's description
{
	key_words_in_description = amount;
}

string Company::get_name() const	//method to get name of a Company
{
	return name;
}

string Company::get_description() const	//method to get description of a Company
{
	return description;
}

accaunt Company::get_type() const	//method to get Company's accaunt type
{
	return type;
}

int Company::get_key_words_found() const	//returns the amount of words that were found
{
	return key_words_found;
}

int Company::get_key_words_in_description() const	//returns the amount of words that were found in description
{
	return key_words_in_description;
}

bool Company::find_key_word(string aKey_word)	//returns true if word was found among Company's key words, if not - false
{
	bool flag = false; //flag to indicate if word was found
	for (auto i : key_words)
	{
		if (i == aKey_word)
		{
			flag = true;
			break;
		}
		else if ((i + 's') == aKey_word || (i + "es") == aKey_word)
		{
			flag = true;
			break;
		}
		else
		{
			string y = i;	//if the last word's letter is 'y'
			y.pop_back();
			y += "ies";
			string f = i;	//if the last word's letter is 'f'
			f.pop_back();
			f += "ves";
			string fe = i;	//if the last two word's letters are 'fe'
			fe.pop_back();
			fe.pop_back();
			fe += "ves";
			if (y == aKey_word || f == aKey_word || fe == aKey_word)
			{
				flag = true;
				break;
			}
		}
	}
	return flag;
}

bool Company::find_key_word_in_description(string aKey_word)	//returns true if word was found in Company's description, if not - false
{
	size_t position = description.find(aKey_word);
	if (position != std::string::npos)
		return true;
	
	string s = aKey_word;
	s[0] += 32;	//if first char in key word in description is uppercase
	position = description.find(s);
	if (position != std::string::npos)
		return true;

	s = aKey_word;
	s[0] -= 32;	//if first char in key word in description is lowercase
	position = description.find(s);
	if (position != std::string::npos)
		return true;

	s = aKey_word;
	if (s.size() > 1 && s[s.size() - 1] == 's')//if 's' was added
	{
		s.pop_back();
		position = description.find(s);	
		if (position != std::string::npos)
			return true;
	}

	s = aKey_word;
	if (s.size() > 2 && s[s.size() - 2] == 'e' && s[s.size() - 1] == 's')
	{
		s.pop_back();
		s.pop_back();
		position = description.find(s);	//if 'es' was added
		if (position != std::string::npos)
			return true;
	}

	s = aKey_word;
	if (s.size() > 3 && s[s.size() - 3] == 'i' && s[s.size() - 2] == 'e' && s[s.size() - 1] == 's')
	{
		s.pop_back();
		s.pop_back();
		s.pop_back();
		s += 'y';
		position = description.find(s);	//if 'y' was changed on 'ies'
		if (position != std::string::npos)
			return true;
	}
	
	s = aKey_word;
	if (s.size() > 3 && s[s.size() - 3] == 'v' && s[s.size() - 2] == 'e' && s[s.size() - 1] == 's')
	{
		s.pop_back();
		s.pop_back();
		s.pop_back();
		s += 'f';
		position = description.find(s);	//if 'f' was changed on 'ves'
		if (position != std::string::npos)
			return true;
	}

	s = aKey_word;
	if (s.size() > 3 && s[s.size() - 3] == 'v' && s[s.size() - 2] == 'e' && s[s.size() - 1] == 's')
	{
		s.pop_back();
		s.pop_back();
		s.pop_back();
		s += 'f';
		s += 'e';
		position = description.find(s);	//if 'fe' was changed on 'ves'
		if (position != std::string::npos)
			return true;
	}
	return false;
}
