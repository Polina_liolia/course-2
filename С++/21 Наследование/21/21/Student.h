#pragma once
#include <iostream>
using std::cout;
using std::ostream;
#include "Person.h"


class Student : public Person
{
private:
	String group;
	
public:
	Student();
	//overloaded constructor:
	Student(String aSurname, String aName, String aPatronymic, short aDay, short aMonth, short aYear, String aPhone, String aUnivercity, String aCountry, String aCity, String aGroup);
	Student(const Student &a);
	~Student();

public:
	//methods to set the meaning of every property:
	void set_group(String aGroup);

	int print_to_file(FILE * f, char * file_name);

	//methods to get the meaning of every property:
	String get_group()const;

	void print_data();		//to print information about student

	friend ostream& operator << (ostream& os, const Student &aSt);
};