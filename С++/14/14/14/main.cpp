#include "queue.h"
using std::ostream;
using std::cout;
using std::endl;

ostream& operator<<(ostream &o, queue &aQueue) 
{
	if (aQueue.get_size() == 0)
		cout << "Empty queue" << "\n";
	for (int i = 0; i < aQueue.get_size(); i++)
		o << aQueue[i] << " ";
	o << endl;
	return o;
}

void main()
{
	queue p;
	//checking insert
	p.insert(7);
	p.insert(10);
	p.insert(0);
	p.insert(-3);
	//checking overloaded output
	cout << "Checking insert and overloaded output:\n";
	cout << p;
	cout << endl;
	//checking copy constructor
	cout << "Checking copy constructor:\n";
	queue p1 = p;
	cout << p1;
	cout << endl;

	// Check of the function, which displays the first line item
	cout << "Check of the function, which displays the first line item:\n";
	for (int i = 0; i < p.get_size(); i++)
		cout << p.pop() <<" ";
	cout << endl;
	
	//checking trying pop elements out of array
	cout << "Checking trying pop elements out of array:\n";
	int sz = p.get_size() + 2;
	for (int i = 0; i < sz; i++)
		cout << p.pop() << " ";
	cout << endl;

	//test []
	queue s(3);
	//with right index;
	cout << "test [] with right index:\n";
	queue w = s[0];
	cout << w;
	//with wrong index;
	cout << "test [] with wrong index:\n";
	queue z = s[2];
	cout << z;

	//testing overloaded ==
	cout << "testing overloaded ==:\n";
	bool flag = false;
	if (w == z)
		flag = true;
	else
		flag = false;
	cout << flag <<"\n";

	//testing overloaded !=
	cout << "testing overloaded !=:\n";
	if (w != z)
		flag = true;
	else
		flag = false;
	cout << flag << "\n";



}

