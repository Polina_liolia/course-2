#pragma once
#include <iostream>
#include <cstring>
using std::cout;
using std::cin;

class point3D			//�������� ������ ��� �������� ���� ��������� �����
{
private:				//�������� �������� ������ (����)
	char *name;
	float x;
	float y;
	float z;

public:
	point3D()			//���������� �� ���������
	{
		x = 0;
		y = 0;
		z = 0;
		name = nullptr;
	}
	
	point3D(char *aName, float aX, float aY, float aZ);	//������������� �����������
	
	~point3D()		//����������
	{
		delete[] name;
	}
	

public:				//�������� ������ ������ (�������)
	void set_coordinateX(float aX)		//������ �������� �
	{
		x = aX;
	}

	void set_coordinateY(float aY)		//������ �������� �
	{
		y = aY;
	}

	void set_coordinateZ(float aZ)    //������ �������� z
	{
		z = aZ;
	}

	void set_name(char *aName);			//������ ��� �����

	void print_name()					//����������� ��� �����
	{
		cout << name << ":\n";
	}

	void print_coordinate()				//����������� ����������
	{
		cout << "x: " << x << "; y: " << y << "; z: " << z << "\n";
	}

	int save_to_file(FILE *f, char *file_name);
	int read_file(FILE *f, char *file_name);
};