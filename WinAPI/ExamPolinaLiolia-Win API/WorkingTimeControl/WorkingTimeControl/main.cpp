// ���� WINDOWS.H �������� �����������, �������, � ��������� ������� ������������ ��� ��������� ���������� ��� Windows. 
#include <windows.h>
#include <tchar.h>
#include "resource.h"
#include <Strsafe.h>

#include<iostream>
using std::ostream;
using std::istream;
#include<fstream>
using std::ios;
using std::ofstream;
using std::ifstream;



// �������� ������� ���������
LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK RegProc(HWND hWnd, UINT message, WPARAM wp, LPARAM lp);
INT_PTR CALLBACK LoginProc(HWND hWnd, UINT message, WPARAM wp, LPARAM lp);
INT_PTR CALLBACK TimeControlProc(HWND hWnd, UINT message, WPARAM wp, LPARAM lp);
#define REGID 300 //constant identifier of button created
#define LOGINID 301 //constant identifier of button created

TCHAR szClassWindow[] = TEXT("���� �������� �������");	/* ��� ������ ���� */

INT WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow)
{
	HWND hWnd;
	MSG Msg;
	WNDCLASSEX wcl;

	// 1. ����������� ������ ����
	wcl.cbSize = sizeof(wcl);	// ������ ��������� WNDCLASSE
								//������������ �� ����, ���� ������� ������ �� ����������� ��� �� ���������:
	wcl.style = CS_HREDRAW | CS_VREDRAW;	// CS (Class Style) - ����� ������ ����
	wcl.lpfnWndProc = WindowProc;	// ����� ������� ���������
	wcl.cbClsExtra = 0;		// ������������ Windows 
	wcl.cbWndExtra = 0; 	// ������������ Windows 
	wcl.hInstance = hInst;	// ���������� ������� ����������
	wcl.hIcon = LoadIcon(NULL, IDI_APPLICATION);	// �������� ����������� ������
	wcl.hCursor = LoadCursor(NULL, IDC_ARROW);	// �������� ������������ �������	
	wcl.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	// ���������� ���� ����� ������
	wcl.lpszMenuName = NULL;	// ���������� �� �������� ����
	wcl.lpszClassName = szClassWindow;	// ��� ������ ����
	wcl.hIconSm = NULL;	// ���������� ��������� ������ ��� ����� � ������� ����

						// 2. ����������� ������ ����
	if (!RegisterClassEx(&wcl))
		return 0; // ��� ��������� ����������� - �����

				  // 3. �������� ����
				  // ��������� ���� �  ���������� hWnd ������������� ���������� ����
	hWnd = CreateWindowEx(
		0,		// ����������� ����� ����
		szClassWindow,	//��� ������ ����
		TEXT("������ Windows ����������"), // ��������� ����
		WS_OVERLAPPEDWINDOW,				// ����� ����
											/* ���������, �����, ����������� ������ �������, ��������� ����, ������ ������������ � ���������� ����  */
		CW_USEDEFAULT,	// �-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// y-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// ������ ����
		CW_USEDEFAULT,	// ������ ����
		NULL,			// ���������� ������������� ����
		NULL,			// ���������� ���� ����
		hInst,		// ������������� ����������, ���������� ����
		NULL);		// ��������� �� ������� ������ ����������

	CreateWindow(L"button", L"�����������", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10, 10, 100, 30, hWnd, (HMENU)REGID, hInst, NULL);
	CreateWindow(L"button", L"����", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 170, 10, 100, 30, hWnd, (HMENU)LOGINID, hInst, NULL);

	// 4. ����������� ����
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd); // ����������� ����



						// 5. ������ ����� ��������� ���������
	while (GetMessage(&Msg, NULL, 0, 0)) // ��������� ���������� ��������� �� ������� ���������
	{
		TranslateMessage(&Msg);	// ���������� ���������
		DispatchMessage(&Msg);	// ��������������� ���������
	}
	return (int)Msg.wParam;
}


LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	switch (uMessage)
	{
	case WM_DESTROY: // ��������� � ���������� ���������
		PostQuitMessage(0); // ������� ��������� WM_QUIT
		break;
	case WM_COMMAND:
	{
		if (HIWORD(wParam) == BN_CLICKED) //control-defined notification code
			switch (LOWORD(wParam)) //control identifier
			{
			case REGID:
			{
				HWND dialog = CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_REG), hWnd, RegProc); //unmodal dialog 
				ShowWindow(dialog, SW_SHOW);//show unmodal dialog
			}
			break;
			case LOGINID:
			{

				HWND dialog = CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_LOGIN), hWnd, LoginProc); //unmodal dialog 
				ShowWindow(dialog, SW_SHOW);//show unmodal dialog
			}
			break;
			}
	}
	break;
	default:
		// ��� ���������, ������� �� �������������� � ������ ������� ������� ������������ ������� Windows �� ��������� �� ���������
		return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}
	return 0;
}

int users_amount = 0;
INT_PTR CALLBACK RegProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)//��������� ������� � ���������� ����
{
	switch (message)
	{
	case WM_CLOSE:
		EndDialog(hWnd, 0); // ��������� ������
		return TRUE;
	case WM_COMMAND:
		if (HIWORD(wParam) == BN_CLICKED) //������� ������ �������
			switch (LOWORD(wParam)) //control identifier
			{
			case IDOK:
			{
				TCHAR buf[100];
				GetDlgItemText(hWnd, IDC_REGLOGIN, buf, 100);
				int size_login = _tcslen(buf) + 1;
				TCHAR *login = new TCHAR[size_login];
				_tcscpy_s(login, size_login * sizeof(TCHAR), buf);

				GetDlgItemText(hWnd, IDC_REGPASSWORD, buf, 100);
				int size_password = _tcslen(buf) + 1;
				TCHAR *password = new TCHAR[size_password];
				_tcscpy_s(password, size_password * sizeof(TCHAR), buf);

				GetDlgItemText(hWnd, IDC_REGPASSWORDREPEAT, buf, 100);
				int size_repeat_password = _tcslen(buf) + 1;
				TCHAR *repeat_password = new TCHAR[size_repeat_password];
				_tcscpy_s(repeat_password, size_repeat_password * sizeof(TCHAR), buf);

				if (_tcscmp(password, repeat_password))
				{
					MessageBox(hWnd, TEXT("����������� ���� ������ �� �����."), TEXT("������!"), MB_OK);
					break;
				}
					GetDlgItemText(hWnd, IDC_REGEMAIL, buf, 100);
					int size_email = _tcslen(buf) + 1;
					TCHAR *email = new TCHAR[size_email];
					_tcscpy_s(email, size_email * sizeof(TCHAR), buf);

					GetDlgItemText(hWnd, IDC_REGPHONE, buf, 100);
					int size_phone = _tcslen(buf) + 1;
					TCHAR *phone = new TCHAR[size_phone];
					_tcscpy_s(phone, size_phone * sizeof(TCHAR), buf);

					
					ifstream iF_amount("users_amount.bin", ios::binary);
					//��������, ������ ���� ��� ���
					long file_size = 0;
					if (iF_amount)
					{
						iF_amount.seekg(0, ios::end);
						file_size = iF_amount.tellg();
					}
					if (iF_amount && file_size)
					{
						iF_amount.seekg(0, ios::beg);
						iF_amount.read(reinterpret_cast<char*>(&users_amount), sizeof(int));
						iF_amount.close();
					}
					else
						users_amount = 0;

					ofstream F_amount("users_amount.bin", ios::binary);
					// checking if file was successfully opened, othervice generating exception:
					if (F_amount)
					{
						users_amount++;
						F_amount.write(reinterpret_cast<char*>(&users_amount), sizeof(int));
						F_amount.close();
					}

					ofstream F("users.bin", ios::binary | ios::app);
					// checking if file was successfully opened, othervice generating exception:
					if (F)
					{
						F.write(reinterpret_cast<char*>(&size_login), sizeof(int));
						F.write(reinterpret_cast<char*>(&login[0]), size_login * sizeof(TCHAR));
						F.write(reinterpret_cast<char*>(&size_password), sizeof(int));
						F.write(reinterpret_cast<char*>(&password[0]), size_password * sizeof(TCHAR));
						F.write(reinterpret_cast<char*>(&size_email), sizeof(int));
						F.write(reinterpret_cast<char*>(&email[0]), size_email * sizeof(TCHAR));
						F.write(reinterpret_cast<char*>(&size_phone), sizeof(int));
						F.write(reinterpret_cast<char*>(&phone[0]), size_phone * sizeof(TCHAR));
						F.close();
					}
					EndDialog(hWnd, 0); // ��������� ������
					return FALSE;
			}
			case IDCANCEL:
				EndDialog(hWnd, 0); // ��������� ������
				return FALSE;
			}
	}
	return FALSE;
}

INT_PTR CALLBACK LoginProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)//��������� ������� � ���������� ����
{
	switch (message)
	{
	case WM_INITDIALOG:	{
		ifstream F_amount("users_amount.bin", ios::binary);
		//��������, ������ ���� ��� ���
		long file_size = 0;
		if (F_amount)
		{
			F_amount.seekg(0, ios::end);
			file_size = F_amount.tellg();
		}
		if (F_amount && file_size)
		{
			F_amount.seekg(0, ios::beg);
			F_amount.read(reinterpret_cast<char*>(&users_amount), sizeof(int));
			F_amount.close();
		}
		else
		{
			users_amount = 0;
			MessageBox(hWnd, TEXT("������������ �� �������./n������� �������� �����������."), TEXT("��������������"), MB_OK);
			EndDialog(hWnd, 0); //��������� ������
			return TRUE;
		}
	}
	break;
	case WM_CLOSE:
		EndDialog(hWnd, 0); // ��������� ������
		return TRUE;
	case WM_COMMAND:
		if (HIWORD(wParam) == BN_CLICKED) //������� ������ �������
			switch (LOWORD(wParam)) //control identifier
			{
			case IDOK:
			{
				if (!users_amount)
				{
					MessageBox(hWnd, TEXT("������������ �� �������."), TEXT("������!"), MB_OK);
					EndDialog(hWnd, 0); //��������� ������
					return TRUE;
				}
				TCHAR buffer[100];
				GetDlgItemText(hWnd, IDC_LOGLOGIN, buffer, 100);
				int size_login = _tcslen(buffer) + 1;
				TCHAR *login = new TCHAR[size_login];
				_tcscpy_s(login, size_login * sizeof(TCHAR), buffer);

				GetDlgItemText(hWnd, IDC_LOGPASSWORD, buffer, 100);
				int size_password = _tcslen(buffer) + 1;
				TCHAR *password = new TCHAR[size_password];
				_tcscpy_s(password, size_password * sizeof(TCHAR), buffer);

				ifstream iF("users.bin", ios::binary);
				// checking if file was successfully opened:
				if (iF)
				{
					for (int i = 0; i < users_amount; i++)
					{
						int buf_size = 0;
						iF.read(reinterpret_cast<char*>(&buf_size), sizeof(int));
						TCHAR *current_login = new TCHAR[buf_size];
						iF.read(reinterpret_cast<char*>(&current_login[0]), buf_size * sizeof(TCHAR));
						iF.read(reinterpret_cast<char*>(&buf_size), sizeof(int));
						TCHAR *current_password = new TCHAR[buf_size];
						iF.read(reinterpret_cast<char*>(&current_password[0]), buf_size * sizeof(TCHAR));
						if (!_tcscmp(login, current_login) && !_tcscmp(password, current_password))
						{
							delete[] current_login, current_password;
							MessageBox(hWnd, TEXT("���� �������"), TEXT("����� ����������!"), MB_OK);
							EndDialog(hWnd, 0); // ��������� ������
							HWND dialog = CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_TIMER), hWnd, TimeControlProc); //unmodal dialog 
							ShowWindow(dialog, SW_SHOW);//show unmodal dialog
							return TRUE;
						}
						iF.read(reinterpret_cast<char*>(&buf_size), sizeof(int));
						TCHAR *email = new TCHAR[buf_size];
						iF.read(reinterpret_cast<char*>(&email[0]), buf_size * sizeof(TCHAR));
						
						iF.read(reinterpret_cast<char*>(&buf_size), sizeof(int));
						TCHAR *phone = new TCHAR[buf_size];
						iF.read(reinterpret_cast<char*>(&phone[0]), buf_size * sizeof(TCHAR));
						delete[] current_login, current_password, email, phone;
					}
					iF.close();
					delete[] login, password;
					MessageBox(hWnd, TEXT("������!"), TEXT("�������� ����� ��� ������."), MB_OK);
				}
				
				EndDialog(hWnd, 0); //��������� ������
				return TRUE;
			}
			case IDCANCEL:
				EndDialog(hWnd, 0); //��������� ������
				return FALSE;
			}
		break;
	}
	return FALSE;
}

UINT TimerTotal = 2;
UINT TimerCheck = 3;
int sec = 0;
HWND h, m, s;

INT_PTR CALLBACK TimeControlProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CLOSE:
		EndDialog(hWnd, 0); // ��������� ������
		return TRUE;
	case WM_COMMAND:
		if (HIWORD(wParam) == BN_CLICKED) //������� ������ �������
			switch (LOWORD(wParam)) //control identifier
			{
			case IDOK:
				EndDialog(hWnd, 0); //��������� ������
				return TRUE;
			case IDCANCEL:
				EndDialog(hWnd, 0); //��������� ������
				return FALSE;
			case IDC_START:
			{
				SetTimer(hWnd, TimerTotal, 1000,NULL);
				SetTimer(hWnd, TimerCheck, 10000, NULL);
			}
			break;
			case IDC_STOP:
			{
				SetWindowText(h, TEXT("0"));
				SetWindowText(m, TEXT("0"));
				SetWindowText(s, TEXT("0"));
				KillTimer(hWnd, TimerTotal);
				KillTimer(hWnd, TimerCheck);
				int hours = sec / 3600;
				int min = (sec - hours * 3600) / 60;
				int seconds = sec - hours * 3600 - min * 60;
				int const arraysize = 50;
				TCHAR result[arraysize];
				size_t sz = arraysize * sizeof(TCHAR);
				LPCTSTR pszFormat = TEXT("���� ������� �����: %d ����� %d ����� %d ������");
				StringCbPrintf(result, sz, pszFormat, hours, min, seconds);
				MessageBox(hWnd, result, TEXT("������ ���������"), MB_OK);
				sec = 0;
			}
			break;
			}
		break;
	case WM_TIMER:
	{
		if (wParam == 2)
		{
			sec++;
			int hours = sec / 3600;
			int min = (sec - hours * 3600) / 60;
			int seconds = sec - hours * 3600 - min * 60;	
			
			h = GetDlgItem(hWnd, IDC_HOUR);
			m = GetDlgItem(hWnd, IDC_MINUTE);
			s = GetDlgItem(hWnd, IDC_SEC);

			int const arraysize = 50;
			TCHAR number[arraysize];
			size_t sz = arraysize * sizeof(TCHAR);
			LPCTSTR pszFormat = TEXT("%d");

			StringCbPrintf(number, sz, pszFormat, hours);
			SetWindowText(h, number);
			
			StringCbPrintf(number, sz, pszFormat, min);
			SetWindowText(m, number);
			
			StringCbPrintf(number, sz, pszFormat, seconds);
			SetWindowText(s, number);
		}
		if (wParam == 3)
		{
			sec++;
			int hours = sec / 3600;
			int min = (sec - hours * 3600) / 60;
			int seconds = sec - hours * 3600 - min * 60;
			
			int const arraysize = 50;
			TCHAR number[arraysize];
			size_t sz = arraysize * sizeof(TCHAR);
			LPCTSTR pszFormat = TEXT("%d");

			StringCbPrintf(number, sz, pszFormat, hours);
			SetWindowText(h, number);

			StringCbPrintf(number, sz, pszFormat, min);
			SetWindowText(m, number);

			StringCbPrintf(number, sz, pszFormat, seconds);
			SetWindowText(s, number);

			KillTimer(hWnd, TimerTotal);
			KillTimer(hWnd, TimerCheck);
		
			int msgResult = MessageBox(hWnd, TEXT("�� ��� ��� �����?"), TEXT("��������"), MB_OK);
			if (msgResult == IDOK)
			{	
				SetTimer(hWnd, TimerTotal, 1000, NULL);
				SetTimer(hWnd, TimerCheck, 10000, NULL);
			}
		}
	}
	break;
	}
	return FALSE;
}
