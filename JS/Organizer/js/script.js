function Event (date_start, date_end, aName, aDesc){
        this.start = new Date(date_start);
        this.end = new Date(date_end);
        this.name = aName;
        this.description = aDesc;
        this.id = String(Math.random());
}

function addEvent (new_event) {
    //saving data about event in the whole events collection:
    var events_list = localStorage.getItem("events_list");
    events_list = events_list == null ? [] : JSON.parse(events_list, function (key, value) {
            if (key == 'start' || key == 'end')
                return new Date(value);
            return value;
        });
    events_list.push(new_event);
    //sorting array by start date:
    events_list.sort(function(a,b){
        return b.start < a.start;
    });
    events_list = JSON.stringify(events_list);
    localStorage.setItem("events_list", events_list);
}


Date.prototype.addDays = function(days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
};

function Calendar (date) {
    this.now = date == undefined ? new Date() : new Date(date);
    this.day = this.now.getDate();
    this.month = this.now.getMonth();
    this.year = this.now.getFullYear();
    var self = this;
    var backup;
    var eventIdToChange = 0;

    //initial adding events listeners:
    this.init = function () {
        //adding click listener on Add event button of the form
        $("#add_event input[type='button']").click(self.addEvent);
        //to open edit form after click on the edit icon
        $(".event_control_elements>img:nth-of-type(2)").click(function (event) {
            event.stopImmediatePropagation();
            //clearing datepicker presaved info:
            $("#to_edit").datepicker("option", "minDate", null);
            $("#from_edit").datepicker("option", "maxDate", null);

            var $event_div = $(this).parent().parent();
            var event_id = eventIdToChange = $event_div.attr("event_id");
            var events_list = localStorage.getItem("events_list");
            events_list = events_list == null ? [] : JSON.parse(events_list, function (key, value) {
                    if (key == 'start' || key == 'end')
                        return new Date(value);
                    return value;
                });
            $(events_list).each(function (j, value) {
                if (String(value.id) == event_id) {
                    $("#from_edit").val(value.start.getMonth() + 1 + "/" +
                        value.start.getDate() + "/" + value.start.getFullYear());
                    $("#to_edit").val(value.end.getMonth() + 1 + "/" +
                        value.end.getDate() + "/" + value.end.getFullYear());
                    $("#event_name_edit").val(value.name);
                    $("#description_edit").val(value.description);
                    return false;
                }
            });

            $("#overlay_edit_event").fadeIn("slow");
        });
        var $edit_form = $("#edit_form");
        $("#edit_form input[type='button']").click(function () {
            event.stopImmediatePropagation();
            var events_list = localStorage.getItem("events_list");
            events_list = events_list == null ? [] : JSON.parse(events_list, function (key, value) {
                    if (key == 'start' || key == 'end')
                        return new Date(value);
                    return value;
                });
            $(events_list).each(function (j, value) {
                if (String(value.id) == eventIdToChange) {
                    value.start = new Date($("#from_edit").val());
                    value.end = new Date($("#to_edit").val());
                    value.name = $("#event_name_edit").val();
                    value.description = $("#description_edit").val();
                    return false;
                }
            });
            //sorting array by start date:
            events_list.sort(function(a,b){
                return b.start < a.start;
            });
            events_list = JSON.stringify(events_list);
            localStorage.setItem("events_list", events_list);
            $("#overlay_edit_event").fadeOut("slow");
            $("div.event_control_elements").hide().appendTo($("body"));
            $("table.month tr:not(:first-of-type)").remove();
            self.fill_in();
        });

        $("#overlay_edit_event img").click(function () {
            event.stopImmediatePropagation();
            $("#overlay_edit_event").fadeOut("slow");
        });

        //to show event information after click on the 'i' icon
        $(".event_control_elements>img:nth-of-type(1)").click(function (event) {
            event.stopImmediatePropagation();
            var $event_div = $(this).parent().parent();
            var event_id = $event_div.attr("event_id");
            var events_list = localStorage.getItem("events_list");
            events_list = events_list == null ? [] : JSON.parse(events_list, function (key, value) {
                    if (key == 'start' || key == 'end')
                        return new Date(value);
                    return value;
                });
            $(events_list).each(function (j, value) {
                if (String(value.id) == event_id) {
                    self.show_event_info(value);
                    $(".all_day_events_overlay").fadeIn("slow");
                    return false;
                }
            });
        });
        //to hide event's info after click on the close icon:
        $(".all_day_events>img").on("click", function () {
            $(".all_day_events_overlay").fadeOut("slow");
            $(".all_day_events>div").remove();
        });


        //to delete event after click on the 'X' icon
        $(".event_control_elements>img:nth-of-type(3)").click(function (event) {
            event.stopImmediatePropagation();
            var $event_div = $(this).parent().parent();
            var event_id = $event_div.attr("event_id");
            var events_list = localStorage.getItem("events_list");
            events_list = events_list == null ? [] : JSON.parse(events_list, function (key, value) {
                    if (key == 'start' || key == 'end')
                        return new Date(value);
                    return value;
                });
            $(events_list).each(function (j, value) {
                if (String(value.id) == event_id) {
                    events_list.splice(j, 1);
                    return false;
                }
            });
            //sorting array by start date:
            events_list.sort(function (a, b) {
                return b.start < a.start;
            });
            events_list = JSON.stringify(events_list);
            localStorage.setItem("events_list", events_list);
            $("div.event_control_elements").hide().appendTo($("body"));
            $("table.month tr:not(:first-of-type)").remove();
            self.fill_in();
        });
        //to show add event form after click on button:
        $(".button_add").on("click", function () {
            //clearing presaved datepicker info:
            $("#to").datepicker("option", "minDate", null);
            $("#from").datepicker("option", "maxDate", null);
            $("#overlay_add_event").fadeIn("slow");
        });


        $("form[name='month_year_chiose'] select").on("change", self.change_month_year);
        $("form[name='month_year_chiose'] input[name='year']").on("input", self.change_month_year);
        $("form[name='month_year_chiose'] input[name='next_month']").on("click", self.next_month);
        $("form[name='month_year_chiose'] input[name='prev_month']").on("click", self.prev_month);

        //to hide add event form and edit form after click on close icon:
        $("#overlay_add_event img").on("click", function () {
            $("#overlay_add_event").fadeOut("slow");
            var form = document.forms.add_event;
            $(form.elements.from).val("");
            $(form.elements.to).val("");
            $(form.elements.event_name).val("");
            $(form.elements.description).val("");
        });

        //show and hide author info:
        $("#about").click(function () {
            if($("#author").css("display") == "none")
                $("#author").fadeIn("slow");
            else
                $("#author").fadeOut("slow");
        })

    };


    //returns the weekday number of first day of month
    this.getFirstDayOfMonthWeekday = function () {
        var _Date = new Date(self.year, self.month, 1);
        var first_day_of_month_number = _Date.getDay();
        return first_day_of_month_number = (first_day_of_month_number === 0) ? 7 : first_day_of_month_number;
    };

    // returns the number of days in current month
    this.getDaysInMonth = function () {
        var _Date = new Date(self.year, self.month, 28);
        while (_Date.getMonth() == self.month) {
            var current_day = _Date.getDate();
            _Date.setDate(++current_day);
        }
        return current_day - 1;
    };

    this.addEvent = function() {
        if(event)
            event.stopImmediatePropagation();
        var form = document.forms.add_event;
        var date_start = form.elements.from.value;
        var date_end = form.elements.to.value;
        var event_name = form.elements.event_name.value;
        var description = form.elements.description.value;

        var event_data = new Event(date_start, date_end, event_name, description);
        addEvent(event_data);

        $("div.event_control_elements").hide().appendTo($("body"));
        $("table.month tr:not(:first-of-type)").remove();
        $("#overlay_add_event").fadeOut("slow");
        $(form.elements.from).val("");
        $(form.elements.to).val("");
        $(form.elements.event_name).val("");
        $(form.elements.description).val("");
        self.fill_in();
    };



    //filling in the calendar table
    this.fill_in = function () {
        var $table = $("table.month").fadeIn(500);
        var days_in_month = self.getDaysInMonth();
        var first_day_number = self.getFirstDayOfMonthWeekday();
        var day = 1;
        var $tr, $td;
        var this_date = new Date();
        for (var i = 0; day <= days_in_month; i++)
        {
            $tr = $("<tr/>").appendTo($table).show(0);
            for (var j = 0; j < 7 && day <= days_in_month; j++) {
                $td = $("<td/>").appendTo($tr).show();
                if (i == 0 && j < first_day_number - 1)
                    continue;
                if (day == self.day && self.month == this_date.getMonth() && self.year == this_date.getFullYear())
                    $td.css("border", "5px solid rgba(0, 0, 255, 0.3)").css("boxSizing", "border-box");
                var $day_number = $("<span>").addClass("day_number").text(day++);
                $day_number.appendTo($td).show();
            }
        }

        //getting the set of all tds, which contain span with a day number (e.g. days of month)
        //and making them dropable:
       $("td>span").parent()
            .droppable({
                accept: "div.event_start, div.event_middle, div.event_end, div.event_single",
                activeClass: "ui-state-highlight",
                scope: "events",
                tolerance: "pointer",
               over: function( event, ui ) {
                    $(this).css("background-color", " #ffd522");
                },
                out: function( event, ui ) {
                    $(this).css("background-color", "");
                },
                deactivate: function( event, ui ) {
                    $(this).css("background-color", "");
                },
                drop: function( event, ui ) {
                    var event_id = ui.draggable.attr("event_id");
                    var new_start_day = +$("span", this).text();
                    var events_list = localStorage.getItem("events_list");
                    events_list = events_list == null ? [] : JSON.parse(events_list, function (key, value) {
                            if (key == 'start' || key == 'end')
                                return new Date(value);
                            return value;
                        });
                    $(events_list).each(function (j) {
                        if (String(events_list[j].id) == event_id) {
                            var change = new_start_day - events_list[j].start.getDate();
                            var event_end = events_list[j].end.getDate();
                            events_list[j].start.setDate(new_start_day);
                            events_list[j].end.setDate(event_end + change);
                            return false;
                        }
                    });
                    //sorting array by start date:
                    events_list.sort(function (a, b) {
                        return b.start < a.start;
                    });
                    events_list = JSON.stringify(events_list);
                    localStorage.setItem("events_list", events_list);
                    $("div.event_control_elements").hide().appendTo($("body"));
                    $("table.month tr:not(:first-of-type)").remove();
                    self.fill_in();
                }
            });


        for (i = 1; i <= days_in_month; i++) {
            //checking if there are some events, starting this day:
            var this_day_events = self.find_event_start(self.year, self.month, i);
            if (this_day_events != null)
                self.show_events(this_day_events);
        }

        for (i = 1; i <= days_in_month; i++) {
            //checking if there are some events this day:
            this_day_events = self.find_any_event(self.year, self.month, i);
            if (this_day_events == null || this_day_events.length <= 3)
                continue;
            var td = $($("td>span")[i-1]).parent();
            $("<div/>").addClass("more_link").text("more...").appendTo(td);
        }

        //to show all day events list:
        $(".more_link").on("click", function () {
            var day = Number($("span", $(this).parent()).text());
            var this_day_events = self.find_any_event(self.year, self.month, day);
            $(this_day_events).each(function (i, value) {
                self.show_event_info(value);
            });
            $(".all_day_events_overlay").fadeIn("slow");
        });

        //to hide all day events list after click on close icon:
        $(".all_day_events>img").on("click", function () {
            $(".all_day_events_overlay").fadeOut("slow");
            $(".all_day_events>div").remove();
        });

        var select_month = $("form[name='month_year_chiose'] select");
        var input_year = $("form[name='month_year_chiose'] input[name='year']");
        input_year.val(self.year);
        select_month.val(self.monthNumberToStr(self.month));
    };


    this.find_event_start = function (year, month, day)
    {
        var date = new Date(year, month, day);
        var events_list = localStorage.getItem("events_list");
        events_list = events_list == null ? null : JSON.parse(events_list, function (key, value) {
                if (key == 'start' || key == 'end')
                    return new Date(value);
                return value;
            });
        if (!events_list) return events_list;
        var this_day_events=[];
        $(events_list).each(function (i) {
            if (events_list[i].start.getTime() == date.getTime() || (day == 1 &&
                events_list[i].start.getMonth() != events_list[i].end.getMonth() &&
                events_list[i].end.getMonth() >= month && events_list[i].start.getMonth() < month))
                this_day_events.push(events_list[i]);
        });
        return this_day_events;
    };

    this.find_any_event = function (year, month, day)
    {
        var date = new Date(year, month, day);
        var events_list = localStorage.getItem("events_list");
        events_list = events_list == null ? null : JSON.parse(events_list, function (key, value) {
                if (key == 'start' || key == 'end')
                    return new Date(value);
                return value;
            });
        if (!events_list) return events_list;
        var this_day_events=[];
        $(events_list).each(function (i) {
            if (events_list[i].start.getTime() <= date.getTime() &&
                events_list[i].end.getTime() >= date.getTime())
                this_day_events.push(events_list[i]);
        });
        return this_day_events;
    };

    this.monthNumberToStr = function (number){
        return number == 0 ? "January" :
            number == 1 ? "February" :
            number == 2 ? "March" :
            number == 3 ? "April" :
            number == 4 ? "May" :
            number == 5 ? "Jun" :
            number == 6 ? "July" :
            number == 7 ? "August" :
            number == 8 ? "September" :
            number == 9 ? "October" :
            number == 10 ? "November" :
            number == 11 ? "December" : "";
    };

    this.strToMonthNumber = function (number) {
        return number == "January" ? "0" :
            number == "February" ? "1" :
            number == "March" ? "2" :
            number == "April" ? "3" :
            number == "May" ? "4" :
            number == "Jun" ? "5" :
            number == "July" ? "6" :
            number == "August" ? "7" :
            number == "September" ? "8" :
            number == "October" ? "9" :
            number == "November" ? "10" :
            number == "December" ? "11" : "";
    };

    this.show_event_info = function (event)
    {
        var start_day = event.start.getDate() >= 10 ?
            event.start.getDate():
            "0" + event.start.getDate();
        var end_day = event.end.getDate() >= 10 ?
            event.end.getDate():
            "0" + event.end.getDate();
        var start_month = self.monthNumberToStr(event.start.getMonth());
        var end_month = self.monthNumberToStr(event.end.getMonth());
        $("<div/>").addClass("day_event").html(start_day + " " + start_month + " - "
            + end_day + " " + end_month + " <b>" + event.name + "<b> " + event.description)
            .appendTo("div.all_day_events");
    };


    this.show_events = function(events)
    {
        if (events.length > 3)
            events = events.slice(0, 3);
        $(events).each(function (i) {
            var flag_multimonth=false,
                flag_end = false,
                flag_middle = false,
                flag_start = false;
            //checking if this event starts last month:
            var month_start = $(this).prop("start").getMonth(),
                month_end = $(this).prop("end").getMonth();
            flag_multimonth = month_start != month_end;
            if (month_start != month_end && self.month != month_start){
                var  day_start = 1;
                var  duration = month_end == self.month ? $(this).prop("end").getDate()
                    : self.getDaysInMonth();
                flag_end = (self.month == month_end);
                flag_middle = (self.month != month_end);
            }
            else if (month_start != month_end && self.month == month_start)
            {
                day_start = $(this).prop("start").getDate();
                duration = self.getDaysInMonth() - day_start + 1;
                flag_start = true;
            }
            else {
                duration = $(this).prop("end").getDate() - $(this).prop("start").getDate() + 1;
                day_start = $(this).prop("start").getDate();
                flag_end = flag_middle = flag_start = false;
            }

                var startSpan = $("td>span")[day_start-1];
                var description = $(this).prop("name") + "\n" + $(this).prop("description");
                var tr = $(startSpan).parent().parent();
                var td1 = $(startSpan).parent();
                var tds = td1.nextAll();
                var weeksCounter = 0;
                var eventLengthPx = td1.width();
                var daysCounter = 0;
                var daysCounterOccupied = 0;
                var position = 0;

                for (var j = 0; j < duration;)
                {
                    if (j == 0)
                        j++;
                    var days_amount = 0;
                    if (j < duration)
                        tds.each(function () {
                            eventLengthPx += $(this).width();
                            if ($(this).css("borderWidth") == "5px")
                                eventLengthPx += 12;
                            days_amount++;
                            j++;
                            if (j >= duration) return false;
                        });
                    eventLengthPx += days_amount;
                    //determining a position to place current event:
                   if (position == 0) {
                       var flag = false;
                   while (!flag) {
                       flag = true;
                       position++;
                       var td_check = td1;
                       while (daysCounter < duration) {
                           if (td_check.attr("pos" + position) == "occupied") {
                               flag = false;
                               break;
                           }
                           td_check = td_check.next("td").length != 0 ? td_check.next("td") : $("td:first-of-type", tr.next());
                           daysCounter++;
                       }
                   }
                   }
                   if(position > 3)
                       return;
                   var resz_grid_horizontal = td1.width();
                    var resz_grid_vertical = td1.height();
                    var new_div = $("<div/>").addClass(function () {
                        if (weeksCounter == 0 && j >= duration
                            && !flag_end && !flag_start && !flag_middle)
                            return "event_single";
                        if ((flag_start || !flag_end) && weeksCounter == 0)
                            return "event_start";
                        if (flag_middle)
                            return "event_middle";
                        if (j >= duration && !flag_multimonth || flag_end && j >= duration)
                            return "event_end";
                        return "event_middle";
                    })
                        .attr("event_id", $(events[i]).prop("id"))
                        .attr("about", description)
                        .css('width', eventLengthPx+'px')
                        .css('top', function () {
                            if (position == 1)
                                return 4 + 'px';
                            return (4+(4 + 22)*(position-1) + 'px');
                        })
                        .text($(this).attr("name"))
                        .prependTo(td1)
                        .resizable({
                            helper: "resizable-helper",
                            autoHide: true,
                            grid: [resz_grid_horizontal, resz_grid_vertical],
                            containment: "table.month",
                            distance: 30,
                            stop: function (event, ui) {
                                //amount of days to change event duration:
                                var event_id = ui.originalElement.attr("event_id");
                                var events_list = localStorage.getItem("events_list");
                                events_list = events_list == null ? [] : JSON.parse(events_list, function (key, value) {
                                    if (key == 'start' || key == 'end')
                                        return new Date(value);
                                    return value;
                                });
                                $(events_list).each(function (j) {
                                    if (String(events_list[j].id) == event_id) {
                                        var duration = events_list[j].end.getDate() - events_list[j].start.getDate() + 1;
                                        var change_horisontal = Math.round((ui.size.width - ui.originalSize.width) / resz_grid_horizontal);
                                        var change_vertical = Math.round((ui.size.height - ui.originalSize.height) / resz_grid_vertical);
                                        var new_duration = !change_vertical ? (duration + change_horisontal)
                                            : (duration + change_horisontal) * (change_vertical + 1) + (7 - (duration +change_horisontal)) * change_vertical;
                                        var day = events_list[j].start.getDate() + new_duration - 1;
                                        events_list[j].end.setDate(day);
                                        return false;
                                    }
                                });
                                //sorting array by start date:
                                events_list.sort(function (a, b) {
                                    return b.start < a.start;
                                });
                                events_list = JSON.stringify(events_list);
                                localStorage.setItem("events_list", events_list);
                                $("div.event_control_elements").hide().appendTo($("body"));
                                $("table.month tr:not(:first-of-type)").remove();
                                self.fill_in();
                            }
                        })
                        .draggable({
                            containment: "table.month",
                            distance: 30,
                            //grid: [resz_grid_horizontal, resz_grid_vertical],
                            helper: "clone",
                            opacity: 0.5,
                            revert: "invalid",
                            revertDuration: 200,
                            scope: "events"
                        })
                        .mouseenter(function () {
                            $("div.event_control_elements").appendTo($(this)).fadeIn("slow");
                        })
                        .mouseleave(function () {
                            $("div.event_control_elements").hide().appendTo($("body"));
                        });




                   // marking in tds, that position is already occupied:
                   td_check = td1;
                    while (daysCounterOccupied < duration)
                    {
                        td_check.attr("pos" + position, "occupied");
                        td_check.attr("event_id" + position, $(this).prop("id"));
                        td_check =  td_check.next("td").length != 0 ? td_check.next("td") : $("td:first-of-type", td_check.parent().next());
                        daysCounterOccupied++;
                    }
                    //moving to the next row (week in calendar):
                    tr = tr.next();
                    tds = tr.children();
                    td1 = $("td:first-of-type", tr);
                    eventLengthPx = 0;
                    weeksCounter++;
                }
        })
    };

    this.change_month_year = function(){
        self.month = self.strToMonthNumber($("select").val());
        self.year = $("form[name='month_year_chiose'] input[name='year']").val();
        self.now = new Date(self.year, self.month, self.day);
        $("div.event_control_elements").hide().appendTo($("body"));
        $("table.month tr:not(:first-of-type)").remove();
        self.fill_in();

    };

    this.next_month = function () {
        self.year = self.month != 11 ? self.year : +self.year+1;
        self.month = self.month != 11 ? +self.month+1 : 0;
        self.now = new Date(self.year, self.month, self.day);
        $("div.event_control_elements").hide().appendTo($("body"));
        $("table.month tr:not(:first-of-type)").remove();
        $("table.month").fadeOut(200);
        self.fill_in();
    };

    this.prev_month = function () {
        self.year = self.month != 0 ? self.year : +self.year-1;
        self.month = self.month != 0 ? +self.month-1 : 11;
        self.now = new Date(self.year, self.month, self.day);
        $("div.event_control_elements").hide().appendTo($("body"));
        $("table.month tr:not(:first-of-type)").remove();
        $("table.month").fadeOut(200);
        self.fill_in();
    };

}

// Document is ready
$(function() {
    // localStorage.removeItem("events_list");

    //creating and filling in a calendar of the current month:
    var current_calendar = new Calendar();
    current_calendar.init();
    current_calendar.fill_in();

    //making inputs for date select look like a smart calendars:
    var dateFormat = "mm/dd/yy";
    var from1 = $("#from")
        .datepicker({
            defaultDate: "w",
            firstDay: 1,
            changeMonth: true,
            numberOfMonths: 1,
            showAnim: "slideDown"
        })
        .on("change", function () {
            to1.datepicker("option", "minDate", getDate(this));
        });
    var to1 = $("#to").datepicker({
        defaultDate: "w",
        firstDay: 1,
        changeMonth: true,
        numberOfMonths: 1,
        showAnim: "slideDown"
    })
        .on("change", function () {
            from1.datepicker("option", "maxDate", getDate(this));
        });

    var from2 = $("#from_edit")
        .datepicker({
            defaultDate: "+1w",
            firstDay: 1,
            changeMonth: true,
            numberOfMonths: 1,
            showAnim: "slideDown"
        })
        .on("change", function () {
            to2.datepicker("option", "minDate", getDate(this));
        });
    var to2 = $("#to_edit").datepicker({
        defaultDate: "+1w",
        firstDay: 1,
        changeMonth: true,
        numberOfMonths: 1,
        showAnim: "slideDown"
    })
        .on("change", function () {
            from2.datepicker("option", "maxDate", getDate(this));
        });

    function getDate(element) {
        var date;
        try {
            date = $.datepicker.parseDate(dateFormat, element.value);
        } catch (error) {
            date = null;
        }
        return date;
    }

});
