#include <iostream>
#include"Tree.h"
#include "Student.h"
#include "Word.h"

//globaly overloaded output of String
ostream& operator << (ostream& os, const String &aString)
{
	if (aString.get_String() != nullptr)
		os << aString.str << " ";
	return os;
}

//globaly overloaded output of ingredient
ostream& operator << (ostream& os, const Student &aSt)
{
		os << "Surname: " << aSt.surname << "\n";
		os << "Name: " << aSt.name << "\n";
		os << "Patronymic: " << aSt.patronymic << "\n";
		os << "Date of birth: " << aSt.birth.day << "." << aSt.birth.month << "." << aSt.birth.year << "\n";
		os << "Phone: " << aSt.phone << "\n";
		os << "Univercity: " << aSt.univercity << "\n";
		os << "Country: " << aSt.country << "\n";
		os << "City: " << aSt.city << "\n";
		os << "Group: " << aSt.group << "\n";
	return os;
}

//globaly overloaded output of Word
ostream & operator << (ostream& os, const Word &aWord)
{
	os << aWord.en << "(popularity: " << aWord.counter << ") - " << aWord.rus;
	os << "\n";
	return os;
}


void main()
{
	setlocale(0, "RUS");

	Student First("Ivanov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "25-ZS-55");
	Student Second("Petrov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "25-ZS-55");
	Student Third("Kozlov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "25-ZS-55");
	Student Fourth("Abramov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "25-ZS-55");
	Student Fifth("Semenov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "25-ZS-55");

	Tree<Student> Students_list;
	Students_list.add(First);
	Students_list.add(Second);
	Students_list.add(Third);
	Students_list.add(Fourth);
	Students_list.add(Fifth);
	
	Students_list.print();

	
	Word A("dog", "������", 5);
	Word B("cat", "�����", 3);
	Word C("mouse", "����", 1);
	Word D("pig", "������", 10);
	Word E("cow", "������", 2);
	Word F("horse", "������", 1);
	Word G("rat", "�����", 7);
	Word H("tiger", "����", 15);

	Tree<Word> Vocabulary;
	Vocabulary.add(A);
	Vocabulary.add(B);
	Vocabulary.add(C);
	Vocabulary.add(D);
	Vocabulary.add(E);
	Vocabulary.add(F);
	Vocabulary.add(G);
	Vocabulary.add(H);

	Vocabulary.print();
}