#pragma once
#include "A.h"
class B : public A {
	int y;
public:
	B() {
		y = 0;
		cout << __FUNCTION__ << endl;
	}
	B(int aX, int aY) :A(aX), y(aY) {
		cout << __FUNCTION__ << endl;
	}
	~B() {
		cout << __FUNCTION__ << endl;
	}

};
