#pragma once
#include <iostream>
#include "Point3D.h"
using std::cout;
using std::endl;

class Point
{
public:
	
	int x;
	int y;
	
	Point(){
		x = y = 0;
	}
	Point(int t){
		cout << "Constructor "<< endl;
		x = y = t;
		/*
		x = 0;
		y = t;

		y = 0;
		x = t;

		/**/
	}
	Point(const Point3D &aP){
		x = aP.getX();
		y = aP.getY();

	}
	void show(){
		cout << "x = " << x << "   y = " << y << endl;
	}
	/*
	Point operator=(const Point& p){
		x = p.x;
		y = p.y;

		cout << " operator= " << __FILE__ << ":" << __LINE__ << endl;
		return *this;
	}*/
	/*
	Point operator=(const int& value){
		x = value;
		y = value;

		cout << " operator= " << __FILE__ << ":" << __LINE__ << endl;
		return *this;
	}*/
	
	operator int(){
		cout << "int conversion" << endl;
		return x + y;
	}
	operator double(){
		cout << "double conversion" << endl;
		return (double)x - (y + 13.66);
	}
	
	
	operator Point3D()
	{
		cout << "Point3D conversion(operator Point3D)" << endl;
		return Point3D(x,y,rand());
	}
};
