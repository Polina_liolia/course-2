var leader = {
    name: "Василий Иванович",
    age: 35
};

var jsonLeader = JSON.stringify(leader, "", 4);
console.log(jsonLeader);

leader = JSON.parse(jsonLeader);
console.log(leader);


//___________________________________________________________________//

leader = {
    name: "Василий Иванович"
};

var soldier = {
    name: "Петька"
};



// эти объекты ссылаются друг на друга!
leader.soldier = soldier;
soldier.leader = leader;

var team = [leader, soldier];
console.log(team);
var jsonTeam = dojox.json.ref.toJson(team);
console.log(jsonTeam);
team = dojox.json.ref.fromJson(jsonTeam);
console.log(team);