function Element (aValue)
{
    var _value = aValue;
    var _SELF_POINTER = this;
    var _prev = null;
    var _next = null;

    //method to set or get _value of the current element
    this.value = function (aValue)
    {
         if (aValue == undefined)
             return _value;
         else
             _value = aValue;
    };

    //method to get a pointer on a current object
    this.SELF_POINTER = function ()
    {
    return _SELF_POINTER;
    };

    //method to set or get _prev element
    this.prev = function (aPrev)
    {
         if (aPrev == undefined)
             return _prev;
         else
             _prev = aPrev;
    };

    //method to set or get _next element
    this.next = function (aNext)
    {
         if (aNext == undefined)
             return _next;
         else
             _next = aNext;
    };
}


function DoubleLinkedList()
{
    var head = new Element();
    var tail = new Element();

    //method to set or get head;
    this.head = function (aHead)
    {
         if (aHead == undefined)
             return head;
         else
             head = aHead;
    };

    //method to set or get tail
    this.tail = function (aTail)
    {
         if (aTail == undefined)
             return tail;
         else
             tail = aTail;
    };

    //method to add an element to DLL
    this.add = function (element)
    {
        if (element == undefined)
        {
           // alert("Nothing to add!");
           return;
        }
        var tmp = new Element(element);
        if (head.value() == undefined) //DLL is empty
        {
            head = tmp.SELF_POINTER();
            tail = tmp.SELF_POINTER();
        }
        else    //DLL includes elements
        {
            tail.next(tmp.SELF_POINTER());
            tmp.prev(tail.SELF_POINTER());
            tail = tmp;
        }
    };

    //method to output to console DLL (straight)
    this.show = function ()
    {
        if(head.value() != undefined)
        {
            var tmp_pointer = head;
            while (tmp_pointer) {
                console.log(tmp_pointer.value());
                tmp_pointer = tmp_pointer.next();
            }
        }
    };

    //method to output to console DLL (reverse)
    this.showReverse = function ()
    {
        if (tail.value() != undefined)
        {
            var tmp_pointer = tail;
            while (tmp_pointer) {
                console.log(tmp_pointer.value());
                tmp_pointer = tmp_pointer.prev();
            }
        }
    };

    //method to delete pointed element from DLL
    this.delByIndex = function (index)
    {
     if (index)
     {
         var previous = head;
         for (var i = 0; i < index - 1 && previous.value(); i++)
             previous = previous.next(); //searching for element before one to delete
         var tmp = previous.next();
         if (tmp.value())
         {
             previous.next(tmp.next());
             if (tmp.next().value())
                 tmp.next().prev(previous.SELF_POINTER());
             if(tmp == tail)
                 tail = previous.SELF_POINTER();
         }
     }
     else
        head = head.next();
    };

    //method to insert nex element to pointed position
    this.insertByIndex = function(index, aValue)
    {
      if (index)
      {
           var previous = head;
           for (var i = 0; i < index - 1 && previous.value(); i++)
               previous = previous.next();
           if (previous.value())
           {
               var Ins = new Element(aValue);
               Ins.next(previous.next());
               if (previous.next())
                   previous.next().prev(Ins);
               Ins.prev(previous.SELF_POINTER());
               previous.next(Ins.SELF_POINTER());
               if (previous == tail)
                    tail = Ins;
           }
      }
      else
      {
          var Ins = new Element (aValue);
          Ins.next(head);
          head = Ins.SELF_POINTER();
      }
    };
}

var MyDLL = new DoubleLinkedList();

    MyDLL.add("My");
    MyDLL.add("name");
    MyDLL.add("is");
    MyDLL.add("Polina");

    MyDLL.show();
    MyDLL.showReverse();

    MyDLL.delByIndex(2);
    MyDLL.show();

    MyDLL.insertByIndex(0, "Hello!");
    MyDLL.show();
