#include <iostream>
using namespace std;
class Point
{
	int x;
	int y;
	

public:
	Point(){
		x = 0;
		y = 0;
		cout << "Default constructor..."<<endl;
	}
	Point (int aX, int aY){
		x = aX;
		y = aY;
		cout << "Overload constructor...(" << this << ")" <<endl;
	}
	Point (const Point &p){
		x = p.x;
		y = p.y;
		cout << "Copy constructor..."<<endl;
	}
	~Point(){
		cout << "Destructor(" << this << ")" <<endl;

	}
	Point operator=(const Point& aPoint)
	{
		x = aPoint.x;
		y = aPoint.y;
		cout << "operator = ..."<<endl;
		return *this;
	}


	Point operator-(){
		cout << "operator - ..."<<endl;
		return Point(-x, -y);
	}

	Point operator!(){
		cout << "operator ! ..."<<endl;
		
		/*
		Point v(y,x);
		return v;
		*/
		
		return Point(y, x);
	}

	int get_x(){
		return x;
	}

	int get_y(){
		return y;
	}

	void set_x(int aX){
		if(x > 0 && x < 1000)
			x = aX;
	}

	void set_y(int aY){
		y = aY;
	}
	
};
void main()
{
	Point p(10,20);
	
	//Point p1 = !p;// p.operator!();
	Point p4 = -p;

/*
	cout << "X:" << p1.get_x() << endl;
	cout << "Y:" << p1.get_y() << endl;

	cout << "X:" << p4.get_x() << endl;
	cout << "Y:" << p4.get_y() << endl;

	p1 = p4;
	*/
	
}

