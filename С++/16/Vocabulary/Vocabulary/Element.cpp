#include "Element.h"
#include <stdio.h>

Element::Element(){
	left = right = nullptr;
}

Element::Element(String En, String Rus, int Counter)
{
	right = left = nullptr;
	value.set_word(En, Rus, Counter);
}

void Element::set_value(String En, String Rus, int Counter)
{
	value.set_word(En, Rus, Counter);
}

void Element::add_translation(String Rus)	//to add a russian translation (takes translation in String)
{
	value.add_rus(Rus);
}

void Element::remove_translation(String Rus)	//to delete russion translation (takes translation in String)
{
	value.del_rus(Rus);
}

void Element::remove_translation(int number)	//to delete russion translation (takes number of translation (starts from 1))
{
	value.del_rus(number);
}

void Element::change_translation(String Rus_old, String Rus_new)	//to change russian translation (takes old and new translations in String)
{
	value.change_rus(Rus_old, Rus_new);
}

void Element::change_translation(int number, String Rus_new)	//to change russian translation (takes number of old translations and a new one in String)
{
	value.change_rus(number, Rus_new);
}

void Element::count()	//to add one more calling of a word
{
	value.add_count();
}

void Element::set_left(Element* aLeft)
{
	left = aLeft;
}
void Element::set_right(Element* aRight)
{
	right = aRight;
}

Word Element::get_value()	//to get a word instance
{
	return value;
}

//
int Element::get_counter() const	//to get a word's calls counter
{
	return value.get_counter();
}

Element* Element::get_left()
{
	return left;
}

Element* Element::get_right()
{
	return right;
}