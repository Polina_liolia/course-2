#pragma once
#include"Transport.h"

class Air_transport : public Transport
{
protected:
	double max_height;

public:
	Air_transport();
	Air_transport(string aModel, double aWeight, double aMax_speed, speed aSpeed_units, double aMax_height);
	~Air_transport() {};

public:
	void set_max_height(double aMax_height);
	double get_max_height()const;

	virtual speed get_speed_units()const;
	virtual void print();
};