#include "SomeClass.h"
#include <memory>
#include <iostream>
using std::cout;
using std::endl;

using std::shared_ptr;

shared_ptr<SomeClass> ptr6;

void main() {
	shared_ptr<SomeClass> ptr(new SomeClass);
	shared_ptr<SomeClass> ptr2 = ptr;
	shared_ptr<SomeClass> ptr3 = ptr;
	shared_ptr<SomeClass> ptr4 = ptr;

	shared_ptr<SomeClass> ptr5;

	ptr5 = ptr2;

	ptr6 = ptr5;


	shared_ptr<SomeClass> *ptr7 = 
		new shared_ptr<SomeClass>(ptr);

	cout << "Use count:" << ptr.use_count() << endl;

	ptr4->someClassFunction();
	ptr->someClassFunction();

}