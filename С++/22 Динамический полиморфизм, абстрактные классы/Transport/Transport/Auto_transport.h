#pragma once
#include"Ground_transport.h"

enum body_type { sedan, hatchback, van, truck };

class Auto_transport : public Ground_transport
{
private:
	body_type body;

public:
	Auto_transport();
	Auto_transport(string aModel, double aWeight, double aMax_speed, speed aSpeed_units, double aEngine_volume, body_type aBody);
	~Auto_transport() {};

public:
	void set_body(body_type aBody);
	body_type get_body()const;

	virtual speed get_speed_units()const;
	virtual void print();
};