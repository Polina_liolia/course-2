#include <iostream>
#include <iomanip>
#include<fstream>
#include<string>
using std::cin;
using std::cout;

void main()
{
/*	int x;
	do
	{
		cin >> x;
		if (cin.good())
			cout << "You entered an integer\n";
		if (cin.fail())
		{
			cout << "It's not an integer\n";
			cin.ignore(10);
			cin.clear();
		}
		if (cin.eof())
			cout << "End of file\n";

	} while (cin.fail());//because cin.clear() cleaned all flags

	/*______________________________________________________________________________________________________*/
/*	cin >> x;
	if (cin.good())
		cout << "You entered an integer\n";
	if (cin.fail())
	{
		cout << "It's not an integer\n";
		cin.ignore(10);
		cin.clear();
	}
	if (cin.eof())
		cout << "End of file\n";

	/*______________________________________________________________________________________________________*/
/*	cin >> x;
	if (cin.good())
		cout << "You entered an integer\n";
	if (cin.fail())
	{
		cout << "It's not an integer\n";
		cin.ignore(10);
		cin.clear();
	}
	if (cin.eof())
		cout << "End of file\n";

	double d = 23.34234;
	cout << std::fixed << d <<"\n";	//outputting in the same format as it was inputed
	cout.unsetf(std::ios::fixed);	//removes flag "fixed"
	cout << std::setprecision(3) << d << "\n";	//sets number of digins avter point
	cout << d <<"\n";
	cout.setf(std::ios::right);
	cout << std::setw(16) << std::setfill('*') << d << "\n";
*/
/*	std::fstream f("2.bin", std::ios::ios_base::in | std::ios::ios_base::binary | std::ios::ios_base::out);
	f << "fjekfjkefje";
	f.close();*/


	// opening file for reading and writing - file must exist:
	std::ofstream oFile("2.bin", std::ios::binary);
	// checking if file was successfully opened, othervice generating exception:
	try
	{
		if (!oFile)
			throw "Can't open file for reading and writing - it must exist";
	}
	catch (char* msg)
	{
		std::cout << msg << "\n";
	}
	std::string s = "sjkjfkejfiewo";
	oFile.write((char*)s.c_str(), s.length());
//	calling operator <<(ostream& os, Reservations_system& rs)
//	oFile << "fjekfjkefje";
}