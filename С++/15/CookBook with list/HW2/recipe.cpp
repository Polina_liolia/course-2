#include <iostream>
#include <cstring>
#include "ingredient.h"
#include "recipe.h"
using std::cout;
using std::cin;

	recipe::recipe()
	{
		name = nullptr;
		type = nullptr;
		process = nullptr;
	}

	recipe::recipe(char *aName, char *aType, char *aProcess)
	{
		if (aName && strlen(aName)) 
		{
			name = new char[strlen(aName) + 1];
			strcpy_s(name, strlen(aName) + 1, aName);
		}
		else 
			name = nullptr;
		
		if (aType && strlen(aType))
		{
			type = new char[strlen(aType) + 1];
			strcpy_s(type, strlen(aType) + 1, aType);
		}
		else
			type = nullptr;

		if (aProcess && strlen(aProcess))
		{
			process = new char[strlen(aProcess) + 1];
			strcpy_s(process, strlen(aProcess) + 1, aProcess);
		}
		else
			process = nullptr;
	}

	recipe::recipe(const recipe &temp)
	{
		if (temp.name && strlen(temp.name))
		{
			name = new char[strlen(temp.name) + 1];
			strcpy_s(name, strlen(temp.name) + 1, temp.name);
		}
		else
			name = nullptr;

		if (temp.type && strlen(temp.type))
		{
			type = new char[strlen(temp.type) + 1];
			strcpy_s(type, strlen(temp.type) + 1, temp.type);
		}
		else
			type = nullptr;

		if (temp.process && strlen(temp.process))
		{
			process = new char[strlen(temp.process) + 1];
			strcpy_s(process, strlen(temp.process) + 1, temp.process);
		}
		else
			process = nullptr;
		components = temp.components;
	}

	recipe::~recipe()
	{
		delete[] name;
		delete[] type;
		delete[] process;
	}

	inline char *recipe::get_name()const					//inline function to get a name
	{
		return name;
	}

	inline char *recipe::get_type()const					//inline function to get a type of a dish
	{
		return type;
	}

	inline char *recipe::get_process()const					//inline function to get a process of cooking a dish
	{
		return process;
	}

	void recipe::set_name(char *aName)			//setting a name of a dish
	{
		if (name)
			delete[] name;
		int sz = strlen(aName) + 1;
		name = new char[sz];
		strcpy_s(name, sz, aName);
	}


	void recipe::set_type(char *aType)			//setting a type of a dish
	{
		if (type)
			delete[] type;
		int sz = strlen(aType) + 1;
		type = new char[sz];
		strcpy_s(type, sz, aType);
	}

	void recipe::set_process(char *aProcess)	//setting a process of cooking a dish
	{
		if (process)
			delete[] process;
		int sz = strlen(aProcess) + 1;
		process = new char[sz];
		strcpy_s(process, sz, aProcess);
	}

	void recipe::set_ingridient(char *aItem, float aDose)	//setting an components (items and dose) for a dish
	{
		ingredient New(aItem, aDose);
		components.add(New);
	}

	void recipe::del_ingridient(int ingredient_number)	//to delete pointed ingridient
	{
		components.delByIndex(ingredient_number - 1);
	}


	int recipe::save_recipe_to_file(FILE *f, char *file_name)const
	{
		if (f == nullptr)
			fopen_s(&f, file_name, "ab");					//opens a binar file to write to the end
		if (f == nullptr)								//if file was not opened
			return -1;
		
		int sz = strlen(name) + 1;
		fwrite(&sz, sizeof(int), 1, f);
		fwrite(name, sz, 1, f);						//to write a recipe name

		sz = strlen(type) + 1;
		fwrite(&sz, 4, 1, f);
		fwrite(type, sz, 1, f);						//to write a recipe process

		sz = strlen(process) + 1;
		fwrite(&sz, 4, 1, f);
		fwrite(process, sz, 1, f);					//to write a recipe process

		Element* tmp_el = components.get_head();
		while (tmp_el)
		{
			int sz = strlen(tmp_el->get_value().item) + 1;
			fwrite(&sz, 4, 1, f);							//writing a size of string
			fwrite(tmp_el->get_value().item, sz, 1, f);		//writing an item
			float tmp_dose = tmp_el->get_value().dose;		//to save meaning of dose and then use it when writing in file
			fwrite(&tmp_dose, 4, 1, f);						//writing a dose
			tmp_el = tmp_el->get_next();
		}
		fclose(f);
		return 0;
	}

	void recipe::print_recipe_name()const							//to print recipe name
	{
		cout << name << "\n";
	}

	void recipe::print_recipe()const									//to print all recipe information
	{
		cout << "Recept name: " << name << "\n";
		cout << "Type: " << type << "\n";
		cout << "Cooking process: " << process << "\n";
		cout << "Ingridients:\n";
		components.show();
	}

	void recipe::print_ingredients()const							//to print the list of components
	{
		cout << "Ingridients:\n";
		components.show();
	}
