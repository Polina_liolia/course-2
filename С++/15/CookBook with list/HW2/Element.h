#pragma once
#include"ingredient.h"

class Element
{
	ingredient value;
	Element* next;
	Element* prev;

public:
	Element();
	Element(ingredient aValue);
	void set_value(ingredient aValue);
	ingredient get_value();
	void set_next(Element* aNext);
	void set_prev(Element* aPrev);
	Element* get_next();
	Element* get_prev();
};