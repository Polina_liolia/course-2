#include<iostream>
#include "space1.h"
#include <typeinfo.h>
#include <iomanip>
#include <string>
using std::string;
using std::cout;


template<typename T>
void insert_sort(T *p_arr, int size)
{
	int j = 0;
	for (int i = 1; i < size; i++)
	{
		T x = *(p_arr + i); //��������� �������
		for (j = i - 1; j >= 0 && *(p_arr + j) > x; j--)
			*(p_arr + j + 1) = *(p_arr + j);
		*(p_arr + j + 1) = x;//������� � ������������ ���.
	}
}

int var1 = 5;
void main()
{
	int var1 = 3;
	std::cout << var1 << "\n";
	std::cout << ::var1 << "\n";
	std::cout << space1::var1 << "\n";

	int x;
	bool y;
	type_info a(int);
	std::cout << typeid(x).name() << "\n";
	std::cout << (typeid(x) == typeid(var1)) << "\n";
	std::cout << (typeid(x) == typeid(y)) << "\n";

	int *arr1 = new int[5];
	arr1[0] = 3;
	arr1[1] = 1;
	arr1[2] = 5;
	arr1[3] = 2;
	arr1[4] = 4;
	insert_sort<int>(arr1, 5);

	double *arr2 = new double[5];
	arr1[0] = 3;
	arr1[1] = 1;
	arr1[2] = 5;
	arr1[3] = 2;
	arr1[4] = 4;
	insert_sort<double>(arr2, 5);

	string *arr3 = new string[5];
	arr3[0] = (string)"c";
	arr3[1] = (string)"b";
	arr3[2] = (string)"a";
	arr3[3] = (string)"e";
	arr3[4] = (string)"d";
	insert_sort<string>(arr3, 5);

	cout << "\n\nint array sort:\n";
	for (int i = 0; i < 5; i++)
		cout << arr1[i] << " ";
	
	cout << "\n\ndouble array sort:\n";
	for (int i = 0; i < 5; i++)  
		cout << std::setprecision(2) << arr2[i] << " ";

	cout << "\n\nstring array sort:\n";
	for (int i = 0; i < 5; i++)
		cout << arr3[i] << " ";


}