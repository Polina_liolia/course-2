#include <iostream>
using namespace std;
#include "DoubleLinkList.h"

DoubleLinkList::DoubleLinkList()
{
	
	tail = head = NULL;
}
void DoubleLinkList::add(int aValue)
{
	Element* tmp_el = new Element(aValue);
	if(head)
	{
		tail->set_next(tmp_el);
		tmp_el->set_prev(tail);
		
	}
	else head = tmp_el;
	tail = tmp_el;
}

void DoubleLinkList::show()
{
		Element* tmp_el = head;
		while(tmp_el)
		{
			cout << tmp_el->get_value()<<'\n';
			tmp_el = tmp_el->get_next();
		}
}

void DoubleLinkList::show_reverse()
{
		Element* tmp_el = tail;
		while(tmp_el)
		{
			cout << tmp_el->get_value()<<'\n';
			tmp_el = tmp_el->get_prev();
		}
}

DoubleLinkList::~DoubleLinkList()
{
		while(head)
		{
			Element* tmp_el = head;
			head = head->get_next();
			delete tmp_el;
		}
}

bool DoubleLinkList::delByIndex(int index)
{
	if(index)
	{
		Element *prev = head;
		for(int i=0; i<index - 1; i++)
			prev = prev->get_next();
		Element* tmp = prev->get_next();
		prev->set_next(prev->get_next()->get_next());
		if(tmp == tail)  tail = prev;
		delete tmp;
	}else
	{
		Element* tmp = head;
		head = head->get_next();
		delete tmp;
	}
	return true;
}


bool DoubleLinkList::insertByIndex(int index, int value){
	if(index){
		Element *prev = head;
		for(int i=0; i<index - 1 && prev; i++)
			prev = prev->get_next();
		if(prev){
			Element* insElem = new Element(value);
			insElem->set_next(prev->get_next());
			prev->set_next(insElem);
			if(prev == tail)  tail = insElem;
		}else return false;
	}else{
		Element* insElem = new Element(value);
		insElem->set_next(head);
		head = insElem;
	}
	return true;
}


DoubleLinkList::Element::Element()
{
	value = 0;
	next = prev = 0;
}
DoubleLinkList::Element::Element(int aValue)
{
	value = aValue;
	next = prev =  0;
}
void DoubleLinkList::Element::set_value(int aValue)
{
	value = aValue;
}
int DoubleLinkList::Element::get_value()
{
	return value;
}
void DoubleLinkList::Element::set_next(DoubleLinkList::Element* aNext)
{
	next = aNext;
}

DoubleLinkList::Element* DoubleLinkList::Element::get_next()
{
	return next;
}


void DoubleLinkList::Element::set_prev(Element* aPrev)
{
	prev = aPrev;
}
DoubleLinkList::Element* DoubleLinkList::Element::get_prev()
{
	return prev;
}



 
