#include <iostream>


class Point2D
{
private:
	char*name;
	int x;
	int y;

public:
	
	Point2D()
	{
		name = nullptr;
		x = 0;
		y = 0;
	}

	~Point2D()
	{
		std::cout << name;
		delete[] name;
	}

public:

	char* set_name(char* Iname)
	{
		int sz = strlen(Iname) + 1;
		strcpy_s(name, sz, Iname);
		return name;
	}
	
	int set_coordinateX(int Ix)
	{
		x = Ix;
		return x;
	}

	int set_coordinateY(int Iy)
	{
		y = Iy;
		return y;
	}
};

void main()
{
	Point2D *arr = new Point2D[2];
	
	arr[0].set_name("P1");
	arr[0].set_coordinateX(5);
	arr[0].set_coordinateY(10);
	
	arr[1].set_name ("P2");
	arr[1].set_coordinateX (15);
	arr[1].set_coordinateY(20);

	delete[] arr;
}
