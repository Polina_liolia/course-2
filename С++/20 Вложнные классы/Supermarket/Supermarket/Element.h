#pragma once
#include "Department.h"

class Element
{
	Department value;
	Element* next;
	Element* prev;

public:
	Element();
	Element(const Element &aEl);
	Element(Department aValue);
	void set_value(Department aValue);
	Department &get_value();
	void set_next(Element* aNext);
	void set_prev(Element* aPrev);
	Element* get_next();
	Element* get_prev();



};