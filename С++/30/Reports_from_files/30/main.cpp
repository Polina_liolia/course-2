#include"admin_report.h"
#include"manager_report.h"
#include"user_report.h"

void main()
{
	user_report UR;
	try
	{
		UR.report("data.txt", "user_report.txt", "UR.txt");
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}

	manager_report MR;
	try
	{
		MR.report("data.txt", "manager_report.txt", "MR.txt");
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}

	admin_report AR;
	try
	{
		AR.report("data.txt", "admin_report.txt", "AR.txt");
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}

}