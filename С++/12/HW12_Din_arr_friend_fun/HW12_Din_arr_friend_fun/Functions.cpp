#include "Array.h"

/*
find_numbers:
			start - minimal number to find
			end - maximal number to find
			aSearch_type:
						line - default
						binar
			aSort_type:
						bubble - default
						insert
						select
*/

void find_numbers(Array A, int start, int end, search_type aSearch_type, sort_type aSort_type) //friend functioa of class Array
{
	if (aSearch_type == line)
		line_search(A.arr, A.sz, start, end);
	else		//binar search
	{
		switch (aSort_type)
		{
		case bubble: insert_sort(A.arr, A.sz); break;
		case insert: insert_sort(A.arr, A.sz); break;
		case select: select_sort(A.arr, A.sz); break;
		}
		binar_search(A.arr, A.sz, start, end);
	}
}

void line_search(int *arr, int sz, int min, int max)
{
	bool flag = false;
	for (int i = 0; i < sz; i++)
		if (arr[i] >= min && arr[i] <= max)
		{
			std::cout << arr[i] << " ";
			flag = true;
		}
	if (!flag)
		std::cout << "No elements from " << min << " to " << max << " found.\n";
	std::cout << "\n";
}

void bubble_sort(int *p_arr, int sz)
{
	for (int b = 0, k = sz; b < sz - 1; b++, k--)
		for (int i = 0; i < k - 1; i++)
			if (p_arr[i] > p_arr[i + 1])
			{
				int j = p_arr[i];
				p_arr[i] = p_arr[i + 1];
				p_arr[i + 1] = j;
			}
}

void insert_sort(int *p_arr, int sz)
{
	int j = 0;
	for (int i = 1; i < sz; i++)
	{
		int x = p_arr[i];
		for (j = i - 1; j >= 0 && p_arr[j] > x; j--)
			p_arr[j + 1] = p_arr[j];
		p_arr[j + 1] = x;
	}
}

void select_sort(int *p_arr, int sz)
{
	for (int j = 0; j < sz; j++)
	{
		int min = p_arr [j];
		int min_pos = j;
		for (int i = j; i < sz; i++)
		{
			if (p_arr[i] < min)
			{
				min_pos = i;
				min = p_arr[i];
			}
		}
		p_arr[min_pos] = p_arr[j];
		p_arr[j] = min;
	}
}

void binar_search(int *arr, int sz, int min, int max)
{
	int begin = 0;
	int end = sz-1;
	while (begin < end) {
		int mid = (begin + end) / 2;
		if (min <= arr[mid]) 
		{
			end = mid;
		}
		else 
		{
			begin = mid + 1;
		}
	}
	if (arr[end] >= min)
	{
		int min_index = 0;
		for (int i = end; i >= 0; i--)
			if (arr[i] >= min)
				min_index = i;
			else
				break;
		for (int i = min_index; i < sz; i++)
			if (arr[i] <= max)
				std::cout << arr[i] << " ";
			else
				break;
		std::cout << "\n";
	}
	else
		std::cout << "No elements from "<< min <<" to " << max <<" found.\n";

}