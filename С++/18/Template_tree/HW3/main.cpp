#include <iostream>
#include"Tree.h"
#include "Student.h"
#include "Word.h"

//globaly overloaded output of String
ostream& operator << (ostream& os, const String &aString)
{
	if (aString.get_String() != nullptr)
		os << aString.str << " ";
	return os;
}

//globaly overloaded output of ingredient
ostream& operator << (ostream& os, const Student &aSt)
{
		os << "Surname: " << aSt.surname << "\n";
		os << "Name: " << aSt.name << "\n";
		os << "Patronymic: " << aSt.patronymic << "\n";
		os << "Date of birth: " << aSt.birth.day << "." << aSt.birth.month << "." << aSt.birth.year << "\n";
		os << "Phone: " << aSt.phone << "\n";
		os << "Univercity: " << aSt.univercity << "\n";
		os << "Country: " << aSt.country << "\n";
		os << "City: " << aSt.city << "\n";
		os << "Group: " << aSt.group << "\n";
	return os;
}

//globaly overloaded output of Word
ostream & operator << (ostream& os, const Word &aWord)
{
	os << aWord.en << "(popularity: " << aWord.counter << ") - " << aWord.rus;
	os << "\n";
	return os;
}


void main()
{
	setlocale(0, "RUS");

	//creating instances of class Sudent:
	Student First("Ivanov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "25-ZS-55");
	Student Second("Petrov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "25-ZS-55");
	Student Third("Kozlov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "25-ZS-55");
	Student Fourth("Abramov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "25-ZS-55");
	Student Fifth("Semenov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "25-ZS-55");

	//creating an intance of class Tree, specified for Student type:
	Tree<Student> Students_list;
	//adding elements in tree:
	Students_list.add(First);
	Students_list.add(Second);
	Students_list.add(Third);
	Students_list.add(Fourth);
	Students_list.add(Fifth);
	//outputting a tree to console:
	cout << "A binar tree, specified for Student:\n";
	Students_list.print();

	cout << "\nTesting del method for tree (deleting student Kozlov)\n";
	Students_list.del(Third);
	Students_list.print();

	cout << "\nTesting del method for deleting a tree root (deleting student Ivanov)\n";
	Students_list.del(First);
	Students_list.print();

	cout << "\nTesting set method for tree (changing student Semenov on Klimenov):\n";
	Student Sixth("Klimenov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "25-ZS-55");
	Students_list.set(Fifth, Sixth);
	Students_list.print();

	cout << "\nTesting searching for student Abramov:\n";
	Students_list.search(Fourth);

	//printing all students' list to file (defalt name - tree.bin)::
	Students_list.print_tree_to_file();

	//creating instances of class Word:
	Word A("dog", "������", 5);
	Word B("cat", "�����", 3);
	Word C("mouse", "����", 1);
	Word D("pig", "������", 10);
	Word E("cow", "������", 2);
	Word F("horse", "������", 1);
	Word G("rat", "�����", 7);
	Word H("tiger", "����", 15);

	//creating an intance of class Tree, specified for Student type:
	Tree<Word> Vocabulary;
	//adding elements in tree:
	Vocabulary.add(A);
	Vocabulary.add(B);
	Vocabulary.add(C);
	Vocabulary.add(D);
	Vocabulary.add(E);
	Vocabulary.add(F);
	Vocabulary.add(G);
	Vocabulary.add(H);

	//outputting a tree to console:
	cout << "\nA binar tree, specified for Word:\n";
	Vocabulary.print();

	cout << "\nTesting del method for tree (deleting word \"mouse\")\n";
	Vocabulary.del(C);
	Vocabulary.print();

	cout << "\nTesting del method for deleting a tree root (deleting word \"dog\")\n";
	Vocabulary.del(A);
	Vocabulary.print();

	cout << "\nTesting set method for tree (changing word \"pig\" on \"elefant\"):\n";
	Word I("elefant", "����", 3);
	Vocabulary.set(D, I);
	Vocabulary.print();

	cout << "\nTesting searching for word (\"cat\"):\n";
	Vocabulary.search(B);

	//printing Vocabulary to file (defalt name - tree.bin):
	Vocabulary.print_tree_to_file();
}