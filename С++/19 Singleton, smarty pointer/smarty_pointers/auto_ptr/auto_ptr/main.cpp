#include <memory>
#include <iostream>
//#include <vector>

using namespace std;

class Int 
{
public:
   Int(int i){
      cout << "Constructing " << ( void* )this  << endl; 
      x = i;
      bIsConstructed = true;
   };
   ~Int( ){
      cout << "Destructing " << ( void* )this << endl; 
      bIsConstructed = false;
   };
   Int &operator++( ){
      x++;
      return *this;
   };
   int x;
private:
   bool bIsConstructed;
};

void function ( auto_ptr<Int> &pi )
{
   ++( *pi );
   auto_ptr<Int> pi2( pi );
   ++( *pi2 );
   pi = pi2;
}

int main( ) 
{
   auto_ptr<Int> pi ( new Int( 5 ) );
   cout << pi->x << endl;
   function( pi );
   cout << pi->x << endl;


}

/*
int main(int argc, char **argv){
    int *i = new int;
	*i = 10;
    auto_ptr<int> x(i);
    auto_ptr<int> y;
 
    y = x;
    //cout << *x<< endl;
    cout << x.get() << endl; // Print NULL
    cout << y.get() << endl; // Print non-NULL address i
    return 0;
}
*/
// ������� ������ ����� ���������� 