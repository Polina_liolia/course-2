#pragma once
#include "Element.h"

class DoubleLinkList
{
	Element* head;
	Element* tail;
public:
	DoubleLinkList();
	Element *get_head()const;
	Element *get_taild()const;
	void add(ingredient aValue);
	void show()const;
	void show_reverse()const;
	bool delByIndex(unsigned int index);
	//bool delPackage(unsigned int index, unsigned int quantity);
	~DoubleLinkList();
	bool insertByIndex(int index, ingredient value);
	bool del_package(int index, int number);	//int number - total number of elements to delete

};