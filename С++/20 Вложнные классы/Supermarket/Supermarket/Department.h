#pragma once
#include"MyString.h"
#include "employee.h"

class Department
{
private:
	String dept_name;
	date founded;
	date edited;
	int size_dept_stuff;
	employee *dept_stuff;

public:
	Department();
	Department(String aName, short aDay, short aMonth, short aYear);
	Department(const Department &aDept);
	~Department();

public:
	void set_dept_name(String aDept_name, short aDay_edited, short aMonth_edited, short aYear_edited);
	void set_date_founded(short aDay_founded, short aMonth_founded, short aYear_founded, short aDay_edited, short aMonth_edited, short aYear_edited);
	String get_dept_name()const;
	date get_date_founded()const;
	date get_date_edited()const;
	int get_size_dept_stuff()const;
	employee *get_dept_stuff()const;
	employee get_employee(int index)const;
	void add_employee(String aSurname, String aName, position aType, String aPhone, short b_aDay, short b_aMonth, short b_aYear, short aDay_edited, short aMonth_edited, short aYear_edited);
	void remove_employee(String aSurname, short aDay_edited, short aMonth_edited, short aYear_edited);
	void edit_employee_surname(int index, String aSurname);
	void edit_employee_name(int index, String aName);
	void edit_employee_birth_date(int index, short aDay = 0, short aMonth = 0, short aYear = 0);
	void edit_employee_phone(int index, String aPhone);
	void edit_employee_position(int index, position aType);

	Department operator=(const Department &aDept);
	bool operator<(const Department &aDept);
	bool operator>(const Department &aDept);
	bool operator<=(const Department &aDept);
	bool operator>=(const Department &aDept);

	
	friend ostream& operator << (ostream& os, Department &aDept);
};