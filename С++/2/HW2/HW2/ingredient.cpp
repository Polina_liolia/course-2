#include <iostream>
#include <cstring>
using std::cout;
using std::cin;
#include "ingredient.h"

	ingredient::ingredient(char *aItem, float aDose)
	{
		if (aItem && strlen(aItem))
		{
			item = new char[(strlen(aItem) + 1)];
			strcpy_s(item, (strlen(aItem) + 1), aItem);
		}
		else
			item = nullptr;
		dose = aDose;
	}

	ingredient::~ingredient()
	{
		delete[] item;
	}
