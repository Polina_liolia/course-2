#pragma once

class A{
	
	int x;
	int y;
	friend void f1(A);
	friend A invert(A);

public:
	A(){
		x = y = 0;
	}
	A(int aX, int aY){
		x = aX;
		y = aY;
	}

	int get_x(){
		return x;
	}

	int get_y(){
		return y;
	}


};

A z;


void f1(A a){
	a.x = a.y = 34;
	z.x = z.y = 56;
	A d;
	d.x = d.y = 45;
}

A invert(A a){
	/*
	A n(a.y, a.x);
	return n;
	*/
	return A(a.y, a.x);
}








