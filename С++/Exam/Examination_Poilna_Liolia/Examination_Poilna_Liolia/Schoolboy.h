#pragma once
#include <iostream>
#include <string>
#include "date.h"
using std::string;

class Schoolboy
{
protected:
	string name;
	string surname;
	date birth_day;
	string address;
	string school;
	string phone;

public:
	Schoolboy();
	Schoolboy(string aName, string aSurname, date aBirth_day, string aAdress, string aSchool, string aPhone = "");
	~Schoolboy();

	void set_name(string aName);
	void set_surname(string aSurname);
	void set_birth_day(date aBirth_day);
	void set_address(string aAdress);
	void set_school(string aSchool);
	void set_phone(string aPhone);

	string get_name()const;
	string get_surname()const;
	date get_birth_day()const;
	string get_address()const;
	string get_school()const;
	string get_phone()const;

	virtual Schoolboy operator= (const Schoolboy &S);
	bool operator==(const Schoolboy &S);
	bool operator!=(const Schoolboy &S);

	virtual void print_to_file(char * file_name);
	virtual void read_from_file(char * file_name);

	friend ostream& operator << (ostream& os, Schoolboy &S);
	friend istream& operator >> (istream& is, Schoolboy &S);

};

