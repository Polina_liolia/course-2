#include "overcoat.h"
#include "Flat.h"
#include <iostream>
using std::cout;

void main()
{
	overcoat a("coat", "red", 1520);
	overcoat b("parka", "black", 2000);
	overcoat c("coat", "blue", 1650);
	overcoat d = a;
	bool flag;
	if (a == c)
		flag = true;
	else
		flag = false;
	cout << flag << "\n";
	d=c;
	if (a > c)
		flag = true;
	else
		flag = false;
	cout << flag << "\n";

	flat first("Pushkinskaya, 28-36", 95, 120000);
	flat second("Klochkovskaya, 145-96", 45, 60000);
	flat third ("Nisova", 20, 30000);
	third = first;
	if (third > second)
		flag = true;
	else
		flag = false;
	cout << flag;
	if (first == second)
		flag = true;
	else
		flag = false;
	cout << flag;

}