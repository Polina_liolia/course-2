#include "Student.h"

Student::Student()		//constructor
{
	birth.day = 0;
	birth.month = 0;
	birth.year = 0;
}

//overloaded constructor:
Student::Student(String aSurname, String aName, String aPatronymic, short aDay, short aMonth, short aYear, String aPhone, String aUnivercity, String aCountry, String aCity, String aGroup)
{
	surname = aSurname;
	name = aName;
	patronymic = aPatronymic;
	birth.day = aDay;
	birth.month = aMonth;
	birth.year = aYear;
	phone = aPhone;
	univercity = aUnivercity;
	country = aCountry;
	city = aCity;
	group = aGroup;
}

Student::Student(const Student & a)
{
	surname = a.surname;
	name = a.name;
	patronymic = a.patronymic;
	birth.day = a.birth.day;
	birth.month = a.birth.month;
	birth.year = a.birth.year;
	phone = a.phone;
	univercity = a.univercity;
	country = a.country;
	city = a.city;
	group = a.group;
}

Student::~Student()		//destructor
{
}

	void Student::set_surname(String aSurname)				//setting surname
	{
		surname = aSurname;
	}

	void Student::set_name(String aName)						//setting name
	{
		name = aName;
	}

	void Student::set_patronymic(String aPatronymic)			//setting patronymic
	{
		patronymic = aPatronymic;
	}

	void Student::set_phone(String aPhone)					//setting phone number
	{
		phone = aPhone;
	}

	void Student::set_univercity(String aUnivercity)					//setting univercity
	{
		univercity = aUnivercity;
	}

	void Student::set_country(String aCountry)						//setting a country
	{
		country = aCountry;
	}

	void Student::set_city(String aCity)								//setting a city
	{
		city = aCity;

	}

	void Student::set_group(String aGroup)							//setting a group
	{
		group = aGroup;
	}
	 
	void Student::print_data()										//to print information about student
	{
		cout << "Surname: " << surname << "\n";
		cout << "Name: " << name << "\n";
		cout << "Patronymic: " << patronymic << "\n";
		cout << "Date of birth: " << birth.day << "." << birth.month << "." << birth.year << "\n";
		cout << "Phone: " << phone << "\n";
		cout << "Univercity: " << univercity << "\n";
		cout << "Country: " << country << "\n";
		cout << "City: " << city << "\n";
		cout << "Group: " << group << "\n";
	}

	Student Student::operator=(Student a)
	{
		surname = a.surname;
		name = a.name;
		patronymic = a.patronymic;
		birth.day = a.birth.day;
		birth.month = a.birth.month;
		birth.year = a.birth.year;
		phone = a.phone;
		univercity = a.univercity;
		country = a.country;
		city = a.city;
		group = a.group;
		return *this;
	}

	bool Student::operator==(Student a)
	{
		return surname == a.surname;
	}

	bool Student::operator!=(Student a)
	{
		return surname != a.surname;
	}

	bool Student::operator>(Student a)
	{
		return surname > a.surname;
	}

	bool Student::operator<(Student a)
	{
		return surname < a.surname;
	}

	bool Student::operator>=(Student a)
	{
		return surname >= a.surname;
	}

	bool Student::operator<=(Student a)
	{
		return surname <= a.surname;
	}
