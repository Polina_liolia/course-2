// ���� WINDOWS.H �������� �����������, �������, � ��������� ������� ������������ ��� ��������� ���������� ��� Windows. 
#include <windows.h>
#include <tchar.h>
#include "resource.h"
#include <Time.h>
#include <ctime>
#include <Strsafe.h>


// �������� ������� ���������
LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wp, LPARAM lp);
#define OPENDIALOGID 300 //constant identifier of button created

TCHAR szClassWindow[] = TEXT("��������� ����������");	/* ��� ������ ���� */

INT WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow)
{
	HWND hWnd;
	MSG Msg;
	WNDCLASSEX wcl;


	// 1. ����������� ������ ����
	wcl.cbSize = sizeof(wcl);	// ������ ��������� WNDCLASSE
								//������������ �� ����, ���� ������� ������ �� ����������� ��� �� ���������:
	wcl.style = CS_HREDRAW | CS_VREDRAW;	// CS (Class Style) - ����� ������ ����
	wcl.lpfnWndProc = WindowProc;	// ����� ������� ���������
	wcl.cbClsExtra = 0;		// ������������ Windows 
	wcl.cbWndExtra = 0; 	// ������������ Windows 
	wcl.hInstance = hInst;	// ���������� ������� ����������
	wcl.hIcon = LoadIcon(NULL, IDI_APPLICATION);	// �������� ����������� ������
	wcl.hCursor = LoadCursor(NULL, IDC_ARROW);	// �������� ������������ �������	
	wcl.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	// ���������� ���� ����� ������
	wcl.lpszMenuName = NULL;	// ���������� �� �������� ����
	wcl.lpszClassName = szClassWindow;	// ��� ������ ����
	wcl.hIconSm = NULL;	// ���������� ��������� ������ ��� ����� � ������� ����

						// 2. ����������� ������ ����
	if (!RegisterClassEx(&wcl))
		return 0; // ��� ��������� ����������� - �����

				  // 3. �������� ����
				  // ��������� ���� �  ���������� hWnd ������������� ���������� ����
	hWnd = CreateWindowEx(
		0,		// ����������� ����� ����
		szClassWindow,	//��� ������ ����
		TEXT("������ Windows ����������"), // ��������� ����
		WS_OVERLAPPEDWINDOW,				// ����� ����
											/* ���������, �����, ����������� ������ �������, ��������� ����, ������ ������������ � ���������� ����  */
		CW_USEDEFAULT,	// �-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// y-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// ������ ����
		CW_USEDEFAULT,	// ������ ����
		NULL,			// ���������� ������������� ����
		NULL,			// ���������� ���� ����
		hInst,		// ������������� ����������, ���������� ����
		NULL);		// ��������� �� ������� ������ ����������

	HWND b1 = CreateWindow(L"button", L"Open Dialog", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10, 10, 100, 30, hWnd, (HMENU)OPENDIALOGID, hInst, NULL);

	// 4. ����������� ����
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd); // ����������� ����



						// 5. ������ ����� ��������� ���������
	while (GetMessage(&Msg, NULL, 0, 0)) // ��������� ���������� ��������� �� ������� ���������
	{
		TranslateMessage(&Msg);	// ���������� ���������
		DispatchMessage(&Msg);	// ��������������� ���������
	}
	return (int)Msg.wParam;
}


LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	switch (uMessage)
	{
	case WM_DESTROY: // ��������� � ���������� ���������
		PostQuitMessage(0); // ������� ��������� WM_QUIT
		break;
	case WM_COMMAND:
	{
		if (HIWORD(wParam) == BN_CLICKED) //control-defined notification code
			switch (LOWORD(wParam)) //control identifier
			{
			case OPENDIALOGID:
			{
				HWND dialog = CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_DIALOG1), hWnd, DlgProc); //unmodal dialog 
				ShowWindow(dialog, SW_SHOW);//show unmodal dialog
			}
			break;
			}
	}
	break;
	default:
		// ��� ���������, ������� �� �������������� � ������ ������� ������� ������������ ������� Windows �� ��������� �� ���������
		return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}
	return 0;
}

INT_PTR CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)//��������� ������� � ���������� ����
{
	switch (message)
	{
	case WM_INITDIALOG:
	{
		//clearing default static text:
		HWND error_field = GetDlgItem(hWnd, IDC_STATIC6);
		SetWindowText(error_field, TEXT(""));
	}
	break;
	case WM_CLOSE:
		EndDialog(hWnd, 0); // ��������� ������
		return TRUE;
	case WM_COMMAND:
		if (HIWORD(wParam) == BN_CLICKED) //������� ������ �������
			switch (LOWORD(wParam)) //control identifier
			{
			case IDOK:
				EndDialog(hWnd, 0); //��������� ������
				return TRUE;
			case IDCANCEL:
				EndDialog(hWnd, 0); //��������� ������
				return FALSE;
			case IDC_BUTTON1:	//result
			{
				HWND static_hours = GetDlgItem(hWnd, IDC_STATIC4);
				SetWindowText(static_hours, TEXT(""));
				HWND static_minutes = GetDlgItem(hWnd, IDC_STATIC5);
				SetWindowText(static_minutes, TEXT(""));
				HWND static_seconds = GetDlgItem(hWnd, IDC_STATIC7);
				SetWindowText(static_seconds, TEXT(""));
				HWND static_year = GetDlgItem(hWnd, IDC_STATIC1);
				SetWindowText(static_year, TEXT(""));
				HWND static_month = GetDlgItem(hWnd, IDC_STATIC2);
				SetWindowText(static_month, TEXT(""));
				HWND static_day = GetDlgItem(hWnd, IDC_STATIC3);
				SetWindowText(static_day, TEXT(""));

				TCHAR buf[100];
				GetDlgItemText(hWnd, IDC_EDIT3, buf, 100);
				int year = _wtoi(buf) - 1900;

				GetDlgItemText(hWnd, IDC_EDIT2, buf, 100);
				int month = _wtoi(buf) - 1;

				GetDlgItemText(hWnd, IDC_EDIT1, buf, 100);
				int day = _wtoi(buf);

				//getting current time:
				struct tm newtime;
				time_t time_data;
				errno_t err;

				// Get time as integer: 
				time(&time_data);
				// Convert to local time:
				err = localtime_s(&newtime, &time_data);
				if (err)
					return FALSE;
				
				//checking correctness of input:
				if (!year || !month || !day)
				{
					//outputting error msg:
					HWND error_field = GetDlgItem(hWnd, IDC_STATIC6);
					SetWindowText(error_field, TEXT("All fields required!"));
					return FALSE;
				}
				if (year < newtime.tm_year
					|| year == newtime.tm_year && month < newtime.tm_mon
					|| year == newtime.tm_year && month == newtime.tm_mon && day <= newtime.tm_mday)
				{
					//outputting error msg:
					HWND error_field = GetDlgItem(hWnd, IDC_STATIC6);
					SetWindowText(error_field, TEXT("Inputed date should not be in past!"));
					return FALSE;
				}
				
				struct tm future_time;
				future_time.tm_hour = future_time.tm_min = future_time.tm_sec = 0;
				future_time.tm_year = year,
					future_time.tm_mon = month,
					future_time.tm_mday = day;

				time_t t1 = mktime(&newtime), t2 = mktime(&future_time);
				time_t time_dif = t2 - t1;
				struct tm time_left;
				localtime_s(&time_left, &time_dif);
				time_left.tm_year -= 70;	//correcting according to different time start points

				int const arraysize = 50;
				TCHAR msg[arraysize];
				size_t sz = arraysize * sizeof(TCHAR);
				LPCTSTR pszFormat = TEXT("%d");
				HRESULT hr; 

				
				hr = StringCbPrintf(msg, sz, pszFormat, time_left.tm_year);
				SetWindowText(static_year, msg);

				hr = StringCbPrintf(msg, sz, pszFormat, time_left.tm_mon);
				SetWindowText(static_month, msg);
				
				hr = StringCbPrintf(msg, sz, pszFormat, time_left.tm_mday);
				SetWindowText(static_day, msg);

				//checking if checkbox is checked:
				HWND checkbox = GetDlgItem(hWnd, IDC_CHECK1);
				LRESULT lResult = SendMessage(checkbox, BM_GETCHECK, 0, 0);
				if (lResult != BST_CHECKED)
					return FALSE;

				hr = StringCbPrintf(msg, sz, pszFormat, time_left.tm_hour);
				SetWindowText(static_hours, msg);

				hr = StringCbPrintf(msg, sz, pszFormat, time_left.tm_min);
				SetWindowText(static_minutes, msg);

				hr = StringCbPrintf(msg, sz, pszFormat, time_left.tm_sec);
				SetWindowText(static_seconds, msg);

			}
			break;
			}
		break;
	}
	return FALSE;
}