#include "Converter.h"

Converter::Converter() {}
Converter::~Converter(){}

char *Converter::month[12] = { "January", "Fabruary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

int Converter::operator[](char* aIndex) 
{
	for (int i = 0; i < 12; i++)
		if (_stricmp(aIndex, month[i]) == 0 || _strnicmp(aIndex, month[i], 3) == 0)
			return i + 1;
	return 0;
}

char* Converter::operator[](int aIndex)
{
	if (aIndex > 0 && aIndex <= 12)
		return month[aIndex - 1];
	return "Unknown";
}