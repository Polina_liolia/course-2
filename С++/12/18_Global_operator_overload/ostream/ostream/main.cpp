#include "Point.h"
#include <iostream>
using namespace std;

istream& operator >> (istream& is, Point &p){
	int x = 0, y = 0;
	char buff[200] = {0};
	is >> x >> y >> buff;
	p.set_x(x);
	p.set_y(y);
	p.set_name(buff);

	
	return is;
}


ostream& operator << (ostream& os, Point p){
	os << p.get_x() << " " << p.get_y() << " "
	   << p.get_name() << endl;
	return os;
}

Point operator-(const Point &aP){
	return Point(-aP.get_x(), -aP.get_y(), aP.get_name());
}

Point operator+(int x, const Point &aP){
	return Point(aP.get_x() + x, aP.get_y() + x, aP.get_name());
}

Point operator+(const Point &aP, int x){
	return Point(aP.get_x() + x, aP.get_y() + x, aP.get_name());
}

void main()
{
	Point p(20,30,"asdv");
	Point p2(20,30,"jkhgkj");
	Point p3(20,30,"tyrw");
	Point p4 = - p;
	Point p5 = 7 + p;
	Point p6 = p + 7;
	cout << p;
	cout << p << p2 << p3 << p4 << p5 << p6;
	//cout << p2 << p3 << p4 << p5 << p6;
	//cout << p3 << p4 << p5 << p6;
	//cout << p4 << p5 << p6;
	//cout << p5 << p6;
	//cout << p6;
	//cout;

	cin >> p;

	cout << p;
	//cout.
	// void  << p2 << p3;
	// cout  << p2 << p3;
	// cout   << p3;
}