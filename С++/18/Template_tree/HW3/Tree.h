#pragma once
#include"Element.h"

template<typename A>
class Tree
{
	Element<A> *root;

public:

	Tree() //default constructor
	{
		root = nullptr;
	}
	
	// to add element to the tree
	void add(A val) {
		if (!root)
			root = new Element<A>(val);
		else
		{
			Element<A> *tmp = root;
			while (tmp)
			{
				if (val < tmp->get_value())
					if (tmp->get_left() == nullptr)
					{
						tmp->set_left(new Element<A>(val));
						break;
					}
					else tmp = tmp->get_left();
				else {
					if (val > tmp->get_value())
						if (tmp->get_right() == nullptr)
						{
							tmp->set_right(new Element<A>(val));
							break;
						}
						else tmp = tmp->get_right();
					else tmp = nullptr;
				}
			}
		}
	}

	//to delete element from the tree
	void del(A to_del)
	{
		del_helper(root, nullptr, to_del);
	}

	//a method-helper for del (finds where to place elements, situated below deleted element, and places them)
	void find_place(Element<A>* el_first, Element<A>* to_place)
	{
		if (el_first->get_value() > to_place->get_value())
		{
			if (!el_first->get_left())
				el_first->set_left(to_place);	//placing the element
			else
				find_place(el_first->get_left(), to_place);
		}
		else
		{
			if (!el_first->get_right())
				el_first->set_right(to_place);	//placing the element
			else
				find_place(el_first->get_right(), to_place);
		}
	}

	//a method-helper for del (finds element to delete and changes all needed links)
	void del_helper(Element<A>* el, Element<A>* parent_el, A to_del)
	{
		if (!el)
			return;
		//if root element is to delete:
		if (el->get_value() == to_del && parent_el == nullptr)
		{
			//setting right element as root (if it exsists):
			if (el->get_right())
			{
				root = el->get_right();
				if(el->get_left())
					find_place(root, el->get_left());
			}
			else if (el->get_left())//setting left element as a root (if it exsists):
			{
				root = el->get_left();
				if(el->get_right())
					find_place(root, el->get_right());
			}
			else //tree is empty
				root = nullptr; 
		}
		else if (el->get_value() == to_del)	//if element to delete is not root 
		{
			//setting right element on the place of deleted element (if it exsists):
			if (el->get_right())
			{
				if (parent_el->get_left()->get_value() == to_del)
					parent_el->set_left (el->get_right());
				else
					parent_el->set_right(el->get_right());
				if(el->get_left())
					find_place(el->get_right(), el->get_left());
			}
			else if (el->get_left())	//setting left element as a root (if it exsists):
			{
				if (parent_el->get_left()->get_value() == to_del)
					parent_el->set_left(el->get_left());
				else
					parent_el->set_right(el->get_left());
				if(el->get_right())
					find_place(el->get_left(), el->get_right());
			}
			else //it is a leaf
			{
				if (parent_el->get_left()->get_value() == to_del)
					parent_el->set_left(nullptr);
				else
					parent_el->set_right(nullptr);
			}
		}
		else	//if element was not found yet
		{
			if (el->get_right())
				del_helper(el->get_right(), el, to_del);
			if (el->get_left())
				del_helper(el->get_left(), el, to_del);
		}
	}

	//changes one element (Old) of the tree on enother one (New)
	void set(A Old, A New)
	{
		del(Old);
		add(New);
	}

	//searching for pointed element (if succeed - prints the element)
	void search(A aVal)
	{
		search_helper(root, aVal);
	}

	void search_helper(Element<A> * el, A aVal)
	{
		if (el->get_value() == aVal)
		{
			cout << el->get_value();
			return;
		}
		else
		{
			if (el->get_right())
				search_helper(el->get_right(), aVal);
			if (el->get_left())
				search_helper(el->get_left(), aVal);
		}
	}


	//returns the minimal element of the tree
	A min() {
		Element<A> *tmp = root;
		while (tmp && tmp->get_left()) {
			tmp = tmp->get_left();
		}
		return tmp ? tmp->get_value() : A();
	}

	//prints to file all elements of the tree
	void print_tree_to_file()
	{
		FILE *f = nullptr;
		print_to_file_helper(root, f, "tree");
	}

	//is used inside the method void print_tree_to_file() to print all elements to file
	void print_to_file_helper(Element<A> * el, FILE *f, char *file_name)
	{
		el->get_value().print_to_file(f, file_name);
		if (el->get_left())
			print_to_file_helper(el->get_left(), f, file_name);
		if (el->get_right())
			print_to_file_helper(el->get_right(), f, file_name);
	}

	//prints all elements of the tree
	void print()
	{
		print_helper_simmetric(root);
	}

	//is used inside the method void print() to output elements this way: min - root - max
	void print_helper_simmetric(Element<A> * el)
	{
		if (el->get_left())
			print_helper_simmetric(el->get_left());
		cout << el->get_value() << " ";
		if (el->get_right())
			print_helper_simmetric(el->get_right());
	}

	//is used inside the method void print() to output elements this way: root - min - max
	void print_helper_straight(Element<A> * el)
	{
		cout << el->get_value() << " ";
		if (el->get_left())
			print_helper_straight(el->get_left());
		if (el->get_right())
			print_helper_straight(el->get_right());
	}

	//is used inside the method void print() to output elements this way: root - max - min
	void print_helper_reversed(Element<A> * el)
	{
		cout << el->get_value() << " ";
		if (el->get_right())
			print_helper_reversed(el->get_right());
		if (el->get_left())
			print_helper_reversed(el->get_left());
	}
};


