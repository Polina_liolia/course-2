#include "MyString.h"
#include <string.h>
#include <stdlib.h>

String::String()									//1
{
	sz = 0;
	str = nullptr;
}

String::String(char *aStr)							//2.1
{
	if (aStr != nullptr)
	{
		sz = (int)strlen(aStr) + 1;
		str = new char[sz];
		strcpy_s(str, sz, aStr);
	}
	else
	{
		sz = 0;
		str = nullptr;
	}
}

String::String(const String &aString)				//2.2
{
	if (aString.str && aString.sz)
	{
		sz = aString.sz;
		str = new char[sz];
		strcpy_s(str, sz, aString.str);
	}
	else
	{
		sz = 0;
		str = nullptr;
	}
}

String::~String()
{
	delete[] str;
}

void String::set_String(char *aStr)
{
	if (str)
		delete[] str;
	if (aStr, strlen(aStr))
	{
		sz = (int)strlen(aStr) + 1;
		str = new char[sz];
		strcpy_s(str, sz, aStr);
	}
	else
	{
		sz = 0;
		str = nullptr;
	}
}

char *String::get_String()const 
{
	return str;
}

int String::get_Stringlen()const
{
	return sz;
}

String String::operator=(const String &aString)		//3
{
	if (str)
		delete[] str;
	if (aString.str, aString.sz)
	{
		sz = aString.sz;
		str = new char[sz];
		strcpy_s(str, sz, aString.str);
	}
	else
	{
		sz = 0;
		str = nullptr;
	}
	return *this;
}

String String::operator+=(const String &aString)	//4
{
	if (aString.str && aString.sz)
	{
		if (str && sz)
		{
			char *temp = new char[sz + aString.sz - 1];
			strcpy_s(temp, sz, str);
			sz += aString.sz - 1;
			strcat_s(temp, sz, aString.str);
			delete[] str;
			str = new char[sz];
			str = temp;
		}
		else
		{
			sz = aString.sz;
			str = new char[sz];
			strcpy_s(str, sz, aString.str);
		}
	}
	return *this;
}

String String::operator+=(char a)					//6
{
	if (str)
	{
		sz++;
		char *temp = new char[sz];
		strcpy_s(temp, sz, str);
		temp[sz - 2] = a;
		temp[sz - 1] = '\0';
		delete[] str;
		str = new char[sz];
		str = temp;
	}
	else
	{
		sz = 2;
		str = new char[sz];
		str[0] = a;
		str[1] = '\0';
	}
	return *this;
}

String String::operator*=(const String &aString)
{
	char *temp = nullptr;
	int sz_temp = 0;
	for (int i = 0; i < sz; i++)
		for (int i = 0; i < aString.sz; i++)
			if (str[i] == aString.str[i])
			{
				char *temp_new = new char [sz_temp + 1];
				for (int i = 0; i < sz_temp; i++)
					temp_new[i] = temp[i];
				temp_new[sz_temp] = str[i];
				delete[] temp;
				sz_temp++;
				temp = temp_new;
				break;
			}
	delete[] str;
	str = temp;
	sz = sz_temp;
	return *this;
}

String String::operator*=(char *aStr)
{
	char *temp = nullptr;
	int sz_temp = 0;
	for (int i = 0; i < sz; i++)
		for (int i = 0; i < strlen(aStr); i++)
			if (str[i] == aStr[i])
			{
				char *temp_new = new char[sz_temp + 1];
				for (int i = 0; i < sz_temp; i++)
					temp_new[i] = temp[i];
				temp_new[sz_temp] = str[i];
				delete[] temp;
				sz_temp++;
				temp = temp_new;
				break;
			}
	delete[] str;
	str = temp;
	sz = sz_temp;
	return *this;
}

String String::operator/=(const String &aString)
{
	char *temp = nullptr;
	int sz_temp = 0;
	for (int i = 0; i < sz; i++)
	{
		bool flag = false;
		for (int j = 0; j < aString.sz; j++)
			if (str[i] == aString.str[j])
			{
				flag = true;
				break;
			}
		if (!flag)		//if current symbol was not found in second String
		{
			char *temp_new = new char[sz_temp + 1];
			for (int i = 0; i < sz_temp; i++)
				temp_new[i] = temp[i];
			temp_new[sz_temp] = str[i];
			delete[] temp;
			sz_temp++;
			temp = temp_new;
		}
	}
	delete[] str;
	str = temp;
	sz = sz_temp;
	return *this;
}

String String::operator/=(char *aStr)
{
	char *temp = nullptr;
	int sz_temp = 0;
	for (int i = 0; i < sz; i++)
	{
		bool flag = false;
		for (int j = 0; j <= strlen(aStr); j++)
			if (str[i] == aStr[i])
			{
				flag = true;
				break;
			}
		if (!flag)		//if current symbol was not found in second String
		{
			char *temp_new = new char[sz_temp + 1];
			for (int i = 0; i < sz_temp; i++)
				temp_new[i] = temp[i];
			temp_new[sz_temp] = str[i];
			delete[] temp;
			sz_temp++;
			temp = temp_new;
		}
	}
	delete[] str;
	str = temp;
	sz = sz_temp;
	return *this;
}

String String::operator+(const String &aString)		//5.1
{
	if (aString.str && aString.sz)
	{
		if (str && sz)
		{
			char *temp = new char[sz + aString.sz - 1];
			strcpy_s(temp, sz, str);
			int sz_tmp = sz + aString.sz - 1;
			strcat_s(temp, sz_tmp, aString.str);
			String tmp(temp);
			return tmp;
		}
		else
			return aString;
	}
	else
		return *this;
}

String String::operator+(char *aStr)				//5.2
{
	if (aStr)
	{
		if (str && sz)
		{
			int sz_tmp = sz + (int)strlen(aStr);
			char *temp = new char[sz_tmp];
			strcpy_s(temp, sz, str);
			strcat_s(temp, sz_tmp, aStr);
			String tmp(temp);
			return tmp;
		}
		else
		{
			String tmp(aStr);
			return tmp;
		}
	}
	else
		return *this;
}


String String::operator*(const String &aString)
{
	String temp;
	for (int i = 0; i < sz; i++)
		for (int j = 0; j < aString.sz; j++)
			if (str[i] == aString.str[j])
			{
				temp += str[i];
				break;
			}
	return temp;
}

String String::operator*(char *aStr)
{
	String temp;
	for (int i = 0; i < sz; i++)
		for (int j = 0; j <= strlen (aStr); j++)
			if (str[i] == aStr[j])
			{
				temp += str[i];
				break;
			}
	return temp;
}

String String::operator/(const String &aString)
{
	String temp;
	for (int i = 0; i < sz; i++)
	{
		bool flag = false;
		for (int j = 0; j < aString.sz; j++)
			if (str[i] == aString.str[j])
			{
				flag = true;
				break;
			}
		if (!flag)		//if current symbol was not found in second String
			temp += str[i];
	}
	return temp;
}

String String::operator/(char *aStr)
{
	String temp;
	for (int i = 0; i < sz; i++)
	{
		bool flag = false;
		for (int j = 0; j <= strlen(aStr); j++)
			if (str[i] == aStr[j])
			{
				flag = true;
				break;
			}
		if (!flag)		//if current symbol was not found in second String
			temp += str[i];
	}
	return temp;
}

bool String::operator==(const String &aString)		//7.1
{
	if (!str && !aString.str)
		return false;
	if (strcmp(str, aString.str) == 0)
		return true;
	return false;
}

bool String::operator==(char *aStr)					//8.1
{
	if (!str && !aStr)
		return false;
	if (strcmp(str, aStr) == 0)
		return true;
	return false;
}

bool String::operator!=(const String &aString)		//7.2
{
	if (!str && !aString.str)
		return false;
	if (strcmp(str, aString.str) != 0)
		return true;
	return false;
}

bool String::operator!=(char *aStr)					//8.2
{
	if (!str && !aStr)
		return false;
	if (strcmp(str, aStr) != 0)
		return true;
	return false;
}

bool String::operator>(const String &aString)		//9.1
{
	if (!str && !aString.str)
		return false;
	if (strcmp(str, aString.str) > 0)
		return true;
	return false;
}

bool String::operator>(char *aStr)					//9.2
{
	if (!str && !aStr)
		return false;
	if (strcmp(str, aStr) > 0)
		return true;
	return false;
}

bool String::operator>=(const String &aString)
{
	if (!str && !aString.str)
		return false;
	if (strcmp(str, aString.str) > 0 || strcmp(str, aString.str) == 0)
		return true;
	return false;
}

bool String::operator>=(char *aStr)
{
	if (!str && !aStr)
		return false;
	if (strcmp(str, aStr) > 0 || strcmp(str, aStr) == 0)
		return true;
	return false;
}

bool String::operator<(const String &aString)		//9.3	
{
	if (!str && !aString.str)
		return false;
	if (strcmp(str, aString.str) < 0)
		return true;
	return false;
}

bool String::operator<(char *aStr)					//9.4
{
	if (!str && !aStr)
		return false;
	if (strcmp(str, aStr) < 0)
		return true;
	return false;
}

bool String::operator<=(const String &aString)
{
	if (!str && !aString.str)
		return false;
	if (strcmp(str, aString.str) < 0 || strcmp(str, aString.str) == 0)
		return true;
	return false;
}

bool String::operator<=(char *aStr)
{
	if (!str && !aStr)
		return false;
	if (strcmp(str, aStr) < 0 || strcmp(str, aStr) == 0)
		return true;
	return false;
}

void String::print()
{
	//std::cout << str << "\n";
}

char& String::operator[](int aIndex)
{
	return str[aIndex];
}

String::operator int()const		//converting String to int
{
	return atoi(str);
}

String::operator double()const		//converting String to double
{
	return atof(str);
}