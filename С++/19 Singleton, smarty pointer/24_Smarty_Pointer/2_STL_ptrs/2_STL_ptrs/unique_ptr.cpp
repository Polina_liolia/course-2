#include "SomeClass.h"
#include <memory>
#include <iostream>
using std::cout;
using std::endl;

using std::unique_ptr;


void main() {

	unique_ptr<SomeClass> ptr(new SomeClass);
	if(true){
		// �������� �������� �� ptr � ptr2
		unique_ptr<SomeClass> ptr2(std::move(ptr));
		ptr2->someClassFunction();
		/**
		���� ������ �������� �������� �� ptr2 � ptr,
		�� ptr2 ��������� ������ � ptr->someClassFunction();
		������� ������
		*/
		ptr = std::move(ptr2);// ������� �������� 
		//ptr = static_cast<unique_ptr<SomeClass> &&>(ptr2);
		ptr = ptr2;

	}
	ptr->someClassFunction();
	

}