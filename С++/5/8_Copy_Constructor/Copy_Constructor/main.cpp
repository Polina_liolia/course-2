#include <iostream>
using namespace std;


class A
{
	int *px;
	int z;
	char *name;

public:
	A(){
		z = 0;
		px = NULL;
		name = NULL;
		cout << "Constructor"<<endl;
	}

	A(int aPx){
		z = 0;
		px = new int;
		*px = aPx;
		name = NULL;
		cout << "Constructor"<<endl;
	}
	
	
	A(const A &a){

		/*
		A b=a;
		b - ��� ���. (�������)
		a - ���. � 
		
		/**/
	
		z = a.z;
		cout << "Construstor Copy" << endl;
		if(a.px) {
			px = new int;
			*px = *a.px;
		}else px = nullptr;

		if (a.name && strlen(a.name)) {
			name = new char[strlen(a.name) + 1];
			strcpy_s(name, strlen(a.name) + 1, a.name);
		}else name = nullptr;
	}
	
	~A()
	{
		
		cout << "Destructor"<<endl;
		delete px;
		delete[] name;
	}

	void show(){
		cout << "PX: " << *px << endl;
	}
};

void fun1(A x){
	// A x = a;
}
void main()
{
	int x = 10;
	int y = x;
	
	
	A a(4);
	A b = a;
	/*A b=a;//copy constructor b.A(a);
	
	b = a;// operator=
	a.show();
	b.show();

	fun1(a);//copy constructor*/
}