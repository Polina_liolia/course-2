#include "Underwather_transport.h"

Underwather_transport::Underwather_transport() : Wather_transport()	//default constructor, inicializes nrg_installation as "basic" (default)
{
	nrg_installation = athom;
}

//overloaded constructor
Underwather_transport::Underwather_transport(string aModel, double aWeight, double aMax_speed, double aDisplacement, energy_type aNrg_installation, speed aSpeed_units) :
	Wather_transport(aModel, aWeight, aMax_speed, aDisplacement, aSpeed_units)
{
	nrg_installation = aNrg_installation;
}

//method to set or change nrg_installationerty nrg_installation
void Underwather_transport::set_nrg_installation(energy_type aNrg_installation)
{
	nrg_installation = aNrg_installation;
}

//method to get meaning of nrg_installationerty nrg_installation
energy_type Underwather_transport::get_nrg_installation() const
{
	return nrg_installation;
}

//virtual method to get meaning of nrg_installationerty speed_units
speed Underwather_transport::get_speed_units() const
{
	cout << "Speed units for underwather transport - ";
	switch (speed_units)
	{
	case 0: cout << "km_h\n"; break;
	case 1: cout << "Mach\n"; break;
	case 2: cout << "miles_h\n"; break;
	case 3: cout << "knots\n"; break;
	}
	return speed_units;
}

//virtual method to print all information about Underwather_transport instance
void Underwather_transport::print()
{
	Wather_transport::print();	//calling of basic class method
	cout << "Energy installation type: ";
	switch (nrg_installation)
	{
	case 0: cout << "athom\n"; break;
	case 1: cout << "non_athom\n"; break;
	}
}