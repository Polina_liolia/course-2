#include <iostream>
using std::cout;
using std::endl;
/*
class ���{
	[����. �������:]
	�������� ������� ������
	[����. �������:]
	�������� ������� ������
}[instance1, instance2];
*/
class A{
public:
	int prop1 = 0;
	int prop2;
public:

	A(){
		prop1 = 0;
		prop2 = 0;
	}

	~A(){
		cout << "Destructor" << endl;
	}

	void set_props(){
		prop1 = 10;
		prop2 = 20;
	}


	void show(){
		cout << "prop1:" << prop1 << endl;
		cout << "prop2:" << prop2 << endl;
	}

}inst1, inst2;

void main(){
	A a1;
	a1.show();
	a1.prop1 = 23;
	a1.show();
	a1.set_props();
	a1.show();

	inst1.set_props();
	inst1.show();

	A* p1 = new A;
	p1->set_props();
	p1->show();

	delete p1;
	cout << "hello\n";
}