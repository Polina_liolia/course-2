#pragma once


class overcoat
{
private:
	char *type;
	char *color;
	float price;

public:
	overcoat(char *aType = nullptr, char *aColor = nullptr, float aPrice = 0);
	overcoat(const overcoat &aOvercoat);
	~overcoat();

public:
	void set_type(char *aType);
	void set_color(char *aColor);
	inline void set_price(float aPrice);
	inline char *get_type()const;
	inline char *get_color()const;
	inline float get_price()const;
	void print()const;
	bool operator==(const overcoat &aOvercoat)const;
	overcoat operator=(const overcoat &aOvercoat);
	bool operator>(const overcoat &aOvercoat)const;
};