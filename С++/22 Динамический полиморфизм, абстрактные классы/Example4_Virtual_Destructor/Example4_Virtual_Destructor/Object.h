#pragma once
class Object
{
	public:
		Object();
		virtual double square();
		virtual ~Object();
};