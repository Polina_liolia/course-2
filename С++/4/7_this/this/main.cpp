#include <iostream>
using namespace std;
class A{
	int x;
public:
	void show(){
		cout << this << endl;
	}
	void set_x(int x){
		this->x = x;
	}
	void show_addr(){
		cout << &x << endl;
	}
};

void main(){
	A a;
	cout << "a:" << &a << endl;
	a.show_addr();
	a.show();
	A b;
	cout << "b:" << &b << endl;
	A c;
	cout << "c:" << &c << endl;

	cout << "a:";
	a.show(); 
	cout << endl;
	cout << "b:";
	b.show(); 
	cout << endl;
	cout << "c:";
	c.show(); 
	cout << endl;
	
}