#include "Point.h"

point::point(char *aName, int aX, int aY)
{
	if (aName && strlen(aName))
	{
		name = new char[strlen(aName) + 1];
		strcpy_s(name, strlen(aName) + 1, aName);
	}
	else
		name = nullptr;
	x = aX;
	y = aY;
}

point::point(char &aName, int aX, int aY)
{
	name = &aName;
	x = aX;
	y = aY;
}

point::point(const point &aPoint)
{
	if (aPoint.name && strlen(aPoint.name))
	{
		name = new char[strlen(aPoint.name) + 1];
		strcpy_s(name, strlen(aPoint.name) + 1, aPoint.name);
	}
	else
		name = nullptr;
	x = aPoint.x;
	y = aPoint.y;
}

point::~point()
{
	delete[] name;
}

void point::set_x(int aX)
{
	x = aX;
}

void point::set_y(int aY)
{
	y = aY;
}
void point::set_name(char *aName)
{
	if (aName && strlen(aName))
	{
		name = new char[strlen(aName) + 1];
		strcpy_s(name, strlen(aName) + 1, aName);
	}
}

int point::get_x()const
{
	return x;
}

int point::get_y()const
{
	return y;
}

char *point::get_name()const
{
	return name;
}

void point::print()const
{
	std::cout << name << ": " << "x: " << x << "; y: " << y << "\n";
}

point point::operator=(const point &aPoint)
{
	if (aPoint.name && strlen(aPoint.name))
	{
		name = new char[strlen(aPoint.name) + 1];
		strcpy_s(name, strlen(aPoint.name) + 1, aPoint.name);
	}
	x = aPoint.x;
	y = aPoint.y;
	return *this;
}

point point::operator+ (int a)
{
	char number[10];			
	_itoa_s(a, number, 10);		//converting int to char array
	char *temp_name = new char[strlen(name) + strlen(number) + 1];	//creating a new name for rezulting point
	strcpy_s(temp_name, (strlen(name) + strlen(number) + 1), name);
	strcat_s(temp_name, (strlen(name) + strlen(number) + 1), number);
	point rezult(temp_name, x + a, y + a);
	return rezult;
}

point point::operator+ (float a)
{
	int a_conv = (int)a;
	char number[10];
	_itoa_s(a_conv, number, 10);		//converting int to char array
	char *temp_name = new char[strlen(name) + strlen(number) + 1];	//creating a new name for rezulting point
	strcpy_s(temp_name, (strlen(name) + strlen(number) + 1), name);
	strcat_s(temp_name, (strlen(name) + strlen(number) + 1), number);
	point rezult(temp_name, x + a_conv, y + a_conv);
	return rezult;
}
point point::operator+ (double a)
{
	int a_conv = (int)a;
	char number[20];
	_itoa_s(a_conv, number, 10);		//converting int to char array
	char *temp_name = new char[strlen(name) + strlen(number) + 1];	//creating a new name for rezulting point
	strcpy_s(temp_name, (strlen(name) + strlen(number) + 1), name);
	strcat_s(temp_name, (strlen(name) + strlen(number) + 1), number);
	point rezult(temp_name, x + a_conv, y + a_conv);
	return rezult;
}

bool point::operator==(const point &aPoint)
{
	if (x == aPoint.x && y == aPoint.y)
		return true;
	return false;
}

bool point::operator!=(const point &aPoint)
{
	if (x != aPoint.x || y != aPoint.y)
		return true;
	return false;
}

