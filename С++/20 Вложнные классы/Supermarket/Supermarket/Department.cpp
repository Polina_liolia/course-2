#include"Department.h"

Department::Department()	//default constructor
{
	size_dept_stuff = 0;
	dept_stuff = nullptr;
}

Department::Department(String aName, short aDay, short aMonth, short aYear)	//overloaded constructor
{
	dept_name = aName;
	founded.set_date(aDay, aMonth, aYear);
	edited = founded;
}

Department::Department(const Department & aDept)	//copy constructor
{
	dept_name = aDept.dept_name;
	founded = aDept.founded;
	edited = aDept.edited;
	size_dept_stuff = aDept.size_dept_stuff;
	dept_stuff = new employee[size_dept_stuff];
	for (int i = 0; i < size_dept_stuff; i++)
		dept_stuff[i] = aDept.dept_stuff[i];
}

Department::~Department()
{
	delete[] dept_stuff;	//deleting pointers, but not stuff
}

Department Department::operator=(const Department & aDept)
{
	dept_name = aDept.dept_name;
	founded = aDept.founded;
	edited = aDept.edited;
	size_dept_stuff = aDept.size_dept_stuff;
	delete[]dept_stuff;
	dept_stuff = new employee[size_dept_stuff];
	for (int i = 0; i < size_dept_stuff; i++)
		dept_stuff[i] = aDept.dept_stuff[i];
	return *this;
}

bool Department::operator<(const Department & aDept)	//compares two instances of Departments by name
{
	return dept_name < aDept.dept_name;
}

bool Department::operator>(const Department & aDept)	//compares two instances of Departments by name
{
	return dept_name > aDept.dept_name;
}

bool Department::operator<=(const Department & aDept)
{
	return  dept_name <= aDept.dept_name;
}

bool Department::operator>=(const Department & aDept)
{
	return dept_name >= aDept.dept_name;
}

//to set or change dept name
void Department::set_dept_name(String aDept_name, short aDay_edited, short aMonth_edited, short aYear_edited)
{
	dept_name = aDept_name;
	edited.set_date(aDay_edited, aMonth_edited, aYear_edited);
}

//to set or change dept foundation date 
void Department::set_date_founded(short aDay_founded, short aMonth_founded, short aYear_founded, short aDay_edited, short aMonth_edited, short aYear_edited)
{
	founded.set_date(aDay_founded, aMonth_founded, aYear_founded);
	edited.set_date(aDay_edited, aMonth_edited, aYear_edited);
}

//returns dept name
String Department::get_dept_name() const
{
	return dept_name;
}

//returns dept foundation date 
date Department::get_date_founded() const
{
	return founded;
}

//returns dept edition date 
date Department::get_date_edited() const
{
	return edited;
}

//returns size of dept stuff array 
int Department::get_size_dept_stuff() const
{
	return size_dept_stuff;
}

//returns dept stuff array 
employee * Department::get_dept_stuff() const
{
	return dept_stuff;
}

//returns employee by index
employee Department::get_employee(int index) const
{
	try
	{
		if (index < 0 || index >= size_dept_stuff)
			throw "Wrong employee index";
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
		return employee();
	}
	return dept_stuff[index];
}

//to add employee to department (in ascending abc order)
void Department::add_employee(String aSurname, String aName, position aType, String aPhone, short b_aDay, short b_aMonth, short b_aYear, short aDay_edited, short aMonth_edited, short aYear_edited)
{
	employee aEmployee(aSurname, aName, aType, aPhone, b_aDay, b_aMonth, b_aYear);
	size_dept_stuff++;
	if (size_dept_stuff > 1)
	{
		employee *buf = new employee[size_dept_stuff];
		int i = 0,
			j = 0;
		while (i < size_dept_stuff - 1 && j < size_dept_stuff && dept_stuff[i] <= aEmployee)
		{
			buf[j++] = dept_stuff[i++];
		}
		buf[j++] = aEmployee;
		while (i < size_dept_stuff - 1 && j < size_dept_stuff)
		{
			buf[j++] = dept_stuff[i++];
		}
		delete[]dept_stuff;
		dept_stuff = buf;
	}
	else
	{
		dept_stuff = new employee[size_dept_stuff];
		dept_stuff[size_dept_stuff - 1] = aEmployee;
	}
	edited.set_date(aDay_edited, aMonth_edited, aYear_edited);
}

//to fire employee from department
void Department::remove_employee(String aSurname, short aDay_edited, short aMonth_edited, short aYear_edited)
{
	int index = -1;
	for (int i = 0; i < size_dept_stuff; i++)
		if (dept_stuff[i].get_surname() == aSurname)
		{
			index = i;
			break;
		}
	try
	{
		if (index == -1)
			throw "Wrong employee index";
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
		return;
	}
	size_dept_stuff--;
	employee *buf = new employee[size_dept_stuff];
	for (int q = 0, i = 0; q < size_dept_stuff + 1; q++)
		if (q != index)
		{
			buf[i] = dept_stuff[q];
			i++;
		}
	delete[] dept_stuff;
	dept_stuff = buf;
	edited.set_date(aDay_edited, aMonth_edited, aYear_edited);
}

void Department::edit_employee_surname(int index, String aSurname)
{
	try
	{
		if (index < 0 || index >= size_dept_stuff)
			throw "Wrong employee index";
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
		return;
	}
	dept_stuff[index].set_surname(aSurname);
}

void Department::edit_employee_name(int index, String aName)
{
	try
	{
		if (index < 0 || index >= size_dept_stuff)
			throw "Wrong employee index";
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
		return;
	}
	dept_stuff[index].set_name(aName);
}

void Department::edit_employee_birth_date(int index, short aDay, short aMonth, short aYear)
{
	try
	{
		if (index < 0 || index >= size_dept_stuff)
			throw "Wrong employee index";
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
		return;
	}
	dept_stuff[index].set_birth_date(aDay, aMonth, aYear);
}

void Department::edit_employee_phone(int index, String aPhone)
{
	try
	{
		if (index < 0 || index >= size_dept_stuff)
			throw "Wrong employee index";
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
		return;
	}
	dept_stuff[index].set_phone(aPhone);
}

void Department::edit_employee_position(int index, position aType)
{
	try
	{
		if (index < 0 || index >= size_dept_stuff)
			throw "Wrong employee index";
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
		return;
	}
	dept_stuff[index].set_position(aType);
}
