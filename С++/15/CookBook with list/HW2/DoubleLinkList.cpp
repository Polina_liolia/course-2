#include <iostream>
using namespace std;
#include "DoubleLinkList.h"


DoubleLinkList::DoubleLinkList()
{
	tail = head = nullptr;
}
Element * DoubleLinkList::get_head() const
{
	return head;
}
Element * DoubleLinkList::get_taild() const
{
	return tail;
}
void DoubleLinkList::add(ingredient aValue)
{
	Element* tmp_el = new Element(aValue);
	if(head)
	{
		tail->set_next(tmp_el);
		tmp_el->set_prev(tail);
	}
	else head = tmp_el;
	tail = tmp_el;
}

void DoubleLinkList::show()const
{
		Element* tmp_el = head;
		while(tmp_el)
		{
			cout << tmp_el->get_value()<<'\n';
			tmp_el = tmp_el->get_next();
		}
}

void DoubleLinkList::show_reverse()const
{
		Element* tmp_el = tail;
		while(tmp_el)
		{
			cout << tmp_el->get_value()<<'\n';
			tmp_el = tmp_el->get_prev();
		}
}

DoubleLinkList::~DoubleLinkList()
{
		while(head)
		{
			Element* tmp_el = head;
			head = head->get_next();
			delete tmp_el;
		}
}

bool DoubleLinkList::delByIndex(unsigned int index)
{
	if(index){
		Element *prev = head;
		for(int i=0; i<index - 1 && prev; i++)
			prev = prev->get_next();
		Element* tmp = prev ? prev->get_next() : nullptr;
		if(tmp){
			prev->set_next(tmp->get_next());
			if(tmp->get_next())
				tmp->get_next()->set_prev(prev);
			if(tmp == tail)  tail = prev;
			delete tmp;
		}
	}else{
		Element* tmp = head;
		head = head->get_next();
		delete tmp;
	}
	return true;
}


bool DoubleLinkList::insertByIndex(int index, ingredient value){
	if(index > 0){
		Element *prev = head;
		for(int i=0; i<index - 1 && prev; i++)
			prev = prev->get_next();

		if(prev){
			Element* insElem = new Element(value);

			insElem->set_next(prev->get_next());
			if(prev->get_next())
				prev->get_next()->set_prev(insElem);
			
			insElem->set_prev(prev);
			prev->set_next(insElem);
			
			if(prev == tail)  tail = insElem;
		}else return false;
	}else{
		Element* insElem = new Element(value);
		insElem->set_next(head);
		head = insElem;
	}
	return true;
}

bool DoubleLinkList::del_package(int index, int number)
{
	int j = number;
	if (index) 
	{
		Element *prev = head;
		for (int i = 0; i<index - 1 && prev; i++)
			prev = prev->get_next();
		if (!prev)
		{
			cout << "Error! A wrong index\n";
			return false;
		}
		Element* tmp = new Element(0);;
		while (j)
		{
			if (j == number)
				tmp = prev->get_next();
			else
				tmp = tmp->get_next();
			j--;
			if (!tmp) break;
		}
			if (tmp) 
			{
				prev->set_next(tmp->get_next());
				if (tmp->get_next())
					tmp->get_next()->set_prev(prev);
				if (tmp == tail)  tail = prev;
				delete tmp;
			}
			else
			{
				prev->set_next(nullptr);
				tail = prev;
				delete tmp;
			}
		}
	else 
	{
		Element* tmp = new Element(0);;
		while (j--)
		{
			tmp = head;
			head = head->get_next();
			if (!head) break;
		}
		if (tmp)
		delete tmp;
	}
	return true;
}
