//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by WorkingTimeControl.rc
//
#define IDD_REG                         103
#define IDD_LOGIN                       105
#define IDD_TIMER                       110
#define IDC_REGLOGIN                    1005
#define IDC_REGPASSWORD                 1006
#define IDC_REGPASSWORDREPEAT           1007
#define IDC_REGEMAIL                    1008
#define IDC_START                       1009
#define IDC_LOGLOGIN                    1010
#define IDC_BUTTON2                     1010
#define IDC_STOP                        1010
#define IDC_LOGPASSWORD                 1011
#define IDC_HOUR                        1011
#define IDC_REGPHONE                    1012
#define IDC_MINUTE                      1012
#define IDC_SEC                         1013

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        109
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1014
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
