#pragma once
#include "Word.h"
class Element{
	Element* left;
	Element* right;
	Word value;
public:
	Element();
	~Element();
	Element(const Element &aEl);
	Element(String En, String Rus, int Counter);
	void set_value(String En, String Rus, int Counter);
	void set_translation(String Rus);
	void count();
	void set_left(Element* aLeft);
	void set_right(Element* aRight);
	Word get_value()const;
	int get_counter()const;
	String get_en()const;
	String get_rus();
	Element* get_left();
	Element* get_right();

};