#pragma once
#include<iostream>
using std::ostream;
using std::cout;
class Stack {
	const int max_size = 10;
	int sz;
	int *mas;
	friend ostream& operator << (ostream& cout, const Stack &);
public:
	Stack();
	void push(int);
	int pop();
	int capacity();
	int size();
	bool isFull();
	bool isEmpty();

};