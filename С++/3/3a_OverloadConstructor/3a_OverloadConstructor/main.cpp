#include <iostream>
using namespace std;
class A
{
	int x;
	char *name;
public:
	A() {
		x = 0;
		name = NULL;
	}
	A(int aX, char* aName) {
		x = aX;
		if (aName && strlen(aName)) {
			name = new char[strlen(aName) + 1];
			strcpy_s(name, strlen(aName) + 1, aName);
		}
		else name = NULL;
	}

	void setX(int aX) {
		x = aX;
	}
	void setName(char *aName) {
		if (aName && strlen(aName)) {
			if (name)
				delete[] name;
			name = new char[strlen(aName) + 1];
			strcpy_s(name, strlen(aName) + 1, aName);

		}
	}

	~A() {
		delete[] name;
	}
	
};


void main(){
	A x;
	x.setX(1);
	x.setName("sefghefth");
	
	A y(45, "Hello");

	//==================================

	int newX;
	cout << "Type X:";
	cin >> newX;
	char nameBuff[200] = {0};
	cout << "Type name:";
	cin >> nameBuff;

	A z(newX, nameBuff);
}