/*
Создать класс “Корзина интернет-магазина”
Класс должен уметь:
	1 добавлять товары в корзину 
(товары должны идти отдельным классом)
	2 предусмотреть возможность удаление 
товара из корзины
	3 предусмотреть возможность 
изменить количество товара в корзине
	4 реализовать метод подсчета суммы 
всей корзины
*/

function Good(aName, aAmount, aPrice)
{
	var name = aName;
	var amount = aAmount;
	var price = aPrice;

	this.name = function(aName)
	{
		if(aName == undefined)
			return name;
		else
			name = aName;
	}

	this.amount = function(aAmount)
	{
		if(aAmount == undefined)
			return amount;
		else
			amount = aAmount;
	}

	this.price = function(aPrice)
	{
		if(aPrice == undefined)
			return price;
		else
			price = aPrice;
	}
}


function Cart()
{
	 var AllGoods = [];

	this.addGood = function(good)
	{
		AllGoods.push(good);
	}

	this.removeGood = function(goodName)
	{
		for (var i = 0; i < AllGoods.length; i++)
			if (AllGoods[i].name() == goodName)
			{
				AllGoods.splice(i,1);
				return;
			}
	}

	this.changeAmount = function(goodName, newAmount)
	{
		for (var i = 0; i < AllGoods.length; i++)
			if (AllGoods[i].name() == goodName)
			{
				AllGoods[i].amount(newAmount);
				return;
			}
	}

	this.getSum = function()
	{
		var sum = 0;
		for (var i = 0; i < AllGoods.length; i++)
			sum += AllGoods[i].amount() * AllGoods[i].price();
		return sum;
	}

	this.showGoods = function ()
	{
        for (var i = 0; i < AllGoods.length; i++)
		{
			console.log(AllGoods[i].name(), AllGoods[i].amount(), AllGoods[i].price());
		}
    }
}

var Good1 = new Good("iPhone", 2, 20000);
var Good2 = new Good("apple", 5, 25);

var YourCart = new Cart();
YourCart.addGood (Good1);
YourCart.addGood (Good2);
var sum = YourCart.getSum();
console.log (sum);
YourCart.changeAmount("apple", 10);
sum = YourCart.getSum();
console.log (sum);
YourCart.showGoods();
YourCart.removeGood("apple");
YourCart.showGoods();
