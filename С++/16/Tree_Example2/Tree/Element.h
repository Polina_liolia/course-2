#pragma once
class Element{
	Element* left;
	Element* right;
	int value;
public:
	Element();
	Element(int x);
	void set_value(int aX);
	void set_left(Element* aLeft);
	void set_right(Element* aRight);
	int get_value();
	Element* get_left();
	Element* get_right();

};