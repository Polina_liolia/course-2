#pragma once
#include "object.h"

class Rectangle :	
	public Object
{
public:
	Rectangle();
	double square();	
};
