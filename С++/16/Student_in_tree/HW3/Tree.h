#pragma once
#include"Element.h"

template <typename T>
class Tree
{
	Element<T> *root;

public:

	Tree() {
		root = NULL;
	}
	// ���������� �������� � ������
	void add(T val) {
		if (!root)
			root = new Element(val);
		else
		{
			Element *tmp = root;
			while (tmp)
			{
				if (val < tmp->get_value())
					if (tmp->get_left() == NULL)
					{
						tmp->set_left(new Element(val));
						break;
					}
					else tmp = tmp->get_left();
				else {
					if (val > tmp->get_value())
						if (tmp->get_right() == NULL)
						{
							tmp->set_right(new Element(val));
							break;
						}
						else tmp = tmp->get_right();
					else tmp = NULL;
				}
			}
		}
	}

	void set(T Old, T New)
	{
		set_helper(root, Old, New);
	}

	void set_helper(Element * el, T Old, T New)
	{
		if (el->get_value() == Old)
		{
			el->get_value() = New;
			return;
		}
		else
		{
			if (el->get_right())
				set_helper(el->get_right(), Old, New);
			if (el->get_left())
				set_helper(el->get_left(), Old, New);
		}
	}

	Element *search(T aVal)
	{
		return search_helper(root, aVal);
	}

	Element *search_helper(Element * el, T aVal)
	{
		if (el->get_value() == aVal)
			return el;
		else
		{
			if (el->get_right())
				el = search_helper(el->get_right(), aVal);
			if (el->get_left())
				el = search_helper(el->get_left(), aVal);
		}
		return nullptr;
	}

	void remove(T aVal)
	{
		remove_helper(root, aVal);
	}

	void remove_helper(Element * el, T aVal);

	T min() {
		Element *tmp = root;
		while (tmp && tmp->get_left()) {
			tmp = tmp->get_left();
		}
		return tmp ? tmp->get_value() : T();
	}

	void print()
	{
		print_helper_simmetric(root);
	}


	void print_helper_simmetric(Element * el)	//min - root - max  (to print 3 unpopular words)
	{
		if (el->get_left())
			print_helper_simmetric(el->get_left());
		cout << el->get_value() << " ";
		if (el->get_right())
			print_helper_simmetric(el->get_right());
	}

	void print_helper_straight(Element * el)	//root - min - max	(to print all vocabulary)
	{
		cout << el->get_value() << " ";
		if (el->get_left())
			print_helper_straight(el->get_left());
		if (el->get_right())
			print_helper_straight(el->get_right());
	}

	void print_helper_reversed(Element * el)	//root - max - min	(to print 3 popular words)
	{
		cout << el->get_value() << " ";
		if (el->get_right())
			print_helper_reversed(el->get_right());
		if (el->get_left())
			print_helper_reversed(el->get_left());
	}
};


