#pragma once
class Exception
{
	char* message;
public:
	Exception();
	Exception(const Exception& aEx);
	Exception(char* aMessage);
	char* get_message();
	~Exception();
};