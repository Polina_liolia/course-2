#include "Points_dinamic_array.h"
using std::cout;

Points_dinamic_array::Points_dinamic_array()
{
	arr = nullptr;
	sz = 0;
}

Points_dinamic_array::Points_dinamic_array(const Points_dinamic_array &A)
{
	if (A.sz > 0)
	{
		sz = A.sz;
		arr = new point[sz];
		for (int i = 0; i < sz; i++)
			arr[i] = A.arr[i];
	}
}

Points_dinamic_array::~Points_dinamic_array()
{
	delete[] arr;
}

Points_dinamic_array Points_dinamic_array::operator= (const Points_dinamic_array &A)
{
	if (A.sz > 0)
	{
		sz = A.sz;
		if (arr)
			delete[] arr;
		arr = new point[sz];
		for (int i = 0; i < sz; i++)
			arr[i] = A.arr[i];
	}
	return *this;
}

Points_dinamic_array Points_dinamic_array::operator+ (const Points_dinamic_array &A)
{
	Points_dinamic_array rezult;
	rezult.sz = sz + A.sz;
	rezult.arr = new point[rezult.sz];
	for (int i = 0, j = 0; i < rezult.sz; i++)
	{
		if (i < sz)
			rezult.arr[i] = arr[i];
		else
		{
			rezult.arr[i] = A.arr[j];
			j++;
		}
	}
	return rezult;
}

Points_dinamic_array Points_dinamic_array::operator+= (const point &A)
{
	point *temp = new point [sz + 1];
	temp[sz] = A;
	if (sz > 0)
	{
		for (int i = 0; i < sz; i++)
			temp[i] = arr[i];
		delete[] arr;
	}
	sz++;
	arr = temp;
	return *this;
}

Points_dinamic_array Points_dinamic_array::operator+= (const Points_dinamic_array &A)
{
	point *temp = new point [sz + A.sz];
	if (sz > 0)
	{
		for (int i = 0, j = 0; i < (sz + A.sz); i++)
		{
			if (i < sz)
				temp[i] = arr[i];
			else
			{
				temp[i] = A.arr[j];
				j++;
			}
		}
		delete[] arr;
	}
	sz += A.sz;
	arr = temp;
	return *this;
}

bool Points_dinamic_array::operator== (const Points_dinamic_array &A)
{
	if (sz != A.sz)
		return false;
	bool *flag1 = new bool[sz];		//boolean arrays to indicate, if the current element of points array has been already used 
	bool *flag2 = new bool[A.sz];
	for (int i = 0; i < sz; i++)	//initializing boolean arrays with false meaning
	{
		flag1[i] = false;
		flag2[i] = false;
	}
	for (int i = 0; i < sz; i++)
	{
		bool flag = false;			//to indicate if two same points were found in arrays
		for (int j = 0; j < A.sz; j++)
			if (arr[i] == A.arr[j] && flag1[i] == false && flag2[i] == false)	//if two same points were found in arrays
			{
				flag = true;
				flag1[i] = true;
				flag2[j] = true;
			}
		if (!flag)			//if two same points were not found in arrays
			return false;
	}		
	return true;
}

void Points_dinamic_array::print()
{
	for (int i = 0; i < sz; i++)
		arr[i].print();
}