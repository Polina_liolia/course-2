#pragma once
#include <iostream>
#include <string>
using std::string;
using std::cout;

//abstract basic class

enum speed { km_h, Mach, miles_h, knots};

class Transport
{
protected:
	string model;
	double weight;
	double max_speed;
	speed speed_units;

public:
	Transport();
	Transport(string aModel, double aWeight, double aMax_speed, speed aSpeed_units);
	~Transport() {}

public:
	void set_model(string aModel);
	void set_weight(double aWeight);
	void set_max_speed(double aMax_speed);
	void set_speed_units(speed aSpeed_units);

	string get_model()const;
	double get_wieght()const;
	double get_max_speed()const;

	virtual speed get_speed_units()const;
	virtual void print();
};
