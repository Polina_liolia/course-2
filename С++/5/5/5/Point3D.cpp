#include "Point3D.h"

point3D::point3D()
{
	x = 0;
	y = 0;
	z = 0;
	name = nullptr;
}

point3D::point3D(float aX, float aY, float aZ, char *aName)
{
	x = aX;
	y = aY;
	z = aZ;
	if (aName && strlen(aName))
	{
		name = new char[strlen(aName) + 1];
		strcpy_s(name, strlen(aName) + 1, aName);
	}
	else
		name = nullptr;
}

point3D::point3D(const point3D &a)
{
	x = a.x;
	y = a.y;
	z = a.z;
	if (a.name && strlen(a.name))
	{
		name = new char[strlen(a.name) + 1];
		strcpy_s(name, strlen(a.name) + 1, a.name);
	}
	else
		name = nullptr;
}

point3D::~point3D()
{
	delete[] name;
}

	inline void point3D::set_coordinateX(float Ix)		//������ �������� �
	{
		x = Ix;
	}

	inline void point3D::set_coordinateY(float Iy)		//������ �������� �
	{
		y = Iy;
	}

	inline void  point3D::set_coordinateZ(float Iz)    //������ �������� z
	{
		z = Iz;
	}

	inline void point3D::set_name(char *Iname)			//������ ��� �����
	{
		strcpy_s(name, 100, Iname);
	}

	inline void point3D::print_name()					//����������� ��� �����
	{
		cout << name << ":\n";
	}

	inline void point3D::print_coordinate()				//����������� ����������
	{
		cout << "x: " << x << "; y: " << y << "; z: " << z << "\n";
	}
