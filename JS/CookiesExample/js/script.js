var messDiv = null;
window.onload = function(){

    document.getElementById("send").addEventListener("click",addCookies, false);
    document.getElementById("clearCookies").addEventListener("click",clearCookiesNames, false);
    document.getElementById("showCookies").addEventListener("click",showCookies, false);
/*    //checking cookies
	var login = getCookie("login");
	var password = getCookie("password");
	messDiv = document.getElementById("message");
	if(login == undefined && password == undefined){
		messDiv.innerHTML = "cookies is undefined";
		
	}else{
		messDiv.innerHTML = login + ":" + password; 
	}*/

}

function addCookies(){
	var cname = document.getElementById("cname")
	var cvalue = document.getElementById("cvalue");

	if(cname == "" || cvalue == ""){
	    console.log("fields is empty");
	    return;
    }
	var cookies_str = getCookie("cookies");
	var cookies_arr = cookies_str == undefined ? [] : cookies_str.split(',');
    cookies_arr.push(cname.value);
    cookies_str = cookies_arr.join(",");
    setCookie("cookies", cookies_str, {expires:3600*24*30});
    setCookie(cname.value, cvalue.value, {expires:3600*24*30});

    console.log("!!"+document.cookie+"!!");
}

function clearCookiesNames() {
    var cookies_str = getCookie("cookies");
    var cookies_arr = cookies_str.split(',');
    for(var i = 0; i < cookies_arr.length; i++) {
        deleteCookie(cookies_arr[i]);
    }

    deleteCookie("cookies");
}

function showCookies() {
    var cookies_str = getCookie("cookies");
    var cookies_arr = cookies_str.split(',');
    var messDiv = document.getElementById("message");
    for(var i = 0; i < cookies_arr.length; i++) {
        messDiv.innerHTML += cookies_arr[i]+" = "+getCookie(cookies_arr[i])+"<br>";

    }
}