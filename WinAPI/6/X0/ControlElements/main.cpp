// ���� WINDOWS.H �������� �����������, �������, � ��������� ������� ������������ ��� ��������� ���������� ��� Windows. 
#include <windows.h>
#include <tchar.h>
#include "resource.h"
#include <ctime>


// �������� ������� ���������
LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wp, LPARAM lp);
#define OPENDIALOGID 300 //constant identifier of button created

TCHAR szClassWindow[] = TEXT("��������� ����������");	/* ��� ������ ���� */

INT WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow)
{
	HWND hWnd;
	MSG Msg;
	WNDCLASSEX wcl;


	// 1. ����������� ������ ����
	wcl.cbSize = sizeof(wcl);	// ������ ��������� WNDCLASSE
								//������������ �� ����, ���� ������� ������ �� ����������� ��� �� ���������:
	wcl.style = CS_HREDRAW | CS_VREDRAW;	// CS (Class Style) - ����� ������ ����
	wcl.lpfnWndProc = WindowProc;	// ����� ������� ���������
	wcl.cbClsExtra = 0;		// ������������ Windows 
	wcl.cbWndExtra = 0; 	// ������������ Windows 
	wcl.hInstance = hInst;	// ���������� ������� ����������
	wcl.hIcon = LoadIcon(NULL, IDI_APPLICATION);	// �������� ����������� ������
	wcl.hCursor = LoadCursor(NULL, IDC_ARROW);	// �������� ������������ �������	
	wcl.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	// ���������� ���� ����� ������
	wcl.lpszMenuName = NULL;	// ���������� �� �������� ����
	wcl.lpszClassName = szClassWindow;	// ��� ������ ����
	wcl.hIconSm = NULL;	// ���������� ��������� ������ ��� ����� � ������� ����

						// 2. ����������� ������ ����
	if (!RegisterClassEx(&wcl))
		return 0; // ��� ��������� ����������� - �����

				  // 3. �������� ����
				  // ��������� ���� �  ���������� hWnd ������������� ���������� ����
	hWnd = CreateWindowEx(
		0,		// ����������� ����� ����
		szClassWindow,	//��� ������ ����
		TEXT("������ Windows ����������"), // ��������� ����
		WS_OVERLAPPEDWINDOW,				// ����� ����
											/* ���������, �����, ����������� ������ �������, ��������� ����, ������ ������������ � ���������� ����  */
		CW_USEDEFAULT,	// �-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// y-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// ������ ����
		CW_USEDEFAULT,	// ������ ����
		NULL,			// ���������� ������������� ����
		NULL,			// ���������� ���� ����
		hInst,		// ������������� ����������, ���������� ����
		NULL);		// ��������� �� ������� ������ ����������

	HWND b1 = CreateWindow(L"button", L"Open Dialog", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10, 10, 100, 30, hWnd, (HMENU)OPENDIALOGID, hInst, NULL);

	// 4. ����������� ����
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd); // ����������� ����



						// 5. ������ ����� ��������� ���������
	while (GetMessage(&Msg, NULL, 0, 0)) // ��������� ���������� ��������� �� ������� ���������
	{
		TranslateMessage(&Msg);	// ���������� ���������
		DispatchMessage(&Msg);	// ��������������� ���������
	}
	return (int)Msg.wParam;
}


LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	switch (uMessage)
	{
	case WM_DESTROY: // ��������� � ���������� ���������
		PostQuitMessage(0); // ������� ��������� WM_QUIT
		break;
	case WM_COMMAND:
	{
		if (HIWORD(wParam) == BN_CLICKED) //control-defined notification code
			switch (LOWORD(wParam)) //control identifier
			{
			case OPENDIALOGID:
			{
				HWND dialog = CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_DIALOG1), hWnd, DlgProc); //unmodal dialog 
				ShowWindow(dialog, SW_SHOW);//show unmodal dialog
			}
			break;
			}
	}
	break;
	default:
		// ��� ���������, ������� �� �������������� � ������ ������� ������� ������������ ������� Windows �� ��������� �� ���������
		return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}
	return 0;
}

int buttonsID[9] = { IDC_BUTTON1, IDC_BUTTON2, IDC_BUTTON3, IDC_BUTTON4, IDC_BUTTON5, IDC_BUTTON6, IDC_BUTTON7, IDC_BUTTON8, IDC_BUTTON9 };
HWND buttonsHWND[9];

bool isFilled()
{
	bool flag = false;
	for (int i = 0; i < 9; i++)
	{
		flag = IsWindowEnabled(buttonsHWND[i]);
		if (flag)
			return false;
	}
	return true;
}

int end_game() {
	TCHAR b1[10], b2[10], b3[10];
	GetWindowText(buttonsHWND[0], b1, 10);
	GetWindowText(buttonsHWND[1], b2, 10);
	GetWindowText(buttonsHWND[2], b3, 10);
	if (!lstrcmp(b1, TEXT("X")) && !lstrcmp(b2, TEXT("X")) && !lstrcmp(b3, TEXT("X")))
		return 1;
	if (!lstrcmp(b1, TEXT("0")) && !lstrcmp(b2, TEXT("0")) && !lstrcmp(b3, TEXT("0")))
		return -1;

	GetWindowText(buttonsHWND[3], b1, 10);
	GetWindowText(buttonsHWND[4], b2, 10);
	GetWindowText(buttonsHWND[5], b3, 10);
	if (!lstrcmp(b1, TEXT("X")) && !lstrcmp(b2, TEXT("X")) && !lstrcmp(b3, TEXT("X")))
		return 1;
	if (!lstrcmp(b1, TEXT("0")) && !lstrcmp(b2, TEXT("0")) && !lstrcmp(b3, TEXT("0")))
		return -1;

	GetWindowText(buttonsHWND[6], b1, 10);
	GetWindowText(buttonsHWND[7], b2, 10);
	GetWindowText(buttonsHWND[8], b3, 10);
	if (!lstrcmp(b1, TEXT("X")) && !lstrcmp(b2, TEXT("X")) && !lstrcmp(b3, TEXT("X")))
		return 1;
	if (!lstrcmp(b1, TEXT("0")) && !lstrcmp(b2, TEXT("0")) && !lstrcmp(b3, TEXT("0")))
		return -1;

	GetWindowText(buttonsHWND[0], b1, 10);
	GetWindowText(buttonsHWND[3], b2, 10);
	GetWindowText(buttonsHWND[6], b3, 10);
	if (!lstrcmp(b1, TEXT("X")) && !lstrcmp(b2, TEXT("X")) && !lstrcmp(b3, TEXT("X")))
		return 1;
	if (!lstrcmp(b1, TEXT("0")) && !lstrcmp(b2, TEXT("0")) && !lstrcmp(b3, TEXT("0")))
		return -1;

	GetWindowText(buttonsHWND[1], b1, 10);
	GetWindowText(buttonsHWND[4], b2, 10);
	GetWindowText(buttonsHWND[7], b3, 10);
	if (!lstrcmp(b1, TEXT("X")) && !lstrcmp(b2, TEXT("X")) && !lstrcmp(b3, TEXT("X")))
		return 1;
	if (!lstrcmp(b1, TEXT("0")) && !lstrcmp(b2, TEXT("0")) && !lstrcmp(b3, TEXT("0")))
		return -1;

	GetWindowText(buttonsHWND[2], b1, 10);
	GetWindowText(buttonsHWND[5], b2, 10);
	GetWindowText(buttonsHWND[8], b3, 10);
	if (!lstrcmp(b1, TEXT("X")) && !lstrcmp(b2, TEXT("X")) && !lstrcmp(b3, TEXT("X")))
		return 1;
	if (!lstrcmp(b1, TEXT("0")) && !lstrcmp(b2, TEXT("0")) && !lstrcmp(b3, TEXT("0")))
		return -1;

	GetWindowText(buttonsHWND[0], b1, 10);
	GetWindowText(buttonsHWND[4], b2, 10);
	GetWindowText(buttonsHWND[8], b3, 10);
	if (!lstrcmp(b1, TEXT("X")) && !lstrcmp(b2, TEXT("X")) && !lstrcmp(b3, TEXT("X")))
		return 1;
	if (!lstrcmp(b1, TEXT("0")) && !lstrcmp(b2, TEXT("0")) && !lstrcmp(b3, TEXT("0")))
		return -1;

	GetWindowText(buttonsHWND[2], b1, 10);
	GetWindowText(buttonsHWND[4], b2, 10);
	GetWindowText(buttonsHWND[6], b3, 10);
	if (!lstrcmp(b1, TEXT("X")) && !lstrcmp(b2, TEXT("X")) && !lstrcmp(b3, TEXT("X")))
		return 1;
	if (!lstrcmp(b1, TEXT("0")) && !lstrcmp(b2, TEXT("0")) && !lstrcmp(b3, TEXT("0")))
		return -1;

	return 0;
}
INT_PTR CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)//��������� ������� � ���������� ����
{
	switch (message)
	{
	case WM_INITDIALOG:
	{
		for (int i = 0; i < 9; i++)
			buttonsHWND[i] = GetDlgItem(hWnd, buttonsID[i]);
	}
	break;
	case WM_CLOSE:
		EndDialog(hWnd, 0); // ��������� ������
		return TRUE;
	case WM_COMMAND:
		if (HIWORD(wParam) == BN_CLICKED) //������� ������ �������
			switch (LOWORD(wParam)) //control identifier
			{
			case IDOK:
				EndDialog(hWnd, 0); //��������� ������
				return TRUE;
			case IDCANCEL:
				EndDialog(hWnd, 0); //��������� ������
				return FALSE;
			case IDC_BUTTON1: case IDC_BUTTON2: case IDC_BUTTON3: case IDC_BUTTON4:case IDC_BUTTON5:
				case IDC_BUTTON6: case IDC_BUTTON7: case IDC_BUTTON8: case IDC_BUTTON9:
				{
					HWND currentButton = GetDlgItem(hWnd, LOWORD(wParam));
					TCHAR text[10];
					GetWindowText(currentButton, text, 10);
					if (!lstrcmp(text, TEXT("?")))
					{
						SetWindowText(currentButton, TEXT("X"));
						EnableWindow(currentButton, FALSE);
						int result = end_game();
						if (result == 1)
						{
							MessageBox(hWnd, _TEXT("Congrats!"), _TEXT("You win!"), MB_OK);
							EndDialog(hWnd, true);
							break;
						}
						if (result == 1)
						{
							MessageBox(hWnd, _TEXT("Oops!"), _TEXT("You loose!"), MB_OK);
							EndDialog(hWnd, false);
							break;
						}
						srand(time(NULL));
						bool flag = false;
						int win_num;
						while (!flag)
						{
							win_num = rand() % (8 - 0 + 1);
							flag = IsWindowEnabled(buttonsHWND[win_num]);
							if (isFilled())
								break;
						}
						SetWindowText(buttonsHWND[win_num], TEXT("0"));
						EnableWindow(buttonsHWND[win_num], FALSE);
						if (result == 1)
						{
							MessageBox(hWnd, _TEXT("Congrats!"), _TEXT("You win!"), MB_OK);
							break;
						}
						if (result == 1)
						{
							MessageBox(hWnd, _TEXT("Oops!"), _TEXT("You loose!"), MB_OK);
							break;
						}
					}
				}
			}
		break;
	}
	return FALSE;
}