#pragma once
template<typename A>
class Element{
	Element* left;
	Element* right;
	A value;

public:
	Element() {
		left = right = NULL;
	}
	Element(A x) {
		right = left = NULL;
		value = x;
	}
	void set_value(A aX) {
		value = aX;
	}
	void set_left(Element* aLeft) {
		left = aLeft;
	}
	void set_right(Element* aRight) {
		right = aRight;
	}
	A get_value() {
		return value;
	}
	Element* get_left()
	{
		return left;
	}
	Element* get_right()
	{
		return right;
	}

	int print_to_file(FILE *f, char* file_name)const;
};

template<typename A>
inline int Element<A>::print_to_file(FILE * f, char * file_name) const
{
	return value.print_to_file(f, file_name);
}
