#include <iostream>
#include "CookBook.h"


void main()
{
	CookBook menu;
	FILE *f=nullptr;

	menu.add_recipe("Honey Glazed Chicken", "Hot", "Heat olive oil in a skillet over medium heat; cook and stir chicken in hot oil until lightly brown, about 5 minutes. Pour honey mixture into skillet and continue to cook and stir until chicken is no longer pink in the center and sauce is thickened, about 5 minutes more.");
	menu.add_ingridient("honey", 100, 1);
	menu.add_ingridient("soy sauce", 20, 1);
	menu.add_ingridient("red pepper flakes", 5, 1);
	menu.add_ingridient("chicken", 1000, 1);
	menu.add_recipe("Fluffy Pancakes", "Hot", "Combine milk with vinegar in a medium bowl and set aside for 5 minutes to \"sour\". Combine flour, sugar, baking powder, baking soda, and salt in a large mixing bowl.Whisk egg and butter into \"soured\" milk.Pour the flour mixture into the wet ingredients and whisk until lumps are gone. Heat a large skillet over medium heat, and coat with cooking spray.Pour 1 / 4 cupfuls of batter onto the skillet, and cook until bubbles appear on the surface.Flip with a spatula, and cook until browned on the other side.");
	menu.print_recipes_list();
	menu.save_to_file(f, "Cook book");
	menu.print_recipe_information(1);
	menu.change_recipe_name (1, "Honey Glazed Chicken 1");
	menu.change_recipe_type (1, "Cold");
	menu.change_recipe_process(1, "Just cook it.");
	menu.change_ingridient_item(1, 4, "skinless, boneless chicken breast halves, cut into bite-size pieces", 1500);
	menu.print_recipe_information(1);
	menu.del_recipe_ingridient(1, 1);
	menu.print_recipe_ingridients(1);
	menu.del_recipe(1);
	menu.print_recipes_list();

				


}