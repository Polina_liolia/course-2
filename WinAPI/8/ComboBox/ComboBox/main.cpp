// ���� WINDOWS.H �������� �����������, �������, � ��������� ������� ������������ ��� ��������� ���������� ��� Windows. 
#include <windows.h>
#include <tchar.h>
#include "resource.h"
#include <Strsafe.h>
#include <ctime>
#include <vector>
using std::vector;


// �������� ������� ���������
LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wp, LPARAM lp);
#define OPENDIALOGID 300 //constant identifier of button created

TCHAR szClassWindow[] = TEXT("��������� ����������");	/* ��� ������ ���� */

INT WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow)
{
	HWND hWnd;
	MSG Msg;
	WNDCLASSEX wcl;


	// 1. ����������� ������ ����
	wcl.cbSize = sizeof(wcl);	// ������ ��������� WNDCLASSE
								//������������ �� ����, ���� ������� ������ �� ����������� ��� �� ���������:
	wcl.style = CS_HREDRAW | CS_VREDRAW;	// CS (Class Style) - ����� ������ ����
	wcl.lpfnWndProc = WindowProc;	// ����� ������� ���������
	wcl.cbClsExtra = 0;		// ������������ Windows 
	wcl.cbWndExtra = 0; 	// ������������ Windows 
	wcl.hInstance = hInst;	// ���������� ������� ����������
	wcl.hIcon = LoadIcon(NULL, IDI_APPLICATION);	// �������� ����������� ������
	wcl.hCursor = LoadCursor(NULL, IDC_ARROW);	// �������� ������������ �������	
	wcl.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	// ���������� ���� ����� ������
	wcl.lpszMenuName = NULL;	// ���������� �� �������� ����
	wcl.lpszClassName = szClassWindow;	// ��� ������ ����
	wcl.hIconSm = NULL;	// ���������� ��������� ������ ��� ����� � ������� ����

						// 2. ����������� ������ ����
	if (!RegisterClassEx(&wcl))
		return 0; // ��� ��������� ����������� - �����

				  // 3. �������� ����
				  // ��������� ���� �  ���������� hWnd ������������� ���������� ����
	hWnd = CreateWindowEx(
		0,		// ����������� ����� ����
		szClassWindow,	//��� ������ ����
		TEXT("������ Windows ����������"), // ��������� ����
		WS_OVERLAPPEDWINDOW,				// ����� ����
											/* ���������, �����, ����������� ������ �������, ��������� ����, ������ ������������ � ���������� ����  */
		CW_USEDEFAULT,	// �-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// y-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// ������ ����
		CW_USEDEFAULT,	// ������ ����
		NULL,			// ���������� ������������� ����
		NULL,			// ���������� ���� ����
		hInst,		// ������������� ����������, ���������� ����
		NULL);		// ��������� �� ������� ������ ����������

	HWND b1 = CreateWindow(L"button", L"Open Dialog", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10, 10, 100, 30, hWnd, (HMENU)OPENDIALOGID, hInst, NULL);

	// 4. ����������� ����
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd); // ����������� ����



						// 5. ������ ����� ��������� ���������
	while (GetMessage(&Msg, NULL, 0, 0)) // ��������� ���������� ��������� �� ������� ���������
	{
		TranslateMessage(&Msg);	// ���������� ���������
		DispatchMessage(&Msg);	// ��������������� ���������
	}
	return (int)Msg.wParam;
}


LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	switch (uMessage)
	{
	case WM_DESTROY: // ��������� � ���������� ���������
		PostQuitMessage(0); // ������� ��������� WM_QUIT
		break;
	case WM_COMMAND:
	{
		if (HIWORD(wParam) == BN_CLICKED) //control-defined notification code
			switch (LOWORD(wParam)) //control identifier
			{
			case OPENDIALOGID:
			{
				HWND dialog = CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_DIALOG1), hWnd, DlgProc); //unmodal dialog 
				ShowWindow(dialog, SW_SHOW);//show unmodal dialog
			}
			break;
			}
	}
	break;
	default:
		// ��� ���������, ������� �� �������������� � ������ ������� ������� ������������ ������� Windows �� ��������� �� ���������
		return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}
	return 0;
}

HWND numbers_list;
HWND result_static;
vector<int> rand_numbers;
int *p;
INT_PTR CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)//��������� ������� � ���������� ����
{
	switch (message)
	{
	case WM_INITDIALOG:
	{
		//clearing default static text:
		result_static = GetDlgItem(hWnd, IDC_RESULT);
		SetWindowText(result_static, TEXT(""));

		srand(time(NULL));
		numbers_list = GetDlgItem(hWnd, IDC_NUMBERSLIST);
		
		int const arraysize = 50;
		TCHAR number[arraysize];
		size_t sz = arraysize * sizeof(TCHAR);
		LPCTSTR pszFormat = TEXT("%d");
		
		for (int i = 0; i < 10; i++)
		{
			rand_numbers.push_back(-10 + rand() % (10 - (-10)));
			StringCbPrintf(number, sz, pszFormat, rand_numbers[i]);
			SendMessage(numbers_list, LB_ADDSTRING, 0, LPARAM(number));
		}
		
	}
	break;
	case WM_CLOSE:
		EndDialog(hWnd, 0); // ��������� ������
		return TRUE;
	case WM_COMMAND:
		if (HIWORD(wParam) == BN_CLICKED) //������� ������ �������
			switch (LOWORD(wParam)) //control identifier
			{
			case IDOK:
				EndDialog(hWnd, 0); //��������� ������
				return TRUE;
			case IDCANCEL:
				EndDialog(hWnd, 0); //��������� ������
				return FALSE;
			case IDC_BUTTONSTART:
				// ��������� ���������� ��������� ��������� � ������ � ������������� ������� 
				int nCount = SendMessage(numbers_list, LB_GETSELCOUNT, 0, 0);
				// ������� ������ ������������ ������� ��� �������� �������� ��������� ��������� ������ 
				p = new int[nCount];
				// �������� ������������ ������ ��������� ���������� ��������� ������
				SendMessage(numbers_list, LB_GETSELITEMS, nCount, LPARAM(p));

				int const arraysize = 50;
				TCHAR number[arraysize];
				size_t sz = arraysize * sizeof(TCHAR);
				LPCTSTR pszFormat = TEXT("%d");
				// ����������� ������ ��������������
				LRESULT result = SendDlgItemMessage(hWnd, IDC_RADIOSUM, BM_GETCHECK, 0, 0);
				if (result == BST_CHECKED)	//sum
				{
					int sum = 0;
					for (int i = 0; i < nCount; i++)
						sum += rand_numbers[p[i]];
					StringCbPrintf(number, sz, pszFormat, sum);
					SetWindowText(result_static, number);
				}
				else
				{
					result = SendDlgItemMessage(hWnd, IDC_RADIOMULT, BM_GETCHECK, 0, 0);
					if (result == BST_CHECKED) //mult
					{
						int mult = 1;
						for (int i = 0; i < nCount; i++)
							mult *= rand_numbers[p[i]];
						StringCbPrintf(number, sz, pszFormat, mult);
						SetWindowText(result_static, number);
					}
					else
					{
						result = SendDlgItemMessage(hWnd, IDC_RADIOARITHM, BM_GETCHECK, 0, 0);
						if (result == BST_CHECKED) //Arithm
						{
							int sum = 0.0;
							for (int i = 0; i < nCount; i++)
								sum += rand_numbers[p[i]];
							float arithm = sum / nCount;
							pszFormat = TEXT("%f");
							StringCbPrintf(number, sz, pszFormat, arithm);
							SetWindowText(result_static, number);
						}
					}
				}
			}
		break;
	}
	return FALSE;
}