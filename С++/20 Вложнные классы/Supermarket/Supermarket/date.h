#pragma once
#include <iostream>
using std::ostream;
class date
{
private:
	short day;
	short month;
	short year;

	friend class inquiry;
	friend class employee;
	friend class stuff;
	friend class director;
	friend class manager;
	
	// friend ostream & operator << (ostream& os, const String &aString); //����� ������ String - ����� � ����� ������ String
	friend ostream& operator << (ostream& os, date &aDate);

public:
	
	date()	//default constructor
	{
		day = 0;
		month = 0;
		year = 0;
	}

	date(short aDay, short aMonth, short aYear)	//overloaded constructor
	{
		day = aDay;
		month = aMonth;
		year = aYear;
	}

	date(const date &BD)	//copy constructor
	{
		day = BD.day;
		month = BD.month;
		year = BD.year;
	}

	~date() {}		//destructor

	// ���������� ��������� ������������
	// ��������� ������ �� ��������� ������ date
	// ���������� ������� ��������� ������ date
	date& operator= (const date &aDate)	
	{
		day = aDate.day;
		month = aDate.month;
		year = aDate.year;
		return *this;
	}

	short get_day()const;
	short get_month()const;
	short get_year()const;

	void set_day(short aDay);
	void set_month(short aMonth);
	void set_year(short aYear);
	void set_date(short aDay, short aMonth, short aYear);
};

