#include <iostream>
#include <cstring>
using std::cout;
using std::cin;
#include "recipe.h"
#include "CookBook.h"

CookBook::CookBook()
	{
		sz_ar = 0;
		ar = nullptr;
	}


CookBook::CookBook(const CookBook &temp)
{
	sz_ar = temp.sz_ar;
	if (temp.ar)
	{
		ar = new recipe*[sz_ar];
		for (int i = 0; i < sz_ar; i++)
			ar[i] = new recipe (*temp.ar[i]);
	}
	else
		ar = nullptr;
}

CookBook::~CookBook()
	{
		delete[] ar;
	}
	

	void CookBook::add_recipe(char *aName, char *aType, char *aProcess)		//to add a recipe
	{
		recipe **temp = new recipe*[(sz_ar + 1)];
		for (int i = 0; i < sz_ar; i++)
			temp[i] = ar[i];
		temp[sz_ar] = new recipe (aName, aType, aProcess);
		delete[] ar;
		sz_ar++;
		ar = temp;
	}

	void CookBook::change_recipe_name(int recipe_number, char *aName)			//to edit a recipe name
	{
		ar[recipe_number - 1][0].set_name(aName);
	}

	void CookBook::change_recipe_type(int recipe_number, char *aType)			//to edit a recipe type
	{
		ar[recipe_number - 1][0].set_type(aType);
	}

	void CookBook::change_recipe_process(int recipe_number, char *aProcess)	//to edit a recipe process
	{
		ar[recipe_number - 1][0].set_process(aProcess);
	}


	void CookBook::del_recipe(int recipe_number)								//to delete a recipe
	{
		recipe **temp = new recipe*[sz_ar -1];
		for (int i = 0, j = 0; i < sz_ar; i++)
		{
			if (i == recipe_number - 1)
				continue;
			temp[j] = ar[i];
			j++;
		}
		delete[] ar;
		sz_ar--;
		ar = temp;
	}

	void CookBook::add_ingridient(char *aItem, float aDose, int recipe_number)	//to add an ingridient to a pointed recipe
	{
		ar[recipe_number - 1][0].set_ingridient(aItem, aDose);
	}

	void CookBook::change_ingridient_item(int recipe_number, int ingridient_number, char* aItem, float aDose)	//to change pointed ingridient of a recipe
	{
		del_recipe_ingridient(recipe_number, ingridient_number);
		add_ingridient(aItem, aDose, recipe_number);
	}

	void CookBook::del_recipe_ingridient(int recipe_number, int ingridient_number)	//to delete pointed ingridient of a pointed recipe
	{
		ar[recipe_number - 1][0].del_ingridient(ingridient_number - 1);
	}

	int CookBook::save_to_file(char *file_name)const		//to write all data to file
	{
		std::ofstream F(file_name, std::ios::binary);
		// checking if file was successfully opened, othervice generating exception:
		if (!F)
			throw "Can't open file for reading and writing - it must exist";
		//to write a total number of recipes
		int sz = sz_ar;
		F.write(reinterpret_cast<char*>(&sz), sizeof(sz));
		//writing an information to file:
		try
		{
			for (int i = 0; i < sz_ar; i++)
				ar[i][0].save_recipe_to_file(F);
		}
		catch (char *msg)
		{
			cout << msg << "\n";
		}
		F.close();
		return 0;
	}

	int CookBook::read_from_file(char *file_name)		//to read all data from file
	{
		std::ifstream F(file_name, std::ios::binary);
		// checking if file was successfully opened, othervice generating exception:
		if (!F)
			throw "Can't open file for reading and writing - it must exist";
		//to read a total number of recipes
		F.read(reinterpret_cast<char*>(&sz_ar), sizeof(int));
		ar = new recipe*[sz_ar];
		for (int i = 0; i < sz_ar; i++)
			ar[i] = new recipe;
		//reading an information to file:
		try
		{
			for (int i = 0; i < sz_ar; i++)
				ar[i][0].read_recipe_from_file(F);
		}
		catch (char *msg)
		{
			cout << msg << "\n";
		}
		F.close();
		return 0;
	}

	void CookBook::print_recipes_list()const							//to show the list of recipes
	{
		if (sz_ar == 0)
		{
			cout << "No recipes to show.\n";
			return;
		}
		for (int i = 0; i < sz_ar; i++)
		{
			cout << (i+1) << ". ";
			ar[i][0].print_recipe_name();
		}
	}

	void CookBook::print_recipe_information(int recipe_number)const
	{
		ar[recipe_number - 1][0].print_recipe();
	}

	void CookBook::print_recipe_ingridients(int recipe_number)const	//to show the list of ingridients for pointed recipe
	{
		ar[recipe_number - 1][0].print_ingredients();
	}

