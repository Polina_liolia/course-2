#include "Element.h"
#include <stdio.h>

Element::Element(){		//default constructor
	left = right = nullptr;
}

Element::~Element()	//destructor
{
	delete left;
	delete right;
}

Element::Element(const Element & aEl)	//copy constructor
{
	left = new Element;
	*left = *aEl.left;
	right = new Element;
	*right = *aEl.right;
	value = aEl.value;
}

Element::Element(String En, String Rus, int Counter)	//overloaded constructor
{
	right = left = nullptr;
	value.set_word(En, Rus, Counter);
}

void Element::set_value(String En, String Rus, int Counter)	//to set value (word) to the element
{
	value.set_word(En, Rus, Counter);
}

void Element::set_translation(String Rus)	//to add a russian translation (takes translation in String)
{
	value.set_rus(Rus);
}


void Element::count()	//to add one more calling of a word
{
	value.add_count();
}

void Element::set_left(Element* aLeft)	//to set left pointer on the element
{
	left = aLeft;
}
void Element::set_right(Element* aRight)	//to set right pointer on the element
{
	right = aRight;
}

Word Element::get_value()const	//to get a word instance
{
	return value;
}


int Element::get_counter() const	//to get a word's calls counter
{
	return value.get_counter();
}

String Element::get_en() const	//to get word in english (instance of class String)
{
	return value.get_en();
}

String Element::get_rus()	//to get word in russion (instance of class String)
{
	return value.get_translation();
}

Element* Element::get_left()	//to get element on the left pointer
{
	return left;
}

Element* Element::get_right()	//to get element on the right pointer
{
	return right;
}