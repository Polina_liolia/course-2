#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;   
class A
{
	int x;
	char * name;
public:
	A()
	{
		x = 0;
		name  = NULL;
	}
	A(int aX, char* aName)
	{
		x = aX;
		name = new char[strlen(aName)+1];
		strcpy(name, aName);
	}

	A& operator=(const A& a)
	{
		x = a.x;
		name = new char[strlen(a.name)+1];
		strcpy(name, a.name);
		return *this;
	}

	bool operator > (const A& a)
	{
		return strcmp(name, a.name) > 0;
	}
	bool operator >= (const A& a)
	{
		return strcmp(name, a.name) >= 0;
	}
	bool operator < (const A& a)
	{
		return strcmp(name, a.name) < 0;
	}
	bool operator <= (const A& a)
	{
		return strcmp(name, a.name) <= 0;
	}

	void show()
	{
		cout << x << ":"<< name <<  endl;
	}

};
int main( )
{
	vector <int> v5(5);

	for (int i : v5) {
		cout << i << " ";
	}

	cout << endl;

	int t = 0;
	for (auto i : v5) {
		v5[i] = t++;
	}

	

	cout << "==============================" << endl;

	
	vector<int> v_int;

	v_int.push_back(34);
	v_int.push_back(34);
	v_int.push_back(2);
	v_int.push_back(13);
	v_int.push_back(56);
	v_int.pop_back();
	cout <<"Size: "<< v_int.size() << endl;
	//v_int[3] = 333;
	

	for(unsigned int i = 0; i < v_int.size(); i++)
		cout << v_int[i] << " ";
	cout << endl;


	vector<int> empty;
	vector<vector<int>> Vec_2D;
	Vec_2D.push_back(empty);
	Vec_2D.push_back(empty);
	Vec_2D.push_back(empty);
	Vec_2D.push_back(empty);
	Vec_2D[0].push_back(45);
	Vec_2D[1].push_back(11);
	cout<< "-*-*-*-*  " << Vec_2D[1][0] << "-*-*-*-*-*" << endl;

	vector<vector<int>> Vec_2D_ver2(10);
	Vec_2D_ver2[0].push_back(457);

	vector<int>::iterator it_int, zu;

	
	for(zu = v_int.begin();zu != v_int.end();zu++){
		cout << *zu << " ";
	}
	cout << endl;
	sort(v_int.begin(),v_int.end());
	        //v_int.begin() + v_int.size()/2

	for(it_int = v_int.begin();
		   it_int != v_int.end();
		   it_int++){
		cout << *it_int << " ";
	}
	cout << endl;


	vector<A> v1;
	//vector<???> v5;

	

	vector<A>::iterator it;

	v1.push_back(A(1,"name"));
	v1.push_back(A(2,"name 2"));
	v1.push_back(A(3,"name 3"));
	
	
	v1.push_back(A(6,"name 6"));
	v1.push_back(A(5,"name 5"));
	v1.push_back(A(4,"name 4 "));

	it = v1.begin()+1;

	
	
	//v1.erase(v1.begin()+1,v1.end()-3);
	//v1.erase(v1.begin());

	for(int i=0; i<v1.size(); i++)
		v1[i].show();
	
	*it = A(56,"******");
	sort(v1.begin(), v1.end());
	cout << "===========================" << endl;

	for(it = v1.begin(); it != v1.end(); it++)
		(*it).show();
		
	// methods
	vector<int> v_int2;
	vector<int> v_int3;
	for(int i = 0; i < 10; i++){
		v_int2.push_back(i);
	}


	v_int3.assign(v_int2.begin() + 2, v_int2.begin() + 6);


	cout << " Assign ================================" << endl;
	cout << "original: ";

	for(unsigned int i = 0; i < v_int2.size(); i++)
		cout << v_int2[i] << "  ";
	cout << endl;

	cout << "v_int3: ";
	for(unsigned int i = 0; i < v_int3.size(); i++)
		cout << v_int3[i] << "  ";
	cout << endl;

	cout << "  Swap =================================" << endl;

	v_int2.clear();
	v_int3.clear();

	for(int i = 0; i < 10; i++){
		v_int2.push_back(i);
	}

	for(int i = 0; i < 5; i++){
		v_int3.push_back(i*3);
	}

	cout << "before: " << endl;
	cout << "v_int2: ";
	for(int i = 0; i < v_int2.size(); i++)
		cout << v_int2[i] << "  ";
	cout << endl;

	cout << "v_int3: ";
	for(int i = 0; i < v_int3.size(); i++)
		cout << v_int3[i] << "  ";
	cout << endl;

	v_int2.swap(v_int3);

	cout << "after: " << endl;
	cout << "v_int2: ";
	for(int i = 0; i < v_int2.size(); i++)
		cout << v_int2[i] << "  ";
	cout << endl;

	cout << "v_int3: ";
	for(int i = 0; i < v_int3.size(); i++)
		cout << v_int3[i] << "  ";
	cout << endl;
    cout << "Resize===========================" << endl;
	v_int2.clear();
	cout << "Size: " << v_int2.size() << endl;

	v_int2.resize(6);
	vector<int>::iterator it;
	for(it = v_int2.begin(); it != v_int2.end(); it++){
		cout << *it << ", ";
	}
	cout << endl;


}