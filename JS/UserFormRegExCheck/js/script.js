var form = document.forms.reg_form;

var user_name = form.elements.user_name;
var phone = form.elements.phone;
var email = form.elements.email;
var address = form.elements.address;

var name_flag = false,
    phone_flag = false,
    email_flag = false,
    address_flag = false;

function check_user_data ()
{
    var submit_error_field = document.querySelector("form>div.error_field");
    submit_error_field.innerText = "";
    if (user_name.value == "" || phone.value == "" || email.value == "" || address.value == ""
    || !name_flag || !phone_flag || !email_flag || !address_flag) {
        event.preventDefault();
        submit_error_field.innerText = "Все поля должны быть корректно заполнены!";
    }
}
form.addEventListener("submit", check_user_data);


function check_name () {
    var regName = /(?:(?: *)(?:[a-zа-яё\-]*)+(?: +)){2}(?:(?: *)(?:[a-zа-яё\-]*)+(?: *))/i;
    var resultName = regName.exec(user_name.value);
    var name_error_field = document.querySelector(".user_name>td.error_field");
    name_error_field.innerText = "";
    if (resultName == null && user_name.value !="") {
        user_name.focus();
        name_error_field.innerText = "Ошибка в формате ФИО!";
        name_flag = false;
    }
    else
        name_flag = true;
}
user_name.addEventListener("change", check_name);

function check_phone() {
    var regPhone = /(?:\+\d+)?[( ]?\d*[) ]?\d*[- ]*\d*[- ]*\d+/g;
    var resultPhone = regPhone.exec(phone.value);
    var phone_error_field = document.querySelector(".phone>td.error_field");
    phone_error_field.innerText = "";
    if (resultPhone == null && phone.value !="") {
        phone.focus();
        phone_error_field.innerText = "Ошибка в формате номера телефона!";
        phone_flag = false;
    }
    else
        phone_flag = true;

}
phone.addEventListener("change", check_phone);

function check_email() {
    var regEmail = /^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9](?:[-a-z0-9]{0,61}[a-z0-9])?\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])/i;
    var resultEmail = regEmail.exec(email.value);
    var email_error_field = document.querySelector(".email>td.error_field");
    email_error_field.innerText = "";
    if (resultEmail == null && email.value !="") {
        email.focus();
        email_error_field.innerText = "Ошибка в формате e-mail!";
        email_flag = false;
    }
    else
        email_flag = true;
}
email.addEventListener("change", check_email);

function check_address() {
    var regAddress = /(?:(?:[а-я \-])*(?:область|обл\.*|обл))*[ ,.\\/;:]*(?:(?:[а-я \-])*(?:район|р-н|р\.*))*[ ,.\\/;:]*(?:(?:город|г\.|г\.)*(?:[а-я \-])*)[ ,.\\/;:]+(?:(?:улица|ул\.*|у\.)(?:[а-я \-])*)[ ,.\\/;:]*(?:(?:дом|д\.| *)*(?:\d+[а-я \-]*))(?:[ ,.\\/;:]*(?:подъезд|п\.*| *)(?:[ ,.\\/;:]*\d+))*(?:[ ,.\\/;:]*(?:этаж|э\.*| *)(?:[ ,.\\/;:]*\d+))*(?:[ ,.\\/;:]*(?:квартира|кв\.*|к\.*| *)(?:[ ,.\\/;:]*\d+))*/i;
    var resultAddress = regAddress.exec(address.value);
    var address_error_field = document.querySelector(".address>td.error_field");
    address_error_field.innerText = "";
    if (resultAddress == null && address.value !="") {
        address.focus();
        address_error_field.innerText = "Ошибка в формате адреса!";
        address_flag = false;
    }
    else
        address_flag = true;
}
address.addEventListener("change", check_address);
