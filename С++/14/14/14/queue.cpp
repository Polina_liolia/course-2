#include "queue.h"
using std::cout;
using std::endl;

queue::queue() { //����������� �� ���������
	size = 0;
	items = nullptr;
}
queue::queue(int aItem) { //������������� �����������, ����������� ������� �������
	size++;
	items = new int[1];
	items[0] = aItem;
}

queue::queue(const queue &aQueue) { //����������� �����������
	if (aQueue.size == 0)
	{
		size = 0;
		items = nullptr;
	}
	else
	{
		size = aQueue.size;
		items = new int[size];
		for (int i = 0; i < size; i++) {
			items[i] = aQueue.items[i];
		}
	}
}

queue::~queue() { //����������
	delete[] items;
}

//���������� ������� ������ �������
//�� ��������� ����������
int queue::get_size()
{
	return size;
}

// Function returns the first element in the queue
// Reduce the size of the queue
// Does not accept values
const int queue::pop()
{
	if (size == 0)
	{
		cout << "Queue is empty\n";
		return 0;
	}
	size--;
	int *newItems = size ? new int[size] : nullptr;
	for (int i = 1; i < size + 1; i++)
	{
		newItems[i - 1] = items[i];
	}
	int result = items[0];
	delete[] items;
	items = newItems;
	return result;
}

// function insert gets number and adds it in our array in the right position 
void queue::insert(int number)
{
	if (size == 0)		//if qeueu is empty
	{
		size++;
		items = new int[size];
		items[0] = number;
	}
	else {
		size++;
		int *buf = new int[size];
		buf[0] = number;
		for (int i = 1, j = 0; j < size - 1; j++)
		{
			buf[i] = items[j];
			i++;
		}
		delete[]items;
		items = buf;
		int item, temp;
		for (int i = 1; i < size; i++)
		{
			temp = items[i];
			item = i - 1;
			while (item >= 0 && items[item] > temp)
			{
				items[item + 1] = items[item];
				items[item] = temp;
				item--;
			}
		}
	}

}

/*this metod takes index of element end return value of element*/
int queue::operator[](int index)
{
	if (index<0 || index>size)
	{
		cout << "wrong number of element" << endl;
		return 0;
	}
	else
		return items[index];

}


//��������� ����������� ������ �� ��������� ������ queue
//���������� �������� �����������, ���� ������� � ��� �������� ����� - ���������� true, ����� - false
bool queue::operator== (const queue &aQueue)
{
	if (size != aQueue.size)
		return false;
	bool flag = true;
	for (int i = 0; i < size; i++)
	{
		if (items[i] != aQueue.items[i])
		{
			flag = false;
			break;
		}
	}
	return flag;
}

//��������� ����������� ������ �� ��������� ������ queue
//���������� �������� �����������, ���� ������� �� ����� ��� �������� �� ����� - ���������� true, ����� - false
bool queue::operator!= (const queue &aQueue)
{
	if (size != aQueue.size)
		return true;
	bool flag = false;
	for (int i = 0; i < size; i++)
	{
		if (items[i] != aQueue.items[i])
		{
			flag = true;
			break;
		}
	}
	return flag;
}