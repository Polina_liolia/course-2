#pragma once
#pragma once
#include"Wather_transport.h"

enum energy_type { athom, non_athom };

class Underwather_transport : public Wather_transport
{
private:
	energy_type nrg_installation;

public:
	Underwather_transport();
	Underwather_transport(string aModel, double aWeight, double aMax_speed, double aDisplacement, energy_type aNrg_installation, speed aSpeed_units = knots);
	~Underwather_transport() {};

public:
	void set_nrg_installation(energy_type aNrg_installation);
	energy_type get_nrg_installation()const;

	virtual speed get_speed_units()const;
	virtual void print();

};