#pragma once
enum search_type { line, binar };
enum sort_type { bubble, insert, select };

void line_search(int *arr, int sz, int min, int max);
void binar_search(int *arr, int sz, int min, int max);
void bubble_sort(int *p_arr, int sz);
void insert_sort(int *p_arr, int sz);
void select_sort(int *p_arr, int sz);

