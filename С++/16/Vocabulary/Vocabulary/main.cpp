#include <iostream>
#include "Vocabulary.h"
#include "MyString.h"
#include "Word.h"
using std::ostream;
using std::cout;

//globaly overloaded output of String
ostream& operator << (ostream& os, const String &aString)
{
	if (aString.get_String() != nullptr)
		os << aString.str << "\n";
	return os;
}

//globaly overloaded output of Word
ostream & operator << (ostream& os, const Word &aWord)
{
	os << aWord.en << "(popularity: " << aWord.counter << "):\n";
	for (int i = 0; i < aWord.size_rus; i++)
		os << i + 1 <<". " << aWord.rus[i];
	os << "\n";
	return os;
}

void main()
{
	Vocabulary A;
	A.add("dog", "sobaka", 5);
	A.add("cat", "koshka", 3);
	A.add("mouse", "mish", 1);
	A.add("pig", "svinya", 10);
	A.add("cow", "korova", 2);
	A.add("horse", "loshad", 1);
	A.add("rat", "krisa", 7);
	A.add("tiger", "tigr", 15);
	A.print();
	std::cout << "___________________________\n";
	A.print_top3_popular();
	std::cout << "___________________________\n";
	A.print_top3_unpopular();
	std::cout << "___________________________\n";
}