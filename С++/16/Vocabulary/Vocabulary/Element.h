#pragma once
#include "Word.h"
class Element{
	Element* left;
	Element* right;
	Word value;
public:
	Element();
	Element(String En, String Rus, int Counter);
	void set_value(String En, String Rus, int Counter);
	void add_translation(String Rus);
	void remove_translation(String Rus);
	void remove_translation(int number);
	void change_translation(String Rus_old, String Rus_new);
	void change_translation(int number, String Rus_new);
	void count();
	void set_left(Element* aLeft);
	void set_right(Element* aRight);
	Word get_value();
	int get_counter()const;
	Element* get_left();
	Element* get_right();

};