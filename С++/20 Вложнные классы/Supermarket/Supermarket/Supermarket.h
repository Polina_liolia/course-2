#pragma once
#include "Department.h"
#include"DoubleLinkList.h"

class Supermarket
{
private:
	int size_depts;
	DoubleLinkList depts;
	int size_stuff;
	employee *all_stuff;

public:
	Supermarket();
	Supermarket(const Supermarket &aS);
	~Supermarket();

	Supermarket operator=(const Supermarket &aS);
	
	void add_department(String aName, short aDay, short aMonth, short aYear);
	void remove_department(int index);
	void edit_dept_name(int index, String name_new, short aDay, short aMonth, short aYear);
	DoubleLinkList &get_depts();
	int get_size_stuff()const;
	employee *get_all_stuff();
	
	friend ostream& operator << (ostream& os, Supermarket &aSm);
};