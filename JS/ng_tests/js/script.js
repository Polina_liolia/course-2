//'switching on' controllers:
angular.module('tiresApp', ['ngAnimate']); //creating tiresApp module and downloading ngAnimation module in it
var tiresApp = angular.module('tiresApp');

//configs to enable defining controllers, ctr. in the global scope:
// tiresApp.config(['$controllerProvider', function($controllerProvider) {
//     $controllerProvider.allowGlobals();
// }]);


//attaching controllers to the current app:
tiresApp.controller ('MessageController', function($scope) {    //creating a new controller with prop, attached directly to the scope:
    $scope.msg = "message!!";
})
    .controller('monthesAmount', function($scope) {
    $scope.monthes = function () {
        return 12;
    }
})
    .controller('AdditionController', function($scope) {
    $scope.operand1 = 0;
    $scope.operand2 = 0;
    $scope.add = function() {
        return $scope.operand1 + $scope.operand2;
    };
    $scope.options = [0,1,2,3,4];
})
    .controller('books', function ($scope) {
    $scope.allBooks = [
        {author: 'Pushkin', price: 8.5},
        {author: 'Lermontov', price: 10},
        {author: 'Tutchev', price: 12},
        {author: 'Lermontov', price: 10},
        {author: 'Tutchev', price: 12}
    ];
})
    .controller('ItemsController', function($scope) {
    $scope.items = [
        {name: 'Item 1', price: 1.99},
        {name: 'Item 2', price: 2.99},
        {name: 'Item 3', price: 2.51},
        {name: 'Item 4', price: 3.81}
    ];
})
    //normal version:
    .controller('localContoller', function ($scope, $locale) {  //language settings
        $scope.local = $locale.id;
})
    //version to avoid minification problems:
    .controller('localContoller2', ['$scope', '$locale', function (s, l) {  //language settings
    s.local = l.id;
}])
    .value('score', {points: 0})
    .value('randomScore', function() {
        return Math.ceil(Math.random() * 10);
    })
    .controller('scoreCtrl', function ($scope, score, randomScore) {
        $scope.score = score;
        $scope.increment = function () {
           $scope.score.points++;
        };
        $scope.random = function () {
            $scope.rnd = randomScore();
        }
    });
