#include <iostream>
using namespace std;
// �����, ��������� �� �������
// ����� ��������������
class Temp
{
	int TEMP;
public:
	//�����������
	Temp(){ TEMP = 25; }
	//������� ������ �� �����
	void TempFunction(){
		cout << "TEMP = " << TEMP << "\n\n";
	}
	//������� ��������� ��������
	void TempSet(int T){
		TEMP = T;
	}
};

// �����, ��������������� ���������
class MyPtr
{
	//��������� �� ����� Temp
	Temp*ptr;

public:
	//�����������
	MyPtr(Temp*p = NULL){
		ptr = p;
	}

	// �������� �������������� ����
	// �� ������������������ � ����������������
	operator Temp*(){
		return ptr;
	};
	// �������� ��������� ->
	// ������� �������� ���������� 
	// �������� � "�����������"
	// ���������
	Temp* operator->(){
		return ptr;
	}
	//�������� ++ ��� �������� ��������� ������
	MyPtr operator++(){
		ptr++;
		return *this;
	}

};

void main()
{
	//�������� ������ �������
	Temp*main_ptr = new Temp;
	//������� ��������� � ������
	//������� ����� "������" ���������
	main_ptr->TempFunction();

	//�������� ������� ������-���������
	MyPtr pTemp(main_ptr);
	//��������� ����� �����-���������
	pTemp->TempFunction();

	//�������� ������� ��������
	//���������������� ������
	Temp*arr_ = new Temp[3];

	//���������� ��������������� ������� 
	//���������� �� 0 �� 2 
	for (int i = 0; i<3; i++) arr_[i].TempSet(i);

	//�������� ������� ������ ���������
	//� ������ � ���� ������ �������
	//(����� �������� �������������� ����)
	MyPtr arr_temp = arr_;
	//����� �� ���� ������� ������
	arr_temp++;
	//������������ ����������
	arr_temp->TempFunction();

	//�������� ��������
	delete main_ptr;
	delete[]arr_;

}

��������� ������ ���������

TEMP = 25

TEMP = 25

TEMP = 1

