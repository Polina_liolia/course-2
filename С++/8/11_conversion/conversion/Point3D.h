#pragma once
#include <iostream>

using std::cout;
using std::endl;
class Point;
class Point3D
{
public:
	
	int x;
	int y;
	int z;
	Point3D();
	Point3D(int t);
	Point3D(int ax, int ay, int az);
	Point3D(Point);
	void show();
	int getX()const;
	int getY()const;
	int getZ()const;
/*
	Point3D operator=(const Point& p){
		cout << "Point3D operator=(const Point& p)\n";
		return Point3D(p.x, p.y, 0);
	}
*/
	
};
