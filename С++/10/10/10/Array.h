#pragma once
#include <iostream>

class Array
{
private:
	int* arr;
	int sz;

public:
	Array();
	Array(int* aArr, int aSz);
	~Array();
	int& operator[] (char* aIndex);
};
