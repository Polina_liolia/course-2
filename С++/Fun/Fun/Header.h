#pragma once
#include <iostream>
#include <stdlib.h>
#include <ctime>
#include <stdio.h>
#include <cstring>
#include <iomanip>
#include <io.h>
#include <direct.h>
using namespace std;

//-----------------------------Otrer-------------------------------------//
/*���� ����������� ����� (a).
Actions:
'a' ���������� ���� 3 � ���;
'b' ������� ��� � ��� ����������� ��������� �����;
'c' ���������� ������ ���� � ���;
'd' ����� ��� ���� ������ 5;
'e' ������������ ��� ���� ������ 7;
'f' ������� ��� � ��� ����������� ����� �� 0 �� 5 ������������(�����).
'g' ����������� �����
'h' ������������ �����
any other letter - returns -1 */

int natural_number(int a, char action);


/*����������, ������� ��� � ������������������ �������� ����
(��������: 10, -4, 12, 56, -4 ���� �������� 3 ����)*/
int change_sign(int* arr, int sz);


//������, ����������� ���������� ������� �����:
bool simple_number(int a);


//������� ��� �������� ����� �� ���������� � ����������������� �������
void convert10_16(int number10);

//������� ���������� ����������� ������ �������� ���� ���������� �����.
int max_common_div(int a, int b);

//������� ������� ����� ���� ������������ �����.
int digits_sum_3(int a);


//������� ��� ������� ����������� ������ �������� ���� ����������� ����� (�������� �������).
int GCD(int a, int b);


//������� ������ �������������.
float diskriminant(float a, float b, float c);


//Function to return number of roots of the square equation; calculating roots
int roots(float a, float b, float c, float &R1, float &R2);


//�������, ����������� ��� ���������� � �������� �� �������� �������.
//Variant 1
void change(int &a, int &b);

//Variant 2
void change(int *a, int* b);


//Function to return number of roots of the square equation; calculating roots
int roots(float a, float b, float c, float &R1, float &R2);


/*������� ������ ����� �������� ������ ������ �� ������� ��������������� � �������� ��������������� ���� ����������
������ ������ ������.*/

int coldest_week(int arr[][31], const int str, float arr_middle_t[]);


//Converts a DEC digit to the BIN format
void DEC_to_BIN_converter(int digit);


//������� �� ����� ����������� ��� ������ ������������� �� ���������.
void print_rectangle(int cols, int rows, int fill);


//-----------------------------Arrays_basic-----------------------------------//

//Inputting an array with random numbers
void randinput(int *arr, int sz, int x = 10, int y = 100);



//���������� ���������� ������� ���������� �������
void randinput2(int **arr, int str, int col, int x, int y);


//Random inputting of array (numbers in second string are always lower than in the first one)

void randinput2_decreas(int *arr, int str, int col);


//Outputting to console
void arr_print(int *arr, int sz);


//����� �� ����� ���������� �������
void arr_print2(int **arr, int str, int col);


//Creating of a two-dimensional dinamic array
void create_2din_arr(int** arr, int sz1, int sz2);


void create_2din_arr(char** str, int sz1, int sz2);


//Inputting a two-dimensional dinamic array with random numbers
void input_2din_arr(int** arr, int sz1, int sz2, int x = 10, int y = 100);


void input_2din_arr(char** str, int sz1, int sz2, int x = 97, int y = 122);


//Inputting a two-dimensional dinamic array using cin
void input_2din_arr_cin(int** arr, int sz1, int sz2);

//Outputting a two-dimensional dinamic array to console
void output_2din_arr(int** arr, int sz1, int sz2);


void output_2din_arr(char** str, int sz1, int sz2);

//Deleting a two-dimensional dinamic array
void delete_2din_arr(int** arr, int sz1, int sz2);


void delete_2din_arr(char** str, int sz1, int sz2);
void delete_2din_arr_(char** str, int sz1);


//Creating of a three-dimensional dinamic array
void create_3din_arr(int*** arr, int plane, int str, int col);


//Inputting a three-dimensional dinamic array with random numbers
void input_3din_arr(int*** arr, int plane, int str, int col, int x = 10, int y = 100);


//Outputting a three-dimensional dinamic array to console
void output_3din_arr(int*** arr, int plane, int str, int col);


//Deleting a three-dimensional dinamic array
void delete_3din_arr(int*** arr, int plane, int str, int col);


//-------------------------------------Arrays_sort-----------------------------------//

//Bubble sort
void bubble_sort(int *p_arr, int str, int col);

//Insert sort
void insert_sort(int *p_arr, int str, int col);


//Select sort
void select_sort(int *p_arr, int str, int col);


//Insert sort (ascending)
void insert_sort_ascending(int *p_arr, int sz);


//Insert sort (descending)
void insert_sort_descending(int *p_arr, int sz);


//Insert sort (from odd to even)
void insert_sort_odd_even(int *p_arr, int sz);


//Insert sort with additional options
void insert_sort(int *arr, int sz, int type);



//���������� ���������� �������
void sort_arr_col(int **ar, int str, int col);


//������� ���������� ���������� ��������� ���� �������� �� �����������
void sort2arr(int **arr1, int **arr2, int str, int col);


//������� ���������� ���������� �������
void sort_arr_7col(int **ar, int str, int col);


//Sotring strings of a two-dimensional dinamic array by abc
void sort_2din_arr_str(char** str, int sz1, int sz2);


//----------------------------------------Arrays_search------------------------------------//

//Binar search of first three elements x in the array
int binar_search(int *arr, int sz, int x, int *rezults);

//Line search of first three elements x in the array
int line_search(int *arr, int sz, int x, int *rezults);


//Universal search of first three elements x in the array
int first_three_indexes(int *arr, int sz, int x, int *rezults, int(*p)(int*, int, int, int*));


//--------------------------------Arrays_dif_operations----------------------------------//


// ������� ���������� �������� � ������������� ������� ���� double
void add_el(double** p_arr, int &sz, double el);

//����������� ���� �������� ���� double. �������� ������� ������� ����������� � �������.
void add_arr(double** p_arr1, int &sz1, double* p_arr2, int sz2);

//������ ���� ������ ��������� ������
void zerochange(int *arr, int sz);

/*������� ��������� ������ � �������� ������� ��������.
���������� ������� ���������� �������� � ������� �� ���������� ���������.*/
int el_amount(int *arr, int sz, int min, int max);

//��������� ������� ������ � ������� ���� ��������, ������� 4 � ����� ���������.
void div4(int *arr, int sz);

//������� ������ ���������� �������� ����� � ������� �� ��������� ����������� �������
void nearest_simple_number(int *arr, int sz);

//����� ������� ������/����� �� �������� ����� �����
void shift_loop(int *arr, int sz, int step, bool d);

//�������, ������� ������� ���������� �������� �� ������� � ���������� ������ ��������������� �������.
int del_same(int *arr, int sz);

//Finding maximal element
int max_el(int *arr, int sz);


/*�������, ������� ���������� 3 ������ �������� �������, �������� ������� ������ �������� ��������������� ������
�������, � ��� �� �������� �������� ���������������.*/
int three_el_more_average(int *arr, const int sz, int *more_average);

//Searching for the same elements and creating a new, pared down, array
int *delete_same(int *parr, int sz);

//Deleting of elements of an array with pointed meaning
int *el_delete(int *arr, int sz, int el_del);

//Deleting of elements of an array with pointed index
int *index_el_delete(int *arr, int sz, int index_del);

//Insert an element with pointed index and meaning
int *insert_el(int *arr, int sz, int index_insert, int el_insert);

//Deleting pointed number of elements startring from pointed index
int *several_elements_delete(int *arr, int sz, int index_start_del, int number_elements);


//Inserting pointed number of elements with pointed meaning, startring from pointed index
int *several_elements_insert(int *arr, int sz, int el_meaning, int index_start_insert, int number_elements);

//Replasing several elements of local array to the end of global array
int *replace_elements_to_global_array(int *p_arr, int sz, int*p_global_arr, int sz_global_arr, int index_start, int index_end);

//��������� ������ � ������������ ������ ����� �����, ����� ��������� ������� ������ ���������� �����.
void add_str_2din_arr(int** arr, int &sz1, int &sz2, int number);

/* ������� ������� ������������ ������, ���������� ���������� �������� �� ���� ��������,
������� �� ����� � ��� �� ��������.**/
int** copy_max_el_2din_arr(int** arr1, int** arr2, int sz1, int sz2);

//������������ ������ � ������� ���, ���� ��� ��� �� ����������� �� ����.
void change_strings_max_sum(int** arr, int sz1, int sz2);

//������� ���������� ������� ������� � ������ ������ � ��� ��������� � ������ ������� �������.
void change_max_and_first(int** arr, int sz1, int sz2);

//������� ��������� ���������� ������� � ������������ ������ ��������� � �������� ������� � ������ �� ������� � ��������� ����������.
void change_max_el_sum_plane(int*** arr, int plane, int str, int col);


//--------------------------------------Phone_book-----------------------------------------------//

struct persone;

struct date;

struct phonebook;

//readig existing file, inputting information from it to structure phonebook
void read_file(phonebook** arr, int &str, int col, char* file_name);

void print_to_file(phonebook** arr, int str, char* file_name);

//Creating of a two-dimensional dinamic array
void create_2din_arr(phonebook** arr, int sz1, int sz2);

//Deleting a two-dimensional dinamic array
void delete_2din_arr(phonebook** arr, int sz1, int sz2);

//Creating contacts for TEST
void create_contact_test(phonebook **arr, int &str, int col);

//Creating a contact
void create_contact(phonebook **arr, int &str, int col);

//Shows the list of all contacts (name, patronymic, surname) 
void show_contacts_list(phonebook **arr, int &str, int col);

//Shows all contacts from start to end (default - all existing contacts)
void show_contacts(phonebook **arr, int &str, int col, int start, int end);

//Sotring all contacts by name or surname (name_surname: true-by name, false-by surname)
void bubble(phonebook **arr, int &str, int col, bool name_surname);

void insert(phonebook **arr, int &str, int col, bool name_surname);

void select(phonebook **arr, int &str, int col, bool name_surname);

//Searching or deleting a contact (by name) (search_or_delete: true - search, false - delete)
void contact_search_or_delete_name(phonebook **arr, int &str, int col, bool search_or_delete);

//Searching or deleting a contact by the surname (search_or_delete: true - search, false - delete)
void contact_search_or_delete_surname(phonebook **arr, int &str, int col, bool search_or_delete);

//Searching or deleting a contact (by patronymic) (search_or_delete: true - search, false - delete)
void contact_search_or_delete_patronymic(phonebook **arr, int &str, int col, bool search_or_delete);

//Searching or deleting a contact by the day of birth (search_or_delete: true - search, false - delete)
void contact_search_or_delete_birthday(phonebook **arr, int &str, int col, bool search_or_delete);

//Searching or deleting a contact (by the month of a birth) (search_or_delete: true - search, false - delete)
void contact_search_or_delete_birthmonth(phonebook **arr, int &str, int col, bool search_or_delete);

//Searching or deleting a contact (by the year of a birth) (search_or_delete: true - search, false - delete)
void contact_search_or_delete_birthyear(phonebook **arr, int &str, int col, bool search_or_delete);

//Searching or deleting a contact (by the phone number or it's part) (search_or_delete: true - search, false - delete)
void contact_search_or_delete_phone(phonebook **arr, int &str, int col, bool search_or_delete);


//Deleting all existing contacts
void del_all_contacts(phonebook **arr, int &str);


//--------------------------------------------Sea_fight-------------------------------------------------//


//Checks, is the horisontal groupe of "1" separated from other groups with only zeros
bool separate_horisontal_location_check(int **arr, int str, int col, int start_col, int end_col, int start_str, int end_str);

//Checks, is the vertical group of "1" separated from other groups with only zeros
bool separate_vertical_location_check(int **arr, int str, int col, int start_col, int end_col, int start_str, int end_str);

//Checks, is the group of "1" (horisontal or vertical) separated from other groups with only zeros
bool separate_location_check(int **arr, int str, int col, int start_col, int end_col, int start_str, int end_str);

//Places the "boat" on a field
void place_boat(int **arr, int start_col, int end_col, int start_str, int end_str);

/*������� �������� ������� ������ ������ (�������������� ��� ������������), ���������� ������,
� �������� �� �� ����� ������*/
void change_ones_group(int** arr, int str, int col);

//������� ���������� ������������ �������� �� ���� �������� ���.
void rand_place_boats(int** arr, int str, int col);


//������� ������������ �������� ������������� �� ���� �������� ���.
void user_places_boats(int** arr, int str, int col);


//---------------------------------------Strings------------------------------------------//


// ������� ������ ������
int stringlen(char *string, int sz);


// ���������� n �������� ������ ������������ ��������
bool stringinput(char *string, int symbol, int amount);

//Checking, does the symbol indicates the end of a word or string, or not
bool word_end(char *str, int j);

//Returning the lenght of a string
int stringlen(char *string, int sz);


//����� ��������� ����� � ������
int word_search(char *text, int sz_text, char *word, int sz_word);

//����� ��������� ����� � ������ ��� ����� �������� (returns the number of word found or -1 if word was not found)
int word_search_no_reg(char *text, int sz_text, char *word, int sz_word);

//����� ��������� ����� � ������ � ������ �������� (returns the number of word found or -1 if word was not found)
int word_search_reg(char *text, int sz_text, char *word, int sz_word);

/* ���������� ������ ������ �������.
� �������� ���������� ���������� ������ �������� � ������.
������ mystrcpy(str, str2) ��� mystrcpy(str, "hello")*/
char *string_to_string(char *str1, const int sz1, const char *str2, const int sz2);

// ����������� ���� �����
char *sum_strings(char *str1, const int sz1, char *str2, const int sz2);

//��������, ������� ��������� ������� ������, ������� �������� � ������� ����� ������.
void check_str(char* string, const int sz, int occupied, int free);

//�������� �� ����� � m �� n �������� ������ � �������� ������ ������� � ������ ������.
char *shortcut_str(char* str1, const int sz1, char* str2, const int sz2, int m, int n);

//������� � m �� n ��������, ������������ ������ � �������� �� �� �����.
char *part_str_del(char *str, int sz, int m, int n);


//�������, ������� � ������ ����� ����������� ������ ����� ���������.
char* capital_letter(char *str);

//Finding the string lenght
int stringlen(char *string);


//Indicating, is the symbol a digit, or not
bool digit_check(char *str, int i);

//Checking, does the symbol indicates the end of a word or string, or not
bool word_end(char *str, int j);

//Finding the longest word in a string and returning a pointer on string with this word
char* longest_word_str(char *str, int &longest_word_sz);

//Finding the longest word in a string and returning an index of its beginning
int longest_word_index(char *str);

//Finding the shortest word in a string and returning a pointer on string with this word
char* shortest_word_str(char *str, int &shortest_word_sz);

//����� ���� ������� ����, ������������ � ����� ���������� ��� ���������� �����, � ����� ���������� ������ ���� �����.
void lng_shrt_word_vowels(char *str, char* (*p)(char*, int &));


//������ ���� ���� �� "*"
void change_digits(char *str);

//To change a word on ***
void hide_word(char *arr, int sz, int word_number);

// ����� � ������� ��� ������� �����(��� ����������), ������� ����������� � ������ � ���������� ����.
int all_vowels(char *str);

//������� ��������� �� �����, ���������� ���������� ���������� ����
char *max_digits_word(char* str, int &sz);

//����� � ������� ��� �����, ���������� ���������� ���������� ������� ����.
void max_vowels_words(char* str);

//��������, �������� �� ������ �����������
bool palindrome(char* str);

//�������� ������������ ����������� ������ � �������������� ���������.
bool brackets_check(char *str);


//������� ������� ����� � ���������� ������� ���������.
int roman_to_dec(char *roman, int sz);

/*����������, ����� �� ��������� ������� �� ���� ������ ������, � ������� ������ ����������� ����� ����������
� ��� �� �����, �� ������� ������������ ����������*/
//additional function
bool chain(char* str, int sz, char first_letter);

//function to call from main
bool words_chain(char* words, int sz);




//---------------------------------------------------Files-----------------------------------------//


//Counts strings and characters in a file (getting file name using cin)
void file_str_chr_counter_cin(int &chr, int &str);


struct book;

//readig existing file, inputting information from it to structure book
bool read_file(book** arr, int &str, int col, char* file_name);


//inputting information from structure book to file
void print_to_file(book** arr, int str, char* file_name);


/*To change a price of a curent book (if number_book_to_change=0 - of all books);
action:
* - multiply price
/ - divide price
% - raise/reduce on price_change, per cents (depends on a sign of price_change: + or -)
+/-  raise/reduce on price_change, dollars
increase_decrease:
i - to increase price
d - to decrease price
*/
void change_price(book** arr, int &str, int col, char action, int price_change, int number_book_to_change);

//To save every string of a file in two-dementional array (returns pointer on array wit a text)
char** save_file_text_in_array(char* file_name, int &sz);

//To save every word of a file in two-dementional array (returns pointer on array with words)
char** save_file_words_in_array(char* file_name, int &sz);


//Prints data from array to file (with deleting all data from the file)
void print_text_to_file(char* file_name, char**text, int text_str);

//Changes pointed words in text file and prints corrected text to another text file (file_to_save)
void censorship(char* text_file, char* words, char* file_to_save);

//Checks, does such a login and password exist in a file
bool find_password(char* passwords_file, char* login, char* password);


//-----------------------------Directory----------------------------------------//

//����� ����� ������������� �������
void find_maxsize_file(char* directory, char* mask, char* name_max, long long int &sz_max);


//	����� ����� � ������������
bool find_file(char* directory, char* mask);


//������� �������� ������� �����
long long int dir_sz(char* directory);
