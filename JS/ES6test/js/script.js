// https://habrahabr.ru/post/305900/

// 1. let, const и блочная область видимости
var a = 2;
{
    let a = 3;
    console.log(a); // 3
}
console.log(a); // 2

{
    const ARR = [5, 6];
    ARR.push(7); //добавлять можно
    console.log(ARR); // [5,6,7]
    //ARR = 10; // TypeError !нельзя менять тип!
    ARR[0] = 3; // значение можно менять
    console.log(ARR); // [3,6,7]
}


//2. Стрелочные функции
// Классическое функциональное выражение
{
    let addition = function(a, b) {
        return a + b;
    };

}
{
    // Стрелочная функция
    let addition = (a, b) => a + b;
}

//пример с использованием блока из фигурных скобок:
let arr = ['apple', 'banana', 'orange'];

let breakfast = arr.map(fruit => {
    return fruit + 's';
});

console.log(breakfast); // ['apples', 'bananas', 'oranges']

// внутри стрелочных функций значение this то же самое, что и снаружи (стрелочные функции не имеют своего this)
function Person() {
    this.age = 0;

    setInterval(() => {
        this.age++; // `this` относится к объекту person
    }, 1000);
}

var p = new Person();
let timerId = setInterval(()=>{console.log(p.age); if(p.age ==10) clearInterval(timerId);}, 1000);

//3. Параметры по умолчанию
let getFinalPrice = (price, tax = 0.7) => price + price * tax;
getFinalPrice(500); // 850, так как значение tax не задано

getFinalPrice(500, 0.2); // 600, значение tax по-умолчанию заменяется на 0.2

//4. Spread / Rest оператор (... оператор )

//При использовании в любом итерируемом объекте (iterable), данный оператор "разбивает" ("spread") его на индивидуальные элементы:
function foo(x, y, z) {
    console.log(x, y, z);
}

let arr = [1, 2, 3];
foo(...arr); // 1 2 3

//бъединение набора значений в один массив
function foo(...args) {
    console.log(args);
}
foo(1, 2, 3, 4, 5); // [1, 2, 3, 4, 5]


//5. Расширение возможностей литералов объекта (для инициализации свойств из переменных и определения функциональных методов)
function getCar(make, model, value) {
    return {
        // с синтаксисом короткой записи можно
        // пропускать значение свойства, если оно
        // совпадает с именем переменной, значение
        // которой мы хотим использовать
        make,  // аналогично make: make
        model, // аналогично model: model
        value, // аналогично value: value

        // вычисляемые свойства теперь работают в
        // литералах объекта
        ['make' + make]: true,

        // Короткая запись метода объекта пропускает
        // ключевое слово `function` и двоеточие. Вместо
        // "depreciate: function() {}" можно написать:
        depreciate() {
            this.value -= 2500;
        }
    };
}

let car = getCar('Kia', 'Sorento', 40000);
console.log(car);
// {
//     make: 'Kia',
//     model:'Sorento',
//     value: 40000,
//     makeKia: true,
//     depreciate: function()
// }