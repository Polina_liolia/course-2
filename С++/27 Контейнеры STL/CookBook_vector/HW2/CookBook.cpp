#include <iostream>
#include <cstring>
using std::cout;
using std::cin;
#include "recipe.h"
#include "CookBook.h"


	void CookBook::add_recipe(char *aName, char *aType, char *aProcess)		//to add a recipe
	{
		recipe temp (aName, aType, aProcess);
		ar.push_back(temp);
	}

	void CookBook::change_recipe_name(int recipe_number, char *aName)			//to edit a recipe name
	{
		ar[recipe_number - 1].set_name(aName);
	}

	void CookBook::change_recipe_type(int recipe_number, char *aType)			//to edit a recipe type
	{
		ar[recipe_number - 1].set_type(aType);
	}

	void CookBook::change_recipe_process(int recipe_number, char *aProcess)	//to edit a recipe process
	{
		ar[recipe_number - 1].set_process(aProcess);
	}


	void CookBook::del_recipe(int recipe_number)								//to delete a recipe
	{
		ar.erase(ar.begin() + recipe_number - 1);
	}

	void CookBook::add_ingridient(char *aItem, float aDose, int recipe_number)	//to add an ingridient to a pointed recipe
	{
		ar[recipe_number - 1].set_ingridient(aItem, aDose);
	}

	void CookBook::change_ingridient_item(int recipe_number, int ingridient_number, char* aItem, float aDose)	//to change pointed ingridient of a recipe
	{
		del_recipe_ingridient(recipe_number, ingridient_number);
		add_ingridient(aItem, aDose, recipe_number);
	}

	void CookBook::del_recipe_ingridient(int recipe_number, int ingridient_number)	//to delete pointed ingridient of a pointed recipe
	{
		ar[recipe_number - 1].del_ingridient(ingridient_number);
	}

	int CookBook::save_to_file(char *file_name)const		//to write all data to file
	{
		std::ofstream F(file_name, std::ios::binary);
		// checking if file was successfully opened, othervice generating exception:
		if (!F)
			throw "Can't open file for reading and writing - it must exist";
		//to write a total number of recipes
		size_t sz = ar.size();
		F.write(reinterpret_cast<char*>(&sz), sizeof(size_t));
		//writing an information to file:
		try
		{
			for (int i = 0; i < sz; i++)
				ar[i].save_recipe_to_file(F);
		}
		catch (char *msg)
		{
			cout << msg << "\n";
		}
		F.close();
		return 0;
	}

	int CookBook::read_from_file(char *file_name)		//to read all data from file
	{
		std::ifstream F(file_name, std::ios::binary);
		// checking if file was successfully opened, othervice generating exception:
		if (!F)
			throw "Can't open file for reading and writing - it must exist";
		//to read a total number of recipes
		size_t sz;
		F.read(reinterpret_cast<char*>(&sz), sizeof(size_t));
		//reading an information to file:
		try
		{
			for (int i = 0; i < sz; i++)
			{
				recipe New;
				New.read_recipe_from_file(F);
				ar.push_back(New);
			}
		}
		catch (char *msg)
		{
			cout << msg << "\n";
		}
		F.close();
		return 0;
	}

	void CookBook::print_recipes_list()						//to show the list of recipes
	{
		if (ar.size() == 0)
		{
			cout << "No recipes to show.\n";
			return;
		}
		for (auto i : ar)
			i.print_recipe_name();
	}

	void CookBook::print_recipe_information(int recipe_number)
	{
		ar[recipe_number - 1].print_recipe();
	}

	void CookBook::print_recipe_ingridients(int recipe_number)	//to show the list of ingridients for pointed recipe
	{
		ar[recipe_number - 1].print_ingredients();
	}

