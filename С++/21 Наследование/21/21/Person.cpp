#include "Person.h"

Person::Person()		//constructor
{

}

//overloaded constructor:
Person::Person(String aSurname, String aName, String aPatronymic, short aDay, short aMonth, short aYear, String aPhone, String aUnivercity, String aCountry, String aCity)
{
	surname = aSurname;
	name = aName;
	patronymic = aPatronymic;
	birth.set_day(aDay);
	birth.set_month(aMonth);
	birth.set_year(aYear);
	phone = aPhone;
	univercity = aUnivercity;
	country = aCountry;
	city = aCity;
}

Person::Person(const Person & a)
{
	surname = a.surname;
	name = a.name;
	patronymic = a.patronymic;
	birth = a.birth;
	phone = a.phone;
	univercity = a.univercity;
	country = a.country;
	city = a.city;
}

Person::~Person()		//destructor
{
}

void Person::set_surname(String aSurname)				//setting surname
{
	surname = aSurname;
}

void Person::set_name(String aName)						//setting name
{
	name = aName;
}

void Person::set_patronymic(String aPatronymic)			//setting patronymic
{
	patronymic = aPatronymic;
}

void Person::set_phone(String aPhone)					//setting phone number
{
	phone = aPhone;
}

void Person::set_univercity(String aUnivercity)					//setting univercity
{
	univercity = aUnivercity;
}

void Person::set_country(String aCountry)						//setting a country
{
	country = aCountry;
}

void Person::set_city(String aCity)								//setting a city
{
	city = aCity;

}


int Person::print_to_file(FILE * f, char * file_name)
{
	if (f == nullptr)
		fopen_s(&f, file_name, "ab");				//opens a binar file to write to the end
	if (f == nullptr)								//if file was not opened
		return -1;

	int sz = strlen(surname.get_String()) + 1;
	fwrite(&sz, sizeof(int), 1, f);
	fwrite(surname.get_String(), sz, 1, f);		//to write a surname

	sz = strlen(name.get_String()) + 1;
	fwrite(&sz, 4, 1, f);
	fwrite(name.get_String(), sz, 1, f);		//to write a name

	sz = strlen(patronymic.get_String()) + 1;
	fwrite(&sz, 4, 1, f);
	fwrite(patronymic.get_String(), sz, 1, f);	//to write a patronymic

												//to write a date of birth:
	short b_day = birth.get_day(),
		b_month = birth.get_month(),
		b_year = birth.get_year();

	fwrite(&b_day, 4, 1, f);
	fwrite(&b_month, 4, 1, f);
	fwrite(&b_year, 4, 1, f);

	sz = strlen(phone.get_String()) + 1;
	fwrite(&sz, 4, 1, f);
	fwrite(phone.get_String(), sz, 1, f);	//to write a phone

	sz = strlen(univercity.get_String()) + 1;
	fwrite(&sz, 4, 1, f);
	fwrite(univercity.get_String(), sz, 1, f);	//to write a univercity

	sz = strlen(country.get_String()) + 1;
	fwrite(&sz, 4, 1, f);
	fwrite(country.get_String(), sz, 1, f);	//to write a country

	sz = strlen(city.get_String()) + 1;
	fwrite(&sz, 4, 1, f);
	fwrite(city.get_String(), sz, 1, f);	//to write a city

	fclose(f);
	return 0;
}

void Person::print_data()										//to print information about Person
{
	cout << "Surname: " << surname << "\n";
	cout << "Name: " << name << "\n";
	cout << "Patronymic: " << patronymic << "\n";
	cout << "Date of birth: " << birth.get_day() << "." << birth.get_month() << "." << birth.get_year() << "\n";
	cout << "Phone: " << phone << "\n";
	cout << "Univercity: " << univercity << "\n";
	cout << "Country: " << country << "\n";
	cout << "City: " << city << "\n";
}

bool Person::operator==(Person a)
{
	return surname == a.surname;
}

bool Person::operator!=(Person a)
{
	return surname != a.surname;
}

bool Person::operator>(Person a)
{
	return surname > a.surname;
}

bool Person::operator<(Person a)
{
	return surname < a.surname;
}

bool Person::operator>=(Person a)
{
	return surname >= a.surname;
}

bool Person::operator<=(Person a)
{
	return surname <= a.surname;
}
