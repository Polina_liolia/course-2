var play_button = document.querySelector(".game_field>input");

function startGame()
{
    //hiding play button:
    event.target.style.display="none";
    //getting all elements:
    var scoreField = document.querySelector(".score>span");
    var time = document.querySelector(".time>span");
    var gameOver = document.querySelector(".game_over");
    var face = document.querySelector(".smile");

    //hiding game over field (in case it was shown before):
    gameOver.style.display = "";

    //getting size of face element
    var faceWidth = face.offsetWidth;
    var faceHeight = face.offsetHeight;

    //meaning of game time, frequency of face show and score in the beginning of a game:
    var timeLeft = 30;
    var faceShowInterval = 900;
    var faceShowIntervalDecreasing = 100;
    var score = 0;

    //adding event listener to face image to count clicks:
    function faceClick()
    {
        if (event == undefined)
            return;
        score++;
        scoreField.innerHTML = String(score);
        face.style.content="url('img/kissed.png')";
    }
    face.addEventListener("mousedown", faceClick);

    //showing the face in random place of a game field:
    var faceMove = setInterval(function () {
        face.style.content="url('img/not_kissed.png')";
        face.style.transition = "1s";
        face.style.visibility = "visible";
        //getting game field coordinates (to have min and max coordinates of face appearance):
        var gameField = document.querySelector(".game_field");
        var xMax = gameField.clientWidth - faceWidth;
        var xMin = 0;
        var yMax = gameField.clientHeight - faceHeight;
        var yMin = 0;
        var faceX = Math.floor(Math.random() * (xMax - xMin + 1)) + xMin;
        var faceY = Math.floor(Math.random() * (yMax - yMin + 1)) + yMin;
        //to show face in random place on a game field
        face.style.top = faceY + "px";
        face.style.left = faceX + "px";
        face.style.display = "block";
        faceShowInterval -= faceShowIntervalDecreasing;
    }, faceShowInterval);

    //decreasing game time left every second:
    var gameTime = setInterval(function () {
        timeLeft--;
        time.innerHTML=String(timeLeft);
        //end of game: stop all interval timers and show score message:
        if (timeLeft == 0)
        {
            face.style.transition = "0s";
            face.style.visibility = "hidden";
            clearInterval(gameTime);
            clearInterval(faceMove);
            var scoreMessage = document.querySelector(".game_over>span");
            scoreMessage.innerHTML = String(score);
            gameOver.style.display = "table-cell";
            play_button.style.display="block";
            scoreField.innerHTML="0";
            time.innerHTML = "30";
        }
    }, 1000);
}

play_button.addEventListener("click", startGame);

var about = document.querySelector(".game_field>input:last-of-type");

function showAbout ()
{
    var info = document.querySelector(".about");
    if (getComputedStyle(info).display == "none")
        info.style.display = "table-cell";
    else
        info.style.display = "none";
}

about.addEventListener("click", showAbout);
