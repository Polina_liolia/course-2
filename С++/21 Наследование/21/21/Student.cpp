#include "Student.h"

Student::Student() : Person::Person()	//constructor
{
}

//overloaded constructor:
Student::Student(String aSurname, String aName, String aPatronymic, short aDay, short aMonth, short aYear, String aPhone, String aUnivercity, String aCountry, String aCity, String aGroup)
	: Person::Person(aSurname, aName, aPatronymic, aDay, aMonth, aYear, aPhone, aUnivercity, aCountry, aCity)
{
	group = aGroup;
} 

Student::Student(const Student & a) : Person::Person(a)
{
	group = a.group;
}

Student::~Student()		//destructor
{
}

	
	void Student::set_group(String aGroup)							//setting a group
	{
		group = aGroup;
	}

	int Student::print_to_file(FILE * f, char * file_name)
	{
		if (f == nullptr)
			fopen_s(&f, file_name, "ab");				//opens a binar file to write to the end
		if (f == nullptr)								//if file was not opened
			return -1;

		int sz = strlen(group.get_String()) + 1;
		fwrite(&sz, 4, 1, f);
		fwrite(group.get_String(), sz, 1, f);	//to write a group

		fclose(f);
		return 0;
	}
	 
	void Student::print_data()										//to print information about student
	{
		Person::print_data();
		cout << "Group: " << group << "\n";
	}

	
	