﻿#include "Reservations_system.h"
using std::ofstream;
using std::cerr;

Reservations_system::Reservations_system()	//default constructor
{
	tables_IDs_counter = 0;
	tables_size = 0;
	all_tables = nullptr;
}

Reservations_system::Reservations_system(const Reservations_system & aSystem)	//copy constructor
{
	tables_IDs_counter = aSystem.tables_IDs_counter;
	tables_size = aSystem.tables_size;
	all_tables = new Table[tables_size];
	for (int i = 0; i < tables_size; i++)
		all_tables[i] = aSystem.all_tables[i];
}

Reservations_system::~Reservations_system()	//destructor
{
	delete[] all_tables;
}

int Reservations_system::get_tables_IDs_counter() const	//returns meaning of property tables_IDs_counter
{
	return tables_IDs_counter;
}

int Reservations_system::get_tables_size() const	//returns meaning of property tables_size
{
	return tables_size;
}

Table * Reservations_system::get_all_tables()	//returns pointer on tables array
{
	return all_tables;
}

void Reservations_system::add_table(int aSitting_places)	//to add one more table to array
{
	Table New(++tables_IDs_counter, aSitting_places);
	tables_size++;
	Table *tmp = new Table[tables_size];
	for (int i = 0; i < tables_size - 1; i++)
		tmp[i] = all_tables[i];
	tmp[tables_size - 1] = New;
	delete[] all_tables;
	all_tables = tmp;
}

int Reservations_system::get_table_index_by_ID(int aID) const	//to get table index in array using it's ID (if table not found returns -1)
{
	int index = -1;
	for (int i = 0; i < tables_size; i++)
		if (all_tables[i].get_ID() == aID)
		{
			index = i;
			return index;
		}
	return index;	//if element with such ID was not found returns -1
}

void Reservations_system::remove_table(int aID)	//removes table with pointed ID from array
{
	int index = get_table_index_by_ID(aID);
	if (index == -1)	//if table was not found
	{
		std::cout << "Wrong table ID\n";
		return;
	}
	tables_size--;
	Table *tmp = new Table[tables_size];
	for (int i = 0, j = 0; i < tables_size + 1; i++)
		if (i != index)
			tmp[j++] = all_tables[i];
	delete[] all_tables;
	all_tables = tmp;
}

void Reservations_system::edit_table(int aID, int aSitting_places)	//to change table's number of sitting places  
{
	int index = get_table_index_by_ID(aID);
	if (index == -1)	//if table was not found
	{
		std::cout << "Wrong table ID\n";
		return;
	}
	all_tables[index].set_sitting_places(aSitting_places);
}

//to set or change table status (reserved, free_table, occupied)
void Reservations_system::set_table_status(int aID, table_status aStatus, int aReserve_duration, string aName, string aPhone)
{
	int index = get_table_index_by_ID(aID);
	if (index == -1)	//if table was not found
	{
		std::cout << "Wrong table ID\n";
		return;
	}
	all_tables[index].set_status(aStatus, aReserve_duration, aName, aPhone);
}

//returns number of sitting places for table with pointed ID (if table not found returns -1)
int Reservations_system::get_sitting_places(int aID) const
{
	int index = get_table_index_by_ID(aID);
	if (index == -1)	//if table was not found
	{
		std::cout << "Wrong table ID\n";
		return -1;
	}
	return all_tables[index].get_sitting_places();
}

//returns status of table with pointed ID
table_status Reservations_system::get_status(int aID) const
{
	int index = get_table_index_by_ID(aID);
	if (index == -1)	//if table was not found
	{
		std::cout << "Wrong table ID\n";
		return table_status();
	}
	return all_tables[index].get_status();
}

//returns reserve duration of table with pointed ID (if table not found returns -1)
int Reservations_system::get_reserve_duration(int aID) const
{
	int index = get_table_index_by_ID(aID);
	if (index == -1)	//if table was not found
	{
		std::cout << "Wrong table ID\n";
		return -1;
	}
	return all_tables[index].get_reserve_duration();
}

//returns reservator name of table with pointed ID
string Reservations_system::get_reservator_name(int aID) const
{
	int index = get_table_index_by_ID(aID);
	if (index == -1)	//if table was not found
	{
		std::cout << "Wrong table ID\n";
		return string();
	}
	return all_tables[index].get_reservator_name();
}

//returns reservator phone of table with pointed ID
string Reservations_system::get_reservator_phone(int aID) const
{
	int index = get_table_index_by_ID(aID);
	if (index == -1)	//if table was not found
	{
		std::cout << "Wrong table ID\n";
		return string();
	}
	return all_tables[index].get_reservator_phone();
}

//removes reserve of table with pointed ID 
void Reservations_system::remove_reserve(int aID)
{
	int index = get_table_index_by_ID(aID);
	if (index == -1)	//if table was not found
	{
		std::cout << "Wrong table ID\n";
		return;
	}
	all_tables[index].remove_reserve();
}

//returns true if table is still reserved, false - if not
bool Reservations_system::check_reserve(int aID)
{
	int index = get_table_index_by_ID(aID);
	if (index == -1)	//if table was not found
	{
		std::cout << "Wrong table ID\n";
		return false;
	}
	return all_tables[index].check_reserve();
}

//saves data of Reservations_system's instance in pointed file; if file does not exist, throws exception
void Reservations_system::print_to_file(char * file_name)
{
	ofstream F(file_name, ios::binary);
	// checking if file was successfully opened, othervice generating exception:
	if (!F)
		throw "Can't open file for writing";
	
	int buf = tables_IDs_counter;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//tables_IDs_counter writing
	buf = tables_size;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//tables_size writing
	try
	{
		for (int i = 0; i < tables_size; i++)
			all_tables[i].print_to_file(F);					//all_tables writing
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}
	F.close();
}

void Reservations_system::read_from_file(char * file_name)
{
	ifstream F(file_name, ios::binary);
	// checking if file was successfully opened, othervice generating exception:
	if (!F)
		throw "Can't open file for reading";

	F.read(reinterpret_cast<char*>(&tables_IDs_counter), sizeof(int));	//tables_IDs_counter reading
	F.read(reinterpret_cast<char*>(&tables_size), sizeof(int));	//tables_size reading
	all_tables = new Table[tables_size];
	try
	{
		for (int i = 0; i < tables_size; i++)
			all_tables[i].read_from_file(F);					//all_tables reading
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}
}
