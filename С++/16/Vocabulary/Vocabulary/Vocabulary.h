#pragma once
#include "Element.h"
// �������� ������ Element
class Element;
class Vocabulary{
	Element* root;
	int print_count;
public:
	Vocabulary();
	void add(String En, String Rus, int Counter);
	void print();
	void print_top3_popular();
	void print_top3_unpopular();
	void print_helper_popular(Element * el);
	void print_helper_unpopular(Element * el);
	void print_helper_simmetric(Element *el);
	void print_helper_straight(Element *el);
	void print_helper_reversed(Element *el);
};


