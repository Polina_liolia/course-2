#include"Student.h"
#include"Teacher.h"

//globaly overloaded output of String
ostream& operator << (ostream& os, const String &aString)
{
	if (aString.get_String() != nullptr)
		os << aString.str << "\n";
	return os;
}

//globaly overloaded output of date
ostream& operator << (ostream& os, date &aDate)
{
	os << aDate.day << "." << aDate.month << "." << aDate.year << "\n";
	return os;
}

//globaly overloaded output of Person
ostream& operator << (ostream& os, const Person &aSt)
{
	os << "Surname: " << aSt.surname << "\n";
	os << "Name: " << aSt.name << "\n";
	os << "Patronymic: " << aSt.patronymic << "\n";
	os << "Date of birth: " << aSt.birth.get_day() << "." << aSt.birth.get_month() << "." << aSt.birth.get_year() << "\n";
	os << "Phone: " << aSt.phone << "\n";
	os << "Univercity: " << aSt.univercity << "\n";
	os << "Country: " << aSt.country << "\n";
	os << "City: " << aSt.city << "\n";
	return os;
}

//globaly overloaded output of Student
ostream& operator << (ostream& os, const Student &aSt)
{
	os << "Surname: " << aSt.surname << "\n";
	os << "Name: " << aSt.name << "\n";
	os << "Patronymic: " << aSt.patronymic << "\n";
	os << "Date of birth: " << aSt.birth.get_day() << "." << aSt.birth.get_month() << "." << aSt.birth.get_year() << "\n";
	os << "Phone: " << aSt.phone << "\n";
	os << "Univercity: " << aSt.univercity << "\n";
	os << "Country: " << aSt.country << "\n";
	os << "City: " << aSt.city << "\n";
	os << "Group: " << aSt.group << "\n";
	return os;
}

//globaly overloaded output of Teacher
ostream& operator << (ostream& os, const Teacher &aSt)
{
	os << "Surname: " << aSt.surname << "\n";
	os << "Name: " << aSt.name << "\n";
	os << "Patronymic: " << aSt.patronymic << "\n";
	os << "Date of birth: " << aSt.birth.get_day() << "." << aSt.birth.get_month() << "." << aSt.birth.get_year() << "\n";
	os << "Phone: " << aSt.phone << "\n";
	os << "Univercity: " << aSt.univercity << "\n";
	os << "Country: " << aSt.country << "\n";
	os << "City: " << aSt.city << "\n";
	os << "Speciality: " << aSt.speciality << "\n";
	return os;
}

void main()
{
	Student FirstS("Ivanov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "25-ZS-55");
	Student SecondS("Petrov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "25-ZS-56");
	Student ThirdS("Kozlov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "25-ZS-57");
	Student FourthS("Abramov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "25-ZS-58");
	Student FifthS("Semenov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "25-ZS-59");
	FifthS = FirstS;

	Teacher FirstT("Verhov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "history");
	Teacher SecondT("Blinov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "math");
	Teacher ThirdT("Mironov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "biology");
	Teacher FourthT("Lisov", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "chemistry");
	Teacher FifthT("Sergeev", "Ivan", "Petrovich", 15, 2, 1992, "+38(067)589-65-21", "Karazina", "Ukraine", "Kharkiv", "English");
	FifthT = FirstT;

	cout << "Students: \n";
	cout << FirstS << SecondS << ThirdS << FourthS << FifthS;

	cout << "_______________________________________________________________\n";

	cout << "Teachers: \n";
	cout << FirstT << SecondT << ThirdT << FourthT << FifthT;

}