#pragma once

class Element{
	int value;
	Element* next;

public:
	Element();
	Element(int aValue);
	void set_value(int aValue);
	int get_value();
	void set_next(Element* aNext);
	Element* get_next();
	
};