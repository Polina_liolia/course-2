#include <memory>
#include <iostream>
//#include <vector>

using namespace std;

auto_ptr<int> fun_test(){
	auto_ptr<int> t(new int);
 	*t = 200;
	cout << *t << endl;
	return t;

}

int main(int argc, char **argv)
{
	auto_ptr<int> z = fun_test();
	cout << *z << endl;
    return 0;
}