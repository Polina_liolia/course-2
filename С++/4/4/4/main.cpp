#include "Header.h"

class A
{
private:
	int a = 9;
	const int b = 10;
	const int c;

public:
	A(int ax, int bx, int cx) : a(ax), b(bx), c(cx)
	{

	}

public:
	void print()
	{
		std::cout << a << " " << b << " " << c << "\n";
	}
};

void main()
{
	A test(15, 6, 8);
	test.print();
}