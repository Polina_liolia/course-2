#include"Log.h"
using std::ostream;

//globaly overloaded output for String
ostream& operator << (ostream& os, const String &aString)
{
	if (aString.get_String() != nullptr)
		os << aString.str << " ";
	return os;
}


//globaly overloaded output for UniVar
ostream& operator << (ostream& os, const UniVar &aVar)
{
	if (aVar.value == Int) 
		os << aVar.Integer << " ";
	else if (aVar.value == Fl)
		os << aVar.Float << " ";
	else if (aVar.value == St)
		os << aVar.Str << " ";
	return os;
}

//initializing of static property:
shared_ptr<Log> Log::l(new Log);


void main()
{
	shared_ptr<Log> my_log(Log::getInstance());
	my_log->add_reg(12);
	my_log->add_reg(32.4f);
	my_log->add_reg((String)"Some_reg");
	my_log->add_reg(55.36f);
	my_log->add_reg(8);
	my_log->add_reg((String)"Some_reg2");
	my_log->add_reg(188);

	for (int i = 0; i < my_log->get_reg_size(); i++)
		std::cout << my_log->get_reg()[i] << ", ";
	std::cout << "\n";

}