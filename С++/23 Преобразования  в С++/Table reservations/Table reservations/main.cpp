#include "Reservations_system.h"
#include <Windows.h>
#include <conio.h>
using std::ostream;
using std::istream;
using std::string;
using std::cout;


//globaly overloaded output for Time
ostream& operator<<(ostream& os, Time& t)
{
	os << "Hours: " << t.hours << "\n";
	os << "Minutes: " << t.minutes << "\n";
	return os;
}

//globaly overloaded output for Reserve
ostream& operator<<(ostream& os, Reserve& r)
{
	os << "Reserve start:\n" << r.reserve_start;
	os << "Reserve duration (min) - " << r.reserve_duration_min << "\n";
	os << "Reservator name " << r.reservator_name << "\n";
	os << "Reservator phone " << r.reservator_phone << "\n";
	return os;
}

//globaly overloaded output for Table
ostream& operator<<(ostream& os, Table& t)
{
	os << "ID " << t.ID << "\n";
	os << "Sitting places " << t.sitting_places << "\n";
	os << "Status ";
	switch (t.status)
	{
	case 0: os << "reserved" << "\n"; break;
	case 1: os << "free_table" << "\n"; break;
	case 2: os << "occupied" << "\n"; break;
	}
	os << "Reserve data: \n" << t.reserve_data;
	return os;
}


//globaly overloaded output for Reservations_system
ostream& operator<<(ostream& os, Reservations_system& rs)
{
	os << "Tables IDs counter " << rs.tables_IDs_counter <<"\n";
	os << "Tables size " << rs.tables_size << "\n";
	os << "All tables: \n";
	for (int i = 0; i < rs.tables_size; i++)
		os << i+1 << ". " << rs.all_tables[i];
	return os;
}


void main()
{
	cout << "Testing method of defining current time:\n";
	Time now;
	now.set_current_time();
	cout << now.get_hours() << " : " << now.get_minutes() << "\n";
	cout << "Current time + 5 minutes:\n";
	now = now + 5;
	cout << now.get_hours() << " : " << now.get_minutes() << "\n";
	

	Reservations_system Kitchen;	//creating an instance of Reservation system
	cout << "Adding tables to Reservation system.\n";
	Kitchen.add_table(4);
	Kitchen.add_table(6);
	Kitchen.add_table(4);
	Kitchen.add_table(2);
	Kitchen.add_table(6);
	Kitchen.add_table(8);
	Kitchen.add_table(10);

	cout << "Removing table with ID 7.\n";
	Kitchen.remove_table(7);

	cout << "Trying to remove table twice:\n";
	Kitchen.remove_table(7);

	std::cout << "Reserving table with ID 1 for 1 minute.\n";
	Kitchen.set_table_status(1, reserved, 1, "Alisa", "066-965-85-74");
	
	cout << "Printing all data to file (streams are used)\n";
	try
	{
		Kitchen.print_to_file("Reserve.bin");
	}
	catch (char *msg)	//if exception has been thrown, outputting error message
	{
		cout << msg << "\n";
	}

	cout << "Reading all data to file (streams are used)\n";
	Reservations_system Readen;
	try
	{
		Readen.read_from_file("Reserve.bin");
	}
	catch (char *msg)	//if exception has been thrown, outputting error message
	{
		cout << msg << "\n";
	}

	cout << "Outputting readen instance:\n";
	cout << Readen;

	cout << "Checking reserve:\n";
	cout << "Waiting 30 seconds...\n";
	Sleep(30000);//sleep 30 sec
	cout << "Checking if table with ID 1 reserved after 30 seconds:\n";
	if (Kitchen.check_reserve(1))
		cout << "Table is reserved\n";
	else
		cout << "Table is free\n";
	cout << "Checking if table with ID 1 reserved after 2 minutes:\n";
	cout << "Waiting 2 minutes...\n";
	Sleep(120000);//sleep 2 min
	if (Kitchen.check_reserve(1))
		cout << "Table is reserved\n";
	else
		cout << "Table is free\n";
}