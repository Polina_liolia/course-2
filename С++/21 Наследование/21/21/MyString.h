#pragma once
#include <iostream>
using std::ostream;

class String
{
private:
	int sz;
	char* str;

public:
	String();									
	String(char *aStr);							
	String(const String &aString);				
	~String();

	

public:
	void set_String(char *aStr);
	char *get_String()const;
	int get_Stringlen()const;
	String operator=(const String &aString);	
	String operator+=(const String &aString);	
	String operator+=(char a);	
	String operator*=(const String &aString);	
	String operator*=(char *aStr);
	String operator+(const String &aString);	
	String operator+(char *aStr);				
	String operator*(const String &aString);	
	String operator*(char *aStr);
	String operator/(const String &aString);	
	String operator/(char *aStr);
	String operator/=(const String &aString);	
	String operator/=(char *aStr);
	bool operator==(const String &aString);		
	bool operator==(char *aStr);				
	bool operator!=(const String &aString);		
	bool operator!=(char *aStr);				
	bool operator>(const String &aString);		
	bool operator>(char *aStr);					
	bool operator>=(const String &aString);		
	bool operator>=(char *aStr);
	bool operator<(const String &aString);			
	bool operator<(char *aStr);	
	bool operator<=(const String &aString);		
	bool operator<=(char *aStr);	
	void print();
	char& operator[](int aIndex);
	operator int ()const;
	operator double()const;
	
	friend ostream & operator << (ostream& os, const String &aString);
};
