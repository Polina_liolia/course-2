#include <iostream>
#include <cstring>
#include "ingredient.h"
#include "recipe.h"
using std::cout;
using std::cin;

	recipe::recipe()
	{
		name = nullptr;
		type = nullptr;
		process = nullptr;
		sz_components = 0;
		components = nullptr;
	}

	recipe::recipe(char *aName, char *aType, char *aProcess)
	{
		if (aName && strlen(aName)) 
		{
			name = new char[strlen(aName) + 1];
			strcpy_s(name, strlen(aName) + 1, aName);
		}
		else 
			name = nullptr;
		
		if (aType && strlen(aType))
		{
			type = new char[strlen(aType) + 1];
			strcpy_s(type, strlen(aType) + 1, aType);
		}
		else
			type = nullptr;

		if (aProcess && strlen(aProcess))
		{
			process = new char[strlen(aProcess) + 1];
			strcpy_s(process, strlen(aProcess) + 1, aProcess);
		}
		else
			process = nullptr;

		sz_components = 0;
		components = nullptr;
	}

	recipe::~recipe()
	{
		delete[] name;
		delete[] type;
		delete[] process;
		delete[] components;
	}

	inline char *recipe::get_name()					//inline function to get a name
	{
		return name;
	}

	inline char *recipe::get_type()					//inline function to get a type of a dish
	{
		return type;
	}

	inline char *recipe::get_process()					//inline function to get a process of cooking a dish
	{
		return process;
	}

	void recipe::set_name(char *aName)			//setting a name of a dish
	{
		delete[] name;
		int sz = strlen(aName) + 1;
		name = new char[sz];
		strcpy_s(name, sz, aName);
	}


	void recipe::set_type(char *aType)			//setting a type of a dish
	{
		delete[] type;
		int sz = strlen(aType) + 1;
		type = new char[sz];
		strcpy_s(type, sz, aType);
	}

	void recipe::set_process(char *aProcess)	//setting a process of cooking a dish
	{
		delete[] process;
		int sz = strlen(aProcess) + 1;
		process = new char[sz];
		strcpy_s(process, sz, aProcess);
	}

	void recipe::set_ingridient(char *aItem, float aDose)	//setting an components (items and dose) for a dish
	{
		ingredient **temp = new ingredient*[(sz_components + 1)];
		for (int i = 0; i < sz_components; i++)
			temp[i] = components[i];
		temp[sz_components] = new ingredient (aItem, aDose);
		if (sz_components != 0)
			delete[] components;
		sz_components++;
		components = new ingredient*[sz_components];
		for (int i = 0; i < sz_components; i++)
			components[i] = temp[i];
		delete[] temp;
	}

	void recipe::del_ingridient(int ingredient_number)	//to delete pointed ingridient
	{
		if (ingredient_number >= sz_components)			//checking the correctness of ingredient_number input
		{
			cout << "No ingridient with such a number found.\n";
			return;
		}
		ingredient **temp = new ingredient*[(sz_components - 1)];
		for (int i = 0, j = 0; i < sz_components; i++)		//moving all elements exept element to delete to a temporary array
		{
			if (i == ingredient_number)						//except deleted ingredient
				continue;
			temp[j] = components[i];
			j++;
		}
		delete[] components;
		sz_components--;
		components = new ingredient*[sz_components];
		for (int i = 0; i < sz_components; i++)
			components[i] = temp[i];
		delete[] temp;
	}


	int recipe::save_recipe_to_file(FILE *f, char *file_name)
	{
		if (f == nullptr)
			fopen_s(&f, file_name, "ab");					//opens a binar file to write to the end
		if (f == nullptr)								//if file was not opened
			return -1;
		
		int sz = strlen(name) + 1;
		fwrite(&sz, sizeof(int), 1, f);
		fwrite(name, sz, 1, f);						//to write a recipe name

		sz = strlen(type) + 1;
		fwrite(&sz, 4, 1, f);
		fwrite(type, sz, 1, f);						//to write a recipe process

		sz = strlen(process) + 1;
		fwrite(&sz, 4, 1, f);
		fwrite(process, sz, 1, f);					//to write a recipe process


		fwrite(&sz_components, 4, 1, f);			//writing a total amount of components

		for (int i = 0; i < sz_components; i++)
		{
			int sz = strlen(components[i][0].item) + 1;
			fwrite(&sz, 4, 1, f);						//writing a size of string
			fwrite(components[i][0].item, sz, 1, f);	//writing an item
			fwrite(&components[i][0].dose, 4, 1, f);	//writing a dose
		}
		fclose(f);
		return 0;
	}

	void recipe::print_recipe_name()							//to print recipe name
	{
		cout << name << "\n";
	}

	void recipe::print_recipe()									//to print all recipe information
	{
		cout << "Recept name: " << name << "\n";
		cout << "Type: " << type << "\n";
		cout << "Cooking process: " << process << "\n";
		cout << "Ingridients:\n";
		for (int i = 0; i < sz_components; i++)
			cout << (i+1) << ". " << components[i][0].item << " - " << components[i][0].dose << "g\n";
	}

	void recipe::print_ingredients()							//to print the list of components
	{
		for (int i = 0; i < sz_components; i++)
			cout << (i + 1) << ". " << components[i][0].item << " - " << components[i][0].dose << "g\n";
	}
