#pragma once
#include "Element.h"
class DoubleLinkList
{
	Element* head;
	Element* tail;
public:
	DoubleLinkList();
	DoubleLinkList(const DoubleLinkList &aList);
	void add(Department aValue);
	void show();
	void show_reverse();
	bool delByIndex(unsigned int index);
	Department &get_value_ByIndex(unsigned int index);
	void add_in_order(Department value);
	~DoubleLinkList();
	bool insertByIndex(int index, Department value);
	bool del_package(int index, int number);	//int number - total number of elements to delete

};