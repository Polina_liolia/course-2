#include "Header.h"

//Checks, is the horisontal groupe of "1" separated from other groups with only zeros
bool separate_horisontal_location_check(int **arr, int str, int col, int start_col, int end_col, int start_str, int end_str)
{
	//checking elements before and after the group in the current string
	if (start_col == 0 && end_col != col - 1)
	{
		if (arr[start_str][end_col + 1] != 0)
			return 0;
	}
	else if (end_col == col - 1 && start_col != 0)
	{
		if (arr[start_str][start_col - 1] != 0)
			return 0;
	}
	else if (start_col != 0 && end_col != col - 1)
	{
		if (arr[start_str][start_col - 1] != 0 || arr[start_str][end_col + 1] != 0)
			return 0;
	}
	//checking elements in the previous and next strings
	int k = 0;
	int lim = 0;
	if (start_str != 0 && start_str != str - 1)
	{
		k = start_str - 1;
		lim = start_str + 1;
	}
	if (start_str == 0)
	{
		k = start_str + 1;
		lim = start_str + 1;
	}
	if (start_str == str - 1)
	{
		k = start_str - 1;
		lim = start_str - 1;
	}
	for (int m = k; m <= lim; m++)
	{
		for (int l = start_col; l <= end_col; l++)
		{
			if (m == start_str)//if it is the element of a current string
				break;
			if (arr[m][l] == 0)//if the group is surrounded with '0'
				continue;
			else//if the group is surrounded with other element (not zero)
				return 0;
		}
	}
	return 1;//if the group is surrounded with only zeros
}

//Checks, is the vertical group of "1" separated from other groups with only zeros
bool separate_vertical_location_check(int **arr, int str, int col, int start_col, int end_col, int start_str, int end_str)
{
	//checking elements before and after the group in the current column
	if (start_str == 0 && end_str != str - 1)
	{
		if (arr[end_str + 1][start_col] != 0)
			return 0;
	}
	else if (end_str == str - 1 && start_str != 0)
	{
		if (arr[start_str - 1][start_col] != 0)
			return 0;
	}
	else if (start_str != 0 && end_str != str - 1)
	{
		if (arr[start_str - 1][start_col] != 0 || arr[end_str + 1][start_col] != 0)
			return 0;
	}
	//checking elements in the previous and next columns
	int k = 0;
	int lim = 0;
	if (start_col != 0 && start_col != col - 1)
	{
		k = start_col - 1;
		lim = start_col + 1;
	}
	if (start_col == 0)
	{
		k = start_col + 1;
		lim = start_col + 1;
	}
	if (start_col == col - 1)
	{
		k = start_col - 1;
		lim = start_col - 1;
	}
	for (int l = start_str; l <= end_str; l++)
	{
		for (int m = k; m <= lim; m++)
		{
			if (m == start_col)//if it is the element of a current column
				break;
			if (arr[l][m] == 0)//if the group is surrounded with '0'
				continue;
			else//if the group is surrounded with other element (not zero)
				return 0;
		}
	}
	return 1;//if the group is surrounded with only zeros
}

//Checks, is the group of "1" (horisontal or vertical) separated from other groups with only zeros
bool separate_location_check(int **arr, int str, int col, int start_col, int end_col, int start_str, int end_str)
{
	bool flag = false;
	if (start_col == end_col)//if it is a vertical group
		flag = separate_vertical_location_check(arr, str, col, start_col, end_col, start_str, end_str);
	else if (start_str == end_str)//if it is a horisontal group
		flag = separate_horisontal_location_check(arr, str, col, start_col, end_col, start_str, end_str);
	else
		cout << "A wrong coordinates!\n";
	return flag;
}

//Places the "boat" on a field
void place_boat(int **arr, int start_col, int end_col, int start_str, int end_str)
{
	if (start_str == end_str)//for horisontal lines
	{
		int sum_ones = end_col - start_col + 1;//calculating the amount of "1" in a group
		for (int k = start_col; k <= end_col; k++)//changing every "1" on the total amount of them in a group
			arr[start_str][k] = sum_ones;
	}

	if (start_col == end_col)//for vertical lines
	{
		int sum_ones = end_str - start_str + 1;//calculating the amount of "1" in a group
		for (int k = start_str; k <= end_str; k++)//changing every "1" on the total amount of them in a group
			arr[k][start_col] = sum_ones;
	}
}

/*������� �������� ������� ������ ������ (�������������� ��� ������������), ���������� ������,
� �������� �� �� ����� ������*/
void change_ones_group(int** arr, int str, int col)
{
	bool flag = true;
	int start_str = 0,//an index of the string of a first element of a group
		start_col = 0,//an index of the column of a first element of a group
		end_str = 0,//an index of the string of the last element of a group
		end_col = 0;//an index of the column of the last element of a group
					//searching for horisontal lines
	for (int i = 0; i<str; i++)
		for (int j = 0; j < col - 1; j++)
		{
			if (arr[i][j] == 1 && arr[i][j + 1] == 1)//if the minimal 2-elements horisontal group was found 
			{
				start_col = j;
				start_str = i;
				end_str = i;
				for (int k = j + 1; k < col; k++)//searching for the end of this group
					if (arr[i][k] == 1)
					{
						if (k == col - 1)
						{
							end_col = k;
							break;
						}
						else
							continue;
					}
					else
					{
						end_col = k - 1;
						break;
					}
			}
			else
				continue;
			//checking, is the group separated with zeros:
			flag = separate_location_check(arr, str, col, start_col, end_col, start_str, end_str);
			if (flag == true)//if the group is separated with zeros
				place_boat(arr, start_col, end_col, start_str, end_str); //changing every "1" with amount of elements in a group
			j = end_col;//turning to the next element to check
			start_col = 0;
			end_col = 0;
			start_str = 0;
			end_str = 0;
		}
	//searching for vertical lines
	flag = true;
	for (int j = 0; j < col; j++)
		for (int i = 0; i<str - 1; i++)
		{
			if (arr[i][j] == 1 && arr[i + 1][j] == 1)//if the minimal 2-elements vertical group was found 
			{
				start_str = i;
				start_col = j;
				end_col = j;
				for (int k = i + 1; k < str; k++)//searching for the end of this group
					if (arr[k][j] == 1)
					{
						if (k == str - 1)
						{
							end_str = k;
							break;
						}
						else
							continue;
					}
					else
					{
						end_str = k - 1;
						break;
					}
			}
			else
				continue;
			//checking, is the group separated with zeros:
			flag = separate_location_check(arr, str, col, start_col, end_col, start_str, end_str);
			if (flag == true)//if the group is separated with zeros
				place_boat(arr, start_col, end_col, start_str, end_str);//changing every "1" with amount of elements in a group
			i = end_str;//turning to the next element to check
			start_str = 0;
			end_str = 0;
			start_col = 0;
			end_col = 0;
		}
}

//������� ���������� ������������ �������� �� ���� �������� ���.
void rand_place_boats(int** arr, int str, int col)
{
	int count = 0;//counter of placed boats
	int start_str = 0;
	int end_str = 0;
	int start_col = 0;
	int end_col = 0;
	while (count < 10)//while all boats are not placed
	{
		int decks = 0;//amount of decks of a current boat
		switch (count)
		{
		case 0: decks = 4; break;
		case 1: case 2: decks = 3; break;
		case 3: case 4: case 5: decks = 2; break;
		case 6: case 7: case 8: case 9: decks = 1; break;
		}
		bool orientation = rand() % (1 - 0 + 1) + 0;//random orientation of a boat choosing
		int start = rand() % (9 - 0 + 1) + 0;//an index of the first deck of a boat
		int end = start + decks - 1;//an index of the last deck of a boat
		if (end >= 10)//if an index of the last deck of a boat is out of a field
			continue;
		int other_coordinate = rand() % (9 - 0 + 1) + 0;//an index of a string or column, where the boat is going to be situated
		if (orientation)//if horisontal boat will be placed
		{
			start_str = other_coordinate;
			end_str = start_str;
			start_col = start;
			end_col = end;
		}
		else //if vertical boat will be placed
		{
			start_col = other_coordinate;
			end_col = start_col;
			start_str = start;
			end_str = end;
		}
		if (separate_location_check(arr, str, col, start_col, end_col, start_str, end_str))//if there are no other boats around
		{
			place_boat(arr, start_col, end_col, start_str, end_str);//placing a boat on the field
			count++;//counting boats placed
		}
	}
}


//������� ������������ �������� ������������� �� ���� �������� ���.
void user_places_boats(int** arr, int str, int col)
{
	int count = 0;//counter of placed boats
	int start_str = 0;
	int end_str = 0;
	int start_col = 0;
	int end_col = 0;
	while (count < 10)//while all boats are not placed
	{
		int decks = 0;//amount of decks of a current boat
		switch (count)//choosing the amount of decks for current boat
		{
		case 0: decks = 4; break;
		case 1: case 2: decks = 3; break;
		case 3: case 4: case 5: decks = 2; break;
		case 6: case 7: case 8: case 9: decks = 1; break;
		}
		bool orientation = 0;//orientation of a boat
		cout << "Input the orientation of " << decks << "-decked boat:\n 1 = horisontal;\n0-vertical.\n";
		cin >> orientation;
		int start = 0;//an index of the first deck of a boat
		int end = 0;//an index of the last deck of a boat
		cout << "Input an index of the first deck of this boat (0-9)\n";
		do
		{
			cin >> start;
			end = start + decks - 1;
		} while (start < 0 || start>9 || end >= 10);
		int other_coordinate = 0;//an index of a string or column, where the boat is going to be situated
		cout << "Input an index of a string or column, where the boat is going to be situated (0-9)\n";
		do
			cin >> other_coordinate; while (other_coordinate < 0 || other_coordinate > 9);
		if (orientation)//if horisontal boat will be placed
		{
			start_str = other_coordinate;
			end_str = start_str;
			start_col = start;
			end_col = end;
		}
		else //if vertical boat will be placed
		{
			start_col = other_coordinate;
			end_col = start_col;
			start_str = start;
			end_str = end;
		}
		if (separate_location_check(arr, str, col, start_col, end_col, start_str, end_str))//if there are no other boats around
		{
			place_boat(arr, start_col, end_col, start_str, end_str);//placing a boat on the field
			count++;//counting boats placed
			cout << "Your boat has been succesfully placed on a field.\n";
		}
		else
			cout << "You can't place the boat here. Please choose another location.\n";
	}
}
