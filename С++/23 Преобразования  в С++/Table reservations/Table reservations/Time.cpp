#include "Time.h"
#include <iostream>

Time::Time()	//default constructor
{
	hours = 0;
	minutes = 0;
}

Time::Time(int aHours, int aMinutes)	//overloaded constructor for initial setting of hours and minutes
{
	hours = aHours;
	minutes = aMinutes;
}

void Time::set_hours(int aHours)	//a method to set or change hours
{
	if (aHours > 23)
		hours = aHours - 24;	//to aviod setting a wrong time
	hours = aHours;
}

void Time::set_minutes(int aMinutes)	//a method to set or change minutes
{
	if (aMinutes > 59)	//to avoid setting a wrong time
	{
		hours += static_cast<int>(aMinutes / 60);
		minutes = aMinutes % 60;
	}
	minutes = aMinutes;
}

void Time::set_time_in_minutes(int time_minutes)	//a method to set or change time, converted from minutes
{
	hours = static_cast<int> (time_minutes / 60);
	minutes = time_minutes % 60;
}

int Time::get_hours() const	//to get meaning of property hours
{
	return hours;
}

int Time::get_minutes() const	//to get meaning of property minutes
{
	return minutes;
}

int Time::get_time_in_minutes() const	//to get time, converted to minutes
{
	return (hours*60 + minutes);
}

void Time::set_current_time()
{
	struct tm newtime;
	__time64_t long_time;
	errno_t err;

	// Get time as 64-bit integer: 
	_time64(&long_time);
	// Convert to local time:
	err = _localtime64_s(&newtime, &long_time);
	if (err)
	{
		std::cout << "Invalid argument to _localtime64_s\n";
		return;
	}
	//setting current time to class properties:
	hours = newtime.tm_hour;
	minutes = newtime.tm_min;
}

//saves data of Time instance to file; takes the reffer of ofstream instance (file has to be already opened)  
void Time::print_to_file(ofstream & F) const
{
	if (!F)
		throw "Time: file was not opened, can't write";
	int var = hours;
	F.write(reinterpret_cast<char*>(&var), sizeof(int));	//writing hours
	var = minutes;
	F.write(reinterpret_cast<char*>(&var), sizeof(int));	//writing minutes
}

//reads data of Time instance from file; takes the reffer of ofstream instance (file has to be already opened)  
void Time::read_from_file(ifstream & F)
{
	if (!F)
		throw "Time: file was not opened, can't read";
	F.read(reinterpret_cast<char*>(&hours), sizeof(int));	//reading hours
	F.read(reinterpret_cast<char*>(&minutes), sizeof(int));	//reading minutes
}

Time Time::operator=(const Time & aTime)
{
	hours = aTime.hours;
	minutes = aTime.minutes;
	return *this;
}

Time Time::operator+=(int aMinutes)
{
	int time_in_minutes = hours * 60 + minutes;
	time_in_minutes += aMinutes;
	hours = static_cast<int> (time_in_minutes / 60);
	minutes = time_in_minutes % 60;
	return *this;
}

Time Time::operator+(int aMinutes)	//to add minutes to current time
{
	Time tmp;
 	int time_in_minutes = hours * 60 + minutes;
	time_in_minutes += aMinutes;
	tmp.hours = static_cast<int> (time_in_minutes / 60);
	tmp.minutes = time_in_minutes % 60;
	return tmp;
}

Time Time::operator-(int aMinutes)	//to deduct minutes from current time
{
	Time tmp;
	int time_in_minutes = hours * 60 + minutes;
	time_in_minutes -= aMinutes;
	tmp.hours = static_cast<int> (time_in_minutes / 60);
	tmp.minutes = time_in_minutes % 60;
	return tmp;
}

//overloaded compare operators for class Time:

bool Time::operator > (const Time &aTime)
{
	return (hours * 60 + minutes) > (aTime.hours * 60 + aTime.minutes);
}

bool Time::operator < (const Time &aTime)
{
	return (hours * 60 + minutes) < (aTime.hours * 60 + aTime.minutes);
}

bool Time::operator >= (const Time &aTime)
{
	return (hours * 60 + minutes) >= (aTime.hours * 60 + aTime.minutes);
}

bool Time::operator <= (const Time &aTime)
{
	return (hours * 60 + minutes) <= (aTime.hours * 60 + aTime.minutes);
}