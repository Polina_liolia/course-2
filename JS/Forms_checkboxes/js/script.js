var form = document.forms.check_input;
//getting all checkboxes:
var checkbox1 = form.elements.first_and_second;
var checkbox2 = form.elements.second_and_third;
var checkbox3 = form.elements.first_only;
//getting all input text fields:
var text1 =  form.elements.first;
var text2 = form.elements.second;
var text3 = form.elements.third;
//getting all error fields:
var errorField1 = document.querySelector(".message>p:first-of-type");
var errorField2 = document.querySelector(".message>p:nth-of-type(2)");
var errorField3 = document.querySelector(".message>p:last-of-type");

var marks = document.querySelector(".marks");

function markSelected() {
    var target = event.target;
    if (target.tagName != "INPUT")
        return;
    //if first checkbox changed:
    if (target == checkbox1 && target.checked == true)
    {
       text1.disabled = false;
       text2.disabled = false;
    }
    if (target == checkbox1 && target.checked == false)
    {
        if (checkbox3.checked != true)
            text1.disabled = true;
        if (checkbox2.checked != true)
            text2.disabled = true;
    }

    //if second checkbox changed:
    if (target == checkbox2 && target.checked == true)
    {
        text2.disabled = false;
        text3.disabled = false;
    }
    if (target == checkbox2 && target.checked == false)
    {
        if (checkbox1.checked != true)
            text2.disabled = true;
        text3.disabled = true;
    }

    //if third checkbox changed:
    if (target == checkbox3 && target.checked == true)
        text1.disabled = false;
    if (target == checkbox3 && target.checked == false
        && checkbox1.checked != true )
        text1.disabled = true;
}

marks.addEventListener("change", markSelected);

//to avoid user to get out from unfilled input:
function checkBlur()
{
    var target = event.target;
    if (target.tagName != "INPUT")
        return;
    if (target.disabled == false && target.value == "") {
        target.focus();
        if (target == text1)
            errorField1.innerText = "Error: first field is empty!";
        else if (target == text2)
            errorField2.innerText = "Error: second field is empty!";
        else if (target == text3)
            errorField3.innerText = "Error: third field is empty!";
    }
    else
    {
        if (target == text1)
            errorField1.innerText = "";
        else if (target == text2)
            errorField2.innerText = "";
        else if (target == text3)
            errorField3.innerText = "";
    }
}

form.addEventListener("blur", checkBlur, true);

//to avoid sending an empty form on server (if user hadn't focus any input):
function checkInput() {
    if (text1.disabled == false && text1.value == "")
    {
        errorField1.innerText = "Error: first field is empty!";
        event.preventDefault();
    }
    else if (text1.value != "" || text1.disabled == true)
        errorField1.innerText = "";

    if (text2.disabled == false && text2.value == "")
    {
        errorField2.innerText = "Error: second field is empty!";
        event.preventDefault();
    }
    else if (text2.value != "" || text2.disabled == true)
        errorField2.innerText = "";

    if (text3.disabled == false && text3.value == "")
    {
        errorField3.innerText = "Error: third field is empty!";
        event.preventDefault();
    }
    else if (text3.value != "" || text3.disabled == true)
        errorField3.innerText = "";

    if (text1.disabled == true && text2.disabled == true && text3.disabled == true) {
        event.preventDefault();
        errorField1.innerText = "No data to send!";
    }
}

form.addEventListener("submit", checkInput);
