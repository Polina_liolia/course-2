#include "Header.h"

// ������� ���������� �������� � ������������� ������� ���� double
void add_el(double** p_arr, int &sz, double el)
{
	sz++;//adding one element
	double *p1_arr = new double[sz];//new array with one additional element
	for (int i = 0; i < sz; i++)
	{
		if (i < sz - 1)//replacing all elements from the basic array to the new one
			*(p1_arr + i) = (*p_arr)[i];
		if (i == sz - 1)//adding one element to the new array
			*(p1_arr + i) = el;
	}
	*(p_arr) = p1_arr;//inputting a pointer on the new array to the pointer, wich is a function argument
}

//����������� ���� �������� ���� double. �������� ������� ������� ����������� � �������.
void add_arr(double** p_arr1, int &sz1, double* p_arr2, int sz2)
{
	int sz = sz1 + sz2;//calculaiting the size of a new integraited array
	double *p_arr = new double[sz];//creating a new array
	for (int i = 0, j = 0; i < sz && j<sz2; i++)
	{
		if (i < sz1)//inputting all elements of the first array to the beginning of a new one
			*(p_arr + i) = (*p_arr1)[i];
		else//inputting all elements of the second array to the end of a new one
		{
			*(p_arr + i) = *(p_arr2 + j);
			j++;
		}
	}
	sz1 = sz;//copying the size of a new integraited array
	*(p_arr1) = p_arr;////inputting a pointer on the new integraited array to the pointer on the first array
}

//������ ���� ������ ��������� ������
void zerochange(int *arr, int sz)
{
	for (int a = 0; a<sz; a++)
		if (arr[a] % 2 == 0)
			arr[a] = 0;
}

/*������� ��������� ������ � �������� ������� ��������.
���������� ������� ���������� �������� � ������� �� ���������� ���������.*/
int el_amount(int *arr, int sz, int min, int max)
{
	if (min > max)
		cout << "Error! Minimal element is bigger than maximal.\n";
	int el_amount = 0;
	for (int i = 0; i < sz; i++)
	{
		if (arr[i] >= min && arr[i] <= max)
			el_amount++;
	}
	return el_amount;
}

//��������� ������� ������ � ������� ���� ��������, ������� 4 � ����� ���������.
void div4(int *arr, int sz)
{
	cout << "Elements of an array, that have four and more divisors:\n";
	for (int i = 0; i < sz; i++)
	{
		int div = 0;
		for (int j = 1; j <= arr[i]; j++)
			if (arr[i] % j == 0)
				div++;
		if (div >= 4)
			cout << arr[i] << " ";
	}
	cout << "\n";
}


//������� ������ ���������� �������� ����� � ������� �� ��������� ����������� �������
void nearest_simple_number(int *arr, int sz)
{
	cout << "The nearest to the elements of an array simple numbers:\n";
	for (int i = 0; i < sz; i++)
	{
		for (int j = 1;; j++)//cicle to find the nearest simple number, that cam be more or less than an elemen of an array
		{
			int nearest_simple_number = arr[i] + j;//cheking numbers more than an elemen of an array
			if (simple_number(nearest_simple_number) == true)
			{
				cout << nearest_simple_number << " ";
				break;
			}
			nearest_simple_number = arr[i] - j;//cheking numbers less than an elemen of an array
			if (simple_number(nearest_simple_number) == true)
			{
				cout << nearest_simple_number << " ";
				break;
			}
		}
	}
	cout << "\n";
}

//����� ������� ������/����� �� �������� ����� �����
void shift_loop(int *arr, int sz, int step, bool d)
{
	if (d)
	{
		for (int i = 0; i < step; i++)
		{
			int temp = arr[sz - 1];
			for (int j = sz - 1; j >= 0; j--)
				arr[j] = arr[j - 1];
			arr[0] = temp;
		}
	}
	else
	{
		{
			for (int i = 0; i < step; i++)
			{
				int temp = arr[0];
				for (int j = 0; j <sz; j++)
					arr[j] = arr[j + 1];
				arr[sz - 1] = temp;
			}
		}
	}
}

//�������, ������� ������� ���������� �������� �� ������� � ���������� ������ ��������������� �������.
int del_same(int *arr, int sz)
{
	int same = 0;//counter of the same elements
	for (int i = 0; i < sz; i++)
	{
		if (i + same == sz)//break a cycle if all elements of an array have been already checked
			break;
		bool flag = false;//a boolean variable to indicate, if there are the same elements in the array 
		for (int j = i + 1; j < sz; j++)
		{
			if (arr[j] == arr[i])
			{
				for (int k = j; k < sz; k++)//deleting an element, that is the same to a current (moving all next elements instead of the current element)
					arr[k] = arr[k + 1];
				same++;
				flag = true;
				j -= 1;
			}
		}
		if (flag)
		{
			for (int k = i; k < sz; k++)//deleting a current element, if the same elements were found (moving all next elements instead of the current element)
				arr[k] = arr[k + 1];
			same++;
			i -= 1;
		}
	}
	cout << "Array after deleting all the same elements:\n";
	arr_print(arr, sz - same);
	return sz - same;
}

//Finding maximal element
int max_el(int *arr, int sz)
{
	int max = 0;
	for (int i = 0; i < sz; i++)
		if (arr[i] > max)
			max = arr[i];
	return max;
}


/*�������, ������� ���������� 3 ������ �������� �������, �������� ������� ������ �������� ��������������� ������
�������, � ��� �� �������� �������� ���������������.*/
int three_el_more_average(int *arr, const int sz, int *more_average)
{
	//Calculaiting the average of all elements of the array
	int sum = 0;
	for (int i = 0; i < sz; i++)
		sum += arr[i];
	float average = sum / sz;
	//looking for first three elements, that are more than average, 
	for (int i = 0, count = 0; i < sz && count < 3; i++)
		if (arr[i] > average)
		{
			more_average[count] = arr[i];//adding element, that is more than average, to the special array
			count++;
		}
	return average;
}

//Searching for the same elements and creating a new, pared down, array
int *delete_same(int *parr, int sz)
{
	bool *flag = new bool[sz];
	for (int i = 0; i < sz; i++)
		*(flag + i) = 1;
	int same = 0;
	for (int i = 0; i < sz; i++)
		for (int j = i + 1; j < sz; j++)
			if (*(parr + i) == *(parr + j) && *(flag + i) != 0)
			{
				same++;
				*(flag + j) = 0;
			}
	int sz_short = sz - same;
	int *parr_short = new int[sz_short];
	for (int i = 0, j = 0; i < sz; i++)
		if (*(flag + i) == 1)
		{
			*(parr_short + j) = *(parr + i);
			j++;
		}
	cout << "\nPared down array with " << sz_short << " elements:\n";
	arr_print(parr_short, sz_short);
	delete[] flag, parr;
	return parr_short;
}

//Deleting of elements of an array with pointed meaning
int *el_delete(int *arr, int sz, int el_del)
{
	int count = 0;
	for (int i = 0; i < sz; i++)
		if (*(arr + i) == el_del)
			count++;
	int sz_short = 0;
	sz_short = sz - count;
	int *p_arr_short = new int[sz_short];
	for (int i = 0, j = 0; i < sz; i++)
		if (*(arr + i) != el_del)
			*(p_arr_short + j++) = *(arr + i);
	arr_print(p_arr_short, sz_short);
	delete[] arr;
	return p_arr_short;
}

//Deleting of elements of an array with pointed index
int *index_el_delete(int *arr, int sz, int index_del)
{
	int sz_short = sz - 1;
	int *p_arr_short = new int[sz_short];
	for (int i = 0, j = 0; i < sz; i++)
		if (i != index_del)
			*(p_arr_short + j++) = *(arr + i);
	arr_print(p_arr_short, sz_short);
	delete[] arr;
	return p_arr_short;
}

//Insert an element with pointed index and meaning
int *insert_el(int *arr, int sz, int index_insert, int el_insert)
{
	int sz2 = sz + 1;
	int *p_arr2 = new int[sz2];
	for (int i = 0, j = 0; i < sz2; i++)
	{
		if (i != index_insert)
			*(p_arr2 + i) = *(arr + j++);
		else
			*(p_arr2 + i) = el_insert;
	}
	arr_print(p_arr2, sz2);
	delete[] arr;
	return p_arr2;
}

//Deleting pointed number of elements startring from pointed index
int *several_elements_delete(int *arr, int sz, int index_start_del, int number_elements)
{
	int sz_short = sz - number_elements;
	int *p_arr_short = new int[sz_short];
	int z = index_start_del + number_elements;
	for (int i = 0, j = 0; i < sz; i++)
		if (i < index_start_del || i >= z)
			*(p_arr_short + j++) = *(arr + i);
	arr_print(p_arr_short, sz_short);
	delete[] arr;
	return p_arr_short;
}


//Inserting pointed number of elements with pointed meaning, startring from pointed index
int *several_elements_insert(int *arr, int sz, int el_meaning, int index_start_insert, int number_elements)
{
	int sz2 = sz + number_elements;
	int *p_arr2 = new int[sz2];
	int z = index_start_insert + number_elements;
	for (int i = 0, j = 0; i < sz2; i++)
	{
		if (i < index_start_insert || i >= z)
			*(p_arr2 + i) = *(arr + j++);
		else
			*(p_arr2 + i) = el_meaning;
	}
	arr_print(p_arr2, sz2);
	delete[] arr;
	return p_arr2;
}

//Replasing several elements of local array to the end of global array
int *replace_elements_to_global_array(int *p_arr, int sz, int*p_global_arr, int sz_global_arr, int index_start, int index_end)
{
	int sz_new = sz_global_arr + (index_end - index_start + 1);
	int *p_arr_new = new int[sz_new];
	for (int i = 0, j = index_start; i < sz_new; i++)
	{
		if (i < sz_global_arr)
			*(p_arr_new + i) = *(p_global_arr + i);
		else
			*(p_arr_new + i) = *(p_arr + j++);
	}
	delete[] p_arr, p_global_arr;
	arr_print(p_arr_new, sz_new);
	return p_arr_new;
}



//��������� ������ � ������������ ������ ����� �����, ����� ��������� ������� ������ ���������� �����.
void add_str_2din_arr(int** arr, int &sz1, int &sz2, int number)
{
	int sum_str = 0;							//the sum of the elements in a string
	for (int i = 0; i < sz1; i++)
	{
		for (int j = 0; j < sz2; j++)			//calculating the sum of the elements in a string
			sum_str += arr[i][j];
		if (sum_str > number)					//if the sum of the elements is bigger than a number, adding a string 
		{
			sz1++;								//adding one string
			arr[sz1 - 1] = new int[sz2];			//creating a new string
			for (int l = sz1 - 1; l > i; l--)
				for (int k = 0; k < sz2; k++)	//replacing all other strings
					arr[l][k] = arr[l - 1][k];
			for (int l = 0; l < sz2; l++)		//inputting a string with zeros
				arr[i + 1][l] = 0;
			i++;
		}
		sum_str = 0;
	}
}

/* ������� ������� ������������ ������, ���������� ���������� �������� �� ���� ��������,
������� �� ����� � ��� �� ��������.**/
int** copy_max_el_2din_arr(int** arr1, int** arr2, int sz1, int sz2)
{
	int **arr3 = new int*[sz1];
	create_2din_arr(arr3, sz1, sz2);			//creating a new two-dementional array to save max elements
	for (int i = 0; i < sz1; i++)
		for (int j = 0; j < sz2; j++)
		{
			int max = 0;
			if (arr1[i][j] > arr2[i][j])		//if the element of a first array is bigger
				max = arr1[i][j];				//copying the element of a first array to variable max
			else
				max = arr2[i][j];				//in other case copying the element of a second array to variable max
			arr3[i][j] = max;					//copying max to the current elemebt of the third array
		}
	return arr3;
}

//������������ ������ � ������� ���, ���� ��� ��� �� ����������� �� ����.
void change_strings_max_sum(int** arr, int sz1, int sz2)
{
	for (int a = 0; a < sz1 - 1; a++)
	{
		for (int i = 0; i < sz1 - 1; i++)
		{
			int sum1 = 0,
				sum2 = 0;
			for (int j = 0; j < sz2; j++)//calculating the sum of elements 
			{
				sum1 += arr[i][j];		//for current string
				sum2 += arr[i + 1][j];	//for the next string
			}
			if (sum2 > sum1)			//if sum of the second string is bigger, replacing them	
				for (int k = 0; k < sz2; k++)
				{
					int temp = arr[i][k];
					arr[i][k] = arr[i + 1][k];
					arr[i + 1][k] = temp;
				}
		}
	}
}

//������� ���������� ������� ������� � ������ ������ � ��� ��������� � ������ ������� �������.
void change_max_and_first(int** arr, int sz1, int sz2)
{
	int max_el = 0,						//the maximal element of a matrix
		max_el_str = 0;					//an index of a string with a maximal element
	for (int i = 0; i < sz1; i++)
		for (int j = 0; j < sz2; j++)
			if (arr[i][j] > max_el)		//searching for the maximal element
			{
				max_el = arr[i][j];
				max_el_str = i;
			}
	for (int k = 0; k < sz2; k++)
	{
		int temp = arr[max_el_str][k];		//replacing a first string and a string with the maximal element
		arr[max_el_str][k] = arr[0][k];
		arr[0][k] = temp;
	}
}



//������� ��������� ���������� ������� � ������������ ������ ��������� � �������� ������� � ������ �� ������� � ��������� ����������.
void change_max_el_sum_plane(int*** arr, int plane, int str, int col)
{
	int sum = 0,
		max_sum = 0,
		max_sum_plane = 0;
	for (int i = 0; i < plane; i++)
	{
		for (int j = 0; j < str; j++)
			for (int k = 0; k < col; k++)
				if (j % 2 != 0)
					sum += arr[i][j][k];
		if (sum > max_sum)
		{
			max_sum = sum;
			max_sum_plane = i;
		}
		sum = 0;
	}
	if (max_sum_plane != plane - 1)
	{
		int** temp = arr[plane - 1];
		arr[plane - 1] = arr[max_sum_plane];
		arr[max_sum_plane] = temp;
	}
	else
		cout << "The last plane contains maximal sum of odd string's elements\n";
}