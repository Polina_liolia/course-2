// vector_ctor.cpp
// compile with: /EHsc
#include <vector>
#include <iostream>

int main()
{
	using namespace std;
	vector <int>::iterator v1_Iter, v2_Iter, v3_Iter, v4_Iter, v5_Iter, v6_Iter;

	// Create an empty vector v0
	vector <int> v0;

	// Create a vector v1 with 3 elements of default value 0
	vector <int> v1(3);

	// Create a vector v2 with 5 elements of value 2
	vector <int> v2(5, 2);

	// Create a vector v3 with 3 elements of value 1 and with the allocator 
	// of vector v2
	vector <int> v3(3, 1, v2.get_allocator());

	// Create a copy, vector v4, of vector v2
	vector <int> v4(v2);

	// Create a new temporary vector for demonstrating copying ranges
	vector <int> v5(5);
	for (auto i : v5) {
		v5[i] = i;
	}

	// Create a vector v6 by copying the range v5[_First, _Last)
	vector <int> v6(v5.begin() + 1, v5.begin() + 3);

	cout << "v1 =";
	for (auto& v : v1) {
		cout << " " << v;
	}
	cout << endl;

	cout << "v2 =";
	for (auto& v : v2) {
		cout << " " << v;
	}
	cout << endl;

	cout << "v3 =";
	for (auto& v : v3) {
		cout << " " << v;
	}
	cout << endl;
	cout << "v4 =";
	for (auto& v : v4) {
		cout << " " << v;
	}
	cout << endl;

	cout << "v5 =";
	for (auto& v : v5) {
		cout << " " << v;
	}
	cout << endl;

	cout << "v6 =";
	for (auto& v : v6) {
		cout << " " << v;
	}
	cout << endl;

	// Move vector v2 to vector v7
	vector <int> v7(move(v2));
	vector <int>::iterator v7_Iter;

	cout << "v7 =";
	for (auto& v : v7) {
		cout << " " << v;
	}
	cout << endl;

	vector<int> v8{ { 1, 2, 3, 4 } };
	for (auto& v : v8) {
		cout << " " << v;
	}
	cout << endl;
}

/*
v1 = 0 0 0
v2 = 2 2 2 2 2
v3 = 1 1 1
v4 = 2 2 2 2 2
v5 = 0 1 2 3 4
v6 = 1 2
v7 = 2 2 2 2 2
1 2 3 4

*/
