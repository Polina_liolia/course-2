#include"Transport.h"

Transport::Transport()	//default constructor
{
	weight = 0;
	max_speed = 0;
}

Transport::Transport(string aModel, double aWeight, double aMax_speed, speed aSpeed_units)	//overloaded constructor
{
	model = aModel;
	weight = aWeight;
	max_speed = aMax_speed;
	speed_units = aSpeed_units;
}

//method to set or change property model
void Transport::set_model(string aModel)	
{
	model = aModel;
}

//method to set or change property weight
void Transport::set_weight(double aWeight)
{
	weight = aWeight;
}

//method to set or change property max_speed
void Transport::set_max_speed(double aMax_speed)
{
	max_speed = aMax_speed;
}

//method to set or change property speed_units
void Transport::set_speed_units(speed aSpeed_units)
{
	speed_units = aSpeed_units;
}

//method to get meaning of property model
string Transport::get_model() const
{
	return model;
}

//method to get meaning of property wieght
double Transport::get_wieght() const
{
	return weight;
}

//method to get meaning of property max_speed
double Transport::get_max_speed() const
{
	return max_speed;
}

//method to get meaning of property speed_units
speed Transport::get_speed_units() const
{
	return speed_units;
}

//virtual method to print ell information about Transport instance
void Transport::print()
{
	cout << "Model: " << model << "\n";
	cout << "Weight: " << weight << "kg.\n";
	cout << "Maximal speed: " << max_speed << " ";
	switch (speed_units)
	{
	case 0: cout << "km_h\n"; break;
	case 1: cout << "Mach\n"; break;
	case 2: cout << "miles_h\n"; break;
	case 3: cout << "knots\n"; break;
	}
}

