#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
using std::ios;
using std::ofstream;
using std::ifstream;
using std::string;
using std::vector;


class user_report
{
protected:
	string customer_name;
	string inquiry_number;
	string item;
	string amount;
	string additional_options;
	string comments;
	
public:
	user_report() {};
	~user_report() {};
	virtual void read_data(char * data_file);
	virtual void report(char * data_file, char * draft_file, char * report_file);
};

