#include <iostream>
using namespace std;
class A
{
protected:
	int x;
public:
	A(){
		x = 0;
		cout << "Constructor A (" << this << ")"<< endl;
	}
	~A(){
		cout << "Destructor A (" << this << ")" << endl;
	}

	void show(){
		cout << "A::show()\n";
	}
	
};

class B: public A
{
	int y;
public:
	B(){
		cout << "Constructor B (" << this << ")" << endl;
		y=0;
	}
	~B(){
		cout << "Destructor B (" << this << ")" << endl;
	}
	void show(){
		cout << "B::show()\n";
		cout << "x = " << x << endl;
		cout << "A::x = " << A::x << endl;
		cout << "y = " << y << endl;
	}

	void f1(){
		show();
		//A::show();
	}



};
void main()
{
	
	B d;
	
	B b;
	//b.f1();


}