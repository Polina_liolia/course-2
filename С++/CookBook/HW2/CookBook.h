#pragma once
#include "recipe.h"
#include "ingredient.h"

class CookBook
{
private:
	int sz_ar;
	recipe **ar;

public:
	CookBook();
	CookBook(const CookBook &temp);
	~CookBook();
	void add_recipe(char *aName, char *aType, char *aProcess);
	void change_recipe_name(int recipe_number, char *aName);
	void change_recipe_type(int recipe_number, char *aType);
	void change_recipe_process(int recipe_number, char *aProcess);
	void del_recipe(int recipe_number);
	void add_ingridient(char *aItem, float aDose, int recipe_number);
	void change_ingridient_item(int recipe_number, int ingridient_number, char* aItem, float aDose);
	void del_recipe_ingridient(int recipe_number, int ingridient_number);
	int save_to_file(char * file_name) const;
	int read_from_file(char * file_name);
	void print_recipes_list()const;
	void print_recipe_information(int recipe_number)const;
	void print_recipe_ingridients(int recipe_number)const;
	
};