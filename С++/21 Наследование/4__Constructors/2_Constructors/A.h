#pragma once
#include <iostream>
using std::cout;
using std::endl;
class A {
	int x;
public:
	A() {
		cout << __FUNCTION__ << endl;
		x = 0;
	}
	A(int aX) : x(aX) {
		//x = aX;
		cout << __FUNCTION__ << endl;
	}

	~A() {
		cout << __FUNCTION__ << endl;
	}
};