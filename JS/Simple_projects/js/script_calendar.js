//takes Data object, returns the number of days in current month
function days_in_month (date)
{
    var current_month = date.getMonth();
    while (date.getMonth() === current_month)
    {
        var current_day = date.getDate();
        date.setDate(++current_day);
    }
    return current_day - 1;
}

var now = new Date();
var year = now.getFullYear();
var month = now.getMonth();
var day = now.getDate();

var first_day_of_month_number = new Date(year, month, 1).getDay();

first_day_of_month_number = (first_day_of_month_number === 0) ? 7 : first_day_of_month_number;

var total_days_in_month = days_in_month(now);

var days_of_week = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];

//creating a table:
document.write('<table class="calendar" cellspacing="5px" frame="box" width = "200px" style="display: inline-block; box-shadow: 5px 5px 2px 0 rgba(0,0,0,0.55); border-radius: 5%;">');
var counter = 1;

//days of week row:
document.write("<tr>");
for (var j = 0; j < 7; j++)
    document.write("<td>" + days_of_week[j] + "</td>");
document.write("</tr>");

//all days of month:
for (var i = 0; i < 5; i++)
{
    document.write("<tr>");
    for (j = 0; j < 7; j++)
        if (i == 0 && j < first_day_of_month_number-1)
            document.write("<td></td>");
        else if (counter <= total_days_in_month)
            document.write("<td>" + (counter++) + "</td>");
    document.write("</tr>");
}
document.write("</table>");

var allTd = document.querySelectorAll("table.calendar > tbody > tr > td");
for(i = 0; i < allTd.length; i++)
    if (allTd[i].innerHTML == String(day)) {
        allTd[i].style.backgroundColor = "red";
        break;
    }