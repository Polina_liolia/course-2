#include "Moto_transport.h"

Moto_transport::Moto_transport() : Ground_transport()	//default constructor, inicializes config as "basic" (default)
{
	config = basic;
}

//overloaded constructor
Moto_transport::Moto_transport(string aModel, double aWeight, double aMax_speed, speed aSpeed_units, double aEngine_volume, moto_configuration aConfig) :
	Ground_transport(aModel, aWeight, aMax_speed, aSpeed_units, aEngine_volume)
{
	config = aConfig;
}

//method to set or change property config
void Moto_transport::set_config(moto_configuration aConfig)
{
	config = aConfig;
}

//method to get meaning of property config
moto_configuration Moto_transport::get_config() const
{
	return config;
}

//virtual method to get meaning of property speed_units
speed Moto_transport::get_speed_units() const
{
	cout << "Speed units for moto transport - ";
	switch (speed_units)
	{
	case 0: cout << "km_h\n"; break;
	case 1: cout << "Mach\n"; break;
	case 2: cout << "miles_h\n"; break;
	case 3: cout << "knots\n"; break;
	}
	return speed_units;
}

//virtual method to print all information about Moto_transport instance
void Moto_transport::print()
{
	Ground_transport::print();	//calling of basic class method
	cout << "Moto configuration type: ";
	switch (config)
	{
	case 0: cout << "basic\n"; break;
	case 1: cout << "tricicle\n"; break;
	case 2: cout << "trike\n"; break;
	}
}
