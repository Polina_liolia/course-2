// ���� WINDOWS.H �������� �����������, �������, � ��������� ������� ������������ ��� ��������� ���������� ��� Windows. 
#include <windows.h>
#include <tchar.h>
#include "resource.h"
#include <ctime>
#include <Strsafe.h>
#include <algorithm>
#include <vector>
using std::vector;



// �������� ������� ���������
LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wp, LPARAM lp);
#define OPENDIALOGID 300 //constant identifier of button created

TCHAR szClassWindow[] = TEXT("��������� ����������");	/* ��� ������ ���� */

INT WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow)
{
	HWND hWnd;
	MSG Msg;
	WNDCLASSEX wcl;


	// 1. ����������� ������ ����
	wcl.cbSize = sizeof(wcl);	// ������ ��������� WNDCLASSE
								//������������ �� ����, ���� ������� ������ �� ����������� ��� �� ���������:
	wcl.style = CS_HREDRAW | CS_VREDRAW;	// CS (Class Style) - ����� ������ ����
	wcl.lpfnWndProc = WindowProc;	// ����� ������� ���������
	wcl.cbClsExtra = 0;		// ������������ Windows 
	wcl.cbWndExtra = 0; 	// ������������ Windows 
	wcl.hInstance = hInst;	// ���������� ������� ����������
	wcl.hIcon = LoadIcon(NULL, IDI_APPLICATION);	// �������� ����������� ������
	wcl.hCursor = LoadCursor(NULL, IDC_ARROW);	// �������� ������������ �������	
	wcl.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	// ���������� ���� ����� ������
	wcl.lpszMenuName = NULL;	// ���������� �� �������� ����
	wcl.lpszClassName = szClassWindow;	// ��� ������ ����
	wcl.hIconSm = NULL;	// ���������� ��������� ������ ��� ����� � ������� ����

						// 2. ����������� ������ ����
	if (!RegisterClassEx(&wcl))
		return 0; // ��� ��������� ����������� - �����

				  // 3. �������� ����
				  // ��������� ���� �  ���������� hWnd ������������� ���������� ����
	hWnd = CreateWindowEx(
		0,		// ����������� ����� ����
		szClassWindow,	//��� ������ ����
		TEXT("������ Windows ����������"), // ��������� ����
		WS_OVERLAPPEDWINDOW,				// ����� ����
											/* ���������, �����, ����������� ������ �������, ��������� ����, ������ ������������ � ���������� ����  */
		CW_USEDEFAULT,	// �-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// y-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// ������ ����
		CW_USEDEFAULT,	// ������ ����
		NULL,			// ���������� ������������� ����
		NULL,			// ���������� ���� ����
		hInst,		// ������������� ����������, ���������� ����
		NULL);		// ��������� �� ������� ������ ����������

	HWND b1 = CreateWindow(L"button", L"Open Dialog", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10, 10, 100, 30, hWnd, (HMENU)OPENDIALOGID, hInst, NULL);

	// 4. ����������� ����
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd); // ����������� ����



						// 5. ������ ����� ��������� ���������
	while (GetMessage(&Msg, NULL, 0, 0)) // ��������� ���������� ��������� �� ������� ���������
	{
		TranslateMessage(&Msg);	// ���������� ���������
		DispatchMessage(&Msg);	// ��������������� ���������
	}
	return (int)Msg.wParam;
}


LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	switch (uMessage)
	{
	case WM_DESTROY: // ��������� � ���������� ���������
		PostQuitMessage(0); // ������� ��������� WM_QUIT
		break;
	case WM_COMMAND:
	{
		if (HIWORD(wParam) == BN_CLICKED) //control-defined notification code
			switch (LOWORD(wParam)) //control identifier
			{
			case OPENDIALOGID:
			{
				HWND dialog = CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_DIALOG1), hWnd, DlgProc); //unmodal dialog 
				ShowWindow(dialog, SW_SHOW);//show unmodal dialog
			}
			break;
			}
	}
	break;
	default:
		// ��� ���������, ������� �� �������������� � ������ ������� ������� ������������ ������� Windows �� ��������� �� ���������
		return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}
	return 0;
}

int buttonsID[16] = { IDC_BUTTON1, IDC_BUTTON2, IDC_BUTTON3, IDC_BUTTON4, IDC_BUTTON5, IDC_BUTTON6, IDC_BUTTON7, IDC_BUTTON8, IDC_BUTTON9, IDC_BUTTON10,
IDC_BUTTON11, IDC_BUTTON12, IDC_BUTTON13, IDC_BUTTON14, IDC_BUTTON15, IDC_BUTTON16};
HWND buttonsHWND[16];


bool is_null(HWND hWnd)
{
	TCHAR title[5];
	GetWindowText(hWnd, title, 5);
	wchar_t null[5];
	_itow_s(0, null, 5);
	if (!lstrcmp(null, title))
		return true;
	return false;
}

HWND find_null(HWND hWnd, int id)
{
	switch (id)
	{
	case IDC_BUTTON1:
	{
		//checking IDC_BUTTON2 
		HWND nextButton = GetDlgItem(hWnd, IDC_BUTTON2);
		if (is_null(nextButton))
			return nextButton;
		
		//checking IDC_BUTTON5
		nextButton = GetDlgItem(hWnd, IDC_BUTTON5);
		if (is_null(nextButton))
			return nextButton;
	}
	break;
	case IDC_BUTTON2:
	{
		//checking IDC_BUTTON1 
		HWND nextButton = GetDlgItem(hWnd, IDC_BUTTON1);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON6
		nextButton = GetDlgItem(hWnd, IDC_BUTTON6);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON3
		nextButton = GetDlgItem(hWnd, IDC_BUTTON3);
		if (is_null(nextButton))
			return nextButton;
	}
	break;
	case IDC_BUTTON3:
	{
		//checking IDC_BUTTON2 
		HWND nextButton = GetDlgItem(hWnd, IDC_BUTTON2);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON7
		nextButton = GetDlgItem(hWnd, IDC_BUTTON7);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON4
		nextButton = GetDlgItem(hWnd, IDC_BUTTON4);
		if (is_null(nextButton))
			return nextButton;
	}
	break;
	case IDC_BUTTON4:
	{
		//checking IDC_BUTTON3 
		HWND nextButton = GetDlgItem(hWnd, IDC_BUTTON3);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON8
		nextButton = GetDlgItem(hWnd, IDC_BUTTON8);
		if (is_null(nextButton))
			return nextButton;
	}
	break;
	case IDC_BUTTON5:
	{
		//checking IDC_BUTTON1 
		HWND nextButton = GetDlgItem(hWnd, IDC_BUTTON1);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON6
		nextButton = GetDlgItem(hWnd, IDC_BUTTON6);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON9
		nextButton = GetDlgItem(hWnd, IDC_BUTTON9);
		if (is_null(nextButton))
			return nextButton;
	}
	break;
	case IDC_BUTTON6:
	{
		//checking IDC_BUTTON2 
		HWND nextButton = GetDlgItem(hWnd, IDC_BUTTON2);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON7
		nextButton = GetDlgItem(hWnd, IDC_BUTTON7);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON10 
		nextButton = GetDlgItem(hWnd, IDC_BUTTON10);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON5
		nextButton = GetDlgItem(hWnd, IDC_BUTTON5);
		if (is_null(nextButton))
			return nextButton;
	}
	break;
	case IDC_BUTTON7:
	{
		//checking IDC_BUTTON6 
		HWND nextButton = GetDlgItem(hWnd, IDC_BUTTON6);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON3
		nextButton = GetDlgItem(hWnd, IDC_BUTTON3);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON8 
		nextButton = GetDlgItem(hWnd, IDC_BUTTON8);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON11
		nextButton = GetDlgItem(hWnd, IDC_BUTTON11);
		if (is_null(nextButton))
			return nextButton;
	}
	break;
	case IDC_BUTTON8:
	{
		//checking IDC_BUTTON7 
		HWND nextButton = GetDlgItem(hWnd, IDC_BUTTON7);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON4
		nextButton = GetDlgItem(hWnd, IDC_BUTTON4);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON12
		nextButton = GetDlgItem(hWnd, IDC_BUTTON12);
		if (is_null(nextButton))
			return nextButton;
	}
	break;
	case IDC_BUTTON9:
	{
		//checking IDC_BUTTON5
		HWND nextButton = GetDlgItem(hWnd, IDC_BUTTON5);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON10
		nextButton = GetDlgItem(hWnd, IDC_BUTTON10);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON13
		nextButton = GetDlgItem(hWnd, IDC_BUTTON13);
		if (is_null(nextButton))
			return nextButton;
	}
	break;
	case IDC_BUTTON10:
	{
		//checking IDC_BUTTON9 
		HWND nextButton = GetDlgItem(hWnd, IDC_BUTTON9);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON6
		nextButton = GetDlgItem(hWnd, IDC_BUTTON6);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON11 
		nextButton = GetDlgItem(hWnd, IDC_BUTTON11);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON14
		nextButton = GetDlgItem(hWnd, IDC_BUTTON14);
		if (is_null(nextButton))
			return nextButton;
	}
	break;
	case IDC_BUTTON11:
	{
		//checking IDC_BUTTON10 
		HWND nextButton = GetDlgItem(hWnd, IDC_BUTTON10);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON7
		nextButton = GetDlgItem(hWnd, IDC_BUTTON7);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON12 
		nextButton = GetDlgItem(hWnd, IDC_BUTTON12);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON15
		nextButton = GetDlgItem(hWnd, IDC_BUTTON15);
		if (is_null(nextButton))
			return nextButton;
	}
	break;
	case IDC_BUTTON12:
	{
		//checking IDC_BUTTON16 
		HWND nextButton = GetDlgItem(hWnd, IDC_BUTTON16);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON11
		nextButton = GetDlgItem(hWnd, IDC_BUTTON11);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON8
		nextButton = GetDlgItem(hWnd, IDC_BUTTON8);
		if (is_null(nextButton))
			return nextButton;
	}
	break;
	case IDC_BUTTON13:
	{
		//checking IDC_BUTTON9 
		HWND nextButton = GetDlgItem(hWnd, IDC_BUTTON9);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON14
		nextButton = GetDlgItem(hWnd, IDC_BUTTON14);
		if (is_null(nextButton))
			return nextButton;
	}
	break;
	case IDC_BUTTON14:
	{
		//checking IDC_BUTTON13 
		HWND nextButton = GetDlgItem(hWnd, IDC_BUTTON13);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON10
		nextButton = GetDlgItem(hWnd, IDC_BUTTON10);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON15
		nextButton = GetDlgItem(hWnd, IDC_BUTTON15);
		if (is_null(nextButton))
			return nextButton;
	}
	break;
	case IDC_BUTTON15:
	{
		//checking IDC_BUTTON14 
		HWND nextButton = GetDlgItem(hWnd, IDC_BUTTON14);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON11
		nextButton = GetDlgItem(hWnd, IDC_BUTTON11);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON16
		nextButton = GetDlgItem(hWnd, IDC_BUTTON16);
		if (is_null(nextButton))
			return nextButton;
	}
	break;
	case IDC_BUTTON16:
	{
		//checking IDC_BUTTON12 
		HWND nextButton = GetDlgItem(hWnd, IDC_BUTTON12);
		if (is_null(nextButton))
			return nextButton;

		//checking IDC_BUTTON15
		nextButton = GetDlgItem(hWnd, IDC_BUTTON15);
		if (is_null(nextButton))
			return nextButton;
	}
	break;
	}
	return GetDlgItem(hWnd, id);//returns pushed button's HWND if no ways to step found
}

int end_game() {
	for (int i = 0; i < 15; i++)
	{
		TCHAR title[10];
		GetWindowText(buttonsHWND[i], title, 10);
		wchar_t current_i[10];
		_itow_s((i+1), current_i, 10);
		if (!lstrcmp(current_i, title))
			continue;
		else return false;
	}
	return true;
}

int game_time = 0;//to get game duration
INT_PTR CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)//��������� ������� � ���������� ����
{
	switch (message)
	{
	case WM_INITDIALOG:
	{
		for (int i = 0; i < 16; i++)
			buttonsHWND[i] = GetDlgItem(hWnd, buttonsID[i]);
		vector<int> titles;
		for (int i = 0; i < 16; i++)
			titles.push_back(i);
		srand(time(NULL));
		std::random_shuffle(titles.begin(), titles.end());
		for (int i = 0; i < 16; i++)
		{
			wchar_t title_str[10];
			_itow_s(titles[i], title_str, 10);
			SetWindowText(buttonsHWND[i], title_str);
		}
		SetTimer(hWnd, 1, 1000, NULL);
	}
	break;
	case WM_CLOSE:
		EndDialog(hWnd, 0); // ��������� ������
		return TRUE;
	case WM_TIMER:
		if (wParam == 1)
			game_time++;
		break;
	case WM_COMMAND:
		if (HIWORD(wParam) == BN_CLICKED) //������� ������ �������
			switch (LOWORD(wParam)) //control identifier
			{
			case IDOK:
				EndDialog(hWnd, 0); //��������� ������
				return TRUE;
			case IDCANCEL:
				EndDialog(hWnd, 0); //��������� ������
				return FALSE;
			case IDC_BUTTON1: case IDC_BUTTON2: case IDC_BUTTON3: case IDC_BUTTON4:case IDC_BUTTON5:
			case IDC_BUTTON6: case IDC_BUTTON7: case IDC_BUTTON8: case IDC_BUTTON9: case IDC_BUTTON10:
			case IDC_BUTTON11: case IDC_BUTTON12: case IDC_BUTTON13: case IDC_BUTTON14: case IDC_BUTTON15:
			case IDC_BUTTON16:
			{
				HWND currentButton = GetDlgItem(hWnd, LOWORD(wParam));
				TCHAR current_text[10];
				GetWindowText(currentButton, current_text, 10);
				HWND result = find_null(hWnd, LOWORD(wParam));
				if (result != hWnd) //if empty field found
				{
					SetWindowText(currentButton, TEXT("0"));
					SetWindowText(result, current_text);
				}
				if(end_game())
				{
					KillTimer(hWnd, 1);
					int const arraysize = 50;
					TCHAR msg[arraysize];
					size_t sz = arraysize * sizeof(TCHAR);
					LPCTSTR pszFormat = TEXT("You win! Your time is %d second(s)!");
					HRESULT hr = StringCbPrintf(msg, sz, pszFormat, game_time);
					MessageBox(hWnd, _TEXT("Congrats!"), msg, MB_OK);
					EndDialog(hWnd, true);
					game_time = 0;
					break;
				}
			}
			break;
			}
		}
	return FALSE;
}
