#include "Stack.h"
/*
void push(int); -�������� ���������� �������� � ����
*/


Stack::Stack()
{
	_capacity = 0;
	_size = 0;
	items = nullptr;
}

Stack::Stack(int capacity) 
{
	_capacity = capacity;
	_size = 0;
	items = nullptr;
}
//copy constructor
Stack::Stack(const Stack &aStack)
{
	if (aStack._size)
	{
		items = new int[aStack._size];
		for (int i = 0; i < aStack._size; i++)
			items[i] = aStack.items[i];
	}
	else
		items = nullptr;
	_capacity = aStack._capacity;
	_size = aStack._size;
}

Stack::~Stack()
{
	delete[] items;
}

//sets the stack capacity
void Stack::set_capacity(int aCapacity)
{
	if (aCapacity > _size)
		_capacity = aCapacity;
}

//places the value passed to the stack
void Stack::push(int digit)
{
	if (_size + 1 <= _capacity)
	{
		int *temp = new int[_size + 1];
		temp[_size] = digit;
		for (int i = 0; i < _size; i++)
			temp[i] = items[i];
		if (_size)
			delete[] items;
		_size++;
		items = temp;
	}
}

//Passes the value located at the top of the stack and returns it
int Stack::pop()
{
	if (!_size) return 0;
	_size--;
	int *newItems = _size ? new int[_size] : nullptr;
	for (int i = 0; i < _size; i++)
		newItems[i] = items[i];
	int result = items[_size];
	delete[] items;
	items = newItems;
	return result;
}

//method should return the number of elements that can put a stack
int Stack::capacity() const
{
	return _capacity;
}

//Returns the number of elements actually are in the stack
int Stack::size()
{
	return _size;
}

//Returns true if the stack is full
bool Stack::isFull()
{
	return (_size == _capacity);
}

//Returns true if the stack is empty
bool Stack::isEmpty()
{
	return (_size == 0);
}


int Stack::operator[](int index)
{
	return items[index];
}
