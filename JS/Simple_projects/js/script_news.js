var news = ["News1!!!", "News2!!!", "News3!!!"];

function chooseNew1 ()
{
    var newsDiv = document.querySelector("div.news");
    newsDiv.innerHTML = news[0];
}

function chooseNew2 ()
{
    var newsDiv = document.querySelector("div.news");
    newsDiv.innerHTML = news[1];
}

function chooseNew3 ()
{
    var newsDiv = document.querySelector("div.news");
    newsDiv.innerHTML = news[2];
}

var button1 = document.querySelector("div.buttons>input:first-of-type");
var button2 = document.querySelector("div.buttons>input:nth-of-type(2)");
var button3 = document.querySelector("div.buttons>input:last-of-type");


button1.addEventListener("click", chooseNew1);
button2.addEventListener("click", chooseNew2);
button3.addEventListener("click", chooseNew3);