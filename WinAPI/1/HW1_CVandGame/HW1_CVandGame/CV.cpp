// ���� WINDOWS.H �������� �����������, �������, � ��������� ������� ������������ ��� ��������� ���������� ��� Windows. 
#define _UNICODE
#include <windows.h>
#include <tchar.h>



// �������� ������� ���������
LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);


TCHAR szClassWindow[] = TEXT("��������� ����������");	/* ��� ������ ���� */


void showCV(HWND hWnd);

INT WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow)
{
	HWND hWnd;
	MSG Msg;
	WNDCLASSEX wcl;


	// 1. ����������� ������ ����
	wcl.cbSize = sizeof(wcl);	// ������ ��������� WNDCLASSE
								//������������ �� ����, ���� ������� ������ �� ����������� ��� �� ���������:
	wcl.style = CS_HREDRAW | CS_VREDRAW;	// CS (Class Style) - ����� ������ ����
	wcl.lpfnWndProc = WindowProc;	// ����� ������� ���������
	wcl.cbClsExtra = 0;		// ������������ Windows 
	wcl.cbWndExtra = 0; 	// ������������ Windows 
	wcl.hInstance = hInst;	// ���������� ������� ����������
	wcl.hIcon = LoadIcon(NULL, IDI_APPLICATION);	// �������� ����������� ������
	wcl.hCursor = LoadCursor(NULL, IDC_ARROW);	// �������� ������������ �������	
	wcl.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	// ���������� ���� ����� ������
	wcl.lpszMenuName = NULL;	// ���������� �� �������� ����
	wcl.lpszClassName = szClassWindow;	// ��� ������ ����
	wcl.hIconSm = NULL;	// ���������� ��������� ������ ��� ����� � ������� ����

						// 2. ����������� ������ ����
	if (!RegisterClassEx(&wcl))
		return 0; // ��� ��������� ����������� - �����

				  // 3. �������� ����
				  // ��������� ���� �  ���������� hWnd ������������� ���������� ����
	hWnd = CreateWindowEx(
		0,		// ����������� ����� ����
		szClassWindow,	//��� ������ ����
		TEXT("������ Windows ����������"), // ��������� ����
		WS_OVERLAPPEDWINDOW,				// ����� ����
											/* ���������, �����, ����������� ������ �������, ��������� ����, ������ ������������ � ���������� ����  */
		CW_USEDEFAULT,	// �-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// y-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// ������ ����
		CW_USEDEFAULT,	// ������ ����
		NULL,			// ���������� ������������� ����
		NULL,			// ���������� ���� ����
		hInst,		// ������������� ����������, ���������� ����
		NULL);		// ��������� �� ������� ������ ����������

					// 4. ����������� ����
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd); // ����������� ����

	showCV(hWnd);

						// 5. ������ ����� ��������� ���������
	while (GetMessage(&Msg, NULL, 0, 0)) // ��������� ���������� ��������� �� ������� ���������
	{
		TranslateMessage(&Msg);	// ���������� ���������
		DispatchMessage(&Msg);	// ��������������� ���������
	}
	return Msg.wParam;
}



LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	switch (uMessage)
	{
	case WM_DESTROY: // ��������� � ���������� ���������
		PostQuitMessage(0); // ������� ��������� WM_QUIT
		break;
	default:
		// ��� ���������, ������� �� �������������� � ������ ������� ������� ������������ ������� Windows �� ��������� �� ���������
		return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}
	return 0;
}

void showCV(HWND hWnd)
{
	_TCHAR szHeadMsg1[] = _TEXT("The first part of CV: personal data.");
	_TCHAR szMsg1[] = _TEXT("Ivanov Ivan Ivanovich");
	_TCHAR szHeadMsg2[] = _TEXT("The second part of CV: main skills.");
	_TCHAR szMsg2[] = _TEXT("Digging a garden");
	_TCHAR szHeadMsg3[] = _TEXT("The third part of CV: work expirience and projects.");
	_TCHAR szMsg3[] = _TEXT("2005-2016 - Queen Elizabeth's gardener.");
	_TCHAR szHeadMsg4[] = _TEXT("The fourth part of CV: education and cources, sertificates.");
	_TCHAR szMsg4[] = _TEXT("Self-educated geek.");
	_TCHAR szHeadMsg5[] = _TEXT("The fifth part of CV: additional skills.");
	_TCHAR szMsg5[] = _TEXT("English - advanced level");

	int nCharsAverage = (_tcslen(szMsg1) + _tcslen(szMsg2) + _tcslen(szMsg3) + _tcslen(szMsg4) + _tcslen(szMsg5)) / 5;

	//converting int to Unicode:
	wchar_t szCharsAverage[10];
	_itow_s(nCharsAverage, szCharsAverage, 10);

	MessageBox(hWnd, szMsg1, szHeadMsg1, MB_OK | MB_ICONINFORMATION);
	MessageBox(hWnd, szMsg2, szHeadMsg2, MB_OK | MB_ICONINFORMATION);
	MessageBox(hWnd, szMsg3, szHeadMsg3, MB_OK | MB_ICONINFORMATION);
	MessageBox(hWnd, szMsg4, szHeadMsg4, MB_OK | MB_ICONINFORMATION);
	MessageBox(hWnd, szMsg5, szHeadMsg5, MB_OK | MB_ICONINFORMATION);
	MessageBox(hWnd, szCharsAverage, _TEXT("Average amount of characters in text"), MB_OK | MB_ICONINFORMATION);
}