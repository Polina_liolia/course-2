$(function(){
    $('button[data-toggle=popover]').popover().click(function(e) {e.preventDefault()});
    var imgs_scr = ['img/1.jpg', 'img/2.jpg', 'img/3.jpg', 'img/4.jpg',
        'img/5.jpg', 'img/6.jpg', 'img/7.jpg', 'img/8.jpg', 'img/1.jpg',
        'img/2.jpg', 'img/3.jpg', 'img/4.jpg', 'img/5.jpg', 'img/6.jpg',
        'img/7.jpg', 'img/8.jpg'];
    var $pictures = $(".game_field");
    //flags:
    var inProcess = false;
    var hidden = true;
    var oneOpened = false;

    //tmp vars and counters:
    var clicks = 0;
    var opened1 = -1, opened2 = -1;
    var total_opened = 0;
    
    $(".play").click(function () {
        if (inProcess)
            return;
        function compareRandom(a, b) {
            return Math.random() - 0.5;
        }
        $(".game_over").hide();
        $(".clicks>span").text('0');
        clicks = 0;
        imgs_scr.sort(compareRandom);
        inProcess = true;
        hidden = false;
        $pictures.each(function (index) {
            $( this ).attr('src', imgs_scr[index]);
        });
        setTimeout(function () {
            hidden = true;
            $pictures.attr('src', 'img/pattern.jpg');
        }, 5000);
    });

    $(".stop").click(function (){
        $pictures.attr('src', 'img/pattern.jpg');
        hidden = true;
        inProcess = false;
        $(".clicks>span").text('0');
        clicks = 0;
        opened1 = -1;
        opened2 = -1;
        total_opened = 0;
        oneOpened = false;
        $pictures.attr('opened', 'false');
        });

    $pictures.click(function () {
        if (!inProcess || !hidden || $(this).attr('opened') == 'true')
            return;
        console.log("click");
        clicks++;
        $(".clicks>span").text(clicks);

        if (!oneOpened)
        {
            oneOpened = true;
            $(this).attr('src', imgs_scr[$(this).index()]);
            opened1 = $(this);
        }
        else{
            oneOpened = false;
            opened2 = $(this);
            $(this).attr('src', imgs_scr[$(this).index()]);

            setTimeout(function () {
                if (opened1.attr('src') == opened2.attr('src'))
                {
                    opened1.attr('opened', 'true');
                    opened2.attr('opened', 'true');
                    total_opened += 2;
                    if (total_opened == 16)
                        game_over();
                }
                else
                {
                    opened1.attr('src', 'img/pattern.jpg');
                    opened2.attr('src', 'img/pattern.jpg');
                }
            }, 1000);

        }
    });

    function game_over() {
        $(".game_over").show();
        inProcess = false;
        $pictures.attr('opened', 'false');
    }
});















