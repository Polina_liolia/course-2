#pragma once
#include <iostream>

class String
{
private:
	int sz;
	char* str;
	friend class var;		//class var is a friend of class Srting, it has a full acsess to all its properties and methods

public:
	String();									//1
	String(char *aStr);							//2.1
	String(const String &aString);				//2.2
	~String();
public:
	void set_string(char *aStr);
	inline char *get_string()const;
	inline int get_stringlen()const;
	String operator=(const String &aString);	//3
	String operator+=(const String &aString);	//4
	String operator+=(char a);	
	//String operator-=(const String &aString);	//4
	//String operator-=(char *aStr);//6
	String operator*=(const String &aString);	//4
	String operator*=(char *aStr);//6
	String operator+(const String &aString);	//5.1
	String operator+(char *aStr);				//5.2
	//String operator-(const String &aString);	//5.1
	//String operator-(char *aStr);
	String operator*(const String &aString);	//5.1
	String operator*(char *aStr);
	String operator/(const String &aString);	//5.1
	String operator/(char *aStr);
	String operator/=(const String &aString);	//5.1
	String operator/=(char *aStr);
	bool operator==(const String &aString);		//7.1
	bool operator==(char *aStr);				//8.1
	bool operator!=(const String &aString);		//7.2
	bool operator!=(char *aStr);				//8.2
	bool operator>(const String &aString);		//9.1
	bool operator>(char *aStr);					//9.2
	bool operator>=(const String &aString);		//9.1
	bool operator>=(char *aStr);
	bool operator<(const String &aString);		//9.3	
	bool operator<(char *aStr);	
	bool operator<=(const String &aString);		//9.3	
	bool operator<=(char *aStr);	//9.4
	void print();
	char& operator[](int aIndex);
	operator int ()const;
	operator double()const;

};