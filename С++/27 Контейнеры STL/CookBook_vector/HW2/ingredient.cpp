#include <iostream>
#include <cstring>
using std::cout;
using std::cin;
#include "ingredient.h"

	ingredient::ingredient(char *aItem, float aDose)
	{
		if (aItem && strlen(aItem))
		{
			item = new char[(strlen(aItem) + 1)];
			strcpy_s(item, (strlen(aItem) + 1), aItem);
		}
		else
			item = nullptr;
		dose = aDose;
	}

	ingredient::ingredient(const ingredient &temp)
	{
		if (temp.item && strlen(temp.item))
		{
			item = new char[(strlen(temp.item) + 1)];
			strcpy_s(item, (strlen(temp.item) + 1), temp.item);
		}
		else
			item = nullptr;
		dose = temp.dose;
	}

	ingredient::~ingredient()
	{
		delete[] item;
	}

	ingredient ingredient::operator=(const ingredient & aI)
	{
		if (aI.item && strlen(aI.item))
		{
			item = new char[(strlen(aI.item) + 1)];
			strcpy_s(item, (strlen(aI.item) + 1), aI.item);
		}
		else
			item = nullptr;
		dose = aI.dose;
		return *this;
	}
