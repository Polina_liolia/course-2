// ���� WINDOWS.H �������� �����������, �������, � ��������� ������� ������������ ��� ��������� ���������� ��� Windows. 
#define _UNICODE
#include <windows.h>
#include <tchar.h>
#include <Strsafe.h>


// �������� ������� ���������
LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);


TCHAR szClassWindow[] = TEXT("��������� ����������");	/* ��� ������ ���� */

INT WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow)
{
	HWND hWnd;
	MSG Msg;
	WNDCLASSEX wcl;


	// 1. ����������� ������ ����
	wcl.cbSize = sizeof(wcl);	// ������ ��������� WNDCLASSE
								//������������ �� ����, ���� ������� ������ �� ����������� ��� �� ���������:

	wcl.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;	// CS (Class Style) - ����� ������ ����
														// CS_DBLCLKS - ��������� ������� ���� ����
	wcl.lpfnWndProc = WindowProc;	// ����� ������� ���������
	wcl.cbClsExtra = 0;		// ������������ Windows 
	wcl.cbWndExtra = 0; 	// ������������ Windows 
	wcl.hInstance = hInst;	// ���������� ������� ����������
	wcl.hIcon = LoadIcon(NULL, IDI_APPLICATION);	// �������� ����������� ������
	wcl.hCursor = LoadCursor(NULL, IDC_ARROW);	// �������� ������������ �������	
	wcl.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	// ���������� ���� ����� ������
	wcl.lpszMenuName = NULL;	// ���������� �� �������� ����
	wcl.lpszClassName = szClassWindow;	// ��� ������ ����
	wcl.hIconSm = NULL;	// ���������� ��������� ������ ��� ����� � ������� ����

						// 2. ����������� ������ ����
	if (!RegisterClassEx(&wcl))
		return 0; // ��� ��������� ����������� - �����

				  // 3. �������� ����
				  // ��������� ���� �  ���������� hWnd ������������� ���������� ����
	hWnd = CreateWindowEx(
		0,		// ����������� ����� ����
		szClassWindow,	//��� ������ ����
		TEXT("������ Windows ����������"), // ��������� ����
		WS_OVERLAPPEDWINDOW,				// ����� ����
											/* ���������, �����, ����������� ������ �������, ��������� ����, ������ ������������ � ���������� ����  */
		CW_USEDEFAULT,	// �-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// y-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// ������ ����
		CW_USEDEFAULT,	// ������ ����
		NULL,			// ���������� ������������� ����
		NULL,			// ���������� ���� ����
		hInst,		// ������������� ����������, ���������� ����
		NULL);		// ��������� �� ������� ������ ����������

					// 4. ����������� ����
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd); // ����������� ����


						// 5. ������ ����� ��������� ���������
	while (GetMessage(&Msg, NULL, 0, 0)) // ��������� ���������� ��������� �� ������� ���������
	{
		TranslateMessage(&Msg);	// ���������� ���������
		DispatchMessage(&Msg);	// ��������������� ���������
	}
	return Msg.wParam;
}

/*
1) �������� ����������, � ������� ������ ������� ���������� ������� �����, ������ � ������� ������ ����.
����������� ���������� ���������� �������� � ��������� ����.
*/

int l_but = 0,
m_but = 0,
r_but = 0,
left = 0,
top = 0;

void show_status(HWND hWnd)
{
	RECT r;
	GetWindowRect(hWnd, &r);
	left = r.left;
	top = r.top;

	int const arraysize = 50;
	TCHAR msg[arraysize];
	size_t sz = arraysize * sizeof(TCHAR);
	LPCTSTR pszFormat = TEXT("L: %d M: %d R: %d Left: %d Top: %d");
	HRESULT hr = StringCbPrintf(msg, sz, pszFormat, l_but, m_but, r_but, left, top);
	SetWindowText(hWnd, msg);
}

//������������ ���� ��������� �� ����������
void move_window(HWND hWnd, WPARAM wParam)
{
	RECT r;
	GetWindowRect(hWnd, &r);
	int width = r.right - r.left;
	int height = r.bottom - r.top;

	int move = 100;
	if (wParam == VK_UP)
		top -= move;
	if (wParam == VK_DOWN)
		top += move;
	if (wParam == VK_LEFT)
		left -= move;
	if (wParam == VK_RIGHT)
		left += move;
	if (wParam == VK_RETURN)
	{
		top = 0;
		left = 0;
		width = 300;
		height = 300;
	}
	MoveWindow(hWnd, left, top, width, height, true);
}

//change name of Calculator window
int counter = 0;
void change_calc_header()
{
	HWND h_calc = #00030A6C; /*FindWindow(TEXT("Windows.UI.Core.CoreWindow"), NULL);*/
	counter++;
	int const arraysize = 50;
	TCHAR msg[arraysize];
	size_t sz = arraysize * sizeof(TCHAR);
	LPCTSTR pszFormat = TEXT("Calculator name changed in %d time(s)!");
	HRESULT hr = StringCbPrintf(msg, sz, pszFormat, counter);
	SetWindowText(h_calc, msg);
}


LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	switch (uMessage)
	{
	case WM_DESTROY: // ��������� � ���������� ���������
		PostQuitMessage(0); // ������� ��������� WM_QUIT
		break;
	case WM_LBUTTONDOWN: //������ ����� ������ ����;
		l_but++;
		show_status(hWnd);
		change_calc_header();
		break;
	case WM_MBUTTONDOWN: // ������ ������� ������ ����;
		m_but++;
		show_status(hWnd);
		break;
	case WM_RBUTTONDOWN: // ������ ������ ������ ����;
		r_but++;
		show_status(hWnd);
		break;
	case WM_KEYDOWN: //������ �������
		move_window(hWnd, wParam);
		break;
	default:
		// ��� ���������, ������� �� �������������� � ������ ������� ������� ������������ ������� Windows �� ��������� �� ���������
		return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}
	return 0;
}