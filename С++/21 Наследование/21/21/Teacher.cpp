#include "Teacher.h"

Teacher::Teacher() : Person::Person()	//constructor
{
}

Teacher::Teacher(String aSurname, String aName, String aPatronymic, short aDay, short aMonth, short aYear, String aPhone, String aUnivercity, String aCountry, String aCity, String aSpeciality)
	: Person::Person(aSurname, aName, aPatronymic, aDay, aMonth, aYear, aPhone, aUnivercity, aCountry, aCity)
{
	speciality = aSpeciality;
}

Teacher::Teacher(const Teacher & a) : Person::Person(a)
{
	speciality = a.speciality;
}

Teacher::~Teacher()
{
}

void Teacher::set_speciality(String aSpeciality)
{
	speciality = aSpeciality;
}

int Teacher::print_to_file(FILE * f, char * file_name)
{
	if (f == nullptr)
		fopen_s(&f, file_name, "ab");				//opens a binar file to write to the end
	if (f == nullptr)								//if file was not opened
		return -1;

	int sz = strlen(speciality.get_String()) + 1;
	fwrite(&sz, 4, 1, f);
	fwrite(speciality.get_String(), sz, 1, f);	//to write a group

	fclose(f);
	return 0;
}

String Teacher::get_speciality() const
{
	return speciality;
}

void Teacher::print_data()
{
	Person::print_data();
	cout << "Speciality: " << speciality << "\n";
}

