#include <iostream>
using namespace std;
#include <stdio.h>

class C 
{
public:
    int i; 
	
	C(){
        i = 0;
    }
    C(const C& aC){
        cout << "copy constructor\n";
    }
    explicit 
	C(int ai ){
        i = ai;
		cout << "overload constructor\n";
    }
   
};

class C2
{
public:
    int i;
    explicit C2(int i )   
    {
		
    } 
};

C fun1(C c)
{  
    c.i = 2;
    return c;   
}

void f2(C2 c)
{
	cout << "f2(c)" << endl;
}

//void f2(int c)
//{
// cout << "f2(int)" << endl;
//
//}
void g(int i)
{
    //f2(i);   
    
}

int main(){
    
	double x = 10.56;
	int y = x;
	int t = (int) x;


	
	C n = 5;
	C m(5);

	fun1(5);

	fun1((C)5);

   	//f2(5);
}