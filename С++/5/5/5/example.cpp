#include "example.h"

A::A()
{
	std::cout << "Constructor default" << "\n";
	name = nullptr;
	digit = 0;
}
A::A(char* aName, int aDigit)
{
	std::cout << "Constructor overloaded" << "\n";
	if (aName && strlen(aName))
	{
		name = new char[strlen(aName) + 1];
		strcpy_s(name, strlen(aName) + 1, aName);
	}
	digit = aDigit;
}

A::~A()
{
	std::cout << "Destructor" << "\n";

	delete[] name;
}