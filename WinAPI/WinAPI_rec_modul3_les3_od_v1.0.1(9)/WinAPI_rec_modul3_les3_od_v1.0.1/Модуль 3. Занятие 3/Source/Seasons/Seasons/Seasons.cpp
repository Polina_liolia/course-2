#include <windows.h>
#include <tchar.h>
#include "resource.h"

HWND hList, hCombo, hCheck, hEdit;

BOOL CALLBACK DlgProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow)
{
	return DialogBox(hInstance, MAKEINTRESOURCE(IDD_DIALOG1), NULL, DlgProc); 
}

BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message)
	{
	case WM_CLOSE:
		EndDialog(hWnd, 0);
		return TRUE;

	case WM_INITDIALOG:
		// ������� ����������� ��������� ����������
		hCombo = GetDlgItem(hWnd, IDC_SEASONS);
		hList = GetDlgItem(hWnd, IDC_MONTH);
		hEdit = GetDlgItem(hWnd, IDC_DAYS);
		hCheck = GetDlgItem(hWnd, IDC_LEAF);

		// ��������� ������� ���� � ComboBox
		SendMessage(hCombo, CB_ADDSTRING, 0, LPARAM(TEXT("����")));
		SendMessage(hCombo, CB_ADDSTRING, 0, LPARAM(TEXT("�����")));
		SendMessage(hCombo, CB_ADDSTRING, 0, LPARAM(TEXT("����")));
		SendMessage(hCombo, CB_ADDSTRING, 0, LPARAM(TEXT("�����")));
		return TRUE;

	case WM_COMMAND:
		// ������� CBN_SELCHANGE ����������� ������ ���������� � ��� ������, ����� ���������� ����� ������� � ������
		if(LOWORD(wParam) == IDC_SEASONS && HIWORD(wParam) == CBN_SELCHANGE)
		{
			// ������� ListBox
			SendMessage(hList, LB_RESETCONTENT, 0, 0);
			// ������� ������ ���������� �������� ComboBox
			int index = SendMessage(hCombo, CB_GETCURSEL, 0, 0); 
			switch(index)
			{
				case 0:
					// ��������� � ListBox �������� ������ � ���������� ������ ������������ ��������
					index = SendMessage(hList, LB_ADDSTRING, 0, LPARAM(TEXT("�������")));
					// ������ �������� ������ � ������������ �����, ������������ ���������� ���� � ������
					SendMessage(hList, LB_SETITEMDATA, index, 31);
					index = SendMessage(hList, LB_ADDSTRING, 0, LPARAM(TEXT("������")));
					SendMessage(hList, LB_SETITEMDATA, index, 31);
					index = SendMessage(hList, LB_ADDSTRING, 0, LPARAM(TEXT("�������")));
					SendMessage(hList, LB_SETITEMDATA, index, 28);
					break;

				case 1:
					index = SendMessage(hList, LB_ADDSTRING, 0, LPARAM(TEXT("����")));
					SendMessage(hList, LB_SETITEMDATA, index, 31);
					index = SendMessage(hList, LB_ADDSTRING, 0, LPARAM(TEXT("������")));
					SendMessage(hList, LB_SETITEMDATA, index, 30);
					index = SendMessage(hList, LB_ADDSTRING, 0, LPARAM(TEXT("���")));
					SendMessage(hList, LB_SETITEMDATA, index, 31);
					break;

				case 2:
					index = SendMessage(hList, LB_ADDSTRING, 0, LPARAM(TEXT("����")));
					SendMessage(hList, LB_SETITEMDATA, index, 30);
					index = SendMessage(hList, LB_ADDSTRING, 0, LPARAM(TEXT("����")));
					SendMessage(hList, LB_SETITEMDATA, index, 31);
					index = SendMessage(hList, LB_ADDSTRING, 0, LPARAM(TEXT("������")));
					SendMessage(hList, LB_SETITEMDATA, index, 31);
					break;

				case 3:
					index = SendMessage(hList, LB_ADDSTRING, 0, LPARAM(TEXT("��������")));
					SendMessage(hList, LB_SETITEMDATA, index, 30);
					index = SendMessage(hList, LB_ADDSTRING, 0, LPARAM(TEXT("�������")));
					SendMessage(hList, LB_SETITEMDATA, index, 31);
					index = SendMessage(hList, LB_ADDSTRING, 0, LPARAM(TEXT("������")));
					SendMessage(hList, LB_SETITEMDATA, index, 30);
					break;
			}

		}

		// ������� LBN_SELCHANGE ������ ���������� � ��� ������, ����� ���������� ����� ������� � ������
		if(LOWORD(wParam) == IDC_MONTH && HIWORD(wParam) == LBN_SELCHANGE)
		{
			// ������� ������ ���������� �������� ListBox
			int index = SendMessage(hList, LB_GETCURSEL, 0, 0);
			// ������� ��������, ��������� � ��������� ��������� ������
			int day = SendMessage(hList, LB_GETITEMDATA, index, 0);
			TCHAR buffer[10];
			// ������ ����� � ���������� �������� ������
			SendMessage(hList, LB_GETTEXT, index, LPARAM(buffer));
			if(!lstrcmp(buffer, TEXT("�������"))) // ��������� ����� - �������?
			{
				// ���������, ���������� ��� ��� ���
				int status = SendMessage(hCheck, BM_GETCHECK, 0, 0);
				if(status == BST_CHECKED)
					day++;
			}
			wsprintf(buffer, TEXT("%d"), day);
			// ������������� ���������� ���� � EditControl
			SendMessage(hEdit, WM_SETTEXT, 0, LPARAM(buffer));
		}
		return TRUE;
	}
	return FALSE;
}