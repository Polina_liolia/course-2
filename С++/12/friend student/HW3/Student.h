#pragma once
#include <iostream>
#include <cstring>
using std::cout;
using std::cin;


class Student
{
private:
	char *surname;
	char *name;
	char *patronymic;
	char *phone;
	char *univercity;
	char *country;
	char *city;
	char *group;
	
public:
	Student();

	//overloaded constructor:
	Student(char *aSurname, char *aName, char *aPatronymic, char *aPhone, char *aUnivercity, char *aCountry, char *aCity, char *aGroup);

	~Student();

public:
	//methods to set the meaning of every property:

	void set_surname(char *aSurname);
	void set_name(char *aName);
	void set_patronymic(char *aPatronymic);
	void set_phone(char *aPhone);
	void set_univercity(char *aUnivercity);
	void set_country(char *aCountry);
	void set_city(char *aCity);
	void set_group(char *aGroup);
	friend void print_student(Student &a);

	
	//methods to get the meaning of every property:
	inline char *get_surname()const;
	inline char *get_name()const;
	inline char *get_patronymic()const;
	inline char *get_phone()const;
	inline char *get_univercity()const;
	inline char *get_country()const;
	inline char *get_city()const;
	inline char *get_group()const;

};