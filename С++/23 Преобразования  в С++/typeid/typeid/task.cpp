#include <iostream>
#include <typeinfo.h>
using namespace std;

template <class T>
void f1(T a)
{
	cout << "+++++++++++++++" << endl;
	cout << typeid(a).name()<<endl;// пример использования оператора typeid
	cout << typeid(T).name()<<endl;// пример использования оператора typeid

	if(strcmp(typeid(T).name(),"char")==0){
		cout << "This is char" << endl;
	}

	if(typeid(T) == typeid(char)){
		cout << "Some text" << endl;
	}

	cout << "---------------" << endl;

	
}
class A{};
class B: public A{};

void main()
{
	A* pa = new B;
	int x=4;
	double y=4;
	A a;
	char c = 'c';
	f1(x);// int
	f1(y);// double
	f1(a);// class A
	f1(pa);// class A *
	f1((B*)pa);// class B *
	f1(c);// char

}