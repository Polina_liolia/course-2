#include "Student.h"

Student::Student()		//constructor
{
	surname = nullptr;
	name = nullptr;
	patronymic = nullptr;
	phone = nullptr;
	univercity = nullptr;
	country = nullptr;
	city = nullptr;
	group = nullptr;
}

//overloaded constructor:
Student::Student(char *aSurname, char *aName, char *aPatronymic, char *aPhone, char *aUnivercity, char *aCountry, char *aCity, char *aGroup)
{
	if (aSurname && strlen(aSurname))					//setting surname
	{
		surname = new char[strlen(aSurname) + 1];
		strcpy_s(surname, strlen(aSurname) + 1, aSurname);
	}
	else
		surname = nullptr;

	if (aName && strlen(aName))							//setting name
	{
		name = new char[strlen(aName) + 1];
		strcpy_s(name, strlen(aName) + 1, aName);
	}
	else
		name = nullptr;

	if (aPatronymic && strlen(aPatronymic))				//setting patronymic
	{
		patronymic = new char[strlen(aPatronymic) + 1];
		strcpy_s(patronymic, strlen(aPatronymic) + 1, aPatronymic);
	}
	else
		patronymic = nullptr;

	if (aPhone && strlen(aPhone))						//setting phone number
	{
		phone = new char[strlen(aPhone) + 1];
		strcpy_s(phone, strlen(aPhone) + 1, aPhone);
	}
	else
		phone = nullptr;

	if (aUnivercity && strlen(aUnivercity))				//setting univercity
	{
		univercity = new char[strlen(aUnivercity) + 1];
		strcpy_s(univercity, strlen(aUnivercity) + 1, aUnivercity);
	}
	else
		univercity = nullptr;

	if (aCountry && strlen(aCountry))					//setting a country
	{
		country = new char[strlen(aCountry) + 1];
		strcpy_s(country, strlen(aCountry) + 1, aCountry);
	}
	else
		country = nullptr;

	if (aCity && strlen(aCity))					//setting a city
	{
		city = new char[strlen(aCity) + 1];
		strcpy_s(city, strlen(aCity) + 1, aCity);
	}
	else
		city = nullptr;

	if (aGroup && strlen(aGroup))					//setting a group
	{
		group = new char[strlen(aGroup) + 1];
		strcpy_s(group, strlen(aGroup) + 1, aGroup);
	}
	else
		group = nullptr;
}

Student::~Student()		//destructor
{
	delete[] surname;
	delete[] name;
	delete[] patronymic;
	delete[] phone;
	delete[] univercity;
	delete[] country;
	delete[] city;
	delete[] group;
}

//methods to get the meaning of every property:
char *Student::get_surname()const
{
	return surname;
}

char *Student::get_name()const
{
	return name;
}

char *Student::get_patronymic()const
{
	return patronymic;
}

char *Student::get_phone()const
{
	return phone;
}

char *Student::get_univercity()const
{
	return univercity;
}

char *Student::get_country()const
{
	return country;
}

char *Student::get_city()const
{
	return city;
}

char *Student::get_group()const
{
	return group;
}

	void Student::set_surname(char *aSurname)				//setting surname
	{
		if (aSurname && strlen(aSurname))
		{
			surname = new char[strlen(aSurname) + 1];
			strcpy_s(surname, strlen(aSurname) + 1, aSurname);
		}
		else
			surname = nullptr;
	}

	void Student::set_name(char *aName)						//setting name
	{
		if (aName && strlen(aName))
		{
			name = new char[strlen(aName) + 1];
			strcpy_s(name, strlen(aName) + 1, aName);
		}
		else
			name = nullptr;
	}

	void Student::set_patronymic(char *aPatronymic)			//setting patronymic
	{
		if (aPatronymic && strlen(aPatronymic))
		{
			patronymic = new char[strlen(aPatronymic) + 1];
			strcpy_s(patronymic, strlen(aPatronymic) + 1, aPatronymic);
		}
		else
			patronymic = nullptr;
	}

	void Student::set_phone(char *aPhone)					//setting phone number
	{
		if (aPhone && strlen(aPhone))
		{
			phone = new char[strlen(aPhone) + 1];
			strcpy_s(phone, strlen(aPhone) + 1, aPhone);
		}
		else
			phone = nullptr;
	}

	void Student::set_univercity(char *aUnivercity)					//setting univercity
	{
		if (aUnivercity && strlen(aUnivercity))
		{
			univercity = new char[strlen(aUnivercity) + 1];
			strcpy_s(univercity, strlen(aUnivercity) + 1, aUnivercity);
		}
		else
			univercity = nullptr;
	}

	void Student::set_country(char *aCountry)						//setting a country
	{
		if (aCountry && strlen(aCountry))
		{
			country = new char[strlen(aCountry) + 1];
			strcpy_s(country, strlen(aCountry) + 1, aCountry);
		}
		else
			country = nullptr;
	}

	void Student::set_city(char *aCity)								//setting a city
	{
		if (aCity && strlen(aCity))
		{
			city = new char[strlen(aCity) + 1];
			strcpy_s(city, strlen(aCity) + 1, aCity);
		}
		else
			city = nullptr;
	}

	void Student::set_group(char *aGroup)							//setting a group
	{
		if (aGroup && strlen(aGroup))
		{
			group = new char[strlen(aGroup) + 1];
			strcpy_s(group, strlen(aGroup) + 1, aGroup);
		}
		else
			group = nullptr;
	}
	
	void print_student(Student &a)
	{
		cout << "Surname: " << a.surname << "\n";
		cout << "Name: " << a.name << "\n";
		cout << "Patronymic: " << a.patronymic << "\n";
		cout << "Phone: " << a.phone << "\n";
		cout << "Univercity: " << a.univercity << "\n";
		cout << "Country: " << a.country << "\n";
		cout << "City: " << a.city << "\n";
		cout << "Group: " << a.group << "\n";
	}


