var button1 = document.querySelector('body > div > div:nth-child(1) > input[type="button"]:nth-child(1)');
var button2 = document.querySelector('body > div > div:nth-child(1) > input[type="button"]:nth-child(2)');
var button3 = document.querySelector('body > div > div:nth-child(1) > input[type="button"]:nth-child(3)');

var divNews = document.querySelector('body > div > div:nth-child(2)');

var underPointer = document.querySelector('.colored>div:first-of-type');

var news1 = "Congrats! This is the first news today!";
var news2 = "This news is the second.";
var news3 = "And the last news for today...";

function chooseNews1()
{
    divNews.innerHTML = news1;
    divNews.style.display='table-cell';
    divNews.style.verticalAlign = 'middle';
    divNews.style.textAlign = 'center';
    divNews.style.fontSize = '1.5em';
    divNews.style.color = 'black';
}

function chooseNews2()
{
    divNews.innerHTML = news2;
    divNews.style.display='table-cell';
    divNews.style.verticalAlign = 'middle';
    divNews.style.textAlign = 'center';
    divNews.style.fontSize = '1.5em';
    divNews.style.color = 'black';
}

function chooseNews3()
{
    divNews.innerHTML = news3;
    divNews.style.display='table-cell';
    divNews.style.verticalAlign = 'middle';
    divNews.style.textAlign = 'center';
    divNews.style.fontSize = '1.5em';
    divNews.style.color = 'black';
}

function makeFieldColored() {
    var x = event.clientX;
    var y = event.clientY;
    //colors to set background color:
    var red = (x < 255) ? x : Math.floor(Math.random() * 255);
    var green =  (x * y < 255) ? x*y : Math.floor(Math.random() * 255);
    var blue = (y < 255) ? y : Math.floor(Math.random() * 255);

    //colors to set font color:
    var fontRed = (red > 127) ? 0 : 255;
    var fontGreen = (green > 127) ? 0 : 255;
    var fontBlue = (blue > 127) ? 0 : 255;

    divNews.style.backgroundColor = 'rgb(' + red + ',' + green + ',' + blue +')';
    divNews.style.color = 'rgb(' + fontRed + ',' + fontGreen + ',' + fontBlue +')';

    var pointerTop = y - divNews.getBoundingClientRect().top + divNews.clientTop - 10;
    var pointerLeft = x - divNews.getBoundingClientRect().left + divNews.clientLeft - 10;

    underPointer.style.display = "block";
    underPointer.style.boxShadow = "0 0 10px rgba(0,0,0,0.5)";
    underPointer.style.top = pointerTop + 'px';
    underPointer.style.left = pointerLeft +'px';
    underPointer.style.backgroundColor = 'rgb(' + (red + 30) + ',' + (green + 30) + ',' + (blue + 30) +')';

    spin.style.backgroundColor = 'rgb(' + (red - 30) + ',' + (green - 30) + ',' + (blue - 30) +')';
    spin.style.boxShadow = "0 0 10px rgba(0,0,0,0.5)";
}

var spin = document.querySelector(".colored>div:last-of-type");

var R = 50; //radius of moving trajectory
var elementRadius = 10;

var pointerX = 0,
    pointerY = 0;

function moveCircle() {
    spin.style.display = "block";
    pointerX = event.clientX;
    pointerY = event.clientY;
}

var angle = 0;

setInterval(function () {
    var x = R * Math.cos((angle*Math.PI)/180);
    var y = R * Math.sin((angle*Math.PI)/180);
    angle++;
    spin.style.left = x + pointerX - elementRadius + 'px';
    spin.style.top =  y + pointerY - elementRadius + 'px';
}, 20);


function clearField() {
    divNews.style.backgroundColor = 'white';
    divNews.style.color = 'rgb(' + 0 + ',' + 0 + ',' + 0 +')';
    underPointer.style.display = "none";
    spin.style.display = "none";
    spin.style.backgroundColor = "none";
}

button1.addEventListener("click", chooseNews1);
button2.addEventListener("click", chooseNews2);
button3.addEventListener("click", chooseNews3);

divNews.addEventListener("mousemove", makeFieldColored);
divNews.addEventListener('mousemove', moveCircle);
divNews.addEventListener("mouseout", clearField);

