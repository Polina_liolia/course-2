#pragma once
#include <iostream>
#include "date.h"
#include "MyString.h"
using std::cout;
using std::ostream;

class Person
{
protected:
	String surname;
	String name;
	String patronymic;
	date birth;
	String phone;
	String univercity;
	String country;
	String city;

	Person();

	//overloaded constructor:
	Person(String aSurname, String aName, String aPatronymic, short aDay, short aMonth, short aYear, String aPhone, String aUnivercity, String aCountry, String aCity);
	Person(const Person &a);
	~Person();

public:
	//methods to set the meaning of every property:

	void set_surname(String aSurname);
	void set_name(String aName);
	void set_patronymic(String aPatronymic);
	void set_birthdate(short aDay, short aMonth, short aYear)
	{
		birth.set_day(aDay);
		birth.set_month(aMonth);
		birth.set_year(aYear);
	}
	void set_phone(String aPhone);
	void set_univercity(String aUnivercity);
	void set_country(String aCountry);
	void set_city(String aCity);
	int print_to_file(FILE *f, char* file_name);

	//methods to get the meaning of every property:
	String get_surname()
	{
		return surname;
	}

	String get_name()
	{
		return name;
	}

	String get_patronymic()
	{
		return patronymic;
	}

	int get_birth_day()
	{
		return birth.get_day();
	}

	int get_birth_month()
	{
		return birth.get_month();
	}

	int get_birth_year()
	{
		return birth.get_year();
	}

	String get_phone()
	{
		return phone;
	}

	String get_univercity()
	{
		return univercity;
	}

	String get_country()
	{
		return country;
	}

	String get_city()
	{
		return city;
	}


	void print_data();		//to print information about Person

	bool operator==(Person a);
	bool operator!=(Person a);
	bool operator>(Person a);
	bool operator<(Person a);
	bool operator>=(Person a);
	bool operator<=(Person a);

	friend ostream& operator << (ostream& os, const Person &aSt);
};