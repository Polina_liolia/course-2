#pragma once
#include"Company.h"
#include <vector>
using std::vector;

class Search
{
private:
	set<Company> all_companies;
	set<string>words_to_find;
	vector<Company> search_rezult;

public:
	Search() {};
	~Search() {};

	void add_company(string aName, string aDescription, accaunt aType = Free);
	void remove_company(Company aName);
	void remove_company(int index);
	void remove_company(string aName);

	int get_company_index(string aName);
	int get_company_index_in_search_rezult(string aName);

	void set_name(Company aName, string name_new);

	void set_description(Company aName, string aDescription);

	void set_type(Company aName, accaunt aType);

	void add_key_word(Company aName, string aKey_word);

	void change_key_word(Company aName, string aWord_old, string aWord_new);

	string get_name(int index)const;
	string get_description(int index)const;
	accaunt get_type(int index)const;

	void set_name(int index, string aName);
	void set_description(int index, string aDescription);
	void set_type(int index, accaunt aType);
	void add_key_word(int index, string aKey_word);
	void change_key_word(int index, string aWord_old, string aWord_new);
	
	void add_word_to_find(string word);
	void clear_words_to_find();

	string get_name(Company aName) const;

	string get_description(Company aName) const;

	accaunt get_type(Company aName) const;

	void clear_search_rezult();
	void find_company_by_words();
	void show_search_rezult();

	friend ostream& operator<<(ostream& os, Search& S);
};
