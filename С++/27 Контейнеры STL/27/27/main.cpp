#include<iostream>
#include<string>
#include<Windows.h>
#include<fstream>
#include<vector>
using std::cout;
using std::string;
using std::wstring;
using std::ofstream;
using std::ifstream;
using std::vector;
using std::ios;


bool is_odd(int i)
{
	return (i % 2 == 1);
}

void main()
{
	string s = "test";
//print string to file	
	ofstream OF("1.bin", ios::binary);
	size_t sz = s.size();
	OF.write(reinterpret_cast<char*>(&sz), sizeof(sz));
	OF.write(s.c_str(), sz);
	OF.close();

//read string from file
	string s1;
	ifstream IF ("1.bin", ios::binary);
	IF.read(reinterpret_cast<char*>(&sz), sizeof(sz));
	s1.resize(sz);
	IF.read(&s1[0], sizeof(sz));
//readen string
	cout << s1 << "\n";

	//creating vector instance, containing 5 elements:
	vector<int> v5;

	v5.push_back(1);
	v5.push_back(7);
	v5.push_back(3);
	v5.push_back(4);

	for (auto i : v5)
	{
		cout << i << " ";
	}
	cout << "\n";

/*	int t = 0;
	for (auto i : v5) 
	{
		v5[i] = t++;
	}

	for (auto i : v5)
	{
		cout << i << " ";
	}
	cout << "\n";
	*/
	cout << v5.back() << "\n";	//last element
	cout << *v5.data() << "\n"; //first element
	
	vector<int> test;
	test.push_back(1);
	test.push_back(2);
	test.push_back(3);
	test.push_back(4);
	test.push_back(5);
	test.push_back(6);
	test.push_back(7);
	
	vector <int>::iterator Iter;
	cout << "test =";
	for (Iter = test.begin(); Iter != test.end(); Iter++)
		cout << " " << *Iter;
	cout << "\n";

	int index = 6;
	if (index < 0 || index >= test.size())
		cout << "Wrong index\n";
	else
		test.erase(test.begin()+index);
	cout << "test =";
	for (Iter = test.begin(); Iter != test.end(); Iter++)
		cout << " " << *Iter;
	cout << "\n";
	
	
}