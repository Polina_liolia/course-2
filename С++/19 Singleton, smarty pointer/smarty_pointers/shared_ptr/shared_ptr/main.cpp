#include <iostream>
#include <memory>

using namespace std;

class A{

public:
	A(){
		cout << "Constructor A" << endl;
	}

	~A(){
		cout << "Destructor A" << endl;
	}
};


void main(){

	const int arrSz = 5;
	shared_ptr<A> arr[arrSz];

	arr[0] = shared_ptr<A>(new A);
	arr[1] = shared_ptr<A>(new A);
	arr[2] = shared_ptr<A>(new A);
	arr[3] = shared_ptr<A>(new A);
	arr[4] = shared_ptr<A>(new A);

	for (int i = 0; i < arrSz; i++)
		cout << "arr[" << i << "]= " << arr[i].get() << endl;

	cout << "=======================" << endl;
	shared_ptr<A> tmp;
	tmp = arr[3]; 


	for (int i = 0; i < arrSz; i++)
		cout << "arr[" << i << "]= " << arr[i].get() << endl;

	cout << "=======================" << endl;

	cout << "tmp = " << tmp.get() << endl;


}