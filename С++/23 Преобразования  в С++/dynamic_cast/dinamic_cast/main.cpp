#include <iostream>
using namespace std;
class A{
	int x;
public:

	virtual void f1(){
		cout << "A::f1" << endl;
	}
	virtual void f2(){
		cout << "A::f2" << endl;
	}
};
class B : public A{
	
public:
	int *y;
	B(){
		y = NULL;
	}
	void f2(){
		cout << "B::f2   " << y<< endl;
	}
	
};

void main(){




	
	A* a1 = new B;
	a1->f1();
	B* b1 = (B*) a1;
	b1->f2();
	cout << "============================" << endl;

	A*a2 = new A;
	a2->f1();
	B* b2 = (B*) a2;
	b2->f2();

	cout  <<"b2->y = "<< b2->y << endl;
	
	cout << "============================" << endl;


	A* pa = new B;

	B* pb = dynamic_cast<B*>(pa);

	if(pb) cout << "pb OK"<<endl;
	else  cout << "pb Error"<<endl;
//============================================
	A* pa2 = new A;

	B* pb2 = dynamic_cast<B*>(pa2);
	
	if(pb2) cout << "pb2 OK"<<endl;
	else  cout << "pb2 Error"<<endl;

	
}