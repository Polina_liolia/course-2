//�����, ������� ��������� ������� ���������, � ������� ����� ������ ��� ������� � ��� ��������� ��������.
#include <iostream>
#include <string>
#include <conio.h>
#include "Array.h"
#include "Element.h"
using namespace std;


void main(){
	Array<int, int> arr1;

	arr1.insert(8,13);
	arr1.insert(9,23);

	arr1[8] = 56;
	arr1[24] = 3;

	cout << arr1[8] << endl;
	cout << arr1[9] << endl;
	cout << arr1[23] << endl;
	cout << arr1[24] << endl;

	cout << "Size: " << arr1.getSize() << endl;
	cout << "==============================================" << endl;
	Array<string, string> arr2;
	arr2["elem1"] = "value 1";
	arr2["elem2"] = "value 2";
	arr2["elem3"] = "value 3";
	arr2["elem4"] = "value 4";

	cout << arr2["elem1"] << endl;
	cout << arr2["elem2"] << endl;
	cout << arr2["elem3"] << endl;
	cout << arr2["elem4"] << endl;
	cout << "==============================================" << endl;

	arr2["elem3"] = "other text";

	cout << arr2["elem1"] << endl;
	cout << arr2["elem2"] << endl;
	cout << arr2["elem3"] << endl;
	cout << arr2["elem4"] << endl;






getch();
}