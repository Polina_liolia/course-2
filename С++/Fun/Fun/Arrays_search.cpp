#include "Header.h"



//Binar search of first three elements x in the array
int binar_search(int *arr, int sz, int x, int *rezults)
{
	for (int i = 0; i<3; i++)
		rezults[i] = -1;
	int begin = 0;
	int end = sz - 1;
	if (x < arr[begin] || x > arr[end])
		return 0;
	int found = 0;
	while (begin < end)
	{
		int mid = (begin + end) / 2;
		if (x < arr[mid])
			end = mid;
		else if (x == arr[mid])
		{
			end = mid;
			begin = mid;
		}
		else
			begin = mid + 1;
	}
	if (arr[end] == x)//if an element was found
	{
		int i = 0;
		for (i = end; i >= 0; i--)
			if (arr[i] != x)
			{
				rezults[found] = i + 1;
				break;
			}
			else if (i == 0)
				rezults[found] = i;
		found++;
		for (found; found < 3; found++)
		{
			if (arr[i + found] == x)
				rezults[found] = i + found;
			else break;
		}
	}
	return found;
}

//Line search of first three elements x in the array
int line_search(int *arr, int sz, int x, int *rezults)
{
	for (int i = 0; i<3; i++)
		rezults[i] = -1;
	int found = 0;
	for (int i = 0; i<sz; i++)
		if (arr[i] == x)
		{
			rezults[found] = i;
			found++;
			for (found; found<3; found++)
				if (arr[i + found] == x)
					rezults[found] = i + found;
				else break;
				break;
		}
	return found;
}

//Universal search of first three elements x in the array
int first_three_indexes(int *arr, int sz, int x, int(*p)(int*, int, int, int*))
{
	insert_sort_ascending(arr, sz);
	int found = 0;
	int *rezults = new int[sz];
	found = p(arr, sz, x, rezults);
	delete[] rezults;
	return found;
}

