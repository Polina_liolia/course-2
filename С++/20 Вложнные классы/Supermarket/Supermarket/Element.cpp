#include "Element.h"
Element::Element()
{
	next = prev = nullptr;
}
Element::Element(const Element & aEl)	//copy constructor
{
	value = aEl.value;
	next = new Element;
	*next = *aEl.next;
	prev = new Element;
	*prev = *aEl.prev;
}

Element::Element(Department aValue)
{
	value = aValue;
	next = prev = nullptr;
}

void Element::set_value(Department aValue)
{
	value = aValue;
}

Department &Element::get_value()
{
	return value;
}
void Element::set_next(Element* aNext)
{
	next = aNext;
}
Element* Element::get_next()
{
	return next;
}

void Element::set_prev(Element* aPrev)
{
	prev = aPrev;
}
Element* Element::get_prev()
{
	return prev;
}
