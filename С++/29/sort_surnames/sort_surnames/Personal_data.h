#pragma once
#include <iostream>
#include <vector>
#include <string>
using std::string;
using std::vector;

class Personal_data
{
private:
	string surname;
	string name;

public:
	Personal_data() {};
	Personal_data(string aSurname, string aName);
	~Personal_data() {};

	void set_name(string aName);
	void set_surname(string aSurname);

	string get_name()const;
	string get_surname()const;

	bool operator >(const Personal_data &D);
	bool operator <(const Personal_data &D);

};