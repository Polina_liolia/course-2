#include "Schoolboy.h"

Schoolboy::Schoolboy(){}

Schoolboy::Schoolboy(string aName, string aSurname, date aBirth_day, string aAdress, string aSchool, string aPhone)
{
	name = aName;
	surname = aSurname;
	birth_day = aBirth_day;
	address = aAdress;
	school = aSchool;
	phone = aPhone;
}

Schoolboy::~Schoolboy(){}

void Schoolboy::set_name(string aName)
{
	name = aName;
}

void Schoolboy::set_surname(string aSurname)
{
	surname = aSurname;
}

void Schoolboy::set_birth_day(date aBirth_day)
{
	birth_day = aBirth_day;
}

void Schoolboy::set_address(string aAdress)
{
	address = aAdress;
}

void Schoolboy::set_school(string aSchool)
{
	school = aSchool;
}

void Schoolboy::set_phone(string aPhone)
{
	phone = aPhone;
}

string Schoolboy::get_name() const
{
	return name;
}

string Schoolboy::get_surname() const
{
	return surname;
}

date Schoolboy::get_birth_day() const
{
	return birth_day;
}

string Schoolboy::get_address() const
{
	return address;
}

string Schoolboy::get_school() const
{
	return school;
}

string Schoolboy::get_phone() const
{
	return phone;
}

Schoolboy Schoolboy::operator=(const Schoolboy &S)
{
	name = S.name;
	surname = S.surname;
	birth_day = S.birth_day;
	address = S.address;
	school = S.school;
	phone = S.school;
	return *this;
}

bool Schoolboy::operator==(const Schoolboy & S)
{
	return name == S.name && surname == S.surname && birth_day == S.birth_day;
}

bool Schoolboy::operator!=(const Schoolboy & S)
{
	return name != S.name && surname != S.surname && birth_day != S.birth_day;
}

void Schoolboy::print_to_file(char * file_name)
{
	ofstream F(file_name, ios::binary);
	// checking if file was successfully opened, othervice generating exception:
	if (!F)
		throw "Schoolboy: Can't open file for writing";
	int buf = name.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));
	F.write(reinterpret_cast<char*>(&name[0]), buf);
	
	buf = surname.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));
	F.write(reinterpret_cast<char*>(&surname[0]), buf);

	birth_day.print_to_file(F);

	buf = address.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));
	F.write(reinterpret_cast<char*>(&address[0]), buf);

	buf = school.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));
	F.write(reinterpret_cast<char*>(&school[0]), buf);

	buf = phone.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));
	F.write(reinterpret_cast<char*>(&phone[0]), buf);
	
	F.close();
}

void Schoolboy::read_from_file(char * file_name)
{
	ifstream F(file_name, ios::binary);
	// checking if file was successfully opened, othervice generating exception:
	if (!F)
		throw "Schoolboy: Can't open file for reading";
	int buf;
	F.read(reinterpret_cast<char*>(&buf), sizeof(int));
	name.resize(buf);
	F.read(reinterpret_cast<char*>(&name[0]), buf);

	F.read(reinterpret_cast<char*>(&buf), sizeof(int));
	surname.resize(buf);
	F.read(reinterpret_cast<char*>(&surname[0]), buf);

	birth_day.read_from_file(F);

	F.read(reinterpret_cast<char*>(&buf), sizeof(int));
	address.resize(buf);
	F.read(reinterpret_cast<char*>(&address[0]), buf);

	F.read(reinterpret_cast<char*>(&buf), sizeof(int));
	school.resize(buf);
	F.read(reinterpret_cast<char*>(&school[0]), buf);

	F.read(reinterpret_cast<char*>(&buf), sizeof(int));
	phone.resize(buf);
	F.read(reinterpret_cast<char*>(&phone[0]), buf);

	F.close();
}

