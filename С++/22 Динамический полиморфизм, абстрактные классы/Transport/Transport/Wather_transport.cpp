#include "Wather_transport.h"


Wather_transport::Wather_transport() : Transport()	//default constructor
{
	displacement = 0;
	speed_units = knots;
}

//overloaded constructor
Wather_transport::Wather_transport(string aModel, double aWeight, double aMax_speed, double aDisplacement, speed aSpeed_units) :
	Transport(aModel, aWeight, aMax_speed, aSpeed_units)
{
	displacement = aDisplacement;
}

//method to set or change property displacement
void Wather_transport::set_displacement(double aDisplacement)
{
	displacement = aDisplacement;
}

//method to get meaning of property displacement
double Wather_transport::get_displacement() const
{
	return displacement;
}

//virtual method to get meaning of property speed_units
speed Wather_transport::get_speed_units() const
{
	cout << "Speed units of wather transport - "; 
	switch (speed_units)
	{
	case 0: cout << "km_h\n"; break;
	case 1: cout << "Mach\n"; break;
	case 2: cout << "miles_h\n"; break;
	case 3: cout << "knots\n"; break;
	}
	return speed_units;
}

//virtual method to print ell information about Wather_transport instance
void Wather_transport::print()
{
	Transport::print();
	cout << "Displacement: " << displacement << " t.\n";
}

