#include "Point3D.h"

point3D::point3D(char *aName, float aX, float aY, float aZ)	//������������� �����������
{
	x = aX;
	y = aY;
	z = aZ;
	if (aName && strlen(aName))
	{
		name = new char[strlen(aName) + 1];
		strcpy_s(name, strlen(aName) + 1, aName);
	}
	else
		name = nullptr;
}

void point3D::set_name(char *aName)			//������ ��� �����
{
	if (aName && strlen(aName))
	{
		name = new char[strlen(aName) + 1];
		strcpy_s(name, strlen(aName) + 1, aName);
	}
	else
		name = nullptr;
}

int point3D::save_to_file(FILE *f, char *file_name)
{
	if (f == nullptr)
		fopen_s(&f, file_name, "wb");			//opens a binar file to write 
	if (f == nullptr)							//if file was not opened
		return -1;

	int sz = strlen(name) + 1;
	fwrite(&sz, sizeof(int), 1, f);				//to write a size of a string
	fwrite(name, sz, 1, f);						//to write a point name
	fwrite(&x, sizeof(float), 1, f);			//to write x
	fwrite(&y, sizeof(float), 1, f);			//to write y
	fwrite(&z, sizeof(float), 1, f);			//to write z

	fclose(f);
	return 0;
}

int point3D::read_file(FILE *f, char *file_name)
{
	if (f == nullptr)
		fopen_s(&f, file_name, "rb");			//opens a binar file to write 
	if (f == nullptr)							//if file was not opened
		return -1;

	int sz = 0;
	fread(&sz, sizeof(int), 1, f);				// to read a size of a string
	name = new char[sz];
	fread(name, sz, 1, f);						//to read a point name		
	fread(&x, sizeof(float), 1, f);				//to read x
	fread(&y, sizeof(float), 1, f);				//to read y
	fread(&z, sizeof(float), 1, f);				//to read z

	fclose(f);
	return 0;
}