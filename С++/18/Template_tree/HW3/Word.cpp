#include "Word.h"

Word::Word()	//default constructor
{
	counter = 0;
}

/*overloaded constructor
takes: 
String aEn - word in english
String aRus - word in russian
int aCounter - counter of word's calls
*/
Word::Word(String aEn, String aRus, int aCounter)	
{
	en = aEn;
	rus = aRus;
	counter = aCounter;
}

Word::Word(const Word & aWord)	//copy constructor
{
	en = aWord.en;
	rus = aWord.rus;
	counter = aWord.counter;
}

Word::~Word()	//destructor
{
}

void Word::set_word(String aEn, String aRus, int aCounter)	//to set english word, russion translation and calls' counter
{
	en = aEn;
	rus = aRus;
	counter = aCounter;
}

void Word::set_rus(String aRus)	//to add a russian translation (takes translation in String)
{
	rus = aRus;
}


int Word::get_counter() const	//returns meaning of word's calls counter
{
	return counter;
}


void Word::add_count()	//to add one more calling of a word
{
	counter++;
}

String Word::get_en() const	//to get word in english (instance of class String)
{
	return en;
}

String Word::get_translation()	//to get word in russion (instance of class String)
{
	return rus;
}

//to save all data about word to file
int Word::print_to_file(FILE * f, char * file_name)
{
	if (f == nullptr)
		fopen_s(&f, file_name, "ab");		//opens a binar file to write to the end
	if (f == nullptr)						//if file was not opened
		return -1;

	int sz = strlen(en.get_String()) + 1;
	fwrite(&sz, sizeof(int), 1, f);
	fwrite(en.get_String(), sz, 1, f);		//to write word in english

	sz = strlen(rus.get_String()) + 1;
	fwrite(&sz, 4, 1, f);
	fwrite(rus.get_String(), sz, 1, f);		//to write word in russion

	fwrite(&counter, 4, 1, f);				//to write access counter

	fclose(f);
	return 0;
}

// operators, overloaded for class Word:

Word Word::operator=(const Word & tmp)	
{
	en = tmp.en;
	rus = tmp.rus;
	counter = tmp.counter;
	return *this;
}

bool Word::operator==(const Word & tmp)
{
	return en == tmp.en;
}

bool Word::operator!=(const Word & tmp)
{
	return en != tmp.en;
}

bool Word::operator>(const Word & tmp)
{
	return en > tmp.en;
}

bool Word::operator>=(const Word & tmp)
{
	return en >= tmp.en;
}

bool Word::operator<(const Word & tmp)
{
	return en < tmp.en;
}

bool Word::operator<=(const Word & tmp)
{
	return en <= tmp.en;
}
