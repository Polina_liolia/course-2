#pragma once
#include "Element.h"
// �������� ������ Element

class Vocabulary{
	Element* root;
public:
	Vocabulary();
	~Vocabulary();
	void add(String En, String Rus, int Counter);
	
	void print();
	void print_helper_simmetric(Element *el);
	void print_helper_straight(Element *el);
	void print_helper_reversed(Element *el);
	
	void print_top3_popular();
	Word *print_helper_popular(Element * el, Word *top);

	void print_top3_unpopular();
	Word *print_helper_unpopular(Element * el, Word *top);

	void show_word(String word);
	void show_word_helper(Element * el, String word);

	void set_translation(String en, String ru);

	void set_transl_helper(Element * el, String en, String ru);
	
};


