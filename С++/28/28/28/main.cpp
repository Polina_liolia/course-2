#include<iostream>
#include<map>
#include<set>
#include<vector>
#include<iterator>
#include<algorithm>
#include<functional>

using std::cout;
using std::map;
using std::multimap;
using std::pair;
using std::set;
using std::vector;
using std::back_inserter;	 //returns iterator to insert to the end of container
using std::back_insert_iterator; //iterator to insert to the end of container
using std::insert_iterator;
using std::inserter;
using std::iterator;
using std::sort;
using std::less;
using std::greater;


void main()
{
	cout << "\n______________________________Map__________________________________________\n";
	map<int, int> a;
	typedef pair<int, int> Int_pair;
	a.insert(Int_pair(1, 21));
	a.insert(Int_pair(2, 22));
	a.insert(Int_pair(3, 33));
	a.insert(Int_pair(99, 4));
	a.insert(Int_pair(55, 1));
	a.insert(Int_pair(15, 2));
	a.insert(Int_pair(19, 3));
	a.insert(Int_pair(5, 1));
	a.insert(Int_pair(3, 7));
	a.insert(Int_pair(222, 9));
	a.insert(Int_pair(222, 9));

	a[85] = 9;
	a[96] = 3;
	a[0] = 7;

	a.at(222) = 6;
	a.erase(15);

	for (auto i : a)
		cout << i.first <<" " << i.second << "\n";

	//inserting random elements to map using inserter:
	insert_iterator<map<int, int>> iter = inserter(a, a.begin());

	for (int i = 0; i < 10; i++)
	{
		auto pp = pair<int, int>(rand() % 10, rand() % 100);
		*iter = pp;
	}
	
	for (auto i : a)
		cout << i.first << " " << i.second << "\n";

	cout << "\n______________________________Multimap_____________________________________\n";
	multimap<int, int> m;

	pair<multimap<int, int>::iterator, multimap<int, int>::iterator> p_non_const;	//non-const iterator (value of elements can be changed)
	pair<const multimap<int, int>::iterator, const multimap<int, int>::iterator> p_const;	//const iterator (value of elements can not be changed)


	m.insert(Int_pair(5, 8));
	m.insert(Int_pair(5, 9));
	m.insert(Int_pair(5, 10));
	m.insert(Int_pair(5, 11));
	m.insert(Int_pair(5, 12));

	for (auto i : m)
		cout << i.first << " " << i.second << "\n";
	
	for (auto p = m.equal_range(5); p.first != p.second; p.first++)	//equal_range returns pair of iterators;
		//first iterator points on the first element with pointed key value;
		//second iterator points on the next element after the last element with pointed key value;
		p.first->second = 45;
	for (auto i : m)
		cout << i.first << " " << i.second << "\n";


	cout << "\n____________________________________Set_______________________________________\n";

	set<int> s;
	auto ps = s.insert(8);	//returns pair: iterator on value inserted and bool (true if insert succeed, false if not)
	cout << "\n Result of \"8\" insertion " << ps.second << "\n";
	s.insert(3);
	s.insert(5);
	s.insert(5);
	s.insert(33);
	for (auto i : s)
		cout << i << "\n";

	s.erase(ps.first);
	cout << "\nAfter \"8\" deletion:\n";
	for (auto i : s)
		cout << i << "\n";

	cout << "\n________________________Vector middle sort_________________________________\n";
	
	vector<int> V;
	
	//inserting random elements to map using inserter:
	insert_iterator<vector<int>> iter_v = inserter(V, V.begin());

	for (int i = 0; i < 10; i++)
	{
		auto pp = rand() % 10;
		*iter_v = pp;
	}

	for (auto i : V)
		cout << i << "\n";

	cout << "Sort (except the first and the last two elements):\n";
	sort(V.begin() + 2, V.end() - 2, greater<int>());	//decendin sort	(accending sort is default)
	
	for (auto i : V)
		cout << i << "\n";
}
