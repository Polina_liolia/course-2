#include "Header.h"

/*���� ����������� ����� (a).
Actions:
'a' ���������� ���� 3 � ���;
'b' ������� ��� � ��� ����������� ��������� �����;
'c' ���������� ������ ���� � ���;
'd' ����� ��� ���� ������ 5;
'e' ������������ ��� ���� ������ 7;
'f' ������� ��� � ��� ����������� ����� �� 0 �� 5 ������������(�����).
'g' ����������� �����
'h' ������������ �����
	any other letter - returns -1 */

int natural_number(int a, char action)
{
	int digit = 0, //digits of a number
		last_digit = 0,//last digit of a number
		digits_3_counter = 0,//cycle counter of digits '3' in number
		last_digit_counter = 0,//cycle counter of digits, which coincide with the last digit in number
		counter_0_5 = 0,//cycle counter of digits, that range from 0 to 5 inclusive in number inputed
		even_counter = 0,//cycle counter of even digits of a number
		sum_more_5 = 0,//sum of digits, that are more than 5 in a number
		mult_more_7 = 1,//multiplication of digits, that are more than 7 in a number
		min = 9,//minimum digit of a number
		max = 0;//maximum digit of a number
	last_digit = a % 10;
	do
	{
		digit = a % 10;
		if (3 == digit)
			digits_3_counter++;
		if (last_digit == digit)
			last_digit_counter++;
		if (digit % 2 == 0)
			even_counter++;
		if (digit > 5)
			sum_more_5 += digit;
		if (digit > 7)
			mult_more_7 *= digit;
		if (digit >= 0 && digit <= 5)
			counter_0_5++;
		if (digit < min)
			min = digit;
		if (digit > max)
			max = digit;
		a /= 10;
	} while (a != 0);
	switch (action)
	{
	case 'a': return digits_3_counter; break;
	case 'b': return last_digit; break;
	case 'c': return even_counter; break;
	case 'd': return sum_more_5; break;
	case 'e': return mult_more_7; break;
	case 'f': return counter_0_5; break;
	case 'g': return min; break;
	case 'h': return max; break;
	default: return -1;
	}
}

/*����������, ������� ��� � ������������������ �������� ����
(��������: 10, -4, 12, 56, -4 ���� �������� 3 ����)*/
int change_sign(int* arr, int sz)
{
	int x1 = 0,//numbers of sequence
		x2 = 0,//previous number of sequence
		change = 0;//cycle counter of sign changes
	for (int i = 0; i < sz; i++)
	{
		x1=arr[i];
		if (x1 < 0 && x2 > 0 || x1 > 0 && x2 < 0)
			change++;
		x2 = x1;
	}
	return change;
}

//������, ����������� ���������� ������� �����:
bool simple_number(int a)
{
	bool simple_number = false;
	int div = 0;
	for (int i = 1; i <= a; i++)
	{
		if (a%i == 0)
		{
			div++;
			if (div > 2)
				break;
		}
		else
			continue;
	}
	if (2 == div)
		simple_number = true;
	return simple_number;
}

//������� ��� �������� ����� �� ���������� � ����������������� �������
void convert10_16(int number10)
{
	cout << "Decimal number: " << number10 << "\nHexadecimal number: ";
	int remainder = 0,//remainder of the division
		rezult = 0,//rezult of divizion
		j = 0;//cycle counter of converting iterations
	rezult = number10;
	while (rezult > 0)//counting a number of iterations, needed to convert a decimal number to hexadecimal
	{
		int temp = rezult;
		rezult /= 16;
		remainder = temp - rezult * 16;
		j++;
	}

	for (int i = j; i > 0; i--)//outputting remainders (starting from the last) to get a hexadecimal number
	{
		rezult = number10;
		for (int k = 0; k < i; k++)
		{
			int temp = rezult;
			rezult /= 16;
			remainder = temp - rezult * 16;
		}
		switch (remainder)
		{
		case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8:
		case 9: cout << remainder; break;
		case 10: cout << "A"; break;
		case 11: cout << "B"; break;
		case 12: cout << "C"; break;
		case 13: cout << "D"; break;
		case 14: cout << "E"; break;
		case 15: cout << "F"; break;
		default: cout << "Error\n";
		}
	}
	cout << "\n";
}

//������� ���������� ����������� ������ �������� ���� ���������� �����.
int max_common_div(int a, int b)
{
	int max = 0;//maximal divisor
	if (a > b)
		max = b;
	else
		max = a;
	for (int i = max; i > 0; i--)
		if (a%i == 0 && b%i == 0)
		{
			max = i;
			break;
		}
	return max;
}

//������� ������� ����� ���� ������������ �����.
int digits_sum_3(int a)
{
	int sum = a % 10 + (a / 10) % 10 + (a / 100) % 10;
	return sum;
}

//������� ��� ������� ����������� ������ �������� ���� ����������� ����� (�������� �������).
int GCD(int a, int b)
{
	int GCD = 1,//the greatest common divisor
		r1 = 0, r2 = 0, r3 = 1;//variables to realise the Euclides' algorithm
	if (a > b)
		r1 = a,
		r2 = b;
	else
		r1 = b,
		r2 = a;
	while (r3 != 0)
	{
		r3 = r1 % r2;
		r1 = r2;
		r2 = r3;
	}
	GCD = r1;
	return GCD;
}

//������� ������ �������������.
float diskriminant(float a, float b, float c)
{
	float D = 0;//meaning of diskriminant
	D = b*b - 4 * a*c;
	return D;
}

//Function to return number of roots of the square equation; calculating roots
int roots(float a, float b, float c, float &R1, float &R2)
{
	float D = 0;//meaning of diskriminant
	D = b*b - 4 * a*c;
	if (D > 0)
	{
		R1 = (-b + sqrt(D)) / (2 * a);
		R2 = (-b - sqrt(D)) / (2 * a);
		return 2;
	}
	if (D = 0)
	{
		R1 = -b / (2 * a);
		R2 = R1;
		return 1;
	}
	R1 = 0;
	R2 = 0;
	return 0;
}

//�������, ����������� ��� ���������� � �������� �� �������� �������.
//Variant 1
void change(int &a, int &b)
{
	int temp = a;
	a = b;
	b = temp;
}

//Variant 2
void change(int *a, int* b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

//Function to return number of roots of the square equation; calculating roots
int roots(float a, float b, float c, float &R1, float &R2)
{
	float D = 0;//meaning of diskriminant
	D = b*b - 4 * a*c;
	if (D > 0)
	{
		R1 = (-b + sqrt(D)) / (2 * a);
		R2 = (-b - sqrt(D)) / (2 * a);
		return 2;
	}
	if (D = 0)
	{
		R1 = -b / (2 * a);
		R2 = R1;
		return 1;
	}
	R1 = 0;
	R2 = 0;
	return 0;
}

/*������� ������ ����� �������� ������ ������ �� ������� ��������������� � �������� ��������������� ���� ����������
������ ������ ������.*/

int coldest_week(int arr[][31], const int str, float arr_middle_t[])
{
	/*Finding middle day and night temperatures for every week (4 full weeks and 1 half-week (3 days))
	Inputing middle temperatures we get to the array*/
	for (int i = 0; i < 5; i++)//counting 5 weeks of the month
	{
		int sum_t = 0;//sum of day and night temperatures of one week
		for (int k = 0; k<str; k++)//strings changing - day or night temperature
			for (int l = i * 7; l < (i * 7) + 7; l++)//counting sum of temperatures in one week
			{
				sum_t += arr[k][l];
				if (i == 4 && l == i * 7 + 2)//if it is the last day of 5th week 
					break;
			}
		if (i != 4)//if it is a full week
			arr_middle_t[i] = sum_t / (7.0 * 2);
		else //if it is a 5th week (half-week)
			arr_middle_t[i] = sum_t / (3.0 * 2);
	}

	/*Finding middle day temperatures for every week (4 full weeks and 1 half-week (3 days))
	Inputing middle temperatures we get to the array*/
	float middlle_day_t[5] = { 0 };
	for (int i = 0; i < 5; i++)//counting 5 weeks of the month
	{
		int sum_day_t = 0;//sum of day temperatures of one week
		for (int j = i * 7; j < (i * 7) + 7; j++)//counting sum of temperatures in one week
		{
			sum_day_t += arr[0][j];
			if (i == 4 && j == i * 7 + 2)//if it is the last day of 5th week 
				break;
		}
		if (i != 4)//if it is a full week
			middlle_day_t[i] = sum_day_t / 7.0;
		else //if it is a 5th week (half-week)
			middlle_day_t[i] = sum_day_t / 3.0;
	}
	//Finding the coldest week of the month (day temperature)
	float lowest_temperature = 0;
	int coldest_week = 0;
	for (int i = 0; i < 5; i++)//counting 5 weeks of the month
		if (*(middlle_day_t + i) < lowest_temperature || i == 0)
		{
			lowest_temperature = *(middlle_day_t + i);
			coldest_week = i;
		}
	return (coldest_week + 1);
}

//Converts a DEC digit to the BIN format
void DEC_to_BIN_converter(int digit)
{
	cout << digit << " = ";
	for (long long int i = pow(2, (sizeof(digit) * 8)); i>0; i = i >> 1)
		if ((digit & i) == 0)
			cout << 0;
		else
			cout << 1;
	cout << "\n";
}

//������� �� ����� ����������� ��� ������ ������������� �� ���������.
void print_rectangle(int cols, int rows, int fill)
{
	if (fill)			//filled
	{
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
				printf("*");
			printf("\n");
		}
		printf("\n");
	}
	else				//without filling
	{
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
				if (i == 0 || i == (rows - 1) || j == 0 || j == (cols - 1))
					printf("*");
				else
					printf(" ");
			printf("\n");
		}
		printf("\n");
	}
}