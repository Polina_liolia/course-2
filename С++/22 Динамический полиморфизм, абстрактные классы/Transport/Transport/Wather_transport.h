#pragma once
#include"Transport.h"

class Wather_transport : public Transport
{
protected:
	double displacement;

public:
	Wather_transport();
	Wather_transport(string aModel, double aWeight, double aMax_speed, double aDisplacement, speed aSpeed_units = knots);
	~Wather_transport() {};

public:
	void set_displacement(double aDisplacement);
	double get_displacement()const;
	virtual speed get_speed_units()const;
	virtual void print();
};