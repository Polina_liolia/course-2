#include <iostream>
#include "Flat.h"
using std::cout;
 
flat::flat(char *aAddress, float aSquare, float aPrice)
{
	if (aAddress && strlen(aAddress))
	{
		address = new char[strlen(aAddress) + 1];
		strcpy_s(address, strlen(aAddress) + 1, aAddress);
	}
	else
		address = nullptr;
	square = aSquare;
	price = aPrice;
}

flat::flat(const flat &aFlat)
{
	if (aFlat.address && strlen(aFlat.address))
	{
		address = new char[strlen(aFlat.address) + 1];
		strcpy_s(address, strlen(aFlat.address) + 1, aFlat.address);
	}
	else
		address = nullptr;
	square = aFlat.square;
	price = aFlat.price;
}

flat::~flat()
{
	delete[] address;
}

void flat::set_address(char *aAddress)
{
	address = new char[strlen(aAddress) + 1];
	strcpy_s(address, strlen(aAddress) + 1, aAddress);
}

inline void flat::set_square(float aSquare)
{
	square = aSquare;
}

inline void flat::set_price(float aPrice)
{
	price = aPrice;
}

inline char *flat::get_address()const
{
	return address;
}

inline float flat::get_square()const
{
	return square;
}

inline float flat::get_price()const
{
	return price;
}

inline void flat::print()const
{
	cout << "Address: " << address << "\n" << "Square: " << square << "\n" << "Price: " << price << "\n\n";
}

bool flat::operator==(const flat &aFlat)const		//comparing squares of flats
{
	if (square == aFlat.square)
		return true;
	return false;
}

flat flat::operator=(const flat &aFlat)				//assigns the value of another example to current one
{
	if (aFlat.address)
	{
		if (address)
			delete[] address;
		address = new char[strlen(aFlat.address) + 1];
		strcpy_s(address, strlen(aFlat.address) + 1, aFlat.address);
	}
	square = aFlat.square;
	price = aFlat.price;
	return *this;
}

bool flat::operator>(const flat &aFlat)const		//comparing prices of two flats
{
	if (price > aFlat.price)
		return true;
	return false;
}