#include <iostream>
using namespace std;
#include "Object.h"
#include "Ellipse.h"
#include "Rectangle.h"
void main()
{

	Object* ob_arr[3];

	ob_arr[0] = new Object;
	ob_arr[1] = new Ellipse;
	ob_arr[2] = new Rectangle;

    for(int i=0; i<3; i++)
		ob_arr[i]->square();

	cout << ((Rectangle*)ob_arr[2])->get_rect_prop() << endl;
	cout << ((Rectangle*)ob_arr[0])->get_rect_prop() << endl;	//������ � �� ���������� ������, ���� ������������ dynamic_cast
	//=======================================================
	
	Rectangle *r = dynamic_cast<Rectangle *>(ob_arr[2]);
	if(r)
		cout << r->get_rect_prop() << endl;

	r = dynamic_cast<Rectangle *>(ob_arr[0]);
	if(r)
		cout << r->get_rect_prop() << endl;


	for(int i=0; i<3; i++)
		delete ob_arr[i];

}