// ���� WINDOWS.H �������� �����������, �������, � ��������� ������� ������������ ��� ��������� ���������� ��� Windows. 
#include <windows.h>
#include <tchar.h>
#include<ctime>
#define _UNICODE


// �������� ������� ���������
LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);


TCHAR szClassWindow[] = TEXT("��������� ����������");	/* ��� ������ ���� */

void guess_number(HWND hWnd);

INT WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow)
{
	HWND hWnd;
	MSG Msg;
	WNDCLASSEX wcl;


	// 1. ����������� ������ ����
	wcl.cbSize = sizeof(wcl);	// ������ ��������� WNDCLASSE
								//������������ �� ����, ���� ������� ������ �� ����������� ��� �� ���������:
	wcl.style = CS_HREDRAW | CS_VREDRAW;	// CS (Class Style) - ����� ������ ����
	wcl.lpfnWndProc = WindowProc;	// ����� ������� ���������
	wcl.cbClsExtra = 0;		// ������������ Windows 
	wcl.cbWndExtra = 0; 	// ������������ Windows 
	wcl.hInstance = hInst;	// ���������� ������� ����������
	wcl.hIcon = LoadIcon(NULL, IDI_APPLICATION);	// �������� ����������� ������
	wcl.hCursor = LoadCursor(NULL, IDC_ARROW);	// �������� ������������ �������	
	wcl.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	// ���������� ���� ����� ������
	wcl.lpszMenuName = NULL;	// ���������� �� �������� ����
	wcl.lpszClassName = szClassWindow;	// ��� ������ ����
	wcl.hIconSm = NULL;	// ���������� ��������� ������ ��� ����� � ������� ����

						// 2. ����������� ������ ����
	if (!RegisterClassEx(&wcl))
		return 0; // ��� ��������� ����������� - �����

				  // 3. �������� ����
				  // ��������� ���� �  ���������� hWnd ������������� ���������� ����
	hWnd = CreateWindowEx(
		0,		// ����������� ����� ����
		szClassWindow,	//��� ������ ����
		TEXT("������ Windows ����������"), // ��������� ����
		WS_OVERLAPPEDWINDOW,				// ����� ����
											/* ���������, �����, ����������� ������ �������, ��������� ����, ������ ������������ � ���������� ����  */
		CW_USEDEFAULT,	// �-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// y-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// ������ ����
		CW_USEDEFAULT,	// ������ ����
		NULL,			// ���������� ������������� ����
		NULL,			// ���������� ���� ����
		hInst,		// ������������� ����������, ���������� ����
		NULL);		// ��������� �� ������� ������ ����������

					// 4. ����������� ����
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd); // ����������� ����

	//function to play in guessing numbers
	guess_number(hWnd);
	

	// 5. ������ ����� ��������� ���������
	while (GetMessage(&Msg, NULL, 0, 0)) // ��������� ���������� ��������� �� ������� ���������
	{
		TranslateMessage(&Msg);	// ���������� ���������
		DispatchMessage(&Msg);	// ��������������� ���������
	}
	return Msg.wParam;
}

LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	switch (uMessage)
	{
	case WM_DESTROY: // ��������� � ���������� ���������
		PostQuitMessage(0); // ������� ��������� WM_QUIT
		break;
	default:
		// ��� ���������, ������� �� �������������� � ������ ������� ������� ������������ ������� Windows �� ��������� �� ���������
		return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}
	return 0;
}

void guess_number(HWND hWnd)
{
	_TCHAR szHeadMsg1[] = _TEXT("Are you ready to play?..");
	_TCHAR szMsg1[] = _TEXT("Guess any number from 1 to 100.\nI am going to try to guess it.");
	MessageBox(hWnd, szMsg1, szHeadMsg1, MB_OK);
	int count = 0;
	srand(time(NULL));
	_TCHAR szHeadMsg2[] = _TEXT("Maybe this number is...");
	int msgRezult = 0;
	while (msgRezult != IDYES)
	{
		int iGuess = rand() % (100 - 1 + 1) + 1;
		wchar_t szGuess[20];
		_itow_s(iGuess, szGuess, 10); // �����, �����, ������� ��������� (����������) 
		msgRezult = MessageBox(hWnd, szGuess, szHeadMsg2, MB_YESNO | MB_ICONQUESTION);
		count++;
	}
	wchar_t szRezult[20];
	_itow_s(count, szRezult, 10);
	_TCHAR szMsgPart1[100] = _TEXT("I have guessed your number after ");
	_TCHAR szMsgPart3[] = _TEXT(" try.");
	wcscat_s(szMsgPart1, szRezult);
	wcscat_s(szMsgPart1, szMsgPart3);
	MessageBox(hWnd, szMsgPart1, TEXT("I've done it!"), MB_OK | MB_ICONEXCLAMATION);
	int play_again = MessageBox(hWnd, TEXT("Do you want to play again?"), TEXT("What are we going to do?"), MB_YESNO | MB_ICONQUESTION);
	if (play_again == IDYES)
		guess_number(hWnd);
}
