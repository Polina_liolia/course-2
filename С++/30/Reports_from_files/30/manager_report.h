#pragma once
#include "user_report.h"

class manager_report :
	public user_report
{
protected:
	string delivery_date;
	string inquiry_status;

public:
	manager_report() {};
	~manager_report() {};

	void read_data(char * data_file);
	void report(char * data_file, char * draft_file, char * report_file);
};

