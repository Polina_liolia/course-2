#include "functor.h"
functor::functor(){
	a = b = 0;
}

bool functor::operator()(int x, int y){
	a = x;
	b = y;
	return true;
}

void functor::show(){
	cout << a << ":" << b << endl;
}