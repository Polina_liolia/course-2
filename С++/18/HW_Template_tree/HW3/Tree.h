#pragma once
#include"Element.h"

template<typename A>
class Tree
{
	Element<A> *root;

public:

	Tree() {
		root = NULL;
	}
	// ���������� �������� � ������
	void add(A val) {
		if (!root)
			root = new Element<A>(val);
		else
		{
			Element<A> *tmp = root;
			while (tmp)
			{
				if (val < tmp->get_value())
					if (tmp->get_left() == NULL)
					{
						tmp->set_left(new Element<A>(val));
						break;
					}
					else tmp = tmp->get_left();
				else {
					if (val > tmp->get_value())
						if (tmp->get_right() == NULL)
						{
							tmp->set_right(new Element<A>(val));
							break;
						}
						else tmp = tmp->get_right();
					else tmp = NULL;
				}
			}
		}
	}

	void set(A Old, A New)
	{
		set_helper(root, Old, New);
	}

	void set_helper(Element<A>* el, A Old, A New)
	{
		if (el->get_value() == Old)
		{
			el->get_value() = New;
			return;
		}
		else
		{
			if (el->get_right())
				set_helper(el->get_right(), Old, New);
			if (el->get_left())
				set_helper(el->get_left(), Old, New);
		}
	}

	Element<A> *search(A aVal)
	{
		return search_helper(root, aVal);
	}

	Element<A> *search_helper(Element<A> * el, A aVal)
	{
		if (el->get_value() == aVal)
			return el;
		else
		{
			if (el->get_right())
				el = search_helper(el->get_right(), aVal);
			if (el->get_left())
				el = search_helper(el->get_left(), aVal);
		}
		return nullptr;
	}

	void remove(A aVal)
	{
		remove_helper(root, aVal);
	}

	void remove_helper(Element<A> * el, A aVal);

	A min() {
		Element<A> *tmp = root;
		while (tmp && tmp->get_left()) {
			tmp = tmp->get_left();
		}
		return tmp ? tmp->get_value() : A();
	}

	void print()
	{
		print_helper_simmetric(root);
	}


	void print_helper_simmetric(Element<A> * el)	//min - root - max  (to print 3 unpopular words)
	{
		if (el->get_left())
			print_helper_simmetric(el->get_left());
		cout << el->get_value() << " ";
		if (el->get_right())
			print_helper_simmetric(el->get_right());
	}

	void print_helper_straight(Element<A> * el)	//root - min - max	(to print all vocabulary)
	{
		cout << el->get_value() << " ";
		if (el->get_left())
			print_helper_straight(el->get_left());
		if (el->get_right())
			print_helper_straight(el->get_right());
	}

	void print_helper_reversed(Element<A> * el)	//root - max - min	(to print 3 popular words)
	{
		cout << el->get_value() << " ";
		if (el->get_right())
			print_helper_reversed(el->get_right());
		if (el->get_left())
			print_helper_reversed(el->get_left());
	}
};


