function changeColor() {
    var coloredDiv = document.querySelector("div.colored");
    var red = Math.floor(Math.random()*256+0);
    var green = Math.floor(Math.random()*256+0);
    var blue = Math.floor(Math.random()*256+0);
    coloredDiv.style.backgroundColor = "rgb(" + red + "," + green + "," + blue + ")";
}

function clearColor() {
    var coloredDiv = document.querySelector("div.colored");
    coloredDiv.style.backgroundColor = "none";
}

var coloredDiv = document.querySelector("div.colored");
coloredDiv.addEventListener("mousemove", changeColor);
coloredDiv.addEventListener("mouseout", clearColor);