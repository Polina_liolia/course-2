var ul_dresses = document.querySelector(".dresses");
function dressChecked ()
{
	var target = event.target;
	if (target.tagName != "LI" && target.tagName != "IMG")
		return;
	if (target.tagName == "IMG")
		target = target.parentNode;
	var check_mark = target.querySelector(".check_mark");
	var selectedItems = document.querySelector(".selected_items>ul");
	if (check_mark != undefined)
	{
		target.removeChild(check_mark);
		target.dataset.condition = undefined;
		var allSelectedLis = selectedItems.querySelectorAll("li");
		for (var i = 0; i < allSelectedLis.length; i++)
		{
			if (allSelectedLis[i].dataset.id == target.dataset.id)
				selectedItems.removeChild(allSelectedLis[i]);
		}
		target.dataset.id = undefined;
	}
	else
	{
		check_mark = document.createElement("div");
		check_mark.classList.add("check_mark");
		var img = target.querySelector("img");
		target.insertBefore(check_mark, img);
		target.dataset.condition = "selected";
		target.dataset.id = Math.random() * (1000 - 1) + 1;
		var clone = target.cloneNode(true);
		clone.removeChild(clone.querySelector(".check_mark"));
		selectedItems.appendChild(clone);
	}
}

ul_dresses.addEventListener("click", dressChecked);