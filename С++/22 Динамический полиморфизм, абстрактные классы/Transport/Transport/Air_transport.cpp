#include"Air_transport.h"

Air_transport::Air_transport() : Transport()	//default constructor
{
	max_height = 0;
	speed_units = km_h;
}

//overloaded constructor
Air_transport::Air_transport(string aModel, double aWeight, double aMax_speed, speed aSpeed_units, double aMax_height) : 
	Transport(aModel, aWeight, aMax_speed, aSpeed_units)
{
	max_height = aMax_height;
}

//method to set or change property max_height
void Air_transport::set_max_height(double aMax_height)
{
	max_height = aMax_height;
}

//method to get meaning of property max_height
double Air_transport::get_max_height() const
{
	return max_height;
}

//virtual method to get meaning of property speed_units
speed Air_transport::get_speed_units() const
{
	cout << "Speed units for air transport - ";
	switch (speed_units)
	{
	case 0: cout << "km_h\n"; break;
	case 1: cout << "Mach\n"; break;
	case 2: cout << "miles_h\n"; break;
	case 3: cout << "knots\n"; break;
	}
	return speed_units;
}

//virtual method to print all information about Air_transport instance
void Air_transport::print()
{
	Transport::print();	//calling of basic class method
	cout << "Maximal height of fly: " << max_height << " m\n";
}


