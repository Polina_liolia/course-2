#include <iostream>
using namespace std;
class A{
	int w;
public:
	int x;
	enum state {opened, closed};
protected:
	int y;
private:
	int z;
public:
	int get_w(){
		return w;
	}
	void set_w(int aW){
		if(w>5) w = aW;
	}
};

void main(){
	A a;
	a.x = 10;
	//a.y = 8;
	//a.z = 7;
	//a.w = 4;
	a.set_w(6);
	cout << a.get_w() << endl;
	a.set_w(8);
	cout << a.get_w() << endl;
	

	cout << A::opened << endl;
	cout << A::closed << endl;
	
}