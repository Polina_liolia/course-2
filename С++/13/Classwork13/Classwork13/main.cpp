#include "Stack.h"
#include <iostream>
using std::ostream;
using std::cout;
using std::endl;

ostream& operator<<(ostream &o, Stack &stack) {
	for (int i = 0; i < stack.size(); i++)
		o << stack[i] << " ";
	o << endl;
	return o;
}

void main() {
	Stack s(10);
	s.set_capacity(5);
	s.push(1);
	s.push(2);
	s.push(3);
	s.push(4);
	s.push(5);
	s.push(6);
	s.push(7);
	cout << "capacity" << s.capacity() << endl;
	cout << s;
	cout << "size:" << s.size() << endl;
	cout << "isEmpty:" << (s.isEmpty() ? "true" : "false") << endl;
	cout << "isFull:" << (s.isFull() ? "true" : "false") << endl;
	cout << "s.set_capacity(6)" << endl;
	s.set_capacity(6);
	cout << "capacity" << s.capacity() << endl;
	cout << "s.set_capacity(1)" << endl;
	s.set_capacity(1);
	cout << "capacity" << s.capacity() << endl;
	cout << "isFull:" << (s.isFull() ? "true" : "false") << endl;
	cout << "pop:" << s.pop() << endl;
	cout << "pop:" << s.pop() << endl;
	cout << "pop:" << s.pop() << endl;
	cout << s;
	cout << "size:" << s.size() << endl;
	cout << "isEmpty:" << (s.isEmpty() ? "true" : "false") << endl;
	cout << "isFull:" << (s.isFull() ? "true" : "false") << endl;
	cout << "pop:" << s.pop() << endl;
	cout << "pop:" << s.pop() << endl;
	cout << "pop:" << s.pop() << endl;
	cout << "pop:" << s.pop() << endl;
	cout << s;
	cout << "size:" << s.size() << endl;
	cout << "isEmpty:" << (s.isEmpty() ? "true" : "false") << endl;


}