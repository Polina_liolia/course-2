#pragma once
#include <iostream>

enum type { odd, even, more7 };

class Remove
{
private:
	type method;
public:
	Remove(type aMethod)
	{
		method = aMethod;
	}

	
	bool operator()(int A)const
	{
		switch (method)
		{
		case odd:
			if (A % 2 != 0)
				return true;
			return false;
		case even:
			if (A % 2 == 0)
				return true;
			return false;
		case more7:
			if (A > 7)
				return true;
			return false;
		}
	}
};
