#include"UniVar.h"

UniVar::UniVar()	//default constructor initializes Integer with 0, sets type_var value as Int
{
	Integer = 0;
	Float = 0;
	value = Int;
}

UniVar::UniVar(int a)	//overloaded constructor initializes Integer with argument-value, sets type_var value as Int
{
	Integer = a;
	value = Int;
}

UniVar::UniVar(float a) //overloaded constructor initializes Float with argument-value, sets type_var value as Fl
{
	Float = a;
	value = Fl;
}

UniVar::UniVar(String a) //overloaded constructor initializes Str with argument-value, sets type_var value as St
{
	Str = a;
	value = St;
}

int UniVar::get_Int() const	//to get int value of instance UniVar
{
	if (value == Int)
		return Integer;
	return 0;
}

float UniVar::get_Float() const //to get float value of instance UniVar
{
	if (value == Fl)
		return Float;
	return 0.0f;
}

String UniVar::get_str() const	//to get String value of instance UniVar
{
	if (value == St)
		return Str;
	return String();
}

void UniVar::set(int i)	//sets Intereg value with argument-value, sets type_var value as Int
{
	Integer = i;
	value = Int;
}

void UniVar::set(float f) //sets Float value with argument-value, sets type_var value as Fl
{
	Float = f;
	value = Fl;
}

void UniVar::set(String s) //sets Str value with argument-value, sets type_var value as St
{
	Str = s;
	value = St;
}

//sets Intereg value with argument-value, sets type_var value as Int
//returns instance of UniVar
UniVar UniVar::operator=(int a) 
{
	Integer = a;
	value = Int;
	return *this;
}

//sets Float value with argument-value, sets type_var value as Fl
//returns instance of UniVar
UniVar UniVar::operator=(float a)  
{
	Float = a;
	value = Fl;
	return *this;
}

//sets Str value with argument-value, sets type_var value as St
//returns instance of UniVar
UniVar UniVar::operator=(String a) 
{
	Str = a;
	value = St;
	return *this;
}

UniVar::operator int()
{
	if (value == Int)
		return Integer;
	return 0;
}

UniVar::operator float()
{
	if (value == Fl)
		return Float;
	return 0.0f;
}

UniVar::operator String()
{
	if (value == St)
		return Str;
	return String();
}

