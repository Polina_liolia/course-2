#include "Header.h"

//Counts strings and characters in a file (getting file name using cin)
void file_str_chr_counter_cin(int &chr, int &str)
{
	char file_name[50];
	printf("Input the name of file to count characters and strings\n");
	gets_s(file_name, 50);
	FILE *f;
	fopen_s(&f, file_name, "a+");	//opens file to read and write in the end (in the case if file does not exist)
	if (f == NULL)					//if file was not opened
	{
		cout << "Opening file error!\n";
		return;
	}
	rewind(f);						//moving to the beginning of the file
	while (true)
	{
		if (fgetc(f) == EOF)		//if it is the end of a file
		{
			str++;
			break;
		}
		chr++;
		fseek(f, -1, SEEK_CUR);		//moving on one element back to avoid passing characters//moving on one element back to avoid passing characters
		if (fgetc(f) == '\n')
			str++;
	}
	fclose(f);
}


struct book
{
	int number;
	char author_name[20];
	char author_surname[20];
	char book_name[20];
	int amount;
	float price;
};

//readig existing file, inputting information from it to structure book
bool read_file(book** arr, int &str, int col, char* file_name)
{
	FILE *f;						//creating a file
	fopen_s(&f, file_name, "at+");	//opens file to read and write to the end of a file (to create a file if it does not exist)
	if (f == NULL)					//if file was not opened
	{
		cout << "Opening file error!\n";
		return 0;
	}
	if (fgetc(f) == EOF)			//if it is an empty file	
	{
		printf("The file is empty, nothing to read!");
		fclose(f);
		return 0;
	}
	fseek(f, 0, SEEK_SET);			//moving to the beginning of a file
	int i = str;
	do
	{
		if (fgetc(f) == EOF)		//if it is the end of a file
			break;
		fseek(f, -1, SEEK_CUR);		//moving on one character back after fgetc check (to avoid passing symbols)
		arr[i] = new book[col];
		str++;
		fscanf_s(f, "%i%s%s%s", &arr[i][0].number, &arr[i][0].author_name, 20, &arr[i][0].author_surname, 20, &arr[i][0].book_name, 20);
		fscanf_s(f, "%i%f\n", &arr[i][0].amount, &arr[i][0].price);
		i++;
	} while (true);
	fclose(f);						//closes file
	return 1;
}


//inputting information from structure book to file
void print_to_file(book** arr, int str, char* file_name)
{
	FILE *f;						//creating a file
	fopen_s(&f, file_name, "wt+");	//opens file to write
	if (f == NULL)					//if file was not opened
	{
		cout << "Opening file error!\n";
		return;
	}
	for (int i = 0; i < str; i++)
	{
		fprintf(f, "%i\t", arr[i][0].number);
		fprintf(f, "%-20s\t", arr[i][0].author_name);
		fprintf(f, "%-20s\t", arr[i][0].author_surname);
		fprintf(f, "%-20s\t", arr[i][0].book_name);
		fprintf(f, "%-i\t", arr[i][0].amount);
		fprintf(f, "%-.2f\n", arr[i][0].price);
	}
	fclose(f);						//closes file

}


/*To change a price of a curent book (if number_book_to_change=0 - of all books);
action:
* - multiply price
/ - divide price
% - raise/reduce on price_change, per cents (depends on a sign of price_change: + or -)
+/-  raise/reduce on price_change, dollars
increase_decrease:
i - to increase price
d - to decrease price
*/
void change_price(book** arr, int &str, int col, char action, int price_change, int number_book_to_change)
{
	int first_book = 0,
		last_book = 0;
	if (number_book_to_change == 0)
		last_book = str - 1;
	else
	{
		first_book = number_book_to_change;
		last_book = number_book_to_change;
	}
	for (int i = first_book; i <= last_book; i++)
		switch (action)
		{
		case '*': 	arr[i][0].price *= price_change; break;
		case '/':	arr[i][0].price /= price_change;	break;
		case '%':
			if (price_change < 0)
				arr[i][0].price -= arr[i][0].price * ((price_change*-1) / 100);
			if (price_change > 0)
				arr[i][0].price += arr[i][0].price * (price_change / 100);
			break;
		case '+':  arr[i][0].price += price_change; break;
		case '-':  arr[i][0].price -= price_change; break;
		default: printf("Unknown sign!\n");
		}
}

//To save every string of a file in two-dementional array (returns pointer on array wit a text)
char** save_file_text_in_array(char* file_name, int &sz)
{
	FILE *f;						//to ofen a file with a text
	fopen_s(&f, file_name, "rt");	//opens file to read 
	if (f == NULL)					//if file was not opened
	{
		cout << "Opening file error!\n";
		return 0;
	}
	if (fgetc(f) == EOF)			//if it is an empty file	
	{
		printf("The file is empty, nothing to read!");
		fclose(f);
		return 0;
	}
	fseek(f, 0, SEEK_SET);				//moving to the beginning of a file
										//counting number of strings in a file
	sz = 0;
	while (true)
	{
		if (fgetc(f) == EOF)			//if it is the end of a file
		{
			sz++;
			break;
		}
		fseek(f, -1, SEEK_CUR);			//moving on one character back after fgetc check (to avoid passing symbols)
		if (fgetc(f) == '\n')			//if it is the end of a string
			sz++;
	}
	fseek(f, 0, SEEK_SET);				//moving to the beginning of a file

	char **text = new char*[sz];		//an array to save strings of a text
										//saving strings of a file in array									
	for (int i = 0; i<sz; i++)
	{
		if (fgetc(f) == EOF)			//if it is the end of a file
			break;
		fseek(f, -1, SEEK_CUR);			//moving on one character back after fgetc check (to avoid passing symbols)
										//counting the amount of characters in a current string of a file
		int chr_count = 0;
		while (true)
		{
			if (fgetc(f) == EOF)		//if it is the end of a file
			{
				fseek(f, -(chr_count), SEEK_CUR);//moving to the beginning of a string
				break;
			}
			fseek(f, -1, SEEK_CUR);		//moving on one character back after fgetc check (to avoid passing symbols)
			if (fgetc(f) == '\n')		//if it is the end of a string
			{
				fseek(f, -(chr_count + 2), SEEK_CUR);//moving to the beginning of a string
				break;
			}
			chr_count++;
		}

		text[i] = new char[chr_count + 1];
		fgets(text[i], chr_count + 1, f);
		if (fgetc(f) == EOF)			//if it is the end of a file
			fseek(f, +1, SEEK_CUR);		//moving 2 characters forward to avoid cycle '\n' reading
		if (strcmp(text[i], " ") == 0)
		{
			delete[] text[i];
			sz--;
			i--;
		}
	}
	fclose(f);
	return text;
}

//To save every word of a file in two-dementional array (returns pointer on array with words)
char** save_file_words_in_array(char* file_name, int &sz)
{
	FILE *f;						//to open a file with a text
	fopen_s(&f, file_name, "rt");	//opens file to read 
	if (f == NULL)					//if file was not opened
	{
		cout << "Opening file error!\n";
		return 0;
	}
	if (fgetc(f) == EOF)			//if it is an empty file	
	{
		printf("The file is empty, nothing to read!");
		fclose(f);
		return 0;
	}
	fseek(f, 0, SEEK_SET);			//moving to the beginning of a file
	char** words = new char*[sz];
	int i = 0;
	while (true)
	{
		if (fgetc(f) == EOF)		//if it is the end of a file
			break;
		fseek(f, -1, SEEK_CUR);		//moving on one character back after fgetc check (to avoid passing symbols)
		words[i] = new char[20];
		fscanf_s(f, "%20[A-z1-9]", words[i], 20);
		if (fgetc(f) != ' ')		//if the next character in file is not a space yet
			fseek(f, +1, SEEK_CUR);
		i++;
		sz++;
	}
	fclose(f);
	return words;
}


//Prints data from array to file (with deleting all data from the file)
void print_text_to_file(char* file_name, char**text, int text_str)
{
	FILE *f;								//to ofen a file to print changed text
	fopen_s(&f, "clean_text.txt", "a+t");	//opens file to write in the end  (if file does not exist)
	if (f == NULL)							//if file was not opened
	{
		cout << "Opening file error!\n";
		return;
	}
	fclose(f);
	FILE *w;								//to ofen a file to print changed text
	fopen_s(&w, "clean_text.txt", "w");	//opens file to write in the end  (if file does not exist)
	if (w == NULL)							//if file was not opened
	{
		cout << "Opening file error!\n";
		return;
	}
	//	fflush(NULL);
	fseek(w, 0, SEEK_SET);				//moving to the beginning of a file
	for (int i = 0; i < text_str; i++)
	{
		fputs(text[i], w);
		fputs("\n", w);
	}

	fclose(w);
}

//Changes pointed words in text file and prints corrected text to another text file (file_to_save)
void censorship(char* text_file, char* words, char* file_to_save)
{
	//getting all words to change and inputting them to array
	int words_str = 0,
		&r_words_str = words_str;
	char** forbidden_words = save_file_words_in_array(words, r_words_str);


	//Save every string of a file in two-dementional array (returns pointer on array wit a text)
	int text_str = 0,
		&r_text_str = text_str;
	char** text = save_file_text_in_array(text_file, r_text_str);

	//checking text	
	for (int i = 0; i < text_str; i++)
	{
		int sz_text = strlen(text[i]);
		bool flag = false;
		while (!flag)
		{
			flag = true;
			for (int j = 0; j < words_str; j++)
			{
				char* temp = new char[sz_text + 1];
				strcpy_s(temp, sz_text + 1, text[i]);
				int forbidden_word_index = 0;
				int sz_word = strlen(forbidden_words[j]);
				int forbidden_word_number = word_search_no_reg(temp, text_str, forbidden_words[j], sz_word);
				if (forbidden_word_number != -1)
				{
					hide_word(text[i], sz_text + 1, forbidden_word_number);
					flag = false;
				}
				delete[] temp;
			}
		}
	}

	//printing corrected text to the file "clean_text.txt"
	print_text_to_file(file_to_save, text, text_str);

	//deleting all dinamic arrays
	delete_2din_arr_(forbidden_words, words_str);
	delete_2din_arr_(text, text_str);
}

//Checks, does such a login and password exist in a file
bool find_password(char* passwords_file, char* login, char* password)
{
	int str = 0,
		&r_str = str;
	char** all_accaunts_data = save_file_text_in_array(passwords_file, r_str);
	//creating a string with login and password, inputted by user (in the same format with a file)
	int sz = strlen(login) + strlen(password) + 4;
	char* user_data = new char[sz];
	user_data[0] = '\0';
	strcat_s(user_data, sz, login);
	strcat_s(user_data, sz, ";");
	strcat_s(user_data, sz, password);
	bool flag = false; //to indicate if the data was found in a file
	for (int i = 0; i < str; i++)
		if (strcmp(all_accaunts_data[i], user_data) == 0)
		{
			flag = true;
			break;
		}
	delete_2din_arr_(all_accaunts_data, str);
	delete[] user_data;
	if (flag)
		return true;
	return false;
}


