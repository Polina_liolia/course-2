#include "Header.h"

void main()
{
	Point2D *arr = new Point2D[2];

	arr[0].set_name("P1");
	arr[0].set_coordinateX(5);
	arr[0].set_coordinateY(10);

	arr[1].set_name("P2");
	arr[1].set_coordinateX(15);
	arr[1].set_coordinateY(20);

	bool flag = arr[0].operator==(arr[1]);
	std::cout << flag;
	flag = arr[0].operator!=(arr[1]);
	std::cout << flag;
	delete[] arr;

}