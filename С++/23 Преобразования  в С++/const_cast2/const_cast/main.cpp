#include <iostream>

using std::cout;
using std::endl;

void f1(const int &x){
	//x++;
	int& y = const_cast<int&>(x);
	y++;
}
int main (){
	int t = 8;
	f1(t);
	cout << t << endl;
	return 0;
}