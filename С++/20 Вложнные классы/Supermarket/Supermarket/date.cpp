#include "date.h"

short date::get_day() const
{
	return day;
}

short date::get_month() const
{
	return month;
}

short date::get_year() const
{
	return year;
}

void date::set_day(short aDay)
{
	day = aDay;
}

void date::set_month(short aMonth)
{
	month = aMonth;
}

void date::set_year(short aYear)
{
	year = aYear;
}

void date::set_date(short aDay, short aMonth, short aYear)
{
	day = aDay;
	month = aMonth;
	year = aYear;
}
