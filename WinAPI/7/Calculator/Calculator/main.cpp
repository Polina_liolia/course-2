// ���� WINDOWS.H �������� �����������, �������, � ��������� ������� ������������ ��� ��������� ���������� ��� Windows. 
#include <windows.h>
#include <tchar.h>
#include "resource.h"
#include <ctime>
#include <Strsafe.h>


// �������� ������� ���������
LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wp, LPARAM lp);
#define OPENDIALOGID 300 //constant identifier of button created

TCHAR szClassWindow[] = TEXT("��������� ����������");	/* ��� ������ ���� */

INT WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow)
{
	HWND hWnd;
	MSG Msg;
	WNDCLASSEX wcl;


	// 1. ����������� ������ ����
	wcl.cbSize = sizeof(wcl);	// ������ ��������� WNDCLASSE
								//������������ �� ����, ���� ������� ������ �� ����������� ��� �� ���������:
	wcl.style = CS_HREDRAW | CS_VREDRAW;	// CS (Class Style) - ����� ������ ����
	wcl.lpfnWndProc = WindowProc;	// ����� ������� ���������
	wcl.cbClsExtra = 0;		// ������������ Windows 
	wcl.cbWndExtra = 0; 	// ������������ Windows 
	wcl.hInstance = hInst;	// ���������� ������� ����������
	wcl.hIcon = LoadIcon(NULL, IDI_APPLICATION);	// �������� ����������� ������
	wcl.hCursor = LoadCursor(NULL, IDC_ARROW);	// �������� ������������ �������	
	wcl.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	// ���������� ���� ����� ������
	wcl.lpszMenuName = NULL;	// ���������� �� �������� ����
	wcl.lpszClassName = szClassWindow;	// ��� ������ ����
	wcl.hIconSm = NULL;	// ���������� ��������� ������ ��� ����� � ������� ����

						// 2. ����������� ������ ����
	if (!RegisterClassEx(&wcl))
		return 0; // ��� ��������� ����������� - �����

				  // 3. �������� ����
				  // ��������� ���� �  ���������� hWnd ������������� ���������� ����
	hWnd = CreateWindowEx(
		0,		// ����������� ����� ����
		szClassWindow,	//��� ������ ����
		TEXT("������ Windows ����������"), // ��������� ����
		WS_OVERLAPPEDWINDOW,				// ����� ����
											/* ���������, �����, ����������� ������ �������, ��������� ����, ������ ������������ � ���������� ����  */
		CW_USEDEFAULT,	// �-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// y-���������� ������ �������� ���� ����
		CW_USEDEFAULT,	// ������ ����
		CW_USEDEFAULT,	// ������ ����
		NULL,			// ���������� ������������� ����
		NULL,			// ���������� ���� ����
		hInst,		// ������������� ����������, ���������� ����
		NULL);		// ��������� �� ������� ������ ����������

	HWND b1 = CreateWindow(L"button", L"Open Dialog", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10, 10, 100, 30, hWnd, (HMENU)OPENDIALOGID, hInst, NULL);

	// 4. ����������� ����
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd); // ����������� ����



						// 5. ������ ����� ��������� ���������
	while (GetMessage(&Msg, NULL, 0, 0)) // ��������� ���������� ��������� �� ������� ���������
	{
		TranslateMessage(&Msg);	// ���������� ���������
		DispatchMessage(&Msg);	// ��������������� ���������
	}
	return (int)Msg.wParam;
}


LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	switch (uMessage)
	{
	case WM_DESTROY: // ��������� � ���������� ���������
		PostQuitMessage(0); // ������� ��������� WM_QUIT
		break;
	case WM_COMMAND:
	{
		if (HIWORD(wParam) == BN_CLICKED) //control-defined notification code
			switch (LOWORD(wParam)) //control identifier
			{
			case OPENDIALOGID:
			{
				HWND dialog = CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_DIALOG1), hWnd, DlgProc); //unmodal dialog 
				ShowWindow(dialog, SW_SHOW);//show unmodal dialog
			}
			break;
			}
	}
	break;
	default:
		// ��� ���������, ������� �� �������������� � ������ ������� ������� ������������ ������� Windows �� ��������� �� ���������
		return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}
	return 0;
}

INT_PTR CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)//��������� ������� � ���������� ����
{
	switch (message)
	{
	case WM_INITDIALOG:
	{
		//clearing default static text:
		HWND result_stat = GetDlgItem(hWnd, IDC_STATIC1);
		SetWindowText(result_stat, TEXT(""));
	}
	break;
	case WM_CLOSE:
		EndDialog(hWnd, 0); // ��������� ������
		return TRUE;
	case WM_COMMAND:
		if (HIWORD(wParam) == BN_CLICKED) //������� ������ �������
			switch (LOWORD(wParam)) //control identifier
			{
			case IDOK:
				EndDialog(hWnd, 0); //��������� ������
				return TRUE;
			case IDCANCEL:
				EndDialog(hWnd, 0); //��������� ������
				return FALSE;
			case IDC_BUTTON9:	//result
			{
				TCHAR buf[100];
				GetDlgItemText(hWnd, IDC_EDIT2, buf, 100);
				double left_operand = _wtof(buf);
				
				GetDlgItemText(hWnd, IDC_EDIT4, buf, 100);
				double right_operand = _wtof(buf);
				
				HWND operation_text = GetDlgItem(hWnd, IDC_EDIT3);
				HWND result_stat = GetDlgItem(hWnd, IDC_STATIC1);
				TCHAR result_text[100];
				double result = 0.0;
				TCHAR Operator[10];

				int const arraysize = 50;
				TCHAR msg[arraysize];
				size_t sz = arraysize * sizeof(TCHAR);
				
				GetWindowText(operation_text, Operator, 10);
				if (!lstrcmp(Operator, TEXT("+")))
				{
					result = left_operand + right_operand;
					LPCTSTR pszFormat = TEXT("%f");
					HRESULT hr = StringCbPrintf(msg, sz, pszFormat, result);
				}
				else if (!lstrcmp(Operator, TEXT("-")))
				{
					result = left_operand - right_operand;
					LPCTSTR pszFormat = TEXT("%f");
					HRESULT hr = StringCbPrintf(msg, sz, pszFormat, result);
				}
				else if (!lstrcmp(Operator, TEXT("*")))
				{
					result = left_operand * right_operand;
					LPCTSTR pszFormat = TEXT("%f");
					HRESULT hr = StringCbPrintf(msg, sz, pszFormat, result);
				}
				else if (!lstrcmp(Operator, TEXT("/")))
				{
					result = left_operand / right_operand;
					LPCTSTR pszFormat = TEXT("%f");
					HRESULT hr = StringCbPrintf(msg, sz, pszFormat, result);
				}
				else
				{
					LPCTSTR pszFormat = TEXT("Wrong operator!");
					HRESULT hr = StringCbPrintf(msg, sz, pszFormat, NULL);
				}
				SetWindowText(result_stat, msg);
			}
			break;
			case IDC_BUTTON10:	//clear
			{
				HWND L_oper = GetDlgItem(hWnd, IDC_EDIT2);
				SetWindowText(L_oper, TEXT(""));
				HWND R_oper = GetDlgItem(hWnd, IDC_EDIT4);
				SetWindowText(R_oper, TEXT(""));
				HWND operation_text = GetDlgItem(hWnd, IDC_EDIT3);
				SetWindowText(operation_text, TEXT(""));
				HWND result_stat = GetDlgItem(hWnd, IDC_STATIC1);
				SetWindowText(result_stat, TEXT(""));
			}
			}
		break;
	}
	return FALSE;
}