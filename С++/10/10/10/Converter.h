#pragma once
#include <iostream>

class Converter
{
private:
	static char *month[12];

public:
	Converter();
	~Converter();
	int operator[](char* aIndex);
	char* operator[](int aIndex);
};