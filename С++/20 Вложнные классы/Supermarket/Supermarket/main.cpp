#include "MyString.h"
#include"date.h"
#include"employee.h"
#include"Supermarket.h"

#include <iostream>
using std::cout;
using std::endl;

//globaly overloaded output of String
ostream& operator << (ostream& os, const String &aString)
{
	if (aString.get_String() != nullptr)
		os << aString.str << "\n";
	return os;
}

//globaly overloaded output of date
ostream& operator << (ostream& os, date &aDate)
{
	os << aDate.day << "." << aDate.month << "." << aDate.year << "\n";
	return os;
}

//global outstream overload for employee
ostream& operator<<(ostream& os, const employee& dt)
{
	os << "Name: " << dt.name;
	os << "Surname: " << dt.surname;
	os << "Phone: " << dt.phone;
	os << "position: ";
	switch (dt.type)
	{
	case 0: os << "manager\n"; break;
	case 1: os << "sales\n"; break;
	case 2: os << "consultant\n"; break;
	case 3: os << "cashier\n"; break;
	case 4: os << "cleaner\n"; break;
	case 5: os << "security\n"; break;
	}
	return os;
}

//global outstream overload for department
ostream& operator << (ostream& os, Department &aDept)
{
	os << "Departmet name: " << aDept.dept_name << "\n";
	os << "Departmet founded: " << aDept.founded.get_day() << "." << aDept.founded.get_month() << "." << aDept.founded.get_year() << "\n";
	os << "Departmet last edition: " << aDept.edited.get_day() << "." << aDept.edited.get_month() << "." << aDept.edited.get_year() << "\n";
	os << "Departmet stuff:\n";
	for (int i = 0; i < aDept.size_dept_stuff; i++)
		os << aDept.dept_stuff[i] << "\n";
	return os;
}

//global outstream overload for Supermarket
ostream& operator << (ostream& os, Supermarket &aSm)
{
	os << "Departments:\n";
	aSm.depts.show();
	os << "All employee of Supermarket: \n";
	for (int i = 0; i < aSm.size_stuff; i++)
		os << i+1 << ". " << aSm.all_stuff[i] <<"\n";
	return os;
}


void main()
{
	Supermarket SM; //creating an instance of class Supermarket

	//creating three departments of Supermarket:
	SM.add_department("Grocery", 1, 1, 2017);
	SM.add_department("Clothes", 1, 1, 2017);
	SM.add_department("Backery", 1, 1, 2017);

	//adding employee to the Supermarket:
	//to the Grocery dept:
	SM.get_depts().get_value_ByIndex(0).add_employee("Misev", "Sergey", sales, "098-632-96-54", 3, 12, 1995, 15, 2, 2017);
	SM.get_depts().get_value_ByIndex(0).add_employee("Ivanov", "Petr", cleaner, "095-658-96-55", 2, 5, 1966, 3, 1, 2017);
	SM.get_depts().get_value_ByIndex(0).add_employee("Klimenov", "Stepan", consultant, "054-658-96-85", 26, 10, 1989, 4, 3, 2017);
	SM.get_depts().get_value_ByIndex(0).add_employee("Lesheva", "Tatiana", cashier, "095-658-96-74", 25, 1, 1974, 2, 5, 2017);
	SM.get_depts().get_value_ByIndex(0).add_employee("Kirsanova", "Lesya", manager, "097-258-96-22", 2, 3, 1989, 7, 1, 2017);
	SM.get_depts().get_value_ByIndex(0).add_employee("Lirov", "Andrey", security, "066-258-96-25", 2, 3, 1989, 5, 1, 2017);

	//to the Clothes dept:
	SM.get_depts().get_value_ByIndex(1).add_employee("Lisichkin", "Oleg", consultant, "099-658-96-23", 25, 2, 1996, 3, 1, 2017);
	SM.get_depts().get_value_ByIndex(1).add_employee("Polyakova", "Mariya", cashier, "098-521-96-36", 14, 2, 1996, 7, 1, 2017);
	SM.get_depts().get_value_ByIndex(1).add_employee("Morozov", "Eugen", manager, "095-33-96-24", 12, 3, 1983, 15, 2, 2017);
	SM.get_depts().get_value_ByIndex(1).add_employee("Prohorov", "Stanislav", security, "095-33-96-99", 12, 5, 1987, 7, 1, 2017);
	SM.get_depts().get_value_ByIndex(1).add_employee("Mirtova", "Svetlana", cleaner, "096-258-96-34", 15, 9, 1964, 5, 1, 2017);

	//to the Backery
	SM.get_depts().get_value_ByIndex(2).add_employee("Vasilev", "Ivan", sales, "098-658-96-55", 25, 2, 1985, 12, 1, 2017);

	cout << "Getting all employees from depts to array in Supermarket instance:\n";
	SM.get_all_stuff();

	//outputting info about Supermarket to console (depts and stuff are in ascending abc order):
	cout << SM;
	system("pause");
	system("cls");

	cout << "\nTrying to fire unexisting employee (Pivovarov) from dept:\n";
	SM.get_depts().get_value_ByIndex(1).remove_employee("Pivovarov", 2, 3, 2017);
	
	cout << "\nFiring employee Polyakova from Grocery dept.\n";
	SM.get_depts().get_value_ByIndex(1).remove_employee("Polyakova", 2, 3, 2017);
	cout << "\nRemoving Backery dept.\n";
	SM.remove_department(2);
	
	cout << SM;
	system("pause");
	system("cls");

	cout << "Changing dept name (Shoes instead Clothes) and changing employee data (Lisichkin Oleg)\n";
	SM.edit_dept_name(1, "Shoes", 3, 5, 2017);
	SM.get_depts().get_value_ByIndex(1).edit_employee_surname(0, "Lisichkin-1");
	SM.get_depts().get_value_ByIndex(1).edit_employee_name(0, "Oleg-1");
	SM.get_depts().get_value_ByIndex(1).edit_employee_phone(0, "0578547-96-3");
	SM.get_depts().get_value_ByIndex(1).edit_employee_position(0, cleaner);
	SM.get_depts().get_value_ByIndex(1).edit_employee_birth_date(0, 2, 8, 1963);

	cout << SM;
	system("pause");
	system("cls");
}