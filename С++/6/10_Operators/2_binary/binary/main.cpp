#include <iostream>
using namespace std;
//#define _MY_DEBUG_
class Point
{
	int x;
	int y;
	char* name;
public:
	Point(){
#ifdef _MY_DEBUG_
		cout << "Default constructor" << endl;
#endif
		x = 0;
		y = 0;
		name = NULL;
	}
	Point (int aX, int aY)
	{
		x = aX;
		y = aY;
		name = NULL;
	}
	~Point()
	{
		delete[] name;
	}
		//1,3,"erygeyhth"
	Point (int aX, int aY, char* aName)
	{
#ifdef _MY_DEBUG_
		cout << "Overload constructor Point (int aX, int aY, char* aName)" << endl;
#endif		
		x = aX;
		y = aY;
		if(aName)
		{
			name = new char[strlen(aName) + 1];
			strcpy(name, aName);
		}else name = NULL;
	}
	void set_name(char * aName)
	{
		if(name) delete[] name;
		name = new char[strlen(aName) + 1];
		strcpy(name, aName);
	}
	Point (const Point &p)
	{
		x = p.x;
		y = p.y;
		name = NULL;
		if(p.name){
			name = new char[strlen(p.name) + 1];
			strcpy(name, p.name);
		}
	}
	Point operator-()
	{
		return Point(-x, -y);
	}
	//Point - Point
	// Point - int
	// int - Point
	Point operator-(Point p)
	{
		return Point(x - p.x, y - p.y);
	}
	Point operator+(Point aPoint){
		char* buff = NULL;
		if(name && aPoint.name){
			buff = new char[strlen(name) + strlen(aPoint.name) + 1];
			strcpy_s(buff,strlen(name)+ strlen(aPoint.name) + 1,name);
			strcat_s(buff,strlen(name)+ strlen(aPoint.name) + 1,aPoint.name);
		}
		Point p(x + aPoint.x , y + aPoint.y, buff);
		delete[] buff;
		return p;
	}
	//        p10.operator+(p11)
	//	p13 = p10 + p11                          + p12 ...;
	//           pn
	Point operator+(int n){
		Point p(x + n, y + n);
		return p;
		
		//return Point(x + n , y + n);
	}
	//   10,11     3,4
	//    p9 =     p8 + 7;
	//   p8 + 7; p8.operator+(7);


	/*
	Point operator!()
	{
	return Point(y, x);
	}
	*/
	Point operator=(const Point& aPoint)
	{
/*
	Point p1(...);
	Point p2(...);

	Point p3 = p1;
	p3 = p2;

*/

#ifdef _MY_DEBUG_
		cout << "operator = " << endl;
#endif
		x = aPoint.x;
		y = aPoint.y;
		//name = aPoint.name;
		
		
		if(aPoint.name){
			if (name) delete[] name;
			name = new char[strlen(aPoint.name) + 1];
			strcpy_s(name, strlen(aPoint.name) + 1, aPoint.name);
		}

		return *this;
		//int x, y, z;
		//x = y = z = 20;
		// p = p1 = p2 = p3;
	}


	Point operator~(){
		return *this;
	}

	int get_x()
	{
		return x;
	}
	void set_x(int aX)
	{
		if(x > 0 && x < 1000)
			x = aX;
	}
	int get_y()
	{
		return y;
	}
	char* get_name()
	{
		return name;
	}

	

};

void main()
{


	//�����?????
	//������ ���!!!!

	Point y;

	Point p10(1,3,"erygeyhth");
	Point p24;
	p24 = ~p10;

	//p24 = p10 = p11 = p12;
	cout << "p24.X:" << p24.get_x() << endl;
	cout << "p24.Y:" << p24.get_y() << endl;
	cout << "p24.name:" << p24.get_name() << endl;
	cout << "====================="<<endl;
	

	Point p11(4,7);

	// I
	//Point p13(p10.get_x() + p11.get_x(),
	//          p10.get_y() + p11.get_y());

	// II

	//Point p13;
	//p13.set_x(p10.get_x() + p11.get_x());
	//p13.set_y(p10.get_y() + p11.get_y());

	//	p13 = p10 + p11 + p12 ...;


	Point p(10,20, "asgqaqaqg");
	//Point p1 = !p;// p.operator!();
	Point p8(45,46, "3erg23rg");
	Point p9;



	p9 = p8 + p;// p8.operator+(p);-> Point(????)
	cout << "========================================" << endl;
	cout << "p8.X:" << p8.get_x() << endl;
	cout << "p8.Y:" << p8.get_y() << endl;
	cout << "p8.name:" << p8.get_name() << endl;
	cout << "p.X:" << p.get_x() << endl;
	cout << "p.Y:" << p.get_y() << endl;
	cout << "p.name:" << p.get_name() << endl;
	cout << "p9.X:" << p9.get_x() << endl;
	cout << "p9.Y:" << p9.get_y() << endl;
	cout << "p9.name:" << p9.get_name() << endl;
	return;

	p9 = p8 + 7;
	//   p8 + 7; p8.operator+(7);
	//   7 + p8; 7.operator+(p8);

	//Point operator~(Point& p);


	///b = ~a;



	//p10 = p9 = p8 = p;

	cout << "p.X:" << p.get_x() << endl;
	cout << "p.Y:" << p.get_y() << endl;
	cout << "p.name:" << p.get_name() << endl;

	cout << "p8.X:" << p8.get_x() << endl;
	cout << "p8.Y:" << p8.get_y() << endl;
	cout << "p8.name:" << p8.get_name() << endl;

	cout << "p10.X:" << p10.get_x() << endl;
	cout << "p10.Y:" << p10.get_y() << endl;
	cout << "p10.name:" << p10.get_name() << endl;

	/*
	cout << "p1.X:" << p1.get_x() << endl;
	cout << "p1.Y:" << p1.get_y() << endl;
	cout << "p1.name:" << p1.get_name() << endl;

	// �������� 
	Point p3 = p + p1; // p.opertator+(p1);

	Point p4 = p + 7;  // p.opertator+(7);

	Point p5 =  - p;

	cout << "P3===========" << endl;
	cout << "X:" << p3.get_x() << endl;
	cout << "Y:" << p3.get_y() << endl;

	cout << "P4===========" << endl;
	cout << "X:" << p4.get_x() << endl;
	cout << "Y:" << p4.get_y() << endl;

	cout << "P5===========" << endl;
	cout << "X:" << p5.get_x() << endl;
	cout << "Y:" << p5.get_y() << endl;

	Point p23(45,67);
	Point p24 = p23;
	Point p25;

	p25 = p24;

	p25 = 7;


	*/
}

